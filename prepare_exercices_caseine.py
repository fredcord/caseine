import os
import re

def trouve_dans_liste(liste, elt):
    for sublist in liste:
        if elt in sublist:
            return True
    return False

############################################################################
# Lit le fichier vpl_evaluate.cases pour connaitre les exercices a inclure #
############################################################################
with open('vpl_evaluate.cases') as file:
    lines = file.readlines()
liste_exercices = []
renommage_methodes = []
for line in lines:
    # \s*                   -> zero ou plusieurs caracteres separateur
    # \s+                   -> au moins un caractere separateur
    # [-+]?(?:\d*\.\d+|\d+) -> nombre a virgule
    # (\w+)                 -> une suite de caracteres alphanumeriques mis dans un groupe
    res=re.match(r'\s*#define\s+(\w+)\s*{\s*[-+]?(?:\d*\.\d+|\d+)\s*,\s*(\w+)',line)
    if res:
        liste_exercices.append(res.groups()[1])
        continue
    res=re.match(r'\s*#define\s+(\w+)\s+(\w+)\s*\n',line)
    # Recupere les informations pour le renommage des methodes
    if res and res.groups()[1] != 'false' and res.groups()[1] != 'true':
        renommage_methodes.append([res.groups()[0],res.groups()[1]])
#print(liste_exercices)
#print(renommage_methodes)



######################################################
# Recupere la declaration de la structure de donnees #
######################################################
with open('vpl_evaluate.cases') as file:
    lines = file.readlines()

ajouter = False
enonce_struct_de_donnees = []
for line in lines:
    res=re.match(r'\s*//\s*DEBUT_ENONCE_STRUCT_DE_DONNEES',line)
    if res:
        ajouter = True
        continue
    res=re.match(r'\s*//\s*FIN_ENONCE_STRUCT_DE_DONNEES',line)
    if res:
        ajouter = False
        continue
    if ajouter == True:
        enonce_struct_de_donnees.append(line)


######################################################
# Recupere l'enonce de chaque exercice               #
######################################################
enonce_exercices = {}
# Charge les enonces locaux si ceux-ci existent
if os.path.isfile('Enonces.txt'):
    with open('Enonces.txt') as file:
        lines = file.readlines()
    exercice_courant = None
    for line in lines:
        res=re.match(r'\s*//\s*DEBUT\s+(\w+)',line)
        if res:
            exercice_courant = res.groups()[0]
            enonce_exercices[exercice_courant] = ''
            continue
        res=re.match(r'\s*//\s*FIN\s+(\w+)',line)
        if res:
            assert(exercice_courant == res.groups()[0])
            exercice_courant = None
            continue
        if exercice_courant is not None:
            enonce_exercices[exercice_courant]+=line
#print(enonce_exercices)

# Charge les enonces generiques
with open('../Enonces.txt') as file:
    lines = file.readlines()
exercice_courant = None
for line in lines:
    res=re.match(r'\s*//\s*DEBUT\s+(\w+)',line)
    if res:
        exercice_courant = res.groups()[0]
        # Si l'enonce local n'existe pas deja, on prend l'enonce generique
        if not trouve_dans_liste(enonce_exercices, exercice_courant):
            enonce_exercices[exercice_courant] = ''
        else:
            exercice_courant = None
        continue
    res=re.match(r'\s*//\s*FIN\s+(\w+)',line)
    if res and exercice_courant != None:
        assert(exercice_courant == res.groups()[0])
        # Renomme la methode si cela est specifie
        for renommage in renommage_methodes:
            if enonce_exercices[exercice_courant].find(renommage[0]) != -1:
                print('renomme ' + renommage[0] + ' en ' + renommage[1])
                enonce_exercices[exercice_courant] = enonce_exercices[exercice_courant].replace(renommage[0], renommage[1])
                # Ajoute l'enonce modifie au fichier de l'enonce local
                fichier_enonce_local=open('Enonces.txt', 'a')
                fichier_enonce_local.write('// DEBUT ' + exercice_courant + '\n')
                fichier_enonce_local.write(enonce_exercices[exercice_courant])
                fichier_enonce_local.write('// FIN ' + exercice_courant + '\n\n\n')
                fichier_enonce_local.close()
        exercice_courant = None
        continue
    if exercice_courant is not None:
        enonce_exercices[exercice_courant]+=line


######################################################
# Recupere la correction de chaque exercice          #
######################################################
with open('Exercices.cpp') as file:
    lines = file.readlines()

correction_exercices = {}
exercice_courant = None
entreDebutEtFin = False
for line in lines:
    res=re.match(r'\s*//\s*DEBUT\s+(\w+)',line)
    if res:
        if entreDebutEtFin == True:
            # Les tag DEBUT et FIN doivent se correspondre
            print("Tag DEBUT n'est pas suivi de FIN")
            assert(False)
        entreDebutEtFin = True
        exercice_courant = res.groups()[0]
        correction_exercices[exercice_courant] = ''
        continue
    res=re.match(r'\s*//\s*FIN\s+(\w+)',line)
    if res:
        if entreDebutEtFin == False:
            # Les tag DEBUT et FIN doivent se correspondre
            print("Deux tags FIN se suivent")
            assert(False)
        entreDebutEtFin = False
        assert(exercice_courant == res.groups()[0])
        # Renomme la methode si cela est specifie
        for renommage in renommage_methodes:
            if correction_exercices[exercice_courant].find(renommage[0]) != -1:
                correction_exercices[exercice_courant] = correction_exercices[exercice_courant].replace(renommage[0], renommage[1]) 
        exercice_courant = None
        continue
    if exercice_courant is not None:
        correction_exercices[exercice_courant]+=line


#####################################################
# Ecrit l'enonce et la correction                   #
#####################################################
os.makedirs('caseine', exist_ok=True) 
fichier_enonce_caseine=open('caseine/requested file.cpp', 'w')
fichier_enonce_caseine.write('#include "ExercicesBase.h"\n\n\n\n')
for exercice in liste_exercices:
    fichier_enonce_caseine.write('////////////////////////////////////////////////////////////////////////////////////////////////\n')
    fichier_enonce_caseine.write('//\n')
    fichier_enonce_caseine.write(enonce_exercices[exercice])
    fichier_enonce_caseine.write('//\n')
    for enonc in enonce_struct_de_donnees:
        fichier_enonce_caseine.write(enonc)
    fichier_enonce_caseine.write('//\n')
    fichier_enonce_caseine.write('////////////////////////////////////////////////////////////////////////////////////////////////\n\n\n\n')
fichier_enonce_caseine.close()

fichier_correction_caseine=open('caseine/corrected file.cpp', 'w')
fichier_correction_caseine.write('#include "ExercicesBase.h"\n\n\n\n')
for exercice in liste_exercices:
    fichier_correction_caseine.write('////////////////////////////////////////////////////////////////////////////////////////////////\n')
    fichier_correction_caseine.write('//\n')
    fichier_correction_caseine.write(enonce_exercices[exercice])
    fichier_correction_caseine.write('//\n')
    for enonc in enonce_struct_de_donnees:
        fichier_correction_caseine.write(enonc)
    fichier_correction_caseine.write('//\n')
    fichier_correction_caseine.write('////////////////////////////////////////////////////////////////////////////////////////////////\n')
    fichier_correction_caseine.write(correction_exercices[exercice])
    fichier_correction_caseine.write('\n\n\n')
fichier_correction_caseine.close()






