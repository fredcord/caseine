////////////////////////////////////////////////////////////////////////////////////
// Fichier genere automatiquement pour l'instrumentation du code. Ne pas modifier //
////////////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include "ExercicesBase.h"

using namespace std;


// DEBUT EXO_CONSTRUCTEUR
LDCCirc_ConstructDestruct::LDCCirc_ConstructDestruct():d_tete{nullptr}
{
}
// FIN EXO_CONSTRUCTEUR



// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
LDCCirc_ConstructDestruct::LDCCirc_ConstructDestruct(const LDCCirc &param):d_tete{nullptr}
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_tete==nullptr)
        return;

    ChainonDC *c2 = param.d_tete, *tete2=param.d_tete, *precC1=nullptr;
    do
    {
        ChainonDC *nc1 = new ChainonDC(c2->d_v);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr)
        {
            d_tete = nc1;
            d_tete->d_suiv=d_tete;
            d_tete->d_prec=d_tete;
        }
        else
        {
            ChainonDC* suivC1=precC1->d_suiv;
            // Insertion entre precC1 et suivC1
            precC1->d_suiv = nc1;
            nc1->d_prec=precC1;
            nc1->d_suiv = suivC1;
            suivC1->d_prec=nc1;
        }
        precC1 = nc1;
        c2 = c2->d_suiv;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=tete2);
}
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE



// DEBUT EXO_CONSTRUCTEUR_AVEC_TABLEAU
LDCCirc_ConstructDestruct::LDCCirc_ConstructDestruct(Double *tab, Int nb):d_tete(nullptr)
{
    ChainonDC *precC1=nullptr;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        ChainonDC *nc1 = new ChainonDC(tab[i]);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr)
        {
            d_tete = nc1;
            d_tete->d_suiv=d_tete;
            d_tete->d_prec=d_tete;
        }
        else
        {
            ChainonDC* suivC1=precC1->d_suiv;
            // Insertion entre precC1 et suivC1
            precC1->d_suiv = nc1;
            nc1->d_prec=precC1;
            nc1->d_suiv = suivC1;
            suivC1->d_prec=nc1;
        }
        precC1 = nc1;
    }
}
// FIN EXO_CONSTRUCTEUR_AVEC_TABLEAU



// DEBUT EXO_DESTRUCTEUR
LDCCirc_ConstructDestruct::~LDCCirc_ConstructDestruct()
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!d_tete)
        return;
    ChainonDC *c=d_tete;
    do
    {
        ChainonDC *tmp=c->d_suiv;
        {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
        c=tmp;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=d_tete);
}
// FIN EXO_DESTRUCTEUR



// DEBUT EXO_CONCATENATION_DEBUT
void LDCCirc_Other::concatenationDebut(const LDCCirc &param)
{
    // Si la liste a inserer est vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_tete==nullptr)
        return;

    ChainonDC *c2=param.d_tete, *tete2=param.d_tete;

    // Recupere la tete et son precedent de la liste this
    ChainonDC *suivC1=d_tete,*precC1=nullptr;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete!=nullptr)
        precC1=d_tete->d_prec;
    // La tete doit etre redefinie
    d_tete=nullptr;

    // Insertion entre la precC1 et c1
    do
    {
        ChainonDC* nc1=new ChainonDC(c2->d_v);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr)
            d_tete=nc1;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
        {
            // insertion entre precC1 et c1
            precC1->d_suiv=nc1;
            nc1->d_prec=precC1;

            nc1->d_suiv=suivC1;
            suivC1->d_prec=nc1;
            precC1=nc1;
        }
        else
        {
            d_tete->d_prec=nc1;
            d_tete->d_suiv=nc1;
            suivC1=nc1;
            precC1=nc1;
        }
        c2=c2->d_suiv;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=tete2);
}
// FIN EXO_CONCATENATION_DEBUT



// DEBUT EXO_CONCATENATION_FIN
void LDCCirc_Other::concatenationFin(const LDCCirc &param)
{
    // Si la liste a inserer est vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_tete==nullptr)
        return;

    ChainonDC *c2=param.d_tete, *tete2=param.d_tete;

    // Recupere la tete et son precedent de la liste this
    ChainonDC *suivC1=d_tete,*precC1=nullptr;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete!=nullptr)
        precC1=d_tete->d_prec;

    // Insertion entre la precC1 et c1
    do
    {
        ChainonDC* nc1=new ChainonDC(c2->d_v);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
        {
            // insertion entre precC1 et c1
            precC1->d_suiv=nc1;
            nc1->d_prec=precC1;

            nc1->d_suiv=suivC1;
            suivC1->d_prec=nc1;
            precC1=nc1;
        }
        else
        {
            d_tete=nc1;
            d_tete->d_prec=nc1;
            d_tete->d_suiv=nc1;
            suivC1=nc1;
            precC1=nc1;
        }
        c2=c2->d_suiv;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=tete2);
}
// FIN EXO_CONCATENATION_FIN



// DEBUT EXO_OP_CONCATENATION
LDCCirc &LDCCirc_Other::operator+=(const LDCCirc &param)
{
    // Si la liste a inserer est vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_tete==nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}

    ChainonDC *c2=param.d_tete, *tete2=param.d_tete;

    // Recupere la tete et son precedent de la liste this
    ChainonDC *suivC1=d_tete,*precC1=nullptr;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete!=nullptr)
        precC1=d_tete->d_prec;

    // Insertion entre la precC1 et c1
    do
    {
        ChainonDC* nc1=new ChainonDC(c2->d_v);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
        {
            // insertion entre precC1 et c1
            precC1->d_suiv=nc1;
            nc1->d_prec=precC1;

            nc1->d_suiv=suivC1;
            suivC1->d_prec=nc1;
            precC1=nc1;
        }
        else
        {
            d_tete=nc1;
            d_tete->d_prec=nc1;
            d_tete->d_suiv=nc1;
            suivC1=nc1;
            precC1=nc1;
        }
        c2=c2->d_suiv;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=tete2);

    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_CONCATENATION



// DEBUT EXO_OP_EGAL
bool LDCCirc_Other::operator==(const LDCCirc &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    // Comparaison vide/pas vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr && param.d_tete==nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr && param.d_tete!=nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete!=nullptr && param.d_tete==nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete->d_v != param.d_tete->d_v)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}

    ChainonDC *c1=d_tete, *tete1=d_tete, *c2=param.d_tete, *tete2=param.d_tete;
    do
    {
        c1=c1->d_suiv;
        c2=c2->d_suiv;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=tete1 && c2!=tete2 && c1->d_v==c2->d_v);
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c1==tete1 && c2==tete2);}
}
// FIN EXO_OP_EGAL



// DEBUT EXO_OP_DIFFERENT
bool LDCCirc_Other::operator!=(const LDCCirc &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    // Comparaison vide/pas vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr && param.d_tete==nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr && param.d_tete!=nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete!=nullptr && param.d_tete==nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete->d_v != param.d_tete->d_v)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}

    ChainonDC *c1=d_tete, *tete1=d_tete, *c2=param.d_tete, *tete2=param.d_tete;
    do
    {
        c1=c1->d_suiv;
        c2=c2->d_suiv;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=tete1 && c2!=tete2 && c1->d_v==c2->d_v);
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c1!=tete1 || c2!=tete2);}
}
// FIN EXO_OP_DIFFERENT



// DEBUT EXO_OP_AFFECTATION
LDCCirc &LDCCirc_Other::operator=(const LDCCirc &param)
{
	if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
		{GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}

    // Cas ou on copie une liste vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_tete==nullptr)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr)
            // Copie d'une liste vide dans une liste vide
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
        // Copie d'une liste vide dans une liste non-vide
        ChainonDC *c1=d_tete;
        do
        {
            ChainonDC *tmp=c1->d_suiv;
            {GestionPntrEtComplexite::get().peutSupprPntr(c1, false); delete c1;}
            c1=tmp;
        }
        while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=d_tete);
        d_tete=nullptr;
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
    }

	ChainonDC *c1=d_tete,*c2=param.d_tete;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1 && c2)
    {
        // Copie d'une liste non vide dans une liste non vide
        do
        {
            c1->d_v=c2->d_v;
            c1=c1->d_suiv;
            c2=c2->d_suiv;
        }
        while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=d_tete && c2!=param.d_tete);

        // Si les listes ont la meme longueur
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1==d_tete && c2==param.d_tete)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}

        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1!=d_tete)
        {
            // Supprime les chainons en trop
            ChainonDC *precC1=c1->d_prec;
            precC1->d_suiv=d_tete;
            d_tete->d_prec=precC1;
            while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=d_tete)
            {
                ChainonDC* tmp=c1;
                c1=c1->d_suiv;
                {GestionPntrEtComplexite::get().peutSupprPntr(tmp, false); delete tmp;}
            }
        }
        else
        {
            // Ajoute les chainons manquants
            do
            {
                ChainonDC* nc1=new ChainonDC(c2->d_v);
                // Insertion de nc1 entre c1 et son suivant
                ChainonDC *precC1=c1->d_prec;
                precC1->d_suiv=nc1;
                nc1->d_prec=precC1;
                nc1->d_suiv=c1;
                c1->d_prec=nc1;
                c2=c2->d_suiv;
            }
            while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=param.d_tete);
        }
    }
    else
	{
        // Copie d'une liste non vide dans une liste vide
        do
        {
            ChainonDC* nc1=new ChainonDC(c2->d_v);
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr)
            {
                d_tete=nc1;
                nc1->d_suiv=nc1;
                nc1->d_prec=nc1;
            }
            else
            {
                // insertion entre la tete et son precedent
                ChainonDC *precTete=d_tete->d_prec;
                nc1->d_prec=precTete;
                precTete->d_suiv=nc1;
                nc1->d_suiv=d_tete;
                d_tete->d_prec=nc1;
            }
            c2=c2->d_suiv;
        }
        while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=param.d_tete);
	}
	{GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_AFFECTATION



// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
void LDCCirc_Other::additionnerATousLesElts(Double val)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr)
        return;
    ChainonDC *c=d_tete;
    do
    {
        c->d_v+=val;
        c=c->d_suiv;
    }
     while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=d_tete);
}
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS



// DEBUT EXO_INSERER_UN_ELT_DEBUT
void LDCCirc_Other::insererUnEltDebut(Double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion dans une liste vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr) {
        d_tete = nc;
        nc->d_suiv=nc;
        nc->d_prec=nc;
        return;
    }
    // Insertion en tete, c'est-à-dire, entre la tete et son precedent
    ChainonDC *precTete=d_tete->d_prec;
    precTete->d_suiv=nc;
    nc->d_prec=precTete;
    nc->d_suiv=d_tete;
    d_tete->d_prec=nc;
    d_tete = nc;
}
// FIN EXO_INSERER_UN_ELT_DEBUT



// DEBUT EXO_INSERER_UN_ELT_MILIEU
void LDCCirc_Other::insererUnEltMilieu(Int idx, Double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr)
    {
        // Insertion dans une liste vide
        d_tete = nc;
        nc->d_suiv=nc;
        nc->d_prec=nc;
        return;
    }
    else if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx==0)
    {
        // Insertion en tete de la liste
        ChainonDC *precTete=d_tete->d_prec;
        precTete->d_suiv=nc;
        nc->d_prec=precTete;
        nc->d_suiv=d_tete;
        d_tete->d_prec=nc;
        d_tete=nc;
        return;
    }
    // Insertion apres la tete
    ChainonDC *c=d_tete;
    Int i=0;
    do
    {
        i++;
        c=c->d_suiv;
    }
     while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=d_tete && i<idx);
     // insertion entre c et son precedent
     ChainonDC *precC=c->d_prec;
     precC->d_suiv=nc;
     nc->d_prec=precC;
     nc->d_suiv=c;
     c->d_prec=nc;
}
// FIN EXO_INSERER_UN_ELT_MILIEU



// DEBUT EXO_INSERER_UN_ELT_FIN
void LDCCirc_Other::insererUnEltFin(Double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion dans une liste vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr) {
        d_tete = nc;
        nc->d_suiv=nc;
        nc->d_prec=nc;
        return;
    }
    // Insertion entre la tete et son precedent sans changer d_tete
    ChainonDC *precTete=d_tete->d_prec;
    precTete->d_suiv=nc;
    nc->d_prec=precTete;
    nc->d_suiv=d_tete;
    d_tete->d_prec=nc;
}
// FIN EXO_INSERER_UN_ELT_FIN



// DEBUT EXO_INSERER_PLUSIEURS_ELTS_DEBUT
void LDCCirc_Other::insererPlusieursEltsDebut(Double *tab, Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0)
        return;
    ChainonDC *precC=nullptr,*c=d_tete;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c!=nullptr)
        precC=c->d_prec;
    // La tete doit etre redefinie
    d_tete=nullptr;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        ChainonDC *nc=new ChainonDC(tab[i]);
        // Si la tete n'est pas encore definie, ce nouveau chainon est la tete
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr)
            d_tete=nc;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==nullptr)
        {
            // Insertion dans une liste vide
            nc->d_suiv=nc;
            nc->d_prec=nc;
            precC=nc;
            c=nc;
        }
        else
        {
            // Insertion entre precC et c
            precC->d_suiv=nc;
            nc->d_prec=precC;
            nc->d_suiv=c;
            c->d_prec=nc;
            precC=nc;
        }
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_DEBUT



// DEBUT EXO_INSERER_PLUSIEURS_ELTS_MILIEU
void LDCCirc_Other::insererPlusieursEltsMilieu(Int idx, Double *tab, Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0)
        return;
    // Insertion du premier chainon
    ChainonDC *precC,*c, *nc;
    nc = new ChainonDC(tab[0]);
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr)
    {
        // Insertion dans une liste vide
        d_tete = nc;
        nc->d_suiv=nc;
        nc->d_prec=nc;
        precC=nc;
        c=nc;
    }
    else if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx==0)
    {
        // Insertion en tete de la liste
        ChainonDC *precTete=d_tete->d_prec;
        precTete->d_suiv=nc;
        nc->d_prec=precTete;
        nc->d_suiv=d_tete;
        d_tete->d_prec=nc;
        precC=nc;
        c=d_tete;
        d_tete=nc;
    }
    else
    {
        // Insertion apres la tete
        c=d_tete;
        Int i=0;
        do
        {
            i++;
            c=c->d_suiv;
        }
        while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=d_tete && i<idx);
        // insertion entre c et son precedent
        precC=c->d_prec;
        precC->d_suiv=nc;
        nc->d_prec=precC;
        nc->d_suiv=c;
        c->d_prec=nc;
        precC=nc;
    }
    // Insertion des chainons restant entre precC et c
    for(Int i=1;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        nc = new ChainonDC(tab[i]);
        precC->d_suiv=nc;
        nc->d_prec=precC;
        nc->d_suiv=c;
        c->d_prec=nc;
        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_MILIEU



// DEBUT EXO_INSERER_PLUSIEURS_ELTS_FIN
void LDCCirc_Other::insererPlusieursEltsFin(Double *tab, Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0)
        return;
    // Insertion du premier chainon
    ChainonDC *precC,*c, *nc;
    // Creation du nouveau chainon
    nc = new ChainonDC(tab[0]);
    // Insertion dans une liste vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr) {
        d_tete = nc;
        nc->d_suiv=nc;
        nc->d_prec=nc;
        precC=nc;
        c=nc;
    }
    else
    {
        // Insertion entre la tete et son precedent sans changer d_tete
        ChainonDC *precTete=d_tete->d_prec;
        precTete->d_suiv=nc;
        nc->d_prec=precTete;
        nc->d_suiv=d_tete;
        d_tete->d_prec=nc;
        precC=nc;
        c=d_tete;
    }

    // Insertion des chainons restants entre precC et c
    for(Int i=1;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__), i<nb;i++)
    {
        nc = new ChainonDC(tab[i]);
        precC->d_suiv=nc;
        nc->d_prec=precC;
        nc->d_suiv=c;
        c->d_prec=nc;
        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_FIN



// DEBUT EXO_ENLEVER_UN_ELT_DEBUT
void LDCCirc_Other::enleverUnEltDebut()
{
    // Liste chainee vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr)
        return;
    // Liste avec un seul chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==d_tete->d_suiv)
    {
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
        d_tete=nullptr;
        return;
    }
    ChainonDC *precTete=d_tete->d_prec;
    ChainonDC *suivTete=d_tete->d_suiv;
    precTete->d_suiv=suivTete;
    suivTete->d_prec=precTete;
    {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
    d_tete=suivTete;
}
// FIN EXO_ENLEVER_UN_ELT_DEBUT



// DEBUT EXO_ENLEVER_UN_ELT_MILIEU
void LDCCirc_Other::enleverUnEltMilieu(Int idx)
{
    // Liste chainee vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr)
        return;
    // Suppression du premier chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx==0)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==d_tete->d_suiv)
        {
            // Liste avec un seul chainon
            {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
            d_tete=nullptr;
        }
        else
        {
            ChainonDC *precC=d_tete->d_prec;
            ChainonDC *suivC=d_tete->d_suiv;
            {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
            precC->d_suiv=suivC;
            suivC->d_prec=precC;
            d_tete=suivC;
        }
        return;
    }
    else
        // Liste avec un seul chainon et idx > 0
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==d_tete->d_suiv)
            return;
    // Cherche le chainon a supprimer
    ChainonDC *c=d_tete;
    Int i=0;
    do
    {
        i++;
        c=c->d_suiv;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=d_tete && i<idx);
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&i==idx)
    {
        // Supprime le chainon si il a ete trouve
        ChainonDC *precC=c->d_prec;
        ChainonDC *suivC=c->d_suiv;
        {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
        precC->d_suiv=suivC;
        suivC->d_prec=precC;
    }
}
// FIN EXO_ENLEVER_UN_ELT_MILIEU



// DEBUT EXO_ENLEVER_UN_ELT_FIN
void LDCCirc_Other::enleverUnEltFin()
{
    // Liste chainee vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr)
        return;
    // Liste avec un seul chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==d_tete->d_suiv)
    {
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
        d_tete=nullptr;
        return;
    }
    ChainonDC *precTete=d_tete->d_prec;
    ChainonDC *precPrecTete=precTete->d_prec;
    precPrecTete->d_suiv=d_tete;
    d_tete->d_prec=precPrecTete;
    {GestionPntrEtComplexite::get().peutSupprPntr(precTete, false); delete precTete;}
}
// FIN EXO_ENLEVER_UN_ELT_FIN



// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
void LDCCirc_Other::enleverPlusieursEltsDebut(Int nb)
{
    // Liste vide ou rien a supprimer
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0 || d_tete==nullptr)
        return;
    // Liste avec un seul chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==d_tete->d_suiv)
    {
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
        d_tete=nullptr;
        return;
    }

    ChainonDC *c=d_tete,*dernierC=d_tete->d_prec;
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=dernierC && c!=nullptr && i<nb)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==c->d_suiv)
        {
            // Suppression dernier chainon
            {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
            d_tete=nullptr;
            c=nullptr;
        }
        else
        {
            // SUpprime le chainon c
            ChainonDC *precC=c->d_prec;
            ChainonDC *suivC=c->d_suiv;
            precC->d_suiv=suivC;
            suivC->d_prec=precC;
            // Si c est la tete, la tete devient son suivant
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==d_tete)
                d_tete=suivC;
            {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
            c=suivC;
        }
        i++;
    }
    // Traite le dernier chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c!=nullptr && i<nb)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==c->d_suiv)
        {
            // Suppression dernier chainon
            {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
            d_tete=nullptr;
            c=nullptr;
        }
        else
        {
            // SUpprime le chainon c
            ChainonDC *precC=c->d_prec;
            ChainonDC *suivC=c->d_suiv;
            precC->d_suiv=suivC;
            suivC->d_prec=precC;
            // Si c est la tete, la tete devient son suivant
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==d_tete)
                d_tete=suivC;
            {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
            c=suivC;
        }
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT



// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU
void LDCCirc_Other::enleverPlusieursEltsMilieu(Int idx, Int nb)
{
    // Liste vide ou rien a supprimer
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0 || d_tete==nullptr)
        return;
    // Liste avec un seul chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==d_tete->d_suiv)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx==0)
        {
            {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
            d_tete=nullptr;
        }
        return;
    }
    // Liste avec plusieurs chainons
    ChainonDC *c=d_tete;
    // Cherche le chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx>0)
    {
        Int i=0;
        do
        {
            i++;
            c=c->d_suiv;
        }
        while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=d_tete && i<idx);
        // Chainon pas trouve
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&i<idx)
            return;
    }
    // Suppression des chainons a partir de c
    Int i=0;
    do
    {
        i++;
        ChainonDC *precC=c->d_prec;
        ChainonDC *suivC=c->d_suiv;
        precC->d_suiv=suivC;
        suivC->d_prec=precC;
        {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
        c=suivC;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=c->d_suiv && c!=d_tete && i<nb);
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx==0)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&i<nb)
        {
            // Tous les chainons ont ete supprimes, on supprime le dernier chainon
            {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
            d_tete=nullptr;
        }
        else
            d_tete=c;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU



// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_FIN
void LDCCirc_Other::enleverPlusieursEltsFin(Int nb)
{
    // Liste vide ou rien a supprimer
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0 || d_tete==nullptr)
        return;
    // Liste avec un seul chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==d_tete->d_suiv)
    {
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
        d_tete=nullptr;
        return;
    }

    ChainonDC *c=d_tete->d_prec;
    Int i=0;
    do
    {
        i++;
        ChainonDC *precC=c->d_prec;
        ChainonDC *suivC=c->d_suiv;
        precC->d_suiv=suivC;
        suivC->d_prec=precC;
        {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
        c=precC;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=c->d_suiv && i<nb);

    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&i<nb)
    {
        // On est arrive a la fin de la liste, on supprime le dernier chainon
        {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
        d_tete=nullptr;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_FIN



// DEBUT EXO_ELTS_EN_ORDRE_DECROISSANT
bool LDCCirc_Other::eltsEnOrdreDecroissant() const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr || d_tete->d_suiv==d_tete)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    ChainonDC *c=d_tete;
    do
    {
        c=c->d_suiv;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=d_tete && c->d_prec->d_v > c->d_v);
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c==d_tete);}
}
// FIN EXO_ELTS_EN_ORDRE_DECROISSANT



// DEBUT EXO_CONTIENT_ELT
bool LDCCirc_Other::contientElt(Double val) const
{
    // Liste vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    // Liste avec un seul chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete->d_suiv==d_tete)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_tete->d_v==val;}
    ChainonDC *c=d_tete;
    do
    {
        c=c->d_suiv;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=d_tete && c->d_v!=val);
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c->d_v==val);}
}
// FIN EXO_CONTIENT_ELT



// DEBUT EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT
void LDCCirc_Other::enleverToutesLesOccurencesDUnElt(Double val)
{
    // Liste vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr)
        return;

    // Parcours de la liste
    ChainonDC *c=d_tete;
    ChainonDC *dernierC=d_tete->d_prec;
    do
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_v!=val)
            c=c->d_suiv;
        else
        {
            // Chainon trouve. On doit le supprimer
            ChainonDC *precC=c->d_prec;
            ChainonDC *suivC=c->d_suiv;
            precC->d_suiv=suivC;
            suivC->d_prec=precC;
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==d_tete)
            {
                if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==c->d_suiv)
                {
                    // Dernier chainon supprime
                    d_tete=nullptr;
                    suivC=nullptr;
                }
                else
                    // Tete supprime. La tete devient le chainon suivant
                    d_tete=suivC;
            }
            {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
            c=suivC;
        }
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&d_tete!=nullptr && c!=dernierC);

    // Traite le dernier chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c!=nullptr && c->d_v==val)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==c->d_suiv)
            d_tete=nullptr;
        else
        {
            ChainonDC *precC=c->d_prec;
            ChainonDC *suivC=c->d_suiv;
            precC->d_suiv=suivC;
            suivC->d_prec=precC;
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==c)
                d_tete=suivC;
            {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
        }
    }
}
// FIN EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT



// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT
Int LDCCirc_Other::chercherLIndexDeLaPremiereOccurenceDUnElt(Double val) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!d_tete)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return -1;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete->d_v==val)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return 0;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==d_tete->d_suiv)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return -1;}
    ChainonDC *c=d_tete;
    Int i=0;
    do
    {
        c=c->d_suiv;
        i++;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c->d_suiv!=d_tete && c->d_v!=val);

    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_v==val)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i;}
    else
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return -1;}
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT



// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT
Int LDCCirc_Other::chercherLIndexDeLaDerniereOccurenceDUnElt(Double val) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!d_tete)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return -1;}
    Int idx=-1;
    // Valeur trouvee dans le chainon tete
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete->d_v==val)
        idx=0;
    // Liste avec un seul chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==d_tete->d_suiv)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return idx;}
    ChainonDC *c=d_tete;
    Int i=0;
    do
    {
        c=c->d_suiv;
        i++;
        // Recupere l'index du dernier chainon trouve
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_v==val)
            idx=i;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c->d_suiv!=d_tete);
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return idx;}
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT



// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
Int LDCCirc_Other::calculerNombreDOccurencesDUnElt(Double val) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!d_tete)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return 0;}
    Int nb=0;
    // Valeur trouvee dans le chainon tete
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete->d_v==val)
        nb++;
    // Liste avec un seul chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==d_tete->d_suiv)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return nb;}
    ChainonDC *c=d_tete;
    do
    {
        c=c->d_suiv;
        // Incremente le compteur pour le chenon veant d'etre trouve
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_v==val)
            nb++;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c->d_suiv!=d_tete);
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return nb;}
}
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT


