#include <iostream>
#include <algorithm>

#include "ExercicesBase.h"

using namespace std;


/////////////////////////////////////////////////////////////////////////////
// Compare la structure de donnees de l'etudiant avec celle de la solution //
/////////////////////////////////////////////////////////////////////////////

// Verifie si la liste chainee est correcte au niveau de sa structure et des pointeurs
bool LDCCircCorr::verifierIntegrite(const LDCCirc &param)
{
    if (!param.d_tete)
        return true;

    ChainonDC *c=param.d_tete;
    int compteur=0;
    vector<ChainonDC*> pntrUilise;
    do
    {
        // Validite des pointeurs
        if (!GestionPntrEtComplexite::get().pntrValide(c))
            return false;
        if (!GestionPntrEtComplexite::get().pntrValide(c->d_prec))
            return false;
        if (!GestionPntrEtComplexite::get().pntrValide(c->d_suiv))
            return false;
        if (c->d_suiv->d_prec!=c)
            return false;
        if (c->d_prec->d_suiv!=c)
            return false;

        // Verifier si le pointeur est bien unique
        if(std::find(pntrUilise.begin(), pntrUilise.end(), c)!=pntrUilise.end())
            return false;
        pntrUilise.push_back(c);

        c=c->d_suiv;
        compteur++;
    }
    while(c!=param.d_tete && compteur<StructDeDonneesVerifCorr::NB_MAX_COLONNES);

    return c==param.d_tete;
}


bool LDCCircCorr::compareCorrAvecEtud(const LDCCirc &student) const
{
    ChainonDC *c1=this->d_tete, *c2=student.d_tete;
    ChainonDC *tete1=c1,*tete2=c2;
    // Listes toutes les deux vides
    if (c1==nullptr && c2==nullptr)
        return true;
    // liste vide vs liste pas vide
    if (c1==nullptr && c2!=nullptr)
        return false;
    do
    {
        // Verifie si le pointeur c2 est valide
        if (c2==nullptr)
            return false;
        if (!GestionPntrEtComplexite::get().pntrValide(c2))
            return false;

        // Verifie le precedent du suivant est correct
        if (c1->d_suiv!=nullptr)
        {
            // Pour la correction aussi
            if (!GestionPntrEtComplexite::get().pntrValide(c1->d_suiv) || c1->d_suiv->d_prec!=c1)
            {
                cout<<"Erreur sur la correction"<<endl;
                return false;
            }
            if (c2->d_suiv==nullptr)
                return false;
            if (!GestionPntrEtComplexite::get().pntrValide(c2->d_suiv) || c2->d_suiv->d_prec!=c2)
                return false;
        }
        // Verifier si ce pointeur c2 n'est pas utilise dans la liste correction
        ChainonDC *tmpC1=d_tete;
        do
        {
            if (tmpC1==c2)
                return false;
            tmpC1=tmpC1->d_suiv;
        }
        while(tmpC1!=d_tete);

        // Vérifie s'il contient la meme valeur
        if (c1->d_v!=c2->d_v)
            return false;
        // Verifie le chainon precedent
        if (c1->d_prec==nullptr)
        {
            if (c2->d_prec!=nullptr) return false;
        }
        else
        {
            if (!GestionPntrEtComplexite::get().pntrValide(c2->d_prec))
                return false;
            if (c1->d_prec->d_v!=c2->d_prec->d_v)
                return false;
        }
        c1=c1->d_suiv;
        c2=c2->d_suiv;

        // verifie la longueur de deux chaines
        if (c1!=tete1 && c2==tete2)
            return false;
        if (c1==tete1 && c2!=tete2)
            return false;
    }
    while(c1!=tete1);

    return true;
}


// genere la structDeDonneesVerifCorr a partir de la liste
StructDeDonneesVerifCorr LDCCircCorr::getStructDeDonneesVerifCorr() const
{
    StructDeDonneesVerifCorr vect;
    if (!d_tete)
    {
        return vect;
    }

    ChainonDC *c=d_tete;
    do
    {
        vect.insererUnEltFin(c->d_v);
        c=c->d_suiv;
    }
    while(c!=d_tete);
    return vect;
}

// Constructeur avec la structDeDonneesVerifCorr
void LDCCircCorr::setStructDeDonneesVerifCorr(const StructDeDonneesVerifCorr &tab)
{
    // Supprime d'abord tous les elements
    if (d_tete)
    {
        ChainonDC *tete=d_tete;
        do
        {
            ChainonDC *tmp=d_tete->d_suiv;
            delete d_tete;
            d_tete=tmp;
        }
        while (d_tete!=tete);
        d_tete=nullptr;
    }
    // On insere les elements du tableau
    for (int i=0;i<tab.getNbColonnes();i++)
        insererUnEltFin(tab[i]);
}

// Constructeur
LDCCircCorr::LDCCircCorr():d_tete{nullptr}
{
}

LDCCircCorr::~LDCCircCorr()
{
    if(!d_tete)
        return;
    ChainonDC *tete=d_tete;
    do
    {
        ChainonDC *tmp=d_tete->d_suiv;
        delete d_tete;
        d_tete=tmp;
    }
    while(d_tete!=tete);
}



// Constructeur par recopie
LDCCircCorr::LDCCircCorr(const LDCCircCorr &param):d_tete(nullptr)
{
    if(param.d_tete==nullptr)
        return;

    ChainonDC *c2 = param.d_tete, *tete2=param.d_tete, *precC1=nullptr;
    do
    {
        ChainonDC *nc1 = new ChainonDC(c2->d_v);
        if(d_tete == nullptr)
        {
            d_tete = nc1;
            d_tete->d_suiv=d_tete;
            d_tete->d_prec=d_tete;
        }
        else
        {
            ChainonDC* suivC1=precC1->d_suiv;
            // Insertion entre precC1 et suivC1
            precC1->d_suiv = nc1;
            nc1->d_prec=precC1;
            nc1->d_suiv = suivC1;
            suivC1->d_prec=nc1;
        }
        precC1 = nc1;
        c2 = c2->d_suiv;
    }
    while(c2!=tete2);
}


// Constructeur avec un tableau
LDCCircCorr::LDCCircCorr(double *tab, int nb):d_tete(nullptr)
{
    ChainonDC *precC1=nullptr;
    for(int i=0;i<nb;i++)
    {
        ChainonDC *nc1 = new ChainonDC(tab[i]);
        if(d_tete == nullptr)
        {
            d_tete = nc1;
            d_tete->d_suiv=d_tete;
            d_tete->d_prec=d_tete;
        }
        else
        {
            ChainonDC* suivC1=precC1->d_suiv;
            // Insertion entre precC1 et suivC1
            precC1->d_suiv = nc1;
            nc1->d_prec=precC1;
            nc1->d_suiv = suivC1;
            suivC1->d_prec=nc1;
        }
        precC1 = nc1;
    }
}


void LDCCircCorr::concatenationDebut(const LDCCircCorr &param)
{
    // Si la liste a inserer est vide
    if(param.d_tete==nullptr)
        return;

    ChainonDC *c2=param.d_tete, *tete2=param.d_tete;

    // Recupere la tete et son precedent de la liste this
    ChainonDC *suivC1=d_tete,*precC1=nullptr;
    if(d_tete!=nullptr)
        precC1=d_tete->d_prec;
    // La tete doit etre redefinie
    d_tete=nullptr;

    // Insertion entre la precC1 et c1
    do
    {
        ChainonDC* nc1=new ChainonDC(c2->d_v);
        if(d_tete==nullptr)
            d_tete=nc1;
        if(precC1!=nullptr)
        {
            // insertion entre precC1 et c1
            precC1->d_suiv=nc1;
            nc1->d_prec=precC1;

            nc1->d_suiv=suivC1;
            suivC1->d_prec=nc1;
            precC1=nc1;
        }
        else
        {
            d_tete->d_prec=nc1;
            d_tete->d_suiv=nc1;
            suivC1=nc1;
            precC1=nc1;
        }
        c2=c2->d_suiv;
    }
    while(c2!=tete2);
}

// Concatenation avec la structure de données passee en parametre
// Pour une liste chainee circulaire, concatenationDebut et concatenationFin
// font la même chose
void LDCCircCorr::concatenationFin(const LDCCircCorr &param)
{
    // Si la liste a inserer est vide
    if(param.d_tete==nullptr)
        return;

    ChainonDC *c2=param.d_tete, *tete2=param.d_tete;

    // Recupere la tete et son precedent de la liste this
    ChainonDC *suivC1=d_tete,*precC1=nullptr;
    if(d_tete!=nullptr)
        precC1=d_tete->d_prec;

    // Insertion entre la precC1 et c1
    do
    {
        ChainonDC* nc1=new ChainonDC(c2->d_v);
        if(precC1!=nullptr)
        {
            // insertion entre precC1 et c1
            precC1->d_suiv=nc1;
            nc1->d_prec=precC1;

            nc1->d_suiv=suivC1;
            suivC1->d_prec=nc1;
            precC1=nc1;
        }
        else
        {
            d_tete=nc1;
            d_tete->d_prec=nc1;
            d_tete->d_suiv=nc1;
            suivC1=nc1;
            precC1=nc1;
        }
        c2=c2->d_suiv;
    }
    while(c2!=tete2);
}

LDCCircCorr &LDCCircCorr::operator+=(const LDCCircCorr &param)
{
    // Si la liste a inserer est vide
    if(param.d_tete==nullptr)
        return *this;

    ChainonDC *c2=param.d_tete, *tete2=param.d_tete;

    // Recupere la tete et son precedent de la liste this
    ChainonDC *suivC1=d_tete,*precC1=nullptr;
    if(d_tete!=nullptr)
        precC1=d_tete->d_prec;

    // Insertion entre la precC1 et c1
    do
    {
        ChainonDC* nc1=new ChainonDC(c2->d_v);
        if(precC1!=nullptr)
        {
            // insertion entre precC1 et c1
            precC1->d_suiv=nc1;
            nc1->d_prec=precC1;

            nc1->d_suiv=suivC1;
            suivC1->d_prec=nc1;
            precC1=nc1;
        }
        else
        {
            d_tete=nc1;
            d_tete->d_prec=nc1;
            d_tete->d_suiv=nc1;
            suivC1=nc1;
            precC1=nc1;
        }
        c2=c2->d_suiv;
    }
    while(c2!=tete2);

    return *this;
}

bool LDCCircCorr::operator==(const LDCCircCorr &param) const
{
    if(this==&param)
        return true;
    // Comparaison vide/pas vide
    if(d_tete==nullptr && param.d_tete==nullptr)
        return true;
    if(d_tete==nullptr && param.d_tete!=nullptr)
        return false;
    if(d_tete!=nullptr && param.d_tete==nullptr)
        return false;
    if(d_tete->d_v != param.d_tete->d_v)
        return false;

    ChainonDC *c1=d_tete, *tete1=d_tete, *c2=param.d_tete, *tete2=param.d_tete;
    do
    {
        c1=c1->d_suiv;
        c2=c2->d_suiv;
    }
    while(c1!=tete1 && c2!=tete2 && c1->d_v==c2->d_v);
    return (c1==tete1 && c2==tete2);
}


bool LDCCircCorr::operator!=(const LDCCircCorr &param) const
{
    if(this==&param)
        return false;
    // Comparaison vide/pas vide
    if(d_tete==nullptr && param.d_tete==nullptr)
        return false;
    if(d_tete==nullptr && param.d_tete!=nullptr)
        return true;
    if(d_tete!=nullptr && param.d_tete==nullptr)
        return true;
    if(d_tete->d_v != param.d_tete->d_v)
        return true;

    ChainonDC *c1=d_tete, *tete1=d_tete, *c2=param.d_tete, *tete2=param.d_tete;
    do
    {
        c1=c1->d_suiv;
        c2=c2->d_suiv;
    }
    while(c1!=tete1 && c2!=tete2 && c1->d_v==c2->d_v);
    return (c1!=tete1 || c2!=tete2);
}

// Methode compliquée. A ne pas poser en examen
LDCCircCorr &LDCCircCorr::operator=(const LDCCircCorr &param)
{
    if(this==&param)
        return *this;

    // Cas ou on copie une liste vide
    if(param.d_tete==nullptr)
    {
        if(d_tete==nullptr)
            // Copie d'une liste vide dans une liste vide
            return *this;
        // Copie d'une liste vide dans une liste non-vide
        ChainonDC *c1=d_tete;
        do
        {
            ChainonDC *tmp=c1->d_suiv;
            delete c1;
            c1=tmp;
        }
        while(c1!=d_tete);
        d_tete=nullptr;
        return *this;
    }

	ChainonDC *c1=d_tete,*c2=param.d_tete;
    if(c1 && c2)
    {
        // Copie d'une liste non vide dans une liste non vide
        do
        {
            c1->d_v=c2->d_v;
            c1=c1->d_suiv;
            c2=c2->d_suiv;
        }
        while(c1!=d_tete && c2!=param.d_tete);
        // Si les listes ont la meme longueur
        if(c1==d_tete && c2==param.d_tete)
            return *this;
        if(c1!=d_tete)
        {
            // Supprime les chainons en trop
            ChainonDC *precC1=c1->d_prec;
            precC1->d_suiv=d_tete;
            d_tete->d_prec=precC1;
            while(c1!=d_tete)
            {
                ChainonDC* tmp=c1;
                c1=c1->d_suiv;
                delete tmp;
            }
        }
        else
        {
            // Ajoute les chainons manquants
            do
            {
                ChainonDC* nc1=new ChainonDC(c2->d_v);
                // Insertion de nc1 entre c1 et son suivant
                ChainonDC *precC1=c1->d_prec;
                precC1->d_suiv=nc1;
                nc1->d_prec=precC1;
                nc1->d_suiv=c1;
                c1->d_prec=nc1;
                c2=c2->d_suiv;
            }
            while(c2!=param.d_tete);
        }
    }
    else
	{
        // Copie d'une liste non vide dans une liste vide
        do
        {
            ChainonDC* nc1=new ChainonDC(c2->d_v);
            if(d_tete==nullptr)
            {
                d_tete=nc1;
                nc1->d_suiv=nc1;
                nc1->d_prec=nc1;
            }
            else
            {
                // insertion entre la tete et son precedent
                ChainonDC *precTete=d_tete->d_prec;
                nc1->d_prec=precTete;
                precTete->d_suiv=nc1;
                nc1->d_suiv=d_tete;
                d_tete->d_prec=nc1;
            }
            c2=c2->d_suiv;
        }
        while(c2!=param.d_tete);
	}
	return *this;
}

void LDCCircCorr::additionnerATousLesElts(double val)
{
    if(d_tete==nullptr)
        return;
    ChainonDC *c=d_tete;
    do
    {
        c->d_v+=val;
        c=c->d_suiv;
    }
     while(c!=d_tete);
}

void LDCCircCorr::insererUnEltDebut(double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion dans une liste vide
    if(d_tete == nullptr) {
        d_tete = nc;
        d_tete->d_suiv=d_tete;
        d_tete->d_prec=d_tete;
        return;
    }
    // Insertion en tete
    ChainonDC *prec_tete=d_tete->d_prec;

    nc->d_suiv = d_tete;
    d_tete->d_prec=nc;

    prec_tete->d_suiv = nc;
    nc->d_prec=prec_tete;

    d_tete = nc;
}


void LDCCircCorr::insererUnEltMilieu(int idx, double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    if(d_tete == nullptr)
    {
        // Insertion dans une liste vide
        d_tete = nc;
        nc->d_suiv=nc;
        nc->d_prec=nc;
        return;
    }
    else if(idx==0)
    {
        // Insertion en tete de la liste
        ChainonDC *precTete=d_tete->d_prec;
        precTete->d_suiv=nc;
        nc->d_prec=precTete;
        nc->d_suiv=d_tete;
        d_tete->d_prec=nc;
        d_tete=nc;
        return;
    }
    // Insertion apres la tete
    ChainonDC *c=d_tete;
    int i=0;
    do
    {
        i++;
        c=c->d_suiv;
    }
     while(c!=d_tete && i<idx);
     // insertion entre c et son precedent
     ChainonDC *precC=c->d_prec;
     precC->d_suiv=nc;
     nc->d_prec=precC;
     nc->d_suiv=c;
     c->d_prec=nc;
}


void LDCCircCorr::insererUnEltFin(double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion dans une liste vide
    if(d_tete == nullptr) {
        d_tete = nc;
        nc->d_suiv=nc;
        nc->d_prec=nc;
        return;
    }
    // Insertion entre la tete et son precedent sans changer d_tete
    ChainonDC *precTete=d_tete->d_prec;
    precTete->d_suiv=nc;
    nc->d_prec=precTete;
    nc->d_suiv=d_tete;
    d_tete->d_prec=nc;
}


void LDCCircCorr::insererPlusieursEltsDebut(double *tab, int nb)
{
    if(nb==0)
        return;
    ChainonDC *precC=nullptr,*c=d_tete;
    if(c!=nullptr)
        precC=c->d_prec;
    // La tete doit etre redefinie
    d_tete=nullptr;
    for(int i=0;i<nb;i++)
    {
        ChainonDC *nc=new ChainonDC(tab[i]);
        // Si la tete n'est pas encore definie, ce nouveau chainon est la tete
        if(d_tete==nullptr)
            d_tete=nc;
        if(c==nullptr)
        {
            // Insertion dans une liste vide
            nc->d_suiv=nc;
            nc->d_prec=nc;
            precC=nc;
            c=nc;
        }
        else
        {
            // Insertion entre precC et c
            precC->d_suiv=nc;
            nc->d_prec=precC;
            nc->d_suiv=c;
            c->d_prec=nc;
            precC=nc;
        }
    }
}


void LDCCircCorr::insererPlusieursEltsMilieu(int idx, double *tab, int nb)
{
    if(nb==0)
        return;
    // Insertion du premier chainon
    ChainonDC *precC,*c, *nc;
    nc = new ChainonDC(tab[0]);
    if(d_tete == nullptr)
    {
        // Insertion dans une liste vide
        d_tete = nc;
        nc->d_suiv=nc;
        nc->d_prec=nc;
        precC=nc;
        c=nc;
    }
    else if(idx==0)
    {
        // Insertion en tete de la liste
        ChainonDC *precTete=d_tete->d_prec;
        precTete->d_suiv=nc;
        nc->d_prec=precTete;
        nc->d_suiv=d_tete;
        d_tete->d_prec=nc;
        precC=nc;
        c=d_tete;
        d_tete=nc;
    }
    else
    {
        // Insertion apres la tete
        c=d_tete;
        int i=0;
        do
        {
            i++;
            c=c->d_suiv;
        }
        while(c!=d_tete && i<idx);
        // insertion entre c et son precedent
        precC=c->d_prec;
        precC->d_suiv=nc;
        nc->d_prec=precC;
        nc->d_suiv=c;
        c->d_prec=nc;
        precC=nc;
    }
    // Insertion des chainons restant entre precC et c
    for(int i=1;i<nb;i++)
    {
        nc = new ChainonDC(tab[i]);
        precC->d_suiv=nc;
        nc->d_prec=precC;
        nc->d_suiv=c;
        c->d_prec=nc;
        precC=nc;
    }
}


void LDCCircCorr::insererPlusieursEltsFin(double *tab, int nb)
{
    if(nb==0)
        return;
    // Insertion du premier chainon
    ChainonDC *precC,*c, *nc;
    // Creation du nouveau chainon
    nc = new ChainonDC(tab[0]);
    // Insertion dans une liste vide
    if(d_tete == nullptr)
    {
        d_tete = nc;
        nc->d_suiv=nc;
        nc->d_prec=nc;
        precC=nc;
        c=nc;
    }
    else
    {
        // Insertion entre la tete et son precedent sans changer d_tete
        ChainonDC *precTete=d_tete->d_prec;
        precTete->d_suiv=nc;
        nc->d_prec=precTete;
        nc->d_suiv=d_tete;
        d_tete->d_prec=nc;
        precC=nc;
        c=d_tete;
    }

    // Insertion des chainons restants entre precC et c
    for(int i=1;i<nb;i++)
    {
        nc = new ChainonDC(tab[i]);
        precC->d_suiv=nc;
        nc->d_prec=precC;
        nc->d_suiv=c;
        c->d_prec=nc;
        precC=nc;
    }
}


void LDCCircCorr::enleverUnEltDebut()
{
    // Liste chainee vide
    if(d_tete==nullptr)
        return;
    // Liste avec un seul chainon
    if(d_tete==d_tete->d_suiv)
    {
        delete d_tete;
        d_tete=nullptr;
        return;
    }
    ChainonDC *precTete=d_tete->d_prec;
    ChainonDC *suivTete=d_tete->d_suiv;
    precTete->d_suiv=suivTete;
    suivTete->d_prec=precTete;
    delete d_tete;
    d_tete=suivTete;
}


void LDCCircCorr::enleverUnEltMilieu(int idx)
{
    // Liste chainee vide
    if(d_tete==nullptr)
        return;
    // Suppression du premier chainon
    if(idx==0)
    {
        if(d_tete==d_tete->d_suiv)
        {
            // Liste avec un seul chainon
            delete d_tete;
            d_tete=nullptr;
        }
        else
        {
            ChainonDC *precC=d_tete->d_prec;
            ChainonDC *suivC=d_tete->d_suiv;
            delete d_tete;
            precC->d_suiv=suivC;
            suivC->d_prec=precC;
            d_tete=suivC;
        }
        return;
    }
    else
        // Liste avec un seul chainon et idx > 0
        if(d_tete==d_tete->d_suiv)
            return;
    // Cherche le chainon a supprimer
    ChainonDC *c=d_tete;
    int i=0;
    do
    {
        i++;
        c=c->d_suiv;
    }
    while(c!=d_tete && i<idx);
    if(i==idx)
    {
        // Supprime le chainon si il a ete trouve
        ChainonDC *precC=c->d_prec;
        ChainonDC *suivC=c->d_suiv;
        delete c;
        precC->d_suiv=suivC;
        suivC->d_prec=precC;
    }
}


void LDCCircCorr::enleverUnEltFin()
{
    // Liste chainee vide
    if(d_tete==nullptr)
        return;
    // Liste avec un seul chainon
    if(d_tete==d_tete->d_suiv)
    {
        delete d_tete;
        d_tete=nullptr;
        return;
    }
    ChainonDC *precTete=d_tete->d_prec;
    ChainonDC *precPrecTete=precTete->d_prec;
    precPrecTete->d_suiv=d_tete;
    d_tete->d_prec=precPrecTete;
    delete precTete;
}


void LDCCircCorr::enleverPlusieursEltsDebut(int nb)
{
    // Liste vide ou rien a supprimer
    if(nb==0 || d_tete==nullptr)
        return;
    // Liste avec un seul chainon
    if(d_tete==d_tete->d_suiv)
    {
        delete d_tete;
        d_tete=nullptr;
        return;
    }

    ChainonDC *c=d_tete,*dernierC=d_tete->d_prec;
    int i=0;
    while(c!=dernierC && c!=nullptr && i<nb)
    {
        if(c==c->d_suiv)
        {
            // Suppression dernier chainon
            delete c;
            d_tete=nullptr;
            c=nullptr;
        }
        else
        {
            // SUpprime le chainon c
            ChainonDC *precC=c->d_prec;
            ChainonDC *suivC=c->d_suiv;
            precC->d_suiv=suivC;
            suivC->d_prec=precC;
            // Si c est la tete, la tete devient son suivant
            if(c==d_tete)
                d_tete=suivC;
            delete c;
            c=suivC;
        }
        i++;
    }
    // Traite le dernier chainon
    if(c!=nullptr && i<nb)
    {
        if(c==c->d_suiv)
        {
            // Suppression dernier chainon
            delete c;
            d_tete=nullptr;
            c=nullptr;
        }
        else
        {
            // SUpprime le chainon c
            ChainonDC *precC=c->d_prec;
            ChainonDC *suivC=c->d_suiv;
            precC->d_suiv=suivC;
            suivC->d_prec=precC;
            // Si c est la tete, la tete devient son suivant
            if(c==d_tete)
                d_tete=suivC;
            delete c;
            c=suivC;
        }
    }
}


void LDCCircCorr::enleverPlusieursEltsMilieu(int idx, int nb)
{
    // Liste vide ou rien a supprimer
    if(nb==0 || d_tete==nullptr)
        return;
    // Liste avec un seul chainon
    if(d_tete==d_tete->d_suiv)
    {
        if(idx==0)
        {
            delete d_tete;
            d_tete=nullptr;
        }
        return;
    }
    // Liste avec plusieurs chainons
    ChainonDC *c=d_tete;
    // Cherche le chainon
    if(idx>0)
    {
        int i=0;
        do
        {
            i++;
            c=c->d_suiv;
        }
        while(c!=d_tete && i<idx);
        // Chainon pas trouve
        if(i<idx)
            return;
    }
    // Suppression des chainons a partir de c
    int i=0;
    do
    {
        i++;
        ChainonDC *precC=c->d_prec;
        ChainonDC *suivC=c->d_suiv;
        precC->d_suiv=suivC;
        suivC->d_prec=precC;
        delete c;
        c=suivC;
    }
    while(c!=c->d_suiv && c!=d_tete && i<nb);
    if(idx==0)
    {
        if(i<nb)
        {
            // Tous les chainons ont ete supprimes, on supprime le dernier chainon
            delete c;
            d_tete=nullptr;
        }
        else
            d_tete=c;
    }
}


void LDCCircCorr::enleverPlusieursEltsFin(int nb)
{
    // Liste vide ou rien a supprimer
    if(nb==0 || d_tete==nullptr)
        return;
    // Liste avec un seul chainon
    if(d_tete==d_tete->d_suiv)
    {
        delete d_tete;
        d_tete=nullptr;
        return;
    }

    ChainonDC *c=d_tete->d_prec;
    int i=0;
    do
    {
        i++;
        ChainonDC *precC=c->d_prec;
        ChainonDC *suivC=c->d_suiv;
        precC->d_suiv=suivC;
        suivC->d_prec=precC;
        delete c;
        c=precC;
    }
    while(c!=c->d_suiv && i<nb);

    if(i<nb)
    {
        // On est arrive a la fin de la liste, on supprime le dernier chainon
        delete c;
        d_tete=nullptr;
    }
}

bool LDCCircCorr::eltsEnOrdreDecroissant() const
{
    if(d_tete==nullptr || d_tete->d_suiv==d_tete)
        return true;
    ChainonDC *c=d_tete;
    do
    {
        c=c->d_suiv;
    }
    while(c!=d_tete && c->d_prec->d_v > c->d_v);
    return (c==d_tete);
}


bool LDCCircCorr::contientElt(double val) const
{
    // Liste vide
    if(d_tete==nullptr)
        return false;
    // Liste avec un seul chainon
    if(d_tete->d_suiv==d_tete)
        return d_tete->d_v==val;
    ChainonDC *c=d_tete;
    do
    {
        c=c->d_suiv;
    }
    while(c!=d_tete && c->d_v!=val);
    return (c->d_v==val);
}


void LDCCircCorr::enleverToutesLesOccurencesDUnElt(double val)
{
    // Liste vide
    if(d_tete==nullptr)
        return;

    // Parcours de la liste
    ChainonDC *c=d_tete;
    ChainonDC *dernierC=d_tete->d_prec;
    do
    {
        if(c->d_v!=val)
            c=c->d_suiv;
        else
        {
            // Chainon trouve. On doit le supprimer
            ChainonDC *precC=c->d_prec;
            ChainonDC *suivC=c->d_suiv;
            precC->d_suiv=suivC;
            suivC->d_prec=precC;
            if(c==d_tete)
            {
                if(c==c->d_suiv)
                {
                    // Dernier chainon supprime
                    d_tete=nullptr;
                    suivC=nullptr;
                }
                else
                    // Tete supprime. La tete devient le chainon suivant
                    d_tete=suivC;
            }
            delete c;
            c=suivC;
        }
    }
    while(d_tete!=nullptr && c!=dernierC);

    // Traite le dernier chainon
    if(c!=nullptr && c->d_v==val)
    {
        if(c==c->d_suiv)
            d_tete=nullptr;
        else
        {
            ChainonDC *precC=c->d_prec;
            ChainonDC *suivC=c->d_suiv;
            precC->d_suiv=suivC;
            suivC->d_prec=precC;
            if(d_tete==c)
                d_tete=suivC;
            delete c;
        }
    }
}

int LDCCircCorr::chercherLIndexDeLaPremiereOccurenceDUnElt(double val) const
{
    if(!d_tete)
        return -1;
    if(d_tete->d_v==val)
        return 0;
    if(d_tete==d_tete->d_suiv)
        return -1;
    ChainonDC *c=d_tete;
    int i=0;
    do
    {
        c=c->d_suiv;
        i++;
    }
    while(c->d_suiv!=d_tete && c->d_v!=val);

    if(c->d_v==val)
        return i;
    else
        return -1;
}

int LDCCircCorr::chercherLIndexDeLaDerniereOccurenceDUnElt(double val) const
{
    if(!d_tete)
        return -1;
    int idx=-1;
    // Valeur trouvee dans le chainon tete
    if(d_tete->d_v==val)
        idx=0;
    // Liste avec un seul chainon
    if(d_tete==d_tete->d_suiv)
        return idx;
    ChainonDC *c=d_tete;
    int i=0;
    do
    {
        c=c->d_suiv;
        i++;
        // Recupere param'index du dernier chainon trouve
        if(c->d_v==val)
            idx=i;
    }
    while(c->d_suiv!=d_tete);
    return idx;
}

int LDCCircCorr::calculerNombreDOccurencesDUnElt(double val) const
{
    if(!d_tete)
        return 0;
    int nb=0;
    // Valeur trouvee dans le chainon tete
    if(d_tete->d_v==val)
        nb++;
    // Liste avec un seul chainon
    if(d_tete==d_tete->d_suiv)
        return nb;
    ChainonDC *c=d_tete;
    do
    {
        c=c->d_suiv;
        // Recupere l'index du dernier chainon trouve
        if(c->d_v==val)
            nb++;
    }
    while(c->d_suiv!=d_tete);
    return nb;
}

