#include "Exercices.h"



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le constructeur pour la structure de données ci-dessous
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
LDCCirc::LDCCirc():d_tete{nullptr}
{
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le destructeur pour la structure de données ci-dessous
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
LDCCirc::~LDCCirc()
{
    if (!d_tete)
        return;
    ChainonDC *c=d_tete;
    do
    {
        ChainonDC *tmp=c->d_suiv;
        delete c;
        c=tmp;
    }
    while(c!=d_tete);
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le constructeur par recopie pour la structure de données ci-dessous
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
LDCCirc::LDCCirc(const LDCCirc &l):d_tete{nullptr}
{
    if (l.d_tete==nullptr)
        return;

    ChainonDC *c2 = l.d_tete, *tete2=l.d_tete, *precC1=nullptr;
    do
    {
        ChainonDC *nc1 = new ChainonDC(c2->d_v);
        if (d_tete == nullptr)
        {
            d_tete = nc1;
            d_tete->d_suiv=d_tete;
            d_tete->d_prec=d_tete;
        }
        else
        {
            ChainonDC* suivC1=precC1->d_suiv;
            // Insertion entre precC1 et suivC1
            precC1->d_suiv = nc1;
            nc1->d_prec=precC1;
            nc1->d_suiv = suivC1;
            suivC1->d_prec=nc1;
        }
        precC1 = nc1;
        c2 = c2->d_suiv;
    }
    while(c2!=tete2);
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le constructeur qui prend comme paramètres un tableau avec sa
// taille: double *tabVal, int nbVal. Ce constructeur doit créer une
// structure de données contenant tous les éléments dans le tableau
// passé en paramètre
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
LDCCirc::LDCCirc(double *tabVal, int nbVal):d_tete(nullptr)
{
    ChainonDC *precC1=nullptr;
    for(int i=0;i<nbVal;i++)
    {
        ChainonDC *nc1 = new ChainonDC(tabVal[i]);
        if (d_tete == nullptr)
        {
            d_tete = nc1;
            d_tete->d_suiv=d_tete;
            d_tete->d_prec=d_tete;
        }
        else
        {
            ChainonDC* suivC1=precC1->d_suiv;
            // Insertion entre precC1 et suivC1
            precC1->d_suiv = nc1;
            nc1->d_prec=precC1;
            nc1->d_suiv = suivC1;
            suivC1->d_prec=nc1;
        }
        precC1 = nc1;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode concatenationDebut(s) qui ajoute les elements de la
// structure de données s au début de la structure de données this. Cette
// structure de données s est passée en paramètre. L'ordre des éléments
// doit être gardé.
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDCCirc::concatenationDebut(const LDCCirc &l)
{
    // Si la liste a inserer est vide
    if (l.d_tete==nullptr)
        return;

    ChainonDC *c2=l.d_tete, *tete2=l.d_tete;

    // Recupere la tete et son precedent de la liste this
    ChainonDC *suivC1=d_tete,*precC1=nullptr;
    if (d_tete!=nullptr)
        precC1=d_tete->d_prec;
    // La tete doit etre redefinie
    d_tete=nullptr;

    // Insertion entre la precC1 et c1
    do
    {
        ChainonDC* nc1=new ChainonDC(c2->d_v);
        if (d_tete==nullptr)
            d_tete=nc1;
        if (precC1!=nullptr)
        {
            // insertion entre precC1 et c1
            precC1->d_suiv=nc1;
            nc1->d_prec=precC1;

            nc1->d_suiv=suivC1;
            suivC1->d_prec=nc1;
            precC1=nc1;
        }
        else
        {
            d_tete->d_prec=nc1;
            d_tete->d_suiv=nc1;
            suivC1=nc1;
            precC1=nc1;
        }
        c2=c2->d_suiv;
    }
    while(c2!=tete2);
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode concatenationFin(s) qui ajoute les elements de la
// structure de données s à la fin de la structure de données this. Cette
// structure de données s est passée en paramètre. L'ordre des éléments
// doit être gardé.
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDCCirc::concatenationFin(const LDCCirc &l)
{
    // Si la liste a inserer est vide
    if (l.d_tete==nullptr)
        return;

    ChainonDC *c2=l.d_tete, *tete2=l.d_tete;

    // Recupere la tete et son precedent de la liste this
    ChainonDC *suivC1=d_tete,*precC1=nullptr;
    if (d_tete!=nullptr)
        precC1=d_tete->d_prec;

    // Insertion entre la precC1 et c1
    do
    {
        ChainonDC* nc1=new ChainonDC(c2->d_v);
        if (precC1!=nullptr)
        {
            // insertion entre precC1 et c1
            precC1->d_suiv=nc1;
            nc1->d_prec=precC1;

            nc1->d_suiv=suivC1;
            suivC1->d_prec=nc1;
            precC1=nc1;
        }
        else
        {
            d_tete=nc1;
            d_tete->d_prec=nc1;
            d_tete->d_suiv=nc1;
            suivC1=nc1;
            precC1=nc1;
        }
        c2=c2->d_suiv;
    }
    while(c2!=tete2);
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode operator+=(s) qui ajoute les elements de la structure de
// données s à la fin de la structure de données this. Cette structure de
// données s est passée en paramètre
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
LDCCirc &LDCCirc::operator+=(const LDCCirc &l)
{
    // Si la liste a inserer est vide
    if (l.d_tete==nullptr)
        return *this;

    ChainonDC *c2=l.d_tete, *tete2=l.d_tete;

    // Recupere la tete et son precedent de la liste this
    ChainonDC *suivC1=d_tete,*precC1=nullptr;
    if (d_tete!=nullptr)
        precC1=d_tete->d_prec;

    // Insertion entre la precC1 et c1
    do
    {
        ChainonDC* nc1=new ChainonDC(c2->d_v);
        if (precC1!=nullptr)
        {
            // insertion entre precC1 et c1
            precC1->d_suiv=nc1;
            nc1->d_prec=precC1;

            nc1->d_suiv=suivC1;
            suivC1->d_prec=nc1;
            precC1=nc1;
        }
        else
        {
            d_tete=nc1;
            d_tete->d_prec=nc1;
            d_tete->d_suiv=nc1;
            suivC1=nc1;
            precC1=nc1;
        }
        c2=c2->d_suiv;
    }
    while(c2!=tete2);

    return *this;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode operator==(s) qui retourne true si la structure de
// données s passée en paramètre est égale à la structure de données this.
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
bool LDCCirc::operator==(const LDCCirc &l) const
{
    if (this==&l)
        return true;
    // Comparaison vide/pas vide
    if (d_tete==nullptr && l.d_tete==nullptr)
        return true;
    if (d_tete==nullptr && l.d_tete!=nullptr)
        return false;
    if (d_tete!=nullptr && l.d_tete==nullptr)
        return false;
    if (d_tete->d_v != l.d_tete->d_v)
        return false;

    ChainonDC *c1=d_tete, *tete1=d_tete, *c2=l.d_tete, *tete2=l.d_tete;
    do
    {
        c1=c1->d_suiv;
        c2=c2->d_suiv;
    }
    while(c1!=tete1 && c2!=tete2 && c1->d_v==c2->d_v);
    return (c1==tete1 && c2==tete2);
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode operator!=(s) qui retourne true si la structure de
// données s passée en paramètre est différente à la structure de données this.
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
bool LDCCirc::operator!=(const LDCCirc &l) const
{
    if (this==&l)
        return false;
    // Comparaison vide/pas vide
    if (d_tete==nullptr && l.d_tete==nullptr)
        return false;
    if (d_tete==nullptr && l.d_tete!=nullptr)
        return true;
    if (d_tete!=nullptr && l.d_tete==nullptr)
        return true;
    if (d_tete->d_v != l.d_tete->d_v)
        return true;

    ChainonDC *c1=d_tete, *tete1=d_tete, *c2=l.d_tete, *tete2=l.d_tete;
    do
    {
        c1=c1->d_suiv;
        c2=c2->d_suiv;
    }
    while(c1!=tete1 && c2!=tete2 && c1->d_v==c2->d_v);
    return (c1!=tete1 || c2!=tete2);
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire l'opérateur d'affectation operator=(s). Après cette méthode, la structure
// de données this doit contenir les mêmes éléments que la structure de données s
// passée en paramètre.
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
LDCCirc &LDCCirc::operator=(const LDCCirc &l)
{
	if (this==&l)
		return *this;

    // Cas ou on copie une liste vide
    if (l.d_tete==nullptr)
    {
        if (d_tete==nullptr)
            // Copie d'une liste vide dans une liste vide
            return *this;
        // Copie d'une liste vide dans une liste non-vide
        ChainonDC *c1=d_tete;
        do
        {
            ChainonDC *tmp=c1->d_suiv;
            delete c1;
            c1=tmp;
        }
        while(c1!=d_tete);
        d_tete=nullptr;
        return *this;
    }

	ChainonDC *c1=d_tete,*c2=l.d_tete;
    if (c1 && c2)
    {
        // Copie d'une liste non vide dans une liste non vide
        do
        {
            c1->d_v=c2->d_v;
            c1=c1->d_suiv;
            c2=c2->d_suiv;
        }
        while(c1!=d_tete && c2!=l.d_tete);

        // Si les listes ont la meme longueur
        if (c1==d_tete && c2==l.d_tete)
            return *this;

        if(c1!=d_tete)
        {
            // Supprime les chainons en trop
            ChainonDC *precC1=c1->d_prec;
            precC1->d_suiv=d_tete;
            d_tete->d_prec=precC1;
            while(c1!=d_tete)
            {
                ChainonDC* tmp=c1;
                c1=c1->d_suiv;
                delete tmp;
            }
        }
        else
        {
            // Ajoute les chainons manquants
            do
            {
                ChainonDC* nc1=new ChainonDC(c2->d_v);
                // Insertion de nc1 entre c1 et son suivant
                ChainonDC *precC1=c1->d_prec;
                precC1->d_suiv=nc1;
                nc1->d_prec=precC1;
                nc1->d_suiv=c1;
                c1->d_prec=nc1;
                c2=c2->d_suiv;
            }
            while(c2!=l.d_tete);
        }
    }
    else
	{
        // Copie d'une liste non vide dans une liste vide
        do
        {
            ChainonDC* nc1=new ChainonDC(c2->d_v);
            if (d_tete==nullptr)
            {
                d_tete=nc1;
                nc1->d_suiv=nc1;
                nc1->d_prec=nc1;
            }
            else
            {
                // insertion entre la tete et son precedent
                ChainonDC *precTete=d_tete->d_prec;
                nc1->d_prec=precTete;
                precTete->d_suiv=nc1;
                nc1->d_suiv=d_tete;
                d_tete->d_prec=nc1;
            }
            c2=c2->d_suiv;
        }
        while(c2!=l.d_tete);
	}
	return *this;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void additionnerATousLesElts(double val) qui ajoute la valeur
// val passée en paramètre à tous les éléments de la stucture de données.
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDCCirc::additionnerATousLesElts(double val)
{
    if (d_tete==nullptr)
        return;
    ChainonDC *c=d_tete;
    do
    {
        c->d_v+=val;
        c=c->d_suiv;
    }
     while(c!=d_tete);
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererUnEltDebut(double val) qui insère un élément
// contenant la valeur val passée en paramètre. Cet élément est inséré au
// début de la stucture de données.
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDCCirc::insererUnEltDebut(double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion dans une liste vide
    if (d_tete == nullptr) {
        d_tete = nc;
        nc->d_suiv=nc;
        nc->d_prec=nc;
        return;
    }
    // Insertion en tete, c'est-à-dire, entre la tete et son precedent
    ChainonDC *precTete=d_tete->d_prec;
    precTete->d_suiv=nc;
    nc->d_prec=precTete;
    nc->d_suiv=d_tete;
    d_tete->d_prec=nc;
    d_tete = nc;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererUnEltMilieu(int idx, double val) qui
// insère un élément contenant la valeur val passée en paramètre. Cet élément
// est inséré à la position idx dans la stucture de données.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - L'index d'insertion peut être une valeur égale ou plus grande que la taille
//   de la structure de données. Dans ce cas, l'insertion se fait à la fin de la
//   structure de données.
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDCCirc::insererUnEltMilieu(int idx, double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    if (d_tete == nullptr)
    {
        // Insertion dans une liste vide
        d_tete = nc;
        nc->d_suiv=nc;
        nc->d_prec=nc;
        return;
    }
    else if (idx==0)
    {
        // Insertion en tete de la liste
        ChainonDC *precTete=d_tete->d_prec;
        precTete->d_suiv=nc;
        nc->d_prec=precTete;
        nc->d_suiv=d_tete;
        d_tete->d_prec=nc;
        d_tete=nc;
        return;
    }
    // Insertion apres la tete
    ChainonDC *c=d_tete;
    int i=0;
    do
    {
        i++;
        c=c->d_suiv;
    }
     while(c!=d_tete && i<idx);
     // insertion entre c et son precedent
     ChainonDC *precC=c->d_prec;
     precC->d_suiv=nc;
     nc->d_prec=precC;
     nc->d_suiv=c;
     c->d_prec=nc;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererUnEltFin(double val) qui insère un élément
// contenant la valeur val passée en paramètre. Cet élément est inséré à la
// fin de la stucture de données.
// Remarques:
// - La structure de données peut être vide
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDCCirc::insererUnEltFin(double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion dans une liste vide
    if (d_tete == nullptr) {
        d_tete = nc;
        nc->d_suiv=nc;
        nc->d_prec=nc;
        return;
    }
    // Insertion entre la tete et son precedent sans changer d_tete
    ChainonDC *precTete=d_tete->d_prec;
    precTete->d_suiv=nc;
    nc->d_prec=precTete;
    nc->d_suiv=d_tete;
    d_tete->d_prec=nc;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererPlusieursEltsDebut(double *tabVal, int nbVal)
// qui insère nbVal éléments au début de la structures de données. Les éléments
// à ajouter sont le tableau tabVal. L'ordre des éléments dans le tableau doit
// être gardé dans la structure de données.
// Remarques:
// - Le paramètre nbVal peut être égal à zéro
// - La structure de données peut être vide
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDCCirc::insererPlusieursEltsDebut(double *tabVal, int nbVal)
{
    if (nbVal==0)
        return;
    ChainonDC *precC=nullptr,*c=d_tete;
    if (c!=nullptr)
        precC=c->d_prec;
    // La tete doit etre redefinie
    d_tete=nullptr;
    for(int i=0;i<nbVal;i++)
    {
        ChainonDC *nc=new ChainonDC(tabVal[i]);
        // Si la tete n'est pas encore definie, ce nouveau chainon est la tete
        if (d_tete==nullptr)
            d_tete=nc;
        if (c==nullptr)
        {
            // Insertion dans une liste vide
            nc->d_suiv=nc;
            nc->d_prec=nc;
            precC=nc;
            c=nc;
        }
        else
        {
            // Insertion entre precC et c
            precC->d_suiv=nc;
            nc->d_prec=precC;
            nc->d_suiv=c;
            c->d_prec=nc;
            precC=nc;
        }
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererPlusieursEltsMilieu(int idx, double *tabVal, int nbVal)
// qui insère nbVal éléments à la position idx dans la structures de données. Les éléments
// à ajouter sont le tableau tabVal. L'ordre des éléments dans le tableau doit
// être gardé dans la structure de données.
// Remarques:
// - Le paramètre nbVal peut être égal à 0.
// - L'index du premier élément de la structure de données est 0.
// - L'index d'insertion idx peut être une valeur égale ou plus grande que la taille
//   de la structure de données. Dans ce cas, l'insertion se fait à la fin de la
//   structure de données.
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDCCirc::insererPlusieursEltsMilieu(int idx, double *tabVal, int nbVal)
{
    if(nbVal==0)
        return;
    // Insertion du premier chainon
    ChainonDC *precC,*c, *nc;
    nc = new ChainonDC(tabVal[0]);
    if (d_tete == nullptr)
    {
        // Insertion dans une liste vide
        d_tete = nc;
        nc->d_suiv=nc;
        nc->d_prec=nc;
        precC=nc;
        c=nc;
    }
    else if (idx==0)
    {
        // Insertion en tete de la liste
        ChainonDC *precTete=d_tete->d_prec;
        precTete->d_suiv=nc;
        nc->d_prec=precTete;
        nc->d_suiv=d_tete;
        d_tete->d_prec=nc;
        precC=nc;
        c=d_tete;
        d_tete=nc;
    }
    else
    {
        // Insertion apres la tete
        c=d_tete;
        int i=0;
        do
        {
            i++;
            c=c->d_suiv;
        }
        while(c!=d_tete && i<idx);
        // insertion entre c et son precedent
        precC=c->d_prec;
        precC->d_suiv=nc;
        nc->d_prec=precC;
        nc->d_suiv=c;
        c->d_prec=nc;
        precC=nc;
    }
    // Insertion des chainons restant entre precC et c
    for(int i=1;i<nbVal;i++)
    {
        nc = new ChainonDC(tabVal[i]);
        precC->d_suiv=nc;
        nc->d_prec=precC;
        nc->d_suiv=c;
        c->d_prec=nc;
        precC=nc;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererPlusieursEltsFin(double *tabVal, int nbVal)
// qui insère nbVal éléments à la fin de la structures de données. Les éléments
// à ajouter sont le tableau tabVal. L'ordre des éléments dans le tableau doit
// être gardé dans la structure de données.
// Remarques:
// - Le paramètre nbVal peut être égal à zéro
// - La structure de données peut être vide
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDCCirc::insererPlusieursEltsFin(double *tabVal, int nbVal)
{
    if(nbVal==0)
        return;
    // Insertion du premier chainon
    ChainonDC *precC,*c, *nc;
    // Creation du nouveau chainon
    nc = new ChainonDC(tabVal[0]);
    // Insertion dans une liste vide
    if (d_tete == nullptr) {
        d_tete = nc;
        nc->d_suiv=nc;
        nc->d_prec=nc;
        precC=nc;
        c=nc;
    }
    else
    {
        // Insertion entre la tete et son precedent sans changer d_tete
        ChainonDC *precTete=d_tete->d_prec;
        precTete->d_suiv=nc;
        nc->d_prec=precTete;
        nc->d_suiv=d_tete;
        d_tete->d_prec=nc;
        precC=nc;
        c=d_tete;
    }

    // Insertion des chainons restants entre precC et c
    for(int i=1; i<nbVal;i++)
    {
        nc = new ChainonDC(tabVal[i]);
        precC->d_suiv=nc;
        nc->d_prec=precC;
        nc->d_suiv=c;
        c->d_prec=nc;
        precC=nc;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverUnEltDebut() qui enlève le premier élément
// de la structure de données.
// Remarque:
// - La structure de données peut être vide
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDCCirc::enleverUnEltDebut()
{
    // Liste chainee vide
    if (d_tete==nullptr)
        return;
    // Liste avec un seul chainon
    if (d_tete==d_tete->d_suiv)
    {
        delete d_tete;
        d_tete=nullptr;
        return;
    }
    ChainonDC *precTete=d_tete->d_prec;
    ChainonDC *suivTete=d_tete->d_suiv;
    precTete->d_suiv=suivTete;
    suivTete->d_prec=precTete;
    delete d_tete;
    d_tete=suivTete;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverUnEltMilieu(int idx) qui enlève
// l'élément d'index idx dans la structure de données.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - La structure de données peut être vide.
// - La valeur idx peut être plus grande ou égale à la taille de la structure
//   de données. Dans ce cas, l'élément n'est pas supprimé.
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDCCirc::enleverUnEltMilieu(int idx)
{
    // Liste chainee vide
    if (d_tete==nullptr)
        return;
    // Suppression du premier chainon
    if (idx==0)
    {
        if (d_tete==d_tete->d_suiv)
        {
            // Liste avec un seul chainon
            delete d_tete;
            d_tete=nullptr;
        }
        else
        {
            ChainonDC *precC=d_tete->d_prec;
            ChainonDC *suivC=d_tete->d_suiv;
            delete d_tete;
            precC->d_suiv=suivC;
            suivC->d_prec=precC;
            d_tete=suivC;
        }
        return;
    }
    else
        // Liste avec un seul chainon et idx > 0
        if (d_tete==d_tete->d_suiv)
            return;
    // Cherche le chainon a supprimer
    ChainonDC *c=d_tete;
    int i=0;
    do
    {
        i++;
        c=c->d_suiv;
    }
    while(c!=d_tete && i<idx);
    if (i==idx)
    {
        // Supprime le chainon si il a ete trouve
        ChainonDC *precC=c->d_prec;
        ChainonDC *suivC=c->d_suiv;
        delete c;
        precC->d_suiv=suivC;
        suivC->d_prec=precC;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverUnEltFin() qui enlève le dernier élément
// de la structure de données.
// Remarque:
// - La structure de données peut être vide
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDCCirc::enleverUnEltFin()
{
    // Liste chainee vide
    if (d_tete==nullptr)
        return;
    // Liste avec un seul chainon
    if (d_tete==d_tete->d_suiv)
    {
        delete d_tete;
        d_tete=nullptr;
        return;
    }
    ChainonDC *precTete=d_tete->d_prec;
    ChainonDC *precPrecTete=precTete->d_prec;
    precPrecTete->d_suiv=d_tete;
    d_tete->d_prec=precPrecTete;
    delete precTete;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverPlusieursEltsDebut(int nb) qui enlève nb
// éléments au début de la structures de données. Cette valeur nb est passée
// en paramètre.
// Remarque:
// - La valeur nb peut être plus grande que la taille de la structures de
// données.
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDCCirc::enleverPlusieursEltsDebut(int nb)
{
    // Liste vide ou rien a supprimer
    if (nb==0 || d_tete==nullptr)
        return;
    // Liste avec un seul chainon
    if (d_tete==d_tete->d_suiv)
    {
        delete d_tete;
        d_tete=nullptr;
        return;
    }

    ChainonDC *c=d_tete,*dernierC=d_tete->d_prec;
    int i=0;
    while(c!=dernierC && c!=nullptr && i<nb)
    {
        if (c==c->d_suiv)
        {
            // Suppression dernier chainon
            delete c;
            d_tete=nullptr;
            c=nullptr;
        }
        else
        {
            // SUpprime le chainon c
            ChainonDC *precC=c->d_prec;
            ChainonDC *suivC=c->d_suiv;
            precC->d_suiv=suivC;
            suivC->d_prec=precC;
            // Si c est la tete, la tete devient son suivant
            if (c==d_tete)
                d_tete=suivC;
            delete c;
            c=suivC;
        }
        i++;
    }
    // Traite le dernier chainon
    if (c!=nullptr && i<nb)
    {
        if (c==c->d_suiv)
        {
            // Suppression dernier chainon
            delete c;
            d_tete=nullptr;
            c=nullptr;
        }
        else
        {
            // SUpprime le chainon c
            ChainonDC *precC=c->d_prec;
            ChainonDC *suivC=c->d_suiv;
            precC->d_suiv=suivC;
            suivC->d_prec=precC;
            // Si c est la tete, la tete devient son suivant
            if (c==d_tete)
                d_tete=suivC;
            delete c;
            c=suivC;
        }
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverPlusieursEltsMilieu(int idx, int nb) qui
// enlève nb éléments à partir de l'élément d'index idx. Ces valeurs idx et
// nb sont passées en paramètre.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - La valeur nb peut être plus grande que la taille de la structures de
// données.
// - La valeur idx peut être plus grande ou égale à la taille de la structure
// de données. Dans ce cas, les éléments ne sont pas supprimés.
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDCCirc::enleverPlusieursEltsMilieu(int idx, int nb)
{
    // Liste vide ou rien a supprimer
    if (nb==0 || d_tete==nullptr)
        return;
    // Liste avec un seul chainon
    if (d_tete==d_tete->d_suiv)
    {
        if (idx==0)
        {
            delete d_tete;
            d_tete=nullptr;
        }
        return;
    }
    // Liste avec plusieurs chainons
    ChainonDC *c=d_tete;
    // Cherche le chainon
    if (idx>0)
    {
        int i=0;
        do
        {
            i++;
            c=c->d_suiv;
        }
        while(c!=d_tete && i<idx);
        // Chainon pas trouve
        if (i<idx)
            return;
    }
    // Suppression des chainons a partir de c
    int i=0;
    do
    {
        i++;
        ChainonDC *precC=c->d_prec;
        ChainonDC *suivC=c->d_suiv;
        precC->d_suiv=suivC;
        suivC->d_prec=precC;
        delete c;
        c=suivC;
    }
    while(c!=c->d_suiv && c!=d_tete && i<nb);
    if (idx==0)
    {
        if (i<nb)
        {
            // Tous les chainons ont ete supprimes, on supprime le dernier chainon
            delete c;
            d_tete=nullptr;
        }
        else
            d_tete=c;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverPlusieursEltsFin(int nb) qui enlève nb
// éléments à la fin de la structures de données. Cette valeur nb est passée
// en paramètre.
// Remarque:
// - La valeur nb peut être plus grande que la taille de la structures de
// données.
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDCCirc::enleverPlusieursEltsFin(int nb)
{
    // Liste vide ou rien a supprimer
    if (nb==0 || d_tete==nullptr)
        return;
    // Liste avec un seul chainon
    if (d_tete==d_tete->d_suiv)
    {
        delete d_tete;
        d_tete=nullptr;
        return;
    }

    ChainonDC *c=d_tete->d_prec;
    int i=0;
    do
    {
        i++;
        ChainonDC *precC=c->d_prec;
        ChainonDC *suivC=c->d_suiv;
        precC->d_suiv=suivC;
        suivC->d_prec=precC;
        delete c;
        c=precC;
    }
    while(c!=c->d_suiv && i<nb);

    if (i<nb)
    {
        // On est arrive a la fin de la liste, on supprime le dernier chainon
        delete c;
        d_tete=nullptr;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode eltsEnOrdreDecroissant() qui retourne true si les éléments
// dans la structures de données sont classés par ordre strictement décroissant.
// Remarque:
// - eltsEnOrdreDecroissant() doit retourner true pour une structure de données vide
// ou ne contenant qu'un seul élément.
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
bool LDCCirc::eltsEnOrdreDecroissant() const
{
    if (d_tete==nullptr || d_tete->d_suiv==d_tete)
        return true;
    ChainonDC *c=d_tete;
    do
    {
        c=c->d_suiv;
    }
    while(c!=d_tete && c->d_prec->d_v > c->d_v);
    return (c==d_tete);
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode contientElt(double val) qui retourne true si la structure
// de données contient au moins un élément avec la valeur val, val étant un
// paramètre de la méthode.
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
bool LDCCirc::contientElt(double val) const
{
    // Liste vide
    if (d_tete==nullptr)
        return false;
    // Liste avec un seul chainon
    if (d_tete->d_suiv==d_tete)
        return d_tete->d_v==val;
    ChainonDC *c=d_tete;
    do
    {
        c=c->d_suiv;
    }
    while(c!=d_tete && c->d_v!=val);
    return (c->d_v==val);
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverToutesLesOccurencesDUnElt(double val) qui
// enlève tous les éléments contenant la valeur val dans la structure de
// données, val étant un paramètre de la méthode.
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDCCirc::enleverToutesLesOccurencesDUnElt(double val)
{
    // Liste vide
    if (d_tete==nullptr)
        return;

    // Parcours de la liste
    ChainonDC *c=d_tete;
    ChainonDC *dernierC=d_tete->d_prec;
    do
    {
        if (c->d_v!=val)
            c=c->d_suiv;
        else
        {
            // Chainon trouve. On doit le supprimer
            ChainonDC *precC=c->d_prec;
            ChainonDC *suivC=c->d_suiv;
            precC->d_suiv=suivC;
            suivC->d_prec=precC;
            if (c==d_tete)
            {
                if (c==c->d_suiv)
                {
                    // Dernier chainon supprime
                    d_tete=nullptr;
                    suivC=nullptr;
                }
                else
                    // Tete supprime. La tete devient le chainon suivant
                    d_tete=suivC;
            }
            delete c;
            c=suivC;
        }
    }
    while(d_tete!=nullptr && c!=dernierC);

    // Traite le dernier chainon
    if (c!=nullptr && c->d_v==val)
    {
        if (c==c->d_suiv)
            d_tete=nullptr;
        else
        {
            ChainonDC *precC=c->d_prec;
            ChainonDC *suivC=c->d_suiv;
            precC->d_suiv=suivC;
            suivC->d_prec=precC;
            if (d_tete==c)
                d_tete=suivC;
            delete c;
        }
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode int chercherLIndexDeLaPremiereOccurenceDUnElt(double val)
// qui retourne l'index du premier l'élément contenant la valeur val dans la
// structure de données, val étant un paramètre de la méthode.
// Remarques:
// - La structure de données ne contient pas nécessairement la valeur val. Dans ce
//   cas, la méthode doit retourner -1
// - La structure de données peut être vide
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
int LDCCirc::chercherLIndexDeLaPremiereOccurenceDUnElt(double val) const
{
    if (!d_tete)
        return -1;
    if (d_tete->d_v==val)
        return 0;
    if (d_tete==d_tete->d_suiv)
        return -1;
    ChainonDC *c=d_tete;
    int i=0;
    do
    {
        c=c->d_suiv;
        i++;
    }
    while(c->d_suiv!=d_tete && c->d_v!=val);

    if (c->d_v==val)
        return i;
    else
        return -1;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode int chercherLIndexDeLaDerniereOccurenceDUnElt(double val)
// qui retourne l'index du dernier l'élément contenant la valeur val dans la
// structure de données, val étant un paramètre de la méthode.
// Remarques:
// - La structure de données ne contient pas nécessairement la valeur val. Dans ce
//   cas, la méthode doit retourner -1
// - La structure de données peut être vide
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
int LDCCirc::chercherLIndexDeLaDerniereOccurenceDUnElt(double val) const
{
    if (!d_tete)
        return -1;
    int idx=-1;
    // Valeur trouvee dans le chainon tete
    if (d_tete->d_v==val)
        idx=0;
    // Liste avec un seul chainon
    if (d_tete==d_tete->d_suiv)
        return idx;
    ChainonDC *c=d_tete;
    int i=0;
    do
    {
        c=c->d_suiv;
        i++;
        // Recupere l'index du dernier chainon trouve
        if (c->d_v==val)
            idx=i;
    }
    while(c->d_suiv!=d_tete);
    return idx;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode int calculerNombreDOccurencesDUnElt(double val) qui
// retourne le nombre d'éléments contenant la valeur val dans la
// structure de données, val étant un paramètre de la méthode.
// Remarques:
// - La structure de données ne contient pas nécessairement la valeur val. Dans ce
//   cas, la méthode doit retourner 0
// - La structure de données peut être vide
//
// class ChainonDC {
//   friend class LDCCirc;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDCCirc {
//   public:
//     LDCCirc();
//     ~LDCCirc();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
int LDCCirc::calculerNombreDOccurencesDUnElt(double val) const
{
    if (!d_tete)
        return 0;
    int nb=0;
    // Valeur trouvee dans le chainon tete
    if (d_tete->d_v==val)
        nb++;
    // Liste avec un seul chainon
    if (d_tete==d_tete->d_suiv)
        return nb;
    ChainonDC *c=d_tete;
    do
    {
        c=c->d_suiv;
        // Incremente le compteur pour le chenon veant d'etre trouve
        if (c->d_v==val)
            nb++;
    }
    while(c->d_suiv!=d_tete);
    return nb;
}



