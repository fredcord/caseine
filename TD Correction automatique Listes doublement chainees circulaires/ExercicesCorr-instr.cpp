////////////////////////////////////////////////////////////////////////////////////
// Fichier genere automatiquement pour l'instrumentation du code. Ne pas modifier //
////////////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <algorithm>

#include "ExercicesBase.h"

using namespace std;


/////////////////////////////////////////////////////////////////////////////
// Compare la structure de donnees de l'etudiant avec celle de la solution //
/////////////////////////////////////////////////////////////////////////////

// Verifie si la liste chainee est correcte au niveau de sa structure et des pointeurs
bool LDCCircCorr_Other::verifierIntegrite(const LDCCirc &param)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!param.d_tete)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}

    ChainonDC *c=param.d_tete;
    Int compteur=0;
    vector<ChainonDC*> pntrUilise;
    do
    {
        // Validite des pointeurs
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(c))
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(c->d_prec))
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(c->d_suiv))
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_suiv->d_prec!=c)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_prec->d_suiv!=c)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}

        // Verifier si le pointeur est bien unique
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&std::find(pntrUilise.begin(), pntrUilise.end(), c)!=pntrUilise.end())
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        pntrUilise.push_back(c);

        c=c->d_suiv;
        compteur++;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=param.d_tete && compteur<StructDeDonneesVerifCorr::NB_MAX_COLONNES);

    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c==param.d_tete;}
}


bool LDCCircCorr_Other::compareCorrAvecEtud(const LDCCirc &student) const
{
    ChainonDC *c1=this->d_tete, *c2=student.d_tete;
    ChainonDC *tete1=c1,*tete2=c2;
    // Listes toutes les deux vides
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1==nullptr && c2==nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    // liste vide vs liste pas vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1==nullptr && c2!=nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    do
    {
        // Verifie si le pointeur c2 est valide
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c2==nullptr)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(c2))
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}

        // Verifie le precedent du suivant est correct
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1->d_suiv!=nullptr)
        {
            // Pour la correction aussi
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(c1->d_suiv) || c1->d_suiv->d_prec!=c1)
            {
                cout<<"Erreur sur la correction"<<endl;
                {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
            }
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c2->d_suiv==nullptr)
                {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(c2->d_suiv) || c2->d_suiv->d_prec!=c2)
                {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        }
        // Verifier si ce pointeur c2 n'est pas utilise dans la liste correction
        ChainonDC *tmpC1=d_tete;
        do
        {
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&tmpC1==c2)
                {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
            tmpC1=tmpC1->d_suiv;
        }
        while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&tmpC1!=d_tete);

        // Vérifie s'il contient la meme valeur
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1->d_v!=c2->d_v)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        // Verifie le chainon precedent
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1->d_prec==nullptr)
        {
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c2->d_prec!=nullptr) {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        }
        else
        {
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(c2->d_prec))
                {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1->d_prec->d_v!=c2->d_prec->d_v)
                {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        }
        c1=c1->d_suiv;
        c2=c2->d_suiv;

        // verifie la longueur de deux chaines
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1!=tete1 && c2==tete2)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1==tete1 && c2!=tete2)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=tete1);

    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
}


// genere la structDeDonneesVerifCorr a partir de la liste
StructDeDonneesVerifCorr LDCCircCorr_Other::getStructDeDonneesVerifCorr() const
{
    StructDeDonneesVerifCorr vect;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!d_tete)
    {
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return vect;}
    }

    ChainonDC *c=d_tete;
    do
    {
        vect.insererUnEltFin(c->d_v);
        c=c->d_suiv;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=d_tete);
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return vect;}
}

// Constructeur avec la structDeDonneesVerifCorr
void LDCCircCorr_Other::setStructDeDonneesVerifCorr(const StructDeDonneesVerifCorr &tab)
{
    // Supprime d'abord tous les elements
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete)
    {
        ChainonDC *tete=d_tete;
        do
        {
            ChainonDC *tmp=d_tete->d_suiv;
            {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
            d_tete=tmp;
        }
        while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&d_tete!=tete);
        d_tete=nullptr;
    }
    // On insere les elements du tableau
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<tab.getNbColonnes();i++)
        insererUnEltFin(tab[i]);
}

// Constructeur
LDCCircCorr_ConstructDestruct::LDCCircCorr_ConstructDestruct():d_tete{nullptr}
{
}

LDCCircCorr_ConstructDestruct::~LDCCircCorr_ConstructDestruct()
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!d_tete)
        return;
    ChainonDC *tete=d_tete;
    do
    {
        ChainonDC *tmp=d_tete->d_suiv;
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
        d_tete=tmp;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&d_tete!=tete);
}



// Constructeur par recopie
LDCCircCorr_ConstructDestruct::LDCCircCorr_ConstructDestruct(const LDCCircCorr &param):d_tete(nullptr)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_tete==nullptr)
        return;

    ChainonDC *c2 = param.d_tete, *tete2=param.d_tete, *precC1=nullptr;
    do
    {
        ChainonDC *nc1 = new ChainonDC(c2->d_v);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr)
        {
            d_tete = nc1;
            d_tete->d_suiv=d_tete;
            d_tete->d_prec=d_tete;
        }
        else
        {
            ChainonDC* suivC1=precC1->d_suiv;
            // Insertion entre precC1 et suivC1
            precC1->d_suiv = nc1;
            nc1->d_prec=precC1;
            nc1->d_suiv = suivC1;
            suivC1->d_prec=nc1;
        }
        precC1 = nc1;
        c2 = c2->d_suiv;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=tete2);
}


// Constructeur avec un tableau
LDCCircCorr_ConstructDestruct::LDCCircCorr_ConstructDestruct(Double *tab, Int nb):d_tete(nullptr)
{
    ChainonDC *precC1=nullptr;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        ChainonDC *nc1 = new ChainonDC(tab[i]);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr)
        {
            d_tete = nc1;
            d_tete->d_suiv=d_tete;
            d_tete->d_prec=d_tete;
        }
        else
        {
            ChainonDC* suivC1=precC1->d_suiv;
            // Insertion entre precC1 et suivC1
            precC1->d_suiv = nc1;
            nc1->d_prec=precC1;
            nc1->d_suiv = suivC1;
            suivC1->d_prec=nc1;
        }
        precC1 = nc1;
    }
}


void LDCCircCorr_Other::concatenationDebut(const LDCCircCorr &param)
{
    // Si la liste a inserer est vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_tete==nullptr)
        return;

    ChainonDC *c2=param.d_tete, *tete2=param.d_tete;

    // Recupere la tete et son precedent de la liste this
    ChainonDC *suivC1=d_tete,*precC1=nullptr;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete!=nullptr)
        precC1=d_tete->d_prec;
    // La tete doit etre redefinie
    d_tete=nullptr;

    // Insertion entre la precC1 et c1
    do
    {
        ChainonDC* nc1=new ChainonDC(c2->d_v);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr)
            d_tete=nc1;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
        {
            // insertion entre precC1 et c1
            precC1->d_suiv=nc1;
            nc1->d_prec=precC1;

            nc1->d_suiv=suivC1;
            suivC1->d_prec=nc1;
            precC1=nc1;
        }
        else
        {
            d_tete->d_prec=nc1;
            d_tete->d_suiv=nc1;
            suivC1=nc1;
            precC1=nc1;
        }
        c2=c2->d_suiv;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=tete2);
}

// Concatenation avec la structure de données passee en parametre
// Pour une liste chainee circulaire, concatenationDebut et concatenationFin
// font la même chose
void LDCCircCorr_Other::concatenationFin(const LDCCircCorr &param)
{
    // Si la liste a inserer est vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_tete==nullptr)
        return;

    ChainonDC *c2=param.d_tete, *tete2=param.d_tete;

    // Recupere la tete et son precedent de la liste this
    ChainonDC *suivC1=d_tete,*precC1=nullptr;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete!=nullptr)
        precC1=d_tete->d_prec;

    // Insertion entre la precC1 et c1
    do
    {
        ChainonDC* nc1=new ChainonDC(c2->d_v);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
        {
            // insertion entre precC1 et c1
            precC1->d_suiv=nc1;
            nc1->d_prec=precC1;

            nc1->d_suiv=suivC1;
            suivC1->d_prec=nc1;
            precC1=nc1;
        }
        else
        {
            d_tete=nc1;
            d_tete->d_prec=nc1;
            d_tete->d_suiv=nc1;
            suivC1=nc1;
            precC1=nc1;
        }
        c2=c2->d_suiv;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=tete2);
}

LDCCircCorr &LDCCircCorr_Other::operator+=(const LDCCircCorr &param)
{
    // Si la liste a inserer est vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_tete==nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}

    ChainonDC *c2=param.d_tete, *tete2=param.d_tete;

    // Recupere la tete et son precedent de la liste this
    ChainonDC *suivC1=d_tete,*precC1=nullptr;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete!=nullptr)
        precC1=d_tete->d_prec;

    // Insertion entre la precC1 et c1
    do
    {
        ChainonDC* nc1=new ChainonDC(c2->d_v);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
        {
            // insertion entre precC1 et c1
            precC1->d_suiv=nc1;
            nc1->d_prec=precC1;

            nc1->d_suiv=suivC1;
            suivC1->d_prec=nc1;
            precC1=nc1;
        }
        else
        {
            d_tete=nc1;
            d_tete->d_prec=nc1;
            d_tete->d_suiv=nc1;
            suivC1=nc1;
            precC1=nc1;
        }
        c2=c2->d_suiv;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=tete2);

    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}

bool LDCCircCorr_Other::operator==(const LDCCircCorr &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    // Comparaison vide/pas vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr && param.d_tete==nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr && param.d_tete!=nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete!=nullptr && param.d_tete==nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete->d_v != param.d_tete->d_v)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}

    ChainonDC *c1=d_tete, *tete1=d_tete, *c2=param.d_tete, *tete2=param.d_tete;
    do
    {
        c1=c1->d_suiv;
        c2=c2->d_suiv;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=tete1 && c2!=tete2 && c1->d_v==c2->d_v);
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c1==tete1 && c2==tete2);}
}


bool LDCCircCorr_Other::operator!=(const LDCCircCorr &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    // Comparaison vide/pas vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr && param.d_tete==nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr && param.d_tete!=nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete!=nullptr && param.d_tete==nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete->d_v != param.d_tete->d_v)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}

    ChainonDC *c1=d_tete, *tete1=d_tete, *c2=param.d_tete, *tete2=param.d_tete;
    do
    {
        c1=c1->d_suiv;
        c2=c2->d_suiv;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=tete1 && c2!=tete2 && c1->d_v==c2->d_v);
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c1!=tete1 || c2!=tete2);}
}

// Methode compliquée. A ne pas poser en examen
LDCCircCorr &LDCCircCorr_Other::operator=(const LDCCircCorr &param)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}

    // Cas ou on copie une liste vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_tete==nullptr)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr)
            // Copie d'une liste vide dans une liste vide
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
        // Copie d'une liste vide dans une liste non-vide
        ChainonDC *c1=d_tete;
        do
        {
            ChainonDC *tmp=c1->d_suiv;
            {GestionPntrEtComplexite::get().peutSupprPntr(c1, false); delete c1;}
            c1=tmp;
        }
        while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=d_tete);
        d_tete=nullptr;
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
    }

	ChainonDC *c1=d_tete,*c2=param.d_tete;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1 && c2)
    {
        // Copie d'une liste non vide dans une liste non vide
        do
        {
            c1->d_v=c2->d_v;
            c1=c1->d_suiv;
            c2=c2->d_suiv;
        }
        while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=d_tete && c2!=param.d_tete);
        // Si les listes ont la meme longueur
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1==d_tete && c2==param.d_tete)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1!=d_tete)
        {
            // Supprime les chainons en trop
            ChainonDC *precC1=c1->d_prec;
            precC1->d_suiv=d_tete;
            d_tete->d_prec=precC1;
            while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=d_tete)
            {
                ChainonDC* tmp=c1;
                c1=c1->d_suiv;
                {GestionPntrEtComplexite::get().peutSupprPntr(tmp, false); delete tmp;}
            }
        }
        else
        {
            // Ajoute les chainons manquants
            do
            {
                ChainonDC* nc1=new ChainonDC(c2->d_v);
                // Insertion de nc1 entre c1 et son suivant
                ChainonDC *precC1=c1->d_prec;
                precC1->d_suiv=nc1;
                nc1->d_prec=precC1;
                nc1->d_suiv=c1;
                c1->d_prec=nc1;
                c2=c2->d_suiv;
            }
            while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=param.d_tete);
        }
    }
    else
	{
        // Copie d'une liste non vide dans une liste vide
        do
        {
            ChainonDC* nc1=new ChainonDC(c2->d_v);
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr)
            {
                d_tete=nc1;
                nc1->d_suiv=nc1;
                nc1->d_prec=nc1;
            }
            else
            {
                // insertion entre la tete et son precedent
                ChainonDC *precTete=d_tete->d_prec;
                nc1->d_prec=precTete;
                precTete->d_suiv=nc1;
                nc1->d_suiv=d_tete;
                d_tete->d_prec=nc1;
            }
            c2=c2->d_suiv;
        }
        while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=param.d_tete);
	}
	{GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}

void LDCCircCorr_Other::additionnerATousLesElts(Double val)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr)
        return;
    ChainonDC *c=d_tete;
    do
    {
        c->d_v+=val;
        c=c->d_suiv;
    }
     while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=d_tete);
}

void LDCCircCorr_Other::insererUnEltDebut(Double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion dans une liste vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr) {
        d_tete = nc;
        d_tete->d_suiv=d_tete;
        d_tete->d_prec=d_tete;
        return;
    }
    // Insertion en tete
    ChainonDC *prec_tete=d_tete->d_prec;

    nc->d_suiv = d_tete;
    d_tete->d_prec=nc;

    prec_tete->d_suiv = nc;
    nc->d_prec=prec_tete;

    d_tete = nc;
}


void LDCCircCorr_Other::insererUnEltMilieu(Int idx, Double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr)
    {
        // Insertion dans une liste vide
        d_tete = nc;
        nc->d_suiv=nc;
        nc->d_prec=nc;
        return;
    }
    else if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx==0)
    {
        // Insertion en tete de la liste
        ChainonDC *precTete=d_tete->d_prec;
        precTete->d_suiv=nc;
        nc->d_prec=precTete;
        nc->d_suiv=d_tete;
        d_tete->d_prec=nc;
        d_tete=nc;
        return;
    }
    // Insertion apres la tete
    ChainonDC *c=d_tete;
    Int i=0;
    do
    {
        i++;
        c=c->d_suiv;
    }
     while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=d_tete && i<idx);
     // insertion entre c et son precedent
     ChainonDC *precC=c->d_prec;
     precC->d_suiv=nc;
     nc->d_prec=precC;
     nc->d_suiv=c;
     c->d_prec=nc;
}


void LDCCircCorr_Other::insererUnEltFin(Double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion dans une liste vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr) {
        d_tete = nc;
        nc->d_suiv=nc;
        nc->d_prec=nc;
        return;
    }
    // Insertion entre la tete et son precedent sans changer d_tete
    ChainonDC *precTete=d_tete->d_prec;
    precTete->d_suiv=nc;
    nc->d_prec=precTete;
    nc->d_suiv=d_tete;
    d_tete->d_prec=nc;
}


void LDCCircCorr_Other::insererPlusieursEltsDebut(Double *tab, Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0)
        return;
    ChainonDC *precC=nullptr,*c=d_tete;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c!=nullptr)
        precC=c->d_prec;
    // La tete doit etre redefinie
    d_tete=nullptr;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        ChainonDC *nc=new ChainonDC(tab[i]);
        // Si la tete n'est pas encore definie, ce nouveau chainon est la tete
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr)
            d_tete=nc;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==nullptr)
        {
            // Insertion dans une liste vide
            nc->d_suiv=nc;
            nc->d_prec=nc;
            precC=nc;
            c=nc;
        }
        else
        {
            // Insertion entre precC et c
            precC->d_suiv=nc;
            nc->d_prec=precC;
            nc->d_suiv=c;
            c->d_prec=nc;
            precC=nc;
        }
    }
}


void LDCCircCorr_Other::insererPlusieursEltsMilieu(Int idx, Double *tab, Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0)
        return;
    // Insertion du premier chainon
    ChainonDC *precC,*c, *nc;
    nc = new ChainonDC(tab[0]);
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr)
    {
        // Insertion dans une liste vide
        d_tete = nc;
        nc->d_suiv=nc;
        nc->d_prec=nc;
        precC=nc;
        c=nc;
    }
    else if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx==0)
    {
        // Insertion en tete de la liste
        ChainonDC *precTete=d_tete->d_prec;
        precTete->d_suiv=nc;
        nc->d_prec=precTete;
        nc->d_suiv=d_tete;
        d_tete->d_prec=nc;
        precC=nc;
        c=d_tete;
        d_tete=nc;
    }
    else
    {
        // Insertion apres la tete
        c=d_tete;
        Int i=0;
        do
        {
            i++;
            c=c->d_suiv;
        }
        while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=d_tete && i<idx);
        // insertion entre c et son precedent
        precC=c->d_prec;
        precC->d_suiv=nc;
        nc->d_prec=precC;
        nc->d_suiv=c;
        c->d_prec=nc;
        precC=nc;
    }
    // Insertion des chainons restant entre precC et c
    for(Int i=1;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        nc = new ChainonDC(tab[i]);
        precC->d_suiv=nc;
        nc->d_prec=precC;
        nc->d_suiv=c;
        c->d_prec=nc;
        precC=nc;
    }
}


void LDCCircCorr_Other::insererPlusieursEltsFin(Double *tab, Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0)
        return;
    // Insertion du premier chainon
    ChainonDC *precC,*c, *nc;
    // Creation du nouveau chainon
    nc = new ChainonDC(tab[0]);
    // Insertion dans une liste vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr)
    {
        d_tete = nc;
        nc->d_suiv=nc;
        nc->d_prec=nc;
        precC=nc;
        c=nc;
    }
    else
    {
        // Insertion entre la tete et son precedent sans changer d_tete
        ChainonDC *precTete=d_tete->d_prec;
        precTete->d_suiv=nc;
        nc->d_prec=precTete;
        nc->d_suiv=d_tete;
        d_tete->d_prec=nc;
        precC=nc;
        c=d_tete;
    }

    // Insertion des chainons restants entre precC et c
    for(Int i=1;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        nc = new ChainonDC(tab[i]);
        precC->d_suiv=nc;
        nc->d_prec=precC;
        nc->d_suiv=c;
        c->d_prec=nc;
        precC=nc;
    }
}


void LDCCircCorr_Other::enleverUnEltDebut()
{
    // Liste chainee vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr)
        return;
    // Liste avec un seul chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==d_tete->d_suiv)
    {
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
        d_tete=nullptr;
        return;
    }
    ChainonDC *precTete=d_tete->d_prec;
    ChainonDC *suivTete=d_tete->d_suiv;
    precTete->d_suiv=suivTete;
    suivTete->d_prec=precTete;
    {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
    d_tete=suivTete;
}


void LDCCircCorr_Other::enleverUnEltMilieu(Int idx)
{
    // Liste chainee vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr)
        return;
    // Suppression du premier chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx==0)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==d_tete->d_suiv)
        {
            // Liste avec un seul chainon
            {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
            d_tete=nullptr;
        }
        else
        {
            ChainonDC *precC=d_tete->d_prec;
            ChainonDC *suivC=d_tete->d_suiv;
            {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
            precC->d_suiv=suivC;
            suivC->d_prec=precC;
            d_tete=suivC;
        }
        return;
    }
    else
        // Liste avec un seul chainon et idx > 0
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==d_tete->d_suiv)
            return;
    // Cherche le chainon a supprimer
    ChainonDC *c=d_tete;
    Int i=0;
    do
    {
        i++;
        c=c->d_suiv;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=d_tete && i<idx);
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&i==idx)
    {
        // Supprime le chainon si il a ete trouve
        ChainonDC *precC=c->d_prec;
        ChainonDC *suivC=c->d_suiv;
        {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
        precC->d_suiv=suivC;
        suivC->d_prec=precC;
    }
}


void LDCCircCorr_Other::enleverUnEltFin()
{
    // Liste chainee vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr)
        return;
    // Liste avec un seul chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==d_tete->d_suiv)
    {
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
        d_tete=nullptr;
        return;
    }
    ChainonDC *precTete=d_tete->d_prec;
    ChainonDC *precPrecTete=precTete->d_prec;
    precPrecTete->d_suiv=d_tete;
    d_tete->d_prec=precPrecTete;
    {GestionPntrEtComplexite::get().peutSupprPntr(precTete, false); delete precTete;}
}


void LDCCircCorr_Other::enleverPlusieursEltsDebut(Int nb)
{
    // Liste vide ou rien a supprimer
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0 || d_tete==nullptr)
        return;
    // Liste avec un seul chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==d_tete->d_suiv)
    {
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
        d_tete=nullptr;
        return;
    }

    ChainonDC *c=d_tete,*dernierC=d_tete->d_prec;
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=dernierC && c!=nullptr && i<nb)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==c->d_suiv)
        {
            // Suppression dernier chainon
            {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
            d_tete=nullptr;
            c=nullptr;
        }
        else
        {
            // SUpprime le chainon c
            ChainonDC *precC=c->d_prec;
            ChainonDC *suivC=c->d_suiv;
            precC->d_suiv=suivC;
            suivC->d_prec=precC;
            // Si c est la tete, la tete devient son suivant
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==d_tete)
                d_tete=suivC;
            {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
            c=suivC;
        }
        i++;
    }
    // Traite le dernier chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c!=nullptr && i<nb)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==c->d_suiv)
        {
            // Suppression dernier chainon
            {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
            d_tete=nullptr;
            c=nullptr;
        }
        else
        {
            // SUpprime le chainon c
            ChainonDC *precC=c->d_prec;
            ChainonDC *suivC=c->d_suiv;
            precC->d_suiv=suivC;
            suivC->d_prec=precC;
            // Si c est la tete, la tete devient son suivant
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==d_tete)
                d_tete=suivC;
            {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
            c=suivC;
        }
    }
}


void LDCCircCorr_Other::enleverPlusieursEltsMilieu(Int idx, Int nb)
{
    // Liste vide ou rien a supprimer
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0 || d_tete==nullptr)
        return;
    // Liste avec un seul chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==d_tete->d_suiv)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx==0)
        {
            {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
            d_tete=nullptr;
        }
        return;
    }
    // Liste avec plusieurs chainons
    ChainonDC *c=d_tete;
    // Cherche le chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx>0)
    {
        Int i=0;
        do
        {
            i++;
            c=c->d_suiv;
        }
        while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=d_tete && i<idx);
        // Chainon pas trouve
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&i<idx)
            return;
    }
    // Suppression des chainons a partir de c
    Int i=0;
    do
    {
        i++;
        ChainonDC *precC=c->d_prec;
        ChainonDC *suivC=c->d_suiv;
        precC->d_suiv=suivC;
        suivC->d_prec=precC;
        {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
        c=suivC;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=c->d_suiv && c!=d_tete && i<nb);
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx==0)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&i<nb)
        {
            // Tous les chainons ont ete supprimes, on supprime le dernier chainon
            {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
            d_tete=nullptr;
        }
        else
            d_tete=c;
    }
}


void LDCCircCorr_Other::enleverPlusieursEltsFin(Int nb)
{
    // Liste vide ou rien a supprimer
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0 || d_tete==nullptr)
        return;
    // Liste avec un seul chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==d_tete->d_suiv)
    {
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
        d_tete=nullptr;
        return;
    }

    ChainonDC *c=d_tete->d_prec;
    Int i=0;
    do
    {
        i++;
        ChainonDC *precC=c->d_prec;
        ChainonDC *suivC=c->d_suiv;
        precC->d_suiv=suivC;
        suivC->d_prec=precC;
        {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
        c=precC;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=c->d_suiv && i<nb);

    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&i<nb)
    {
        // On est arrive a la fin de la liste, on supprime le dernier chainon
        {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
        d_tete=nullptr;
    }
}

bool LDCCircCorr_Other::eltsEnOrdreDecroissant() const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr || d_tete->d_suiv==d_tete)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    ChainonDC *c=d_tete;
    do
    {
        c=c->d_suiv;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=d_tete && c->d_prec->d_v > c->d_v);
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c==d_tete);}
}


bool LDCCircCorr_Other::contientElt(Double val) const
{
    // Liste vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    // Liste avec un seul chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete->d_suiv==d_tete)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_tete->d_v==val;}
    ChainonDC *c=d_tete;
    do
    {
        c=c->d_suiv;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=d_tete && c->d_v!=val);
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c->d_v==val);}
}


void LDCCircCorr_Other::enleverToutesLesOccurencesDUnElt(Double val)
{
    // Liste vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr)
        return;

    // Parcours de la liste
    ChainonDC *c=d_tete;
    ChainonDC *dernierC=d_tete->d_prec;
    do
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_v!=val)
            c=c->d_suiv;
        else
        {
            // Chainon trouve. On doit le supprimer
            ChainonDC *precC=c->d_prec;
            ChainonDC *suivC=c->d_suiv;
            precC->d_suiv=suivC;
            suivC->d_prec=precC;
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==d_tete)
            {
                if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==c->d_suiv)
                {
                    // Dernier chainon supprime
                    d_tete=nullptr;
                    suivC=nullptr;
                }
                else
                    // Tete supprime. La tete devient le chainon suivant
                    d_tete=suivC;
            }
            {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
            c=suivC;
        }
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&d_tete!=nullptr && c!=dernierC);

    // Traite le dernier chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c!=nullptr && c->d_v==val)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==c->d_suiv)
            d_tete=nullptr;
        else
        {
            ChainonDC *precC=c->d_prec;
            ChainonDC *suivC=c->d_suiv;
            precC->d_suiv=suivC;
            suivC->d_prec=precC;
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==c)
                d_tete=suivC;
            {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
        }
    }
}

Int LDCCircCorr_Other::chercherLIndexDeLaPremiereOccurenceDUnElt(Double val) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!d_tete)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return -1;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete->d_v==val)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return 0;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==d_tete->d_suiv)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return -1;}
    ChainonDC *c=d_tete;
    Int i=0;
    do
    {
        c=c->d_suiv;
        i++;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c->d_suiv!=d_tete && c->d_v!=val);

    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_v==val)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i;}
    else
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return -1;}
}

Int LDCCircCorr_Other::chercherLIndexDeLaDerniereOccurenceDUnElt(Double val) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!d_tete)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return -1;}
    Int idx=-1;
    // Valeur trouvee dans le chainon tete
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete->d_v==val)
        idx=0;
    // Liste avec un seul chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==d_tete->d_suiv)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return idx;}
    ChainonDC *c=d_tete;
    Int i=0;
    do
    {
        c=c->d_suiv;
        i++;
        // Recupere param'index du dernier chainon trouve
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_v==val)
            idx=i;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c->d_suiv!=d_tete);
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return idx;}
}

Int LDCCircCorr_Other::calculerNombreDOccurencesDUnElt(Double val) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!d_tete)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return 0;}
    Int nb=0;
    // Valeur trouvee dans le chainon tete
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete->d_v==val)
        nb++;
    // Liste avec un seul chainon
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==d_tete->d_suiv)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return nb;}
    ChainonDC *c=d_tete;
    do
    {
        c=c->d_suiv;
        // Recupere l'index du dernier chainon trouve
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_v==val)
            nb++;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c->d_suiv!=d_tete);
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return nb;}
}

