// DEBUT EXO_CONSTRUCTEUR
// Ecrire le constructeur pour la structure de données ci-dessous
// FIN EXO_CONSTRUCTEUR




// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
// Ecrire le constructeur par recopie pour la structure de données ci-dessous
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE




// DEBUT EXO_CONSTRUCTEUR_AVEC_TABLEAU
// Ecrire le constructeur qui prend comme paramètres un tableau avec sa
// taille: double *tab, int val. Ce constructeur doit créer une
// structure de données contenant tous les éléments dans le tableau
// passé en paramètre
// FIN EXO_CONSTRUCTEUR_AVEC_TABLEAU




// DEBUT EXO_DESTRUCTEUR
// Ecrire le destructeur pour la structure de données ci-dessous
// FIN EXO_DESTRUCTEUR




// DEBUT EXO_CONCATENATION_DEBUT
// Ecrire la méthode concatenationDebut(param) qui ajoute les elements de la
// structure de données param au début de la structure de données this. Cette
// structure de données param est passée en paramètre. L'ordre des éléments
// doit être gardé.
// FIN EXO_CONCATENATION_DEBUT




// DEBUT EXO_CONCATENATION_MILIEU
// Ecrire la méthode concatenationMilieu(int idx, param) qui insère les
// éléments de la structure de données param en paramètre. Ces éléments
// sont insérés à la position idx dans la stucture de données.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - L'index d'insertion peut être une valeur égale ou plus grande que la taille
//   de la structure de données. Dans ce cas, l'insertion se fait à la fin de la
//   structure de données.
// FIN EXO_CONCATENATION_MILIEU




// DEBUT EXO_CONCATENATION_FIN
// Ecrire la méthode concatenationFin(param) qui ajoute les elements de la
// structure de données param à la fin de la structure de données this. Cette
// structure de données param est passée en paramètre. L'ordre des éléments
// doit être gardé.
// FIN EXO_CONCATENATION_FIN




// DEBUT EXO_OP_CONCATENATION
// Ecrire la méthode operator+=(param) qui ajoute les elements de la structure de
// données param à la fin de la structure de données this. Cette structure de
// données param est passée en paramètre
// FIN EXO_OP_CONCATENATION




// DEBUT EXO_OP_EGAL
// Ecrire la méthode operator==(param) qui retourne true si la structure de
// données param passée en paramètre est égale à la structure de données this.
// FIN EXO_OP_EGAL




// DEBUT EXO_OP_DIFFERENT
// Ecrire la méthode operator!=(param) qui retourne true si la structure de
// données param passée en paramètre est différente à la structure de données this.
// FIN EXO_OP_DIFFERENT




// DEBUT EXO_OP_AFFECTATION
// Ecrire l'opérateur d'affectation operator=(param). Après cette méthode, la structure
// de données this doit contenir les mêmes éléments que la structure de données param
// passée en paramètre.
// FIN EXO_OP_AFFECTATION




// DEBUT EXO_OP_CROCHET
// Ecrire l'opérateur crochet [](idx). Cette méthode permet l'accès aux éléments en 
// écriture. Elle doit retourne la référence de l'élément d'index idx dans la structure de
// données. Le premier élément a pour index 0.
// FIN EXO_OP_CROCHET




// DEBUT EXO_GETTER
// Ecrire la méthode double get(int idx). Cette méthode doit retourne la valeur de 
// l'élément d'index idx dans la structure de données. Le premier élément a pour index 0. 
// Cette méthode ne modifie pas l'objet; elle doit être déclarée constante.
// FIN EXO_GETTER




// DEBUT EXO_SETTER
// Ecrire la méthode double set(int idx, double val). Cette méthode permet de mettre la
// valeur val à l'index idx dans la structure de données. Le premier élément a pour 
// index 0.
// FIN EXO_SETTER




// DEBUT EXO_OP_CROCHET_CONST
// Ecrire l'opérateur crochet [](idx) const. Cette méthode permet l'accès aux éléments en 
// lecture. Elle doit retourne la valeur de l'élément d'index idx dans la structure de
// données. Le premier élément a pour index 0. Cette méthode ne modifie pas l'objet; elle 
// doit être déclarée constante.
// FIN EXO_OP_CROCHET_CONST




// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
// Ecrire la méthode void additionnerATousLesElts(double val) qui ajoute la valeur
// val passée en paramètre à tous les éléments de la stucture de données.
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS




// DEBUT EXO_INSERER_UN_ELT_DEBUT
// Ecrire la méthode void insererUnEltDebut(double val) qui insère un élément
// contenant la valeur val passée en paramètre. Cet élément est inséré au
// début de la stucture de données.
// FIN EXO_INSERER_UN_ELT_DEBUT




// DEBUT EXO_INSERER_UN_ELT_MILIEU
// Ecrire la méthode void insererUnEltMilieu(int idx, double val) qui
// insère un élément contenant la valeur val passée en paramètre. Cet élément
// est inséré à la position idx dans la stucture de données.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - L'index d'insertion peut être une valeur égale ou plus grande que la taille
//   de la structure de données. Dans ce cas, l'insertion se fait à la fin de la
//   structure de données.
// FIN EXO_INSERER_UN_ELT_MILIEU




// DEBUT EXO_INSERER_UN_ELT_FIN
// Ecrire la méthode void insererUnEltFin(double val) qui insère un élément
// contenant la valeur val passée en paramètre. Cet élément est inséré à la
// fin de la stucture de données.
// Remarques:
// - La structure de données peut être vide
// FIN EXO_INSERER_UN_ELT_FIN




// DEBUT EXO_INSERER_PLUSIEURS_ELTS_DEBUT
// Ecrire la méthode void insererPlusieursEltsDebut(double *tab, int nb)
// qui insère nb éléments au début de la structures de données. Les éléments
// à inserer sont le tableau tab. L'ordre des éléments dans le tableau doit
// être gardé dans la structure de données.
// Remarques:
// - Le paramètre nb peut être égal à zéro
// - La structure de données peut être vide
// FIN EXO_INSERER_PLUSIEURS_ELTS_DEBUT




// DEBUT EXO_INSERER_PLUSIEURS_ELTS_MILIEU
// Ecrire la méthode void insererPlusieursEltsMilieu(int idx, double *tab, int nb)
// qui insère nb éléments à la position idx dans la structures de données. Les éléments
// à inserer sont le tableau tab. L'ordre des éléments dans le tableau doit
// être gardé dans la structure de données.
// Remarques:
// - Le paramètre nb peut être égal à 0.
// - L'index du premier élément de la structure de données est 0.
// - L'index d'insertion idx peut être une valeur égale ou plus grande que la taille
//   de la structure de données. Dans ce cas, l'insertion se fait à la fin de la
//   structure de données.
// FIN EXO_INSERER_PLUSIEURS_ELTS_MILIEU



// DEBUT EXO_INSERER_PLUSIEURS_ELTS_FIN
// Ecrire la méthode void insererPlusieursEltsFin(double *tab, int nb)
// qui insère nb éléments à la fin de la structures de données. Les éléments
// à inserer sont le tableau tab. L'ordre des éléments dans le tableau doit
// être gardé dans la structure de données.
// Remarques:
// - Le paramètre nb peut être égal à zéro
// - La structure de données peut être vide
// FIN EXO_INSERER_PLUSIEURS_ELTS_FIN




// DEBUT EXO_ENLEVER_UN_ELT_DEBUT
// Ecrire la méthode void enleverUnEltDebut() qui enlève le premier élément
// de la structure de données.
// Remarque:
// - La structure de données peut être vide
// FIN EXO_ENLEVER_UN_ELT_DEBUT




// DEBUT EXO_ENLEVER_UN_ELT_MILIEU
// Ecrire la méthode void enleverUnEltMilieu(int idx) qui enlève
// l'élément d'index idx dans la structure de données.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - La structure de données peut être vide.
// - La valeur idx peut être plus grande ou égale à la taille de la structure
//   de données. Dans ce cas, l'élément n'est pas supprimé.
// FIN EXO_ENLEVER_UN_ELT_MILIEU




// DEBUT EXO_ENLEVER_UN_ELT_FIN
// Ecrire la méthode void enleverUnEltFin() qui enlève le dernier élément
// de la structure de données.
// Remarque:
// - La structure de données peut être vide
// FIN EXO_ENLEVER_UN_ELT_FIN




// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
// Ecrire la méthode void enleverPlusieursEltsDebut(int nb) qui enlève nb
// éléments au début de la structures de données. Cette valeur nb est passée
// en paramètre.
// Remarque:
// - La valeur nb peut être plus grande que la taille de la structures de
// données. Dans ce cas, tous les éléments doivent être supprimés.
// - La valeur nb peut être égale à 0. Dans ce cas, rien n'est supprimé.
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT




// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU
// Ecrire la méthode void enleverPlusieursEltsMilieu(int idx, int nb) qui
// enlève nb éléments à partir de l'élément d'index idx. Ces valeurs idx et
// nb sont passées en paramètre.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - La valeur nb peut être plus grande que la taille de la structures de
// données.
// - La valeur idx peut être plus grande ou égale à la taille de la structure
// de données. Dans ce cas, les éléments ne sont pas supprimés.
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU




// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_FIN
// Ecrire la méthode void enleverPlusieursEltsFin(int nb) qui enlève nb
// éléments à la fin de la structures de données. Cette valeur nb est passée
// en paramètre.
// Remarque:
// - La valeur nb peut être plus grande que la taille de la structures de
// données.
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_FIN




// DEBUT EXO_ELTS_EN_ORDRE_DECROISSANT
// Ecrire la méthode eltsEnOrdreDecroissant() qui retourne true si les éléments
// dans la structures de données sont classés par ordre strictement décroissant.
// Remarque:
// - eltsEnOrdreDecroissant() doit retourner true pour une structure de données vide
// ou ne contenant qu'un seul élément.
// FIN EXO_ELTS_EN_ORDRE_DECROISSANT




// DEBUT EXO_CONTIENT_ELT
// Ecrire la méthode contientElt(double val) qui retourne true si la structure
// de données contient au moins un élément avec la valeur val, val étant un
// paramètre de la méthode.
// FIN EXO_CONTIENT_ELT




// DEBUT EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT
// Ecrire la méthode void enleverToutesLesOccurencesDUnElt(double val) qui
// enlève tous les éléments contenant la valeur val dans la structure de
// données, val étant un paramètre de la méthode.
// FIN EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT




// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT
// Ecrire la méthode int chercherLIndexDeLaPremiereOccurenceDUnElt(double val)
// qui retourne l'index du premier l'élément contenant la valeur val dans la
// structure de données, val étant un paramètre de la méthode.
// Remarques:
// - La structure de données ne contient pas nécessairement la valeur val. Dans ce
//   cas, la méthode doit retourner -1
// - La structure de données peut être vide
// FIN EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT




// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT
// Ecrire la méthode int chercherLIndexDeLaDerniereOccurenceDUnElt(double val)
// qui retourne l'index du dernier l'élément contenant la valeur val dans la
// structure de données, val étant un paramètre de la méthode.
// Remarques:
// - La structure de données ne contient pas nécessairement la valeur val. Dans ce
//   cas, la méthode doit retourner -1
// - La structure de données peut être vide
// FIN EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT




// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
// Ecrire la méthode int calculerNombreDOccurencesDUnElt(double val) qui
// retourne le nombre d'éléments contenant la valeur val dans la
// structure de données, val étant un paramètre de la méthode.
// Remarques:
// - La structure de données ne contient pas nécessairement la valeur val. Dans ce
//   cas, la méthode doit retourner 0
// - La structure de données peut être vide
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT




// DEBUT EXO_OP_ADDITION
// Ecrire la fonction operator+(param1,param2) qui construit une structure de
// données qui est le résultat de param1 + param2. Les variables param1 et param2
// sont passées en paramètre. Cette structure de données est ensuite retournée
// par la fonction.
// FIN EXO_OP_ADDITION




// DEBUT EXO_OP_SOUSTRACTION
// Ecrire la fonction operator-(param1,param2) qui construit une structure de
// données qui est le résultat de param1 - param2. Les variables param1 et param2
// sont passées en paramètre. Cette structure de données est ensuite retournée
// par la fonction.
// FIN EXO_OP_SOUSTRACTION

