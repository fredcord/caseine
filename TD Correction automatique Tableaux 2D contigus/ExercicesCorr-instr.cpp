////////////////////////////////////////////////////////////////////////////////////
// Fichier genere automatiquement pour l'instrumentation du code. Ne pas modifier //
////////////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include "ExercicesBase.h"

using namespace std;


/////////////////////////////////////////////////////////////////////////////
// Compare la structure de donnees de param'etudiant avec celle de la solution //
/////////////////////////////////////////////////////////////////////////////


// Verifie si la structure de donnees est correcte au niveau de sa sctructure et des poInteurs
bool Tableau2DContiguCorr_Other::verifierIntegrite(const Tableau2DContigu &param)
{
    // Verifie si les valeurs sont dans les bons intervals pour eviter les crash
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_nl<0)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_nc<0)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_nl==0 || param.d_nc==0)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_nl!=0 || param.d_nc!=0)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_v!=nullptr)
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(param.d_v))
                {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_tab!=nullptr)
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(param.d_tab))
                {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    }
    // Verifie l'allocation des pointeurs
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(param.d_v))
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(param.d_tab))
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    // Verifie la taille allouee
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&GestionPntrEtComplexite::get().pntrTailleAllouee(param.d_v)<param.d_nl*param.d_nc)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&GestionPntrEtComplexite::get().pntrTailleAllouee(param.d_tab)<param.d_nl)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    // Verifie la bonne initialisation de d_tab
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<param.d_nl;i++)
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_tab[i]!=param.d_v+i*param.d_nc)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
}

// Compare la correction avec la struct de param'etudiant. Retourne false si different
bool Tableau2DContiguCorr_Other::compareCorrAvecEtud(const Tableau2DContigu &student) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl!=student.d_nl)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc!=student.d_nc)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_nl*d_nc &&  d_v[i]==student.d_v[i])
        i++;
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i==d_nl*d_nc;}
}

// genere la structDeDonneesVerifCorr a partir de la liste
StructDeDonneesVerifCorr Tableau2DContiguCorr_Other::getStructDeDonneesVerifCorr() const
{
    StructDeDonneesVerifCorr vect(d_nl,d_nc);

    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            vect.set(i,j,d_tab[i][j]);
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return vect;}
}

// Constructeur avec la structDeDonneesVerifCorr
void Tableau2DContiguCorr_Other::setStructDeDonneesVerifCorr(const StructDeDonneesVerifCorr &tab)
{
    d_nl=tab.getNbLignes();
    d_nc=tab.getNbColonnes();

    d_v=new Double[d_nl*d_nc];
    d_tab=new Double*[d_nl];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
        d_tab[i]=d_v+i*d_nc;
    // On insere les elements du tableau
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            d_tab[i][j]=tab.get(i,j);
}

// DEBUT EXO_CONSTRUCTEUR
Tableau2DContiguCorr_ConstructDestruct::Tableau2DContiguCorr_ConstructDestruct():d_nl{0},d_nc{0},d_v{nullptr},d_tab{nullptr}
{
}
// FIN EXO_CONSTRUCTEUR


// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
Tableau2DContiguCorr_ConstructDestruct::Tableau2DContiguCorr_ConstructDestruct(const Tableau2DContiguCorr &param):d_nl{param.d_nl},d_nc{param.d_nc},
                                                         d_v{new Double[d_nl*d_nc]},
                                                         d_tab{new Double*[d_nl]}
{
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
        d_tab[i]=d_v+i*d_nc;
    // Copie le tableau
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl*d_nc;i++)
        d_v[i]=param.d_v[i];
}
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE


// DEBUT EXO_CONSTRUCTEUR_AVEC_TABLEAU
Tableau2DContiguCorr_ConstructDestruct::Tableau2DContiguCorr_ConstructDestruct(Double **tab, Int nl, Int nc):d_nl{nl},d_nc{nc},
                                                           d_v{new Double[d_nl*d_nc]},
                                                           d_tab{new Double*[d_nl]}
{
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
        d_tab[i]=d_v+i*d_nc;

    // Copie le tableau
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            d_tab[i][j]=tab[i][j];
}
// FIN EXO_CONSTRUCTEUR_AVEC_TABLEAU


// DEBUT DESTRUCTEUR
Tableau2DContiguCorr_ConstructDestruct::~Tableau2DContiguCorr_ConstructDestruct()
{
    {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
    {GestionPntrEtComplexite::get().peutSupprPntr(d_tab, true); delete[] d_tab;}
}
// FIN DESTRUCTEUR

// DEBUT EXO_CONCATENATION_DEBUT
void Tableau2DContiguCorr_Other::concatenationDebut(const Tableau2DContiguCorr &param)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_nc==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl!=0 && param.d_nl!=0)
        assert(d_nl==param.d_nl);
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl==0)
    {
         // Concatenation a un tableau vide
        d_nl=param.d_nl;
        d_tab=new Double*[d_nl];
    }
    // Allouer un nouveau tableau
    Double *new_d_v=new Double[d_nl*(d_nc+param.d_nc)];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<param.d_nc;j++)
            new_d_v[i*(d_nc+param.d_nc)+j]=param.d_tab[i][j];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            new_d_v[i*(d_nc+param.d_nc)+param.d_nc+j]=d_tab[i][j];
        d_tab[i]=new_d_v+i*(d_nc+param.d_nc);
    }
    {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
    d_v=new_d_v;
    d_nc+=param.d_nc;
}
// FIN EXO_CONCATENATION_DEBUT


// DEBUT EXO_CONCATENATION_MILIEU
void Tableau2DContiguCorr_Other::concatenationMilieu(Int idx, const Tableau2DContiguCorr &param)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_nc==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl!=0 && param.d_nl!=0)
        assert(d_nl==param.d_nl);
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl==0)
    {
         // Concatenation a un tableau vide
        d_nl=param.d_nl;
        d_tab=new Double*[d_nl];
    }
    // Allouer un nouveau tableau
    Double *new_d_v=new Double[d_nl*(d_nc+param.d_nc)];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<idx;j++)
            new_d_v[i*(d_nc+param.d_nc)+j]=d_tab[i][j];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<param.d_nc;j++)
            new_d_v[i*(d_nc+param.d_nc)+idx+j]=param.d_tab[i][j];
        for(Int j=idx;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            new_d_v[i*(d_nc+param.d_nc)+param.d_nc+j]=d_tab[i][j];
        d_tab[i]=new_d_v+i*(d_nc+param.d_nc);
    }
    {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
    d_v=new_d_v;
    d_nc+=param.d_nc;
}
// FIN EXO_CONCATENATION_MILIEU


// DEBUT EXO_CONCATENATION_FIN
void Tableau2DContiguCorr_Other::concatenationFin(const Tableau2DContiguCorr &param)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_nc==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl!=0 && param.d_nl!=0)
        assert(d_nl==param.d_nl);
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl==0)
    {
         // Concatenation a un tableau vide
        d_nl=param.d_nl;
        d_tab=new Double*[d_nl];
    }
    // Allouer un nouveau tableau
    Double *new_d_v=new Double[d_nl*(d_nc+param.d_nc)];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            new_d_v[i*(d_nc+param.d_nc)+j]=d_tab[i][j];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<param.d_nc;j++)
            new_d_v[i*(d_nc+param.d_nc)+d_nc+j]=param.d_tab[i][j];
        d_tab[i]=new_d_v+i*(d_nc+param.d_nc);
    }
    {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
    d_v=new_d_v;
    d_nc+=param.d_nc;
}
// FIN EXO_CONCATENATION_FIN

// DEBUT EXO_OP_CONCATENATION
Tableau2DContiguCorr &Tableau2DContiguCorr_Other::operator+=(const Tableau2DContiguCorr &param)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_nc==0)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl!=0 && param.d_nl!=0)
        assert(d_nl==param.d_nl);
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl==0)
    {
         // Concatenation a un tableau vide
        d_nl=param.d_nl;
        d_tab=new Double*[d_nl];
    }
    // Allouer un nouveau tableau
    Double *new_d_v=new Double[d_nl*(d_nc+param.d_nc)];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            new_d_v[i*(d_nc+param.d_nc)+j]=d_tab[i][j];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<param.d_nc;j++)
            new_d_v[i*(d_nc+param.d_nc)+d_nc+j]=param.d_tab[i][j];
        d_tab[i]=new_d_v+i*(d_nc+param.d_nc);
    }
    {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
    d_v=new_d_v;
    d_nc+=param.d_nc;

    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_CONCATENATION



// DEBUT EXO_OP_EGAL
bool Tableau2DContiguCorr_Other::operator==(const Tableau2DContiguCorr &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl!=param.d_nl || d_nc!=param.d_nc)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    Int i=0,j=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_nl &&  d_tab[i][j]==param.d_tab[i][j])
    {
        j++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&j==d_nc)
        {
            i++;
            j=0;
        }
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i==d_nl;}
}
// FIN EXO_OP_EGAL


// DEBUT EXO_OP_DIFFERENT
bool Tableau2DContiguCorr_Other::operator!=(const Tableau2DContiguCorr &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl!=param.d_nl || d_nc!=param.d_nc)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    Int i=0,j=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_nl &&  d_tab[i][j]==param.d_tab[i][j])
    {
        j++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&j==d_nc)
        {
            i++;
            j=0;
        }
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i!=d_nl;}
}
// FIN EXO_OP_DIFFERENT

// DEBUT EXO_OP_AFFECTATION
Tableau2DContiguCorr &Tableau2DContiguCorr_Other::operator=(const Tableau2DContiguCorr &param)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl*d_nc!=param.d_nl*param.d_nc)
    {
        {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
        d_v=new Double[param.d_nl*param.d_nc];
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl!=param.d_nl)
    {
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tab, true); delete[] d_tab;}
        d_tab=new Double*[param.d_nl];
    }
    d_nl=param.d_nl;
    d_nc=param.d_nc;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        d_tab[i]=d_v+i*d_nc;
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            d_tab[i][j]=param.d_tab[i][j];
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_AFFECTATION


// DEBUT EXO_OP_PARENTHESE
Double& Tableau2DContiguCorr_Other::operator()(Int i, Int j)
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_tab[i][j];}
}
// FIN EXO_OP_PARENTHESE


// DEBUT EXO_OP_PARENTHESE_CONST
Double Tableau2DContiguCorr_Other::operator()(Int i, Int j) const
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_tab[i][j];}
}
// FIN EXO_OP_PARENTHESE_CONST


// DEBUT EXO_SETTER
void Tableau2DContiguCorr_Other::set(Int i, Int j, Double val)
{
    d_tab[i][j]=val;
}
// FIN EXO_SETTER


// DEBUT EXO_GETTER
Double Tableau2DContiguCorr_Other::get(Int i, Int j) const
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_tab[i][j];}
}
// FIN EXO_GETTER


// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
void Tableau2DContiguCorr_Other::additionnerATousLesElts(Double val)
{
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            d_tab[i][j]+=val;
}
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS


// DEBUT EXO_INSERER_UN_ELT_DEBUT
void Tableau2DContiguCorr_Other::insererUnEltDebut(Double* tab, Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl==0)
    {
        // Concatenation a un tableau vide
        assert(d_nc==0);
        d_nl=nb;
        d_tab=new Double*[d_nl];
    }
    // Allouer un nouveau tableau
    Double *new_d_v=new Double[d_nl*(d_nc+1)];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        new_d_v[i*(d_nc+1)]=tab[i];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            new_d_v[i*(d_nc+1)+1+j]=d_tab[i][j];
        d_tab[i]=new_d_v+i*(d_nc+1);
    }
    {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
    d_v=new_d_v;
    d_nc+=1;
}
// FIN EXO_INSERER_UN_ELT_DEBUT


// DEBUT EXO_INSERER_UN_ELT_MILIEU
void Tableau2DContiguCorr_Other::insererUnEltMilieu(Int idx, Double* tab, Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl==0)
    {
        // Concatenation a un tableau vide
        assert(d_nc==0);
        d_nl=nb;
        d_tab=new Double*[d_nl];
    }
    // Allouer un nouveau tableau
    Double *new_d_v=new Double[d_nl*(d_nc+1)];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<idx;j++)
            new_d_v[i*(d_nc+1)+j]=d_tab[i][j];
        new_d_v[i*(d_nc+1)+idx]=tab[i];
        for(Int j=idx;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            new_d_v[i*(d_nc+1)+1+j]=d_tab[i][j];
        d_tab[i]=new_d_v+i*(d_nc+1);
    }
    {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
    d_v=new_d_v;
    d_nc+=1;
}
// FIN EXO_INSERER_UN_ELT_MILIEU


// DEBUT EXO_INSERER_UN_ELT_FIN
void Tableau2DContiguCorr_Other::insererUnEltFin(Double* tab, Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl==0)
    {
        // Concatenation a un tableau vide
        assert(d_nc==0);
        d_nl=nb;
        d_tab=new Double*[d_nl];
    }
    // Allouer un nouveau tableau
    Double *new_d_v=new Double[d_nl*(d_nc+1)];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            new_d_v[i*(d_nc+1)+j]=d_tab[i][j];
        new_d_v[i*(d_nc+1)+d_nc]=tab[i];
        d_tab[i]=new_d_v+i*(d_nc+1);
    }
    {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
    d_v=new_d_v;
    d_nc+=1;
}
// FIN EXO_INSERER_UN_ELT_FIN


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_DEBUT
void Tableau2DContiguCorr_Other::insererPlusieursEltsDebut(Double **tab, Int nbl, Int nbc)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nbc==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl==0)
    {
         // Concatenation a un tableau vide
        d_nl=nbl;
        d_tab=new Double*[d_nl];
    }
    // Allouer un nouveau tableau
    Double *new_d_v=new Double[d_nl*(d_nc+nbc)];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<nbc;j++)
            new_d_v[i*(d_nc+nbc)+j]=tab[i][j];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            new_d_v[i*(d_nc+nbc)+nbc+j]=d_tab[i][j];
        d_tab[i]=new_d_v+i*(d_nc+nbc);
    }
    {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
    d_v=new_d_v;
    d_nc+=nbc;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_MILIEU
void Tableau2DContiguCorr_Other::insererPlusieursEltsMilieu(Int idx, Double **tab, Int nbl, Int nbc)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nbc==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl!=0 && nbl!=0)
        assert(d_nl==nbl);
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl==0)
    {
         // Concatenation a un tableau vide
        d_nl=nbl;
        d_tab=new Double*[d_nl];
    }
    // Allouer un nouveau tableau
    Double *new_d_v=new Double[d_nl*(d_nc+nbc)];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<idx;j++)
            new_d_v[i*(d_nc+nbc)+j]=d_tab[i][j];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<nbc;j++)
            new_d_v[i*(d_nc+nbc)+idx+j]=tab[i][j];
        for(Int j=idx;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            new_d_v[i*(d_nc+nbc)+nbc+j]=d_tab[i][j];
        d_tab[i]=new_d_v+i*(d_nc+nbc);
    }
    {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
    d_v=new_d_v;
    d_nc+=nbc;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_FIN
void Tableau2DContiguCorr_Other::insererPlusieursEltsFin(Double **tab, Int nbl, Int nbc)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nbc==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl!=0 && nbl!=0)
        assert(d_nl==nbl);
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl==0)
    {
         // Concatenation a un tableau vide
        d_nl=nbl;
        d_tab=new Double*[d_nl];
    }
    // Allouer un nouveau tableau
    Double *new_d_v=new Double[d_nl*(d_nc+nbc)];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            new_d_v[i*(d_nc+nbc)+j]=d_tab[i][j];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<nbc;j++)
            new_d_v[i*(d_nc+nbc)+d_nc+j]=tab[i][j];
        d_tab[i]=new_d_v+i*(d_nc+nbc);
    }
    {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
    d_v=new_d_v;
    d_nc+=nbc;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ENLEVER_UN_ELT_DEBUT
void Tableau2DContiguCorr_Other::enleverUnEltDebut()
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc==1)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tab, true); delete[] d_tab;}
        d_tab=nullptr;
        {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
        d_v=nullptr;
        return;
    }

    Double *new_d_v=new Double[d_nl*(d_nc-1)];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc-1;j++)
            new_d_v[i*(d_nc-1)+j]=d_tab[i][j+1];
        d_tab[i]=new_d_v+i*(d_nc-1);
    }
    d_nc--;
    {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
    d_v=new_d_v;
}
// FIN EXO_ENLEVER_UN_ELT_DEBUT


// DEBUT EXO_ENLEVER_UN_ELT_MILIEU
void Tableau2DContiguCorr_Other::enleverUnEltMilieu(Int idx)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx>=d_nc)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc==1)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tab, true); delete[] d_tab;}
        d_tab=nullptr;
        {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
        d_v=nullptr;
        return;
    }

    Double *new_d_v=new Double[d_nl*(d_nc-1)];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<idx;j++)
            new_d_v[i*(d_nc-1)+j]=d_tab[i][j];
        for(Int j=idx;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc-1;j++)
            new_d_v[i*(d_nc-1)+j]=d_tab[i][j+1];
        d_tab[i]=new_d_v+i*(d_nc-1);
    }
    d_nc--;
    {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
    d_v=new_d_v;
}
// FIN EXO_ENLEVER_UN_ELT_MILIEU


// DEBUT EXO_ENLEVER_UN_ELT_FIN
void Tableau2DContiguCorr_Other::enleverUnEltFin()
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc==1)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tab, true); delete[] d_tab;}
        d_tab=nullptr;
        {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
        d_v=nullptr;
        return;
    }

    Double *new_d_v=new Double[d_nl*(d_nc-1)];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc-1;j++)
            new_d_v[i*(d_nc-1)+j]=d_tab[i][j];
        d_tab[i]=new_d_v+i*(d_nc-1);
    }
    d_nc--;
    {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
    d_v=new_d_v;
}
// FIN EXO_ENLEVER_UN_ELT_FIN


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
void Tableau2DContiguCorr_Other::enleverPlusieursEltsDebut(Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc<=nb)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tab, true); delete[] d_tab;}
        d_tab=nullptr;
        {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
        d_v=nullptr;
        return;
    }

    Double *new_d_v=new Double[d_nl*(d_nc-nb)];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc-nb;j++)
            new_d_v[i*(d_nc-nb)+j]=d_tab[i][j+nb];
        d_tab[i]=new_d_v+i*(d_nc-nb);
    }
    d_nc-=nb;
    {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
    d_v=new_d_v;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU
void Tableau2DContiguCorr_Other::enleverPlusieursEltsMilieu(Int idx, Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx>=d_nc)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx+nb>d_nc)
        nb=d_nc-idx;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx==0 && d_nc<=nb)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tab, true); delete[] d_tab;}
        d_tab=nullptr;
        {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
        d_v=nullptr;
        return;
    }

    Double *new_d_v=new Double[d_nl*(d_nc-nb)];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<idx;j++)
            new_d_v[i*(d_nc-nb)+j]=d_tab[i][j];
        for(Int j=idx;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc-nb;j++)
            new_d_v[i*(d_nc-nb)+j]=d_tab[i][j+nb];
        d_tab[i]=new_d_v+i*(d_nc-nb);
    }
    d_nc-=nb;
    {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
    d_v=new_d_v;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_FIN
void Tableau2DContiguCorr_Other::enleverPlusieursEltsFin(Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc<=nb)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tab, true); delete[] d_tab;}
        d_tab=nullptr;
        {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
        d_v=nullptr;
        return;
    }

    Double *new_d_v=new Double[d_nl*(d_nc-nb)];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc-nb;j++)
            new_d_v[i*(d_nc-nb)+j]=d_tab[i][j];
        d_tab[i]=new_d_v+i*(d_nc-nb);
    }
    d_nc-=nb;
    {GestionPntrEtComplexite::get().peutSupprPntr(d_v, true); delete[] d_v;}
    d_v=new_d_v;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_FIN



// DEBUT EXO_ELTS_EN_ORDRE_DECROISSANT
bool Tableau2DContiguCorr_Other::eltsEnOrdreDecroissant() const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl*d_nc==0 || d_nl*d_nc==1)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    Int i=0,j=0;
    Int nextI=i,nextJ=j+1;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nextJ==d_nc)
    {
        nextI++;
        nextJ=0;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&nextI<d_nl && d_tab[i][j]>d_tab[nextI][nextJ])
    {
        i=nextI;
        j=nextJ;
        nextJ++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nextJ==d_nc)
        {
            nextI++;
            nextJ=0;
        }
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return nextI==d_nl;}
}
// FIN EXO_ELTS_EN_ORDRE_DECROISSANT


// DEBUT EXO_CONTIENT_ELT
bool Tableau2DContiguCorr_Other::contientElt(Double val) const
{
    Int i=0,j=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_nl && d_tab[i][j]!=val)
    {
        j++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&j==d_nc)
        {
            j=0;
            i++;
        }
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i<d_nl;}
}
// FIN EXO_CONTIENT_ELT


// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
Int Tableau2DContiguCorr_Other::calculerNombreDOccurencesDUnElt(Double val) const
{
    Int nbOccurences=0;
    Int i=0,j=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_nl)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tab[i][j]==val)
            nbOccurences++;
        j++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&j==d_nc)
        {
            j=0;
            i++;
        }
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return nbOccurences;}
}
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT


// DEBUT EXO_OP_ADDITION
Tableau2DContiguCorr operator_plus(const Tableau2DContiguCorr& a, const Tableau2DContiguCorr& b)
{
    Tableau2DContiguCorr res(a);
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<res.d_nl;i++)
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<res.d_nc;j++)
            res.d_tab[i][j]+=b.d_tab[i][j];
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return res;}
}
// FIN EXO_OP_ADDITION


// DEBUT EXO_OP_SOUSTRACTION
Tableau2DContiguCorr operator_moins(const Tableau2DContiguCorr& a, const Tableau2DContiguCorr& b)
{
    Tableau2DContiguCorr res(a);
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<res.d_nl;i++)
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<res.d_nc;j++)
            res.d_tab[i][j]-=b.d_tab[i][j];
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return res;}
}
// FIN EXO_OP_SOUSTRACTION

