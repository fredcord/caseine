#include "ExercicesBase.h"


// DEBUT EXO_CONSTRUCTEUR
Tableau2DContigu::Tableau2DContigu():d_nl{0},d_nc{0},d_v{nullptr},d_tab{nullptr}
{
}
// FIN EXO_CONSTRUCTEUR


// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
Tableau2DContigu::Tableau2DContigu(const Tableau2DContigu &param):d_nl{param.d_nl},d_nc{param.d_nc},
                                             d_v{new double[d_nl*d_nc]},
                                             d_tab{new double*[d_nl]}
{
    for (int i=0;i<d_nl;i++)
        d_tab[i]=d_v+i*d_nc;
    // Copie le tableau
    for (int i=0;i<d_nl*d_nc;i++)
        d_v[i]=param.d_v[i];
}
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE



// DEBUT EXO_CONSTRUCTEUR_AVEC_TABLEAU
Tableau2DContigu::Tableau2DContigu(double **tab, int nl, int nc):d_nl{nl},d_nc{nc},
                                                   d_v{new double[d_nl*d_nc]},
                                                   d_tab{new double*[d_nl]}
{
    for (int i=0;i<d_nl;i++)
        d_tab[i]=d_v+i*d_nc;
    // Copie le tableau
    for (int i=0;i<d_nl;i++)
        for (int j=0;j<d_nc;j++)
            d_tab[i][j]=tab[i][j];
}
// FIN EXO_CONSTRUCTEUR_AVEC_TABLEAU


// DEBUT EXO_DESTRUCTEUR
Tableau2DContigu::~Tableau2DContigu()
{
    delete[] d_v;
    delete[] d_tab;
}
// FIN EXO_DESTRUCTEUR


// DEBUT EXO_CONCATENATION_DEBUT
void Tableau2DContigu::concatenationDebut(const Tableau2DContigu &param)
{
    if (param.d_nc==0)
        return;
    if (d_nl!=0 && param.d_nl!=0)
        assert(d_nl==param.d_nl);
    if (d_nl==0)
    {
         // Concatenation a un tableau vide
        d_nl=param.d_nl;
        d_tab=new double*[d_nl];
    }
    // Allouer un nouveau tableau
    double *new_d_v=new double[d_nl*(d_nc+param.d_nc)];
    for (int i=0;i<d_nl;i++)
    {
        for (int j=0;j<param.d_nc;j++)
            new_d_v[i*(d_nc+param.d_nc)+j]=param.d_tab[i][j];
        for (int j=0;j<d_nc;j++)
            new_d_v[i*(d_nc+param.d_nc)+param.d_nc+j]=d_tab[i][j];
        d_tab[i]=new_d_v+i*(d_nc+param.d_nc);
    }
    delete[] d_v;
    d_v=new_d_v;
    d_nc+=param.d_nc;
}
// FIN EXO_CONCATENATION_DEBUT


// DEBUT EXO_CONCATENATION_MILIEU
void Tableau2DContigu::concatenationMilieu(int idx, const Tableau2DContigu &param)
{
    if (param.d_nc==0)
        return;
    if (d_nl!=0 && param.d_nl!=0)
        assert(d_nl==param.d_nl);
    if (d_nl==0)
    {
         // Concatenation a un tableau vide
        d_nl=param.d_nl;
        d_tab=new double*[d_nl];
    }
    // Allouer un nouveau tableau
    double *new_d_v=new double[d_nl*(d_nc+param.d_nc)];
    for (int i=0;i<d_nl;i++)
    {
        for (int j=0;j<idx;j++)
            new_d_v[i*(d_nc+param.d_nc)+j]=d_tab[i][j];
        for (int j=0;j<param.d_nc;j++)
            new_d_v[i*(d_nc+param.d_nc)+idx+j]=param.d_tab[i][j];
        for (int j=idx;j<d_nc;j++)
            new_d_v[i*(d_nc+param.d_nc)+param.d_nc+j]=d_tab[i][j];
        d_tab[i]=new_d_v+i*(d_nc+param.d_nc);
    }
    delete[] d_v;
    d_v=new_d_v;
    d_nc+=param.d_nc;
}
// FIN EXO_CONCATENATION_MILIEU


// DEBUT EXO_CONCATENATION_FIN
void Tableau2DContigu::concatenationFin(const Tableau2DContigu &param)
{
    if (param.d_nc==0)
        return;
    if (d_nl!=0 && param.d_nl!=0)
        assert(d_nl==param.d_nl);
    if (d_nl==0)
    {
         // Concatenation a un tableau vide
        d_nl=param.d_nl;
        d_tab=new double*[d_nl];
    }
    // Allouer un nouveau tableau
    double *new_d_v=new double[d_nl*(d_nc+param.d_nc)];
    for (int i=0;i<d_nl;i++)
    {
        for (int j=0;j<d_nc;j++)
            new_d_v[i*(d_nc+param.d_nc)+j]=d_tab[i][j];
        for (int j=0;j<param.d_nc;j++)
            new_d_v[i*(d_nc+param.d_nc)+d_nc+j]=param.d_tab[i][j];
        d_tab[i]=new_d_v+i*(d_nc+param.d_nc);
    }
    delete[] d_v;
    d_v=new_d_v;
    d_nc+=param.d_nc;
}
// FIN EXO_CONCATENATION_FIN


// DEBUT EXO_OP_CONCATENATION
Tableau2DContigu &Tableau2DContigu::operator+=(const Tableau2DContigu &param)
{
    if (param.d_nc==0)
        return *this;
    if (d_nl!=0 && param.d_nl!=0)
        assert(d_nl==param.d_nl);
    if (d_nl==0)
    {
         // Concatenation a un tableau vide
        d_nl=param.d_nl;
        d_tab=new double*[d_nl];
    }
    // Allouer un nouveau tableau
    double *new_d_v=new double[d_nl*(d_nc+param.d_nc)];
    for (int i=0;i<d_nl;i++)
    {
        for (int j=0;j<d_nc;j++)
            new_d_v[i*(d_nc+param.d_nc)+j]=d_tab[i][j];
        for (int j=0;j<param.d_nc;j++)
            new_d_v[i*(d_nc+param.d_nc)+d_nc+j]=param.d_tab[i][j];
        d_tab[i]=new_d_v+i*(d_nc+param.d_nc);
    }
    delete[] d_v;
    d_v=new_d_v;
    d_nc+=param.d_nc;

    return *this;
}
// FIN EXO_OP_CONCATENATION


// DEBUT EXO_OP_EGAL
bool Tableau2DContigu::operator==(const Tableau2DContigu &param) const
{
    if (this==&param)
        return true;
    if (d_nl!=param.d_nl || d_nc!=param.d_nc)
        return false;
    int i=0,j=0;
    while(i<d_nl &&  d_tab[i][j]==param.d_tab[i][j])
    {
        j++;
        if (j==d_nc)
        {
            i++;
            j=0;
        }
    }
    return i==d_nl;
}
// FIN EXO_OP_EGAL


// DEBUT EXO_OP_DIFFERENT
bool Tableau2DContigu::operator!=(const Tableau2DContigu &param) const
{
    if (this==&param)
        return false;
    if (d_nl!=param.d_nl || d_nc!=param.d_nc)
        return true;
    int i=0,j=0;
    while(i<d_nl &&  d_tab[i][j]==param.d_tab[i][j])
    {
        j++;
        if (j==d_nc)
        {
            i++;
            j=0;
        }
    }
    return i!=d_nl;
}
// FIN EXO_OP_DIFFERENT


// DEBUT EXO_OP_AFFECTATION
Tableau2DContigu &Tableau2DContigu::operator=(const Tableau2DContigu &param)
{
    if (this==&param)
        return *this;
    if (d_nl*d_nc!=param.d_nl*param.d_nc)
    {
        delete[] d_v;
        d_v=new double[param.d_nl*param.d_nc];
    }
    if (d_nl!=param.d_nl)
    {
        delete[] d_tab;
        d_tab=new double*[param.d_nl];
    }
    d_nl=param.d_nl;
    d_nc=param.d_nc;
    for (int i=0;i<d_nl;i++)
    {
        d_tab[i]=d_v+i*d_nc;
        for (int j=0;j<d_nc;j++)
            d_tab[i][j]=param.d_tab[i][j];
    }
    return *this;
}
// FIN EXO_OP_AFFECTATION


// DEBUT EXO_OP_PARENTHESE
double& Tableau2DContigu::operator()(int i, int j)
{
    return d_tab[i][j];
}
// FIN EXO_OP_PARENTHESE


// DEBUT EXO_OP_PARENTHESE_CONST
double Tableau2DContigu::operator()(int i, int j) const
{
    return d_tab[i][j];
}
// FIN EXO_OP_PARENTHESE_CONST


// DEBUT EXO_SETTER
void Tableau2DContigu::set(int i, int j, double val)
{
    d_tab[i][j]=val;
}
// FIN EXO_SETTER


// DEBUT EXO_GETTER
double Tableau2DContigu::get(int i, int j) const
{
    return d_tab[i][j];
}
// FIN EXO_GETTER


// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
void Tableau2DContigu::additionnerATousLesElts(double val)
{
    for (int i=0;i<d_nl;i++)
        for (int j=0;j<d_nc;j++)
            d_tab[i][j]+=val;
}
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS


// DEBUT EXO_INSERER_UN_ELT_DEBUT
void Tableau2DContigu::insererUnEltDebut(double* tab, int nb)
{
    if (nb==0)
        return;
    if (d_nl==0)
    {
        // Concatenation a un tableau vide
        assert(d_nc==0);
        d_nl=nb;
        d_tab=new double*[d_nl];
    }
    // Allouer un nouveau tableau
    double *new_d_v=new double[d_nl*(d_nc+1)];
    for (int i=0;i<d_nl;i++)
    {
        new_d_v[i*(d_nc+1)]=tab[i];
        for (int j=0;j<d_nc;j++)
            new_d_v[i*(d_nc+1)+1+j]=d_tab[i][j];
        d_tab[i]=new_d_v+i*(d_nc+1);
    }
    delete[] d_v;
    d_v=new_d_v;
    d_nc+=1;
}
// FIN EXO_INSERER_UN_ELT_DEBUT


// DEBUT EXO_INSERER_UN_ELT_MILIEU
void Tableau2DContigu::insererUnEltMilieu(int idx, double* tab, int nb)
{
    if (nb==0)
        return;
    if (d_nl==0)
    {
        // Concatenation a un tableau vide
        assert(d_nc==0);
        d_nl=nb;
        d_tab=new double*[d_nl];
    }
    // Allouer un nouveau tableau
    double *new_d_v=new double[d_nl*(d_nc+1)];
    for (int i=0;i<d_nl;i++)
    {
        for (int j=0;j<idx;j++)
            new_d_v[i*(d_nc+1)+j]=d_tab[i][j];
        new_d_v[i*(d_nc+1)+idx]=tab[i];
        for (int j=idx;j<d_nc;j++)
            new_d_v[i*(d_nc+1)+1+j]=d_tab[i][j];
        d_tab[i]=new_d_v+i*(d_nc+1);
    }
    delete[] d_v;
    d_v=new_d_v;
    d_nc+=1;

}
// FIN EXO_INSERER_UN_ELT_MILIEU


// DEBUT EXO_INSERER_UN_ELT_FIN
void Tableau2DContigu::insererUnEltFin(double* tab, int nb)
{
    if (nb==0)
        return;
    if (d_nl==0)
    {
        // Concatenation a un tableau vide
        assert(d_nc==0);
        d_nl=nb;
        d_tab=new double*[d_nl];
    }
    // Allouer un nouveau tableau
    double *new_d_v=new double[d_nl*(d_nc+1)];
    for (int i=0;i<d_nl;i++)
    {
        for (int j=0;j<d_nc;j++)
            new_d_v[i*(d_nc+1)+j]=d_tab[i][j];
        new_d_v[i*(d_nc+1)+d_nc]=tab[i];
        d_tab[i]=new_d_v+i*(d_nc+1);
    }
    delete[] d_v;
    d_v=new_d_v;
    d_nc+=1;
}
// FIN EXO_INSERER_UN_ELT_FIN


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_DEBUT
void Tableau2DContigu::insererPlusieursEltsDebut(double **tab, int nbl, int nbc)
{
    if (nbc==0)
        return;
    if (d_nl==0)
    {
         // Concatenation a un tableau vide
        d_nl=nbl;
        d_tab=new double*[d_nl];
    }
    // Allouer un nouveau tableau
    double *new_d_v=new double[d_nl*(d_nc+nbc)];
    for (int i=0;i<d_nl;i++)
    {
        for (int j=0;j<nbc;j++)
            new_d_v[i*(d_nc+nbc)+j]=tab[i][j];
        for (int j=0;j<d_nc;j++)
            new_d_v[i*(d_nc+nbc)+nbc+j]=d_tab[i][j];
        d_tab[i]=new_d_v+i*(d_nc+nbc);
    }
    delete[] d_v;
    d_v=new_d_v;
    d_nc+=nbc;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_MILIEU
void Tableau2DContigu::insererPlusieursEltsMilieu(int idx, double **tab, int nbl, int nbc)
{
    if (nbc==0)
        return;
    if (d_nl!=0 && nbl!=0)
        assert(d_nl==nbl);
    if (d_nl==0)
    {
         // Concatenation a un tableau vide
        d_nl=nbl;
        d_tab=new double*[d_nl];
    }
    // Allouer un nouveau tableau
    double *new_d_v=new double[d_nl*(d_nc+nbc)];
    for (int i=0;i<d_nl;i++)
    {
        for (int j=0;j<idx;j++)
            new_d_v[i*(d_nc+nbc)+j]=d_tab[i][j];
        for (int j=0;j<nbc;j++)
            new_d_v[i*(d_nc+nbc)+idx+j]=tab[i][j];
        for (int j=idx;j<d_nc;j++)
            new_d_v[i*(d_nc+nbc)+nbc+j]=d_tab[i][j];
        d_tab[i]=new_d_v+i*(d_nc+nbc);
    }
    delete[] d_v;
    d_v=new_d_v;
    d_nc+=nbc;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_FIN
void Tableau2DContigu::insererPlusieursEltsFin(double **tab, int nbl, int nbc)
{
    if (nbc==0)
        return;
    if (d_nl==0)
    {
         // Concatenation a un tableau vide
        d_nl=nbl;
        d_tab=new double*[d_nl];
    }
    // Allouer un nouveau tableau
    double *new_d_v=new double[d_nl*(d_nc+nbc)];
    for (int i=0;i<d_nl;i++)
    {
        for (int j=0;j<d_nc;j++)
            new_d_v[i*(d_nc+nbc)+j]=d_tab[i][j];
        for (int j=0;j<nbc;j++)
            new_d_v[i*(d_nc+nbc)+d_nc+j]=tab[i][j];
        d_tab[i]=new_d_v+i*(d_nc+nbc);
    }
    delete[] d_v;
    d_v=new_d_v;
    d_nc+=nbc;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ENLEVER_UN_ELT_DEBUT
void Tableau2DContigu::enleverUnEltDebut()
{
    if (d_nc==0)
        return;
    if (d_nc==1)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        delete[] d_tab;
        d_tab=nullptr;
        delete[] d_v;
        d_v=nullptr;
        return;
    }

    double *new_d_v=new double[d_nl*(d_nc-1)];
    for (int i=0;i<d_nl;i++)
    {
        for (int j=0;j<d_nc-1;j++)
            new_d_v[i*(d_nc-1)+j]=d_tab[i][j+1];
        d_tab[i]=new_d_v+i*(d_nc-1);
    }
    d_nc--;
    delete[] d_v;
    d_v=new_d_v;
}
// FIN EXO_ENLEVER_UN_ELT_DEBUT


// DEBUT EXO_ENLEVER_UN_ELT_MILIEU
void Tableau2DContigu::enleverUnEltMilieu(int idx)
{
    if (d_nc==0)
        return;
    if (idx>=d_nc)
        return;
    if (d_nc==1)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        delete[] d_tab;
        d_tab=nullptr;
        delete[] d_v;
        d_v=nullptr;
        return;
    }

    double *new_d_v=new double[d_nl*(d_nc-1)];
    for (int i=0;i<d_nl;i++)
    {
        for (int j=0;j<idx;j++)
            new_d_v[i*(d_nc-1)+j]=d_tab[i][j];
        for (int j=idx;j<d_nc-1;j++)
            new_d_v[i*(d_nc-1)+j]=d_tab[i][j+1];
        d_tab[i]=new_d_v+i*(d_nc-1);
    }
    d_nc--;
    delete[] d_v;
    d_v=new_d_v;
}
// FIN EXO_ENLEVER_UN_ELT_MILIEU


// DEBUT EXO_ENLEVER_UN_ELT_FIN
void Tableau2DContigu::enleverUnEltFin()
{
    if (d_nc==0)
        return;
    if (d_nc==1)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        delete[] d_tab;
        d_tab=nullptr;
        delete[] d_v;
        d_v=nullptr;
        return;
    }

    double *new_d_v=new double[d_nl*(d_nc-1)];
    for (int i=0;i<d_nl;i++)
    {
        for (int j=0;j<d_nc-1;j++)
            new_d_v[i*(d_nc-1)+j]=d_tab[i][j];
        d_tab[i]=new_d_v+i*(d_nc-1);
    }
    d_nc--;
    delete[] d_v;
    d_v=new_d_v;
}
// FIN EXO_ENLEVER_UN_ELT_FIN


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
void Tableau2DContigu::enleverPlusieursEltsDebut(int nb)
{
    if (d_nc==0)
        return;
    if (d_nc<=nb)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        delete[] d_tab;
        d_tab=nullptr;
        delete[] d_v;
        d_v=nullptr;
        return;
    }

    double *new_d_v=new double[d_nl*(d_nc-nb)];
    for (int i=0;i<d_nl;i++)
    {
        for (int j=0;j<d_nc-nb;j++)
            new_d_v[i*(d_nc-nb)+j]=d_tab[i][j+nb];
        d_tab[i]=new_d_v+i*(d_nc-nb);
    }
    d_nc-=nb;
    delete[] d_v;
    d_v=new_d_v;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT



// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU
void Tableau2DContigu::enleverPlusieursEltsMilieu(int idx, int nb)
{
    if (d_nc==0)
        return;
    if (idx>=d_nc)
        return;
    if (idx+nb>d_nc)
        nb=d_nc-idx;
    if (idx==0 && d_nc<=nb)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        delete[] d_tab;
        d_tab=nullptr;
        delete[] d_v;
        d_v=nullptr;
        return;
    }

    double *new_d_v=new double[d_nl*(d_nc-nb)];
    for (int i=0;i<d_nl;i++)
    {
        for (int j=0;j<idx;j++)
            new_d_v[i*(d_nc-nb)+j]=d_tab[i][j];
        for (int j=idx;j<d_nc-nb;j++)
            new_d_v[i*(d_nc-nb)+j]=d_tab[i][j+nb];
        d_tab[i]=new_d_v+i*(d_nc-nb);
    }
    d_nc-=nb;
    delete[] d_v;
    d_v=new_d_v;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU



// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_FIN
void Tableau2DContigu::enleverPlusieursEltsFin(int nb)
{
    if (d_nc==0)
        return;
    if (d_nc<=nb)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        delete[] d_tab;
        d_tab=nullptr;
        delete[] d_v;
        d_v=nullptr;
        return;
    }

    double *new_d_v=new double[d_nl*(d_nc-nb)];
    for (int i=0;i<d_nl;i++)
    {
        for (int j=0;j<d_nc-nb;j++)
            new_d_v[i*(d_nc-nb)+j]=d_tab[i][j];
        d_tab[i]=new_d_v+i*(d_nc-nb);
    }
    d_nc-=nb;
    delete[] d_v;
    d_v=new_d_v;}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ELTS_EN_ORDRE_DECROISSANT
bool Tableau2DContigu::eltsEnOrdreDecroissant() const
{
    if (d_nl*d_nc==0 || d_nl*d_nc==1)
        return true;
    int i=0,j=0;
    int nextI=i,nextJ=j+1;
    if (nextJ==d_nc)
    {
        nextI++;
        nextJ=0;
    }
    while(nextI<d_nl && d_tab[i][j]>d_tab[nextI][nextJ])
    {
        i=nextI;
        j=nextJ;
        nextJ++;
        if (nextJ==d_nc)
        {
            nextI++;
            nextJ=0;
        }
    }
    return nextI==d_nl;
}
// FIN EXO_ELTS_EN_ORDRE_DECROISSANT


// DEBUT EXO_CONTIENT_ELT
bool Tableau2DContigu::contientElt(double val) const
{
    int i=0,j=0;
    while(i<d_nl && d_tab[i][j]!=val)
    {
        j++;
        if (j==d_nc)
        {
            j=0;
            i++;
        }
    }
    return i<d_nl;
}
// FIN EXO_CONTIENT_ELT


// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
int Tableau2DContigu::calculerNombreDOccurencesDUnElt(double val) const
{
    int nbOccurences=0;
    int i=0,j=0;
    while(i<d_nl)
    {
        if (d_tab[i][j]==val)
            nbOccurences++;
        j++;
        if (j==d_nc)
        {
            j=0;
            i++;
        }
    }
    return nbOccurences;
}
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT


// DEBUT EXO_OP_ADDITION
Tableau2DContigu operator+(const Tableau2DContigu& a, const Tableau2DContigu& b)
{
    Tableau2DContigu res(a);
    for (int i=0;i<res.d_nl;i++)
        for (int j=0;j<res.d_nc;j++)
            res.d_tab[i][j]+=b.d_tab[i][j];
    return res;
}
// FIN EXO_OP_ADDITION


// DEBUT EXO_OP_SOUSTRACTION
Tableau2DContigu operator-(const Tableau2DContigu& a, const Tableau2DContigu& b)
{
    Tableau2DContigu res(a);
    for (int i=0;i<res.d_nl;i++)
        for (int j=0;j<res.d_nc;j++)
            res.d_tab[i][j]-=b.d_tab[i][j];
    return res;
}
// FIN EXO_OP_SOUSTRACTION


