#include "Exercices.h"



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le constructeur pour la structure de données ci-dessous
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
TableauPseudoDynamique::TableauPseudoDynamique():d_n{0}
{
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le destructeur pour la structure de données ci-dessous
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
TableauPseudoDynamique::~TableauPseudoDynamique()
{
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le constructeur par recopie pour la structure de données ci-dessous
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
TableauPseudoDynamique::TableauPseudoDynamique(const TableauPseudoDynamique &param):d_n{param.d_n}
{
    // Copie le tableau
    for (int i=0;i<d_n;i++)
        d_table[i]=param.d_table[i];
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le constructeur qui prend comme paramètres un tableau avec sa
// taille: double *tab, int val. Ce constructeur doit créer une
// structure de données contenant tous les éléments dans le tableau
// passé en paramètre
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
TableauPseudoDynamique::TableauPseudoDynamique(double *tab, int nb):d_n{nb}
{
    // Copie le tableau
    for (int i=0;i<d_n;i++)
        d_table[i]=tab[i];
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode concatenationDebut(param) qui ajoute les elements de la
// structure de données param au début de la structure de données this. Cette
// structure de données param est passée en paramètre. L'ordre des éléments
// doit être gardé.
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauPseudoDynamique::concatenationDebut(const TableauPseudoDynamique &param)
{
    // Deplace les donnees dans le tableau
    for (int i=d_n-1;i>=0;i--)
        d_table[i+param.d_n]=d_table[i];
    for (int i=0;i<param.d_n;i++)
        d_table[i]=param.d_table[i];
    d_n+=param.d_n;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode concatenationMilieu(int idx, param) qui insère les
// éléments de la structure de données param en paramètre. Ces éléments
// sont insérés à la position idx dans la stucture de données.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - L'index d'insertion peut être une valeur égale ou plus grande que la taille
//   de la structure de données. Dans ce cas, l'insertion se fait à la fin de la
//   structure de données.
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauPseudoDynamique::concatenationMilieu(int idx, const TableauPseudoDynamique &param)
{
    if (idx>d_n)
        idx=d_n;

    // Deplace les donnees dans le tableau
    for (int i=d_n-1;i>=idx;i--)
        d_table[i+param.d_n]=d_table[i];
    for (int i=0;i<param.d_n;i++)
        d_table[i+idx]=param.d_table[i];
    d_n+=param.d_n;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode concatenationFin(param) qui ajoute les elements de la
// structure de données param à la fin de la structure de données this. Cette
// structure de données param est passée en paramètre. L'ordre des éléments
// doit être gardé.
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauPseudoDynamique::concatenationFin(const TableauPseudoDynamique &param)
{
    for (int i=0;i<param.d_n;i++)
        d_table[i+d_n]=param.d_table[i];
    d_n+=param.d_n;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode operator+=(param) qui ajoute les elements de la structure de
// données param à la fin de la structure de données this. Cette structure de
// données param est passée en paramètre
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
TableauPseudoDynamique &TableauPseudoDynamique::operator+=(const TableauPseudoDynamique &param)
{
    for (int i=0;i<param.d_n;i++)
        d_table[i+d_n]=param.d_table[i];
    d_n+=param.d_n;

    return *this;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode operator==(param) qui retourne true si la structure de
// données param passée en paramètre est égale à la structure de données this.
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
bool TableauPseudoDynamique::operator==(const TableauPseudoDynamique &param) const
{
    if (this==&param)
        return true;
    if (d_n!=param.d_n)
        return false;
    int i=0;
    while(i<d_n &&  d_table[i]==param.d_table[i])
        i++;
    return i==d_n;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode operator!=(param) qui retourne true si la structure de
// données param passée en paramètre est différente à la structure de données this.
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
bool TableauPseudoDynamique::operator!=(const TableauPseudoDynamique &param) const
{
    if (this==&param)
        return false;
    if (d_n!=param.d_n)
        return true;
    int i=0;
    while(i<d_n &&  d_table[i]==param.d_table[i])
        i++;
    return i!=d_n;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire l'opérateur d'affectation operator=(param). Après cette méthode, la structure
// de données this doit contenir les mêmes éléments que la structure de données param
// passée en paramètre.
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
TableauPseudoDynamique &TableauPseudoDynamique::operator=(const TableauPseudoDynamique &param)
{
    if (this==&param)
        return *this;
    d_n=param.d_n;
    for (int i=0;i<d_n;i++)
        d_table[i]=param.d_table[i];

    return *this;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire l'opérateur crochet [](param). Cette méthode retourne la référence de l'ieme
// élément, c'est-à-dire la référence de l'élément d'index i.
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
double &TableauPseudoDynamique::operator[](int i)
{
    return d_table[i];
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire l'opérateur crochet [](param) const. Cette méthode retourne la valeur de l'ieme
// élément, c'est-à-dire la valeur de l'élément d'index i. Cette méthode ne modifie pas
// l'objet; elle doit être déclarée constante.
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
double TableauPseudoDynamique::operator[](int i) const
{
    return d_table[i];
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void additionnerATousLesElts(double val) qui ajoute la valeur
// val passée en paramètre à tous les éléments de la stucture de données.
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauPseudoDynamique::additionnerATousLesElts(double val)
{
    for (int i=0;i<d_n;i++)
        d_table[i]+=val;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererUnEltDebut(double val) qui insère un élément
// contenant la valeur val passée en paramètre. Cet élément est inséré au
// début de la stucture de données.
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauPseudoDynamique::insererUnEltDebut(double valeur)
{
    for (int i=d_n-1;i>=0;i--)
        d_table[i+1]=d_table[i];
    d_table[0]=valeur;
    d_n++;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererUnEltMilieu(int idx, double val) qui
// insère un élément contenant la valeur val passée en paramètre. Cet élément
// est inséré à la position idx dans la stucture de données.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - L'index d'insertion peut être une valeur égale ou plus grande que la taille
//   de la structure de données. Dans ce cas, l'insertion se fait à la fin de la
//   structure de données.
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauPseudoDynamique::insererUnEltMilieu(int idx, double valeur)
{
    for (int i=d_n-1;i>=idx;i--)
        d_table[i+1]=d_table[i];
    d_table[idx]=valeur;
    d_n++;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void push(double val) qui insère un élément
// contenant la valeur val passée en paramètre. Cet élément est inséré à la
// fin de la stucture de données.
// Remarques:
// - La structure de données peut être vide
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauPseudoDynamique::push(double valeur)
{
    d_table[d_n]=valeur;
    d_n++;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererPlusieursEltsDebut(double *tab, int nb)
// qui insère nb éléments au début de la structures de données. Les éléments
// à inserer sont le tableau tab. L'ordre des éléments dans le tableau doit
// être gardé dans la structure de données.
// Remarques:
// - Le paramètre nb peut être égal à zéro
// - La structure de données peut être vide
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauPseudoDynamique::insererPlusieursEltsDebut(double *tab, int nb)
{
    for (int i=d_n-1;i>=0;i--)
        d_table[i+nb]=d_table[i];
    for (int i=0;i<nb;i++)
        d_table[i]=tab[i];
    d_n+=nb;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererPlusieursEltsMilieu(int idx, double *tab, int nb)
// qui insère nb éléments à la position idx dans la structures de données. Les éléments
// à inserer sont le tableau tab. L'ordre des éléments dans le tableau doit
// être gardé dans la structure de données.
// Remarques:
// - Le paramètre nb peut être égal à 0.
// - L'index du premier élément de la structure de données est 0.
// - L'index d'insertion idx peut être une valeur égale ou plus grande que la taille
//   de la structure de données. Dans ce cas, l'insertion se fait à la fin de la
//   structure de données.
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauPseudoDynamique::insererPlusieursEltsMilieu(int idx, double *tab, int nb)
{
    for (int i=d_n-1;i>=idx;i--)
        d_table[i+nb]=d_table[i];
    for (int i=0;i<nb;i++)
        d_table[i+idx]=tab[i];
    d_n+=nb;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererPlusieursEltsFin(double *tab, int nb)
// qui insère nb éléments à la fin de la structures de données. Les éléments
// à inserer sont le tableau tab. L'ordre des éléments dans le tableau doit
// être gardé dans la structure de données.
// Remarques:
// - Le paramètre nb peut être égal à zéro
// - La structure de données peut être vide
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauPseudoDynamique::insererPlusieursEltsFin(double *tab, int nb)
{
    for (int i=0;i<nb;i++)
        d_table[i+d_n]=tab[i];
    d_n+=nb;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverUnEltDebut() qui enlève le premier élément
// de la structure de données.
// Remarque:
// - La structure de données peut être vide
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauPseudoDynamique::enleverUnEltDebut()
{
    if (d_n==0)
        return;
    for (int i=0;i<d_n-1;i++)
        d_table[i]=d_table[i+1];
    d_n--;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverUnEltMilieu(int idx) qui enlève
// l'élément d'index idx dans la structure de données.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - La structure de données peut être vide.
// - La valeur idx peut être plus grande ou égale à la taille de la structure
//   de données. Dans ce cas, l'élément n'est pas supprimé.
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauPseudoDynamique::enleverUnEltMilieu(int idx)
{
    if (d_n==0)
        return;
    if (idx>=d_n)
        return;
    for (int i=idx;i<d_n-1;i++)
        d_table[i]=d_table[i+1];
    d_n--;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverUnEltFin() qui enlève le dernier élément
// de la structure de données.
// Remarque:
// - La structure de données peut être vide
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauPseudoDynamique::enleverUnEltFin()
{
    if (d_n==0)
        return;
    d_n--;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverPlusieursEltsDebut(int nb) qui enlève nb
// éléments au début de la structures de données. Cette valeur nb est passée
// en paramètre.
// Remarque:
// - La valeur nb peut être plus grande que la taille de la structures de
// données. Dans ce cas, tous les éléments doivent être supprimés.
// - La valeur nb peut être égale à 0. Dans ce cas, rien n'est supprimé.
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauPseudoDynamique::enleverPlusieursEltsDebut(int nb)
{
    if (nb>=d_n)
    {
        d_n=0;
        return;
    }
    for (int i=nb;i<d_n;i++)
        d_table[i-nb]=d_table[i];
    d_n-=nb;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverPlusieursEltsMilieu(int idx, int nb) qui
// enlève nb éléments à partir de l'élément d'index idx. Ces valeurs idx et
// nb sont passées en paramètre.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - La valeur nb peut être plus grande que la taille de la structures de
// données.
// - La valeur idx peut être plus grande ou égale à la taille de la structure
// de données. Dans ce cas, les éléments ne sont pas supprimés.
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauPseudoDynamique::enleverPlusieursEltsMilieu(Int idx, Int nb)
{
    if (idx>=d_n)
        return;
    if (nb+idx>d_n)
        nb=d_n-idx;
    if (nb==0)
        return;
    for (int i=idx+nb;i<d_n;i++)
        d_table[i-nb]=d_table[i];
    d_n-=nb;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverPlusieursEltsFin(int nb) qui enlève nb
// éléments à la fin de la structures de données. Cette valeur nb est passée
// en paramètre.
// Remarque:
// - La valeur nb peut être plus grande que la taille de la structures de
// données.
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauPseudoDynamique::enleverPlusieursEltsFin(Int nb)
{
    if (nb==0)
        return;
    if (nb>=d_n)
    {
        d_n=0;
        return;
    }
    d_n-=nb;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode eltsEnOrdreDecroissant() qui retourne true si les éléments
// dans la structures de données sont classés par ordre strictement décroissant.
// Remarque:
// - eltsEnOrdreDecroissant() doit retourner true pour une structure de données vide
// ou ne contenant qu'un seul élément.
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
bool TableauPseudoDynamique::eltsEnOrdreDecroissant() const
{
    if (d_n<=1)
        return true;
    int i=1;
    while(i<d_n && d_table[i-1]>d_table[i])
        i++;
    return i==d_n;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode contientElt(double val) qui retourne true si la structure
// de données contient au moins un élément avec la valeur val, val étant un
// paramètre de la méthode.
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
bool TableauPseudoDynamique::contientElt(double val) const
{
    int i=0;
    while(i<d_n && d_table[i]!=val)
        i++;
    return i<d_n;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverToutesLesOccurencesDUnElt(double val) qui
// enlève tous les éléments contenant la valeur val dans la structure de
// données, val étant un paramètre de la méthode.
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauPseudoDynamique::enleverToutesLesOccurencesDUnElt(double val)
{
    int nbOccurences=0;
    for(int i=0;i<d_n;i++)
    {
        if (d_table[i]==val)
            nbOccurences++;
        else
            d_table[i-nbOccurences]=d_table[i];
    }
    d_n-=nbOccurences;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode int chercherLIndexDeLaPremiereOccurenceDUnElt(double val)
// qui retourne l'index du premier l'élément contenant la valeur val dans la
// structure de données, val étant un paramètre de la méthode.
// Remarques:
// - La structure de données ne contient pas nécessairement la valeur val. Dans ce
//   cas, la méthode doit retourner -1
// - La structure de données peut être vide
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
int TableauPseudoDynamique::chercherLIndexDeLaPremiereOccurenceDUnElt(double val) const
{
    int i=0;
    while(i<d_n && d_table[i]!=val)
        i++;
    if (i==d_n)
        return -1;
    return i;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode int chercherLIndexDeLaDerniereOccurenceDUnElt(double val)
// qui retourne l'index du dernier l'élément contenant la valeur val dans la
// structure de données, val étant un paramètre de la méthode.
// Remarques:
// - La structure de données ne contient pas nécessairement la valeur val. Dans ce
//   cas, la méthode doit retourner -1
// - La structure de données peut être vide
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////





////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode int calculerNombreDOccurencesDUnElt(double val) qui
// retourne le nombre d'éléments contenant la valeur val dans la
// structure de données, val étant un paramètre de la méthode.
// Remarques:
// - La structure de données ne contient pas nécessairement la valeur val. Dans ce
//   cas, la méthode doit retourner 0
// - La structure de données peut être vide
//
// class TableauPseudoDynamique
// {
//   public:
//     TableauPseudoDynamique();
//     ~TableauPseudoDynamique();
//     ...
//   private:
//    static const int MAX_ELTS{1000}; // Une constante optionnelle mais bienvenue.
//    double d_table[MAX_ELTS]; // un tableau statique (trop grand)
//    int d_n; // et un entier pour sa dimension effective.
//
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
int TableauPseudoDynamique::calculerNombreDOccurencesDUnElt(double val) const
{
    int i=0;
    int nbOccurences=0;
    while(i<d_n)
    {
        if (d_table[i]==val)
            nbOccurences++;
        i++;
    }
    return nbOccurences;
}



