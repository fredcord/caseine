////////////////////////////////////////////////////////////////////////////////////
// Fichier genere automatiquement pour l'instrumentation du code. Ne pas modifier //
////////////////////////////////////////////////////////////////////////////////////


#include "ExercicesBase.h"


// DEBUT EXO_CONSTRUCTEUR
Tableau2DNonContigu_ConstructDestruct::Tableau2DNonContigu_ConstructDestruct():d_nl{0},d_nc{0},d_tab{nullptr}
{
}
// FIN EXO_CONSTRUCTEUR


// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
Tableau2DNonContigu_ConstructDestruct::Tableau2DNonContigu_ConstructDestruct(const Tableau2DNonContigu &param):d_nl{param.d_nl},d_nc{param.d_nc},
                                             d_tab{new Double*[d_nl]}
{
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        d_tab[i]=new Double[d_nc];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            d_tab[i][j]=param.d_tab[i][j];
    }
}
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE


// DEBUT EXO_CONSTRUCTEUR_AVEC_TABLEAU
Tableau2DNonContigu_ConstructDestruct::Tableau2DNonContigu_ConstructDestruct(Double **tab, Int nl, Int nc):d_nl{nl},d_nc{nc},
                                         d_tab{new Double*[d_nl]}
{
    d_nl=nl;
    d_nc=nc;

    // Copie le tableau
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        d_tab[i]=new Double[d_nc];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            d_tab[i][j]=tab[i][j];
    }
}
// FIN EXO_CONSTRUCTEUR_AVEC_TABLEAU


// DEBUT EXO_DESTRUCTEUR
Tableau2DNonContigu_ConstructDestruct::~Tableau2DNonContigu_ConstructDestruct()
{
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
        delete[] d_tab[i];
    {GestionPntrEtComplexite::get().peutSupprPntr(d_tab, true); delete[] d_tab;}
}
// FIN EXO_DESTRUCTEUR


// DEBUT EXO_CONCATENATION_DEBUT
void Tableau2DNonContigu_Other::concatenationDebut(const Tableau2DNonContigu &param)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_nc==0)
        return;
    // Concatenation a un tableau vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl==0)
        d_tab=new Double*[param.d_nl];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<param.d_nl;i++)
    {
        // Allouer un nouveau tableau pour la ligne
        Double *precLigne=d_tab[i];
        d_tab[i]=new Double[d_nc+param.d_nc];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<param.d_nc;j++)
            d_tab[i][j]=param.d_tab[i][j];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            d_tab[i][param.d_nc+j]=precLigne[j];
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl>0)
            {GestionPntrEtComplexite::get().peutSupprPntr(precLigne, true); delete[] precLigne;}
    }
    d_nc+=param.d_nc;
    d_nl=param.d_nl;
}
// FIN EXO_CONCATENATION_DEBUT


// DEBUT EXO_CONCATENATION_MILIEU
void Tableau2DNonContigu_Other::concatenationMilieu(Int idx, const Tableau2DNonContigu &param)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_nc==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx>d_nc)
        idx=d_nc;
    // Concatenation a un tableau vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl==0)
        d_tab=new Double*[param.d_nl];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<param.d_nl;i++)
    {
        // Allouer un nouveau tableau pour la ligne
        Double *precLigne=d_tab[i];
        d_tab[i]=new Double[d_nc+param.d_nc];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<idx;j++)
            d_tab[i][j]=precLigne[j];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<param.d_nc;j++)
            d_tab[i][j+idx]=param.d_tab[i][j];
        for(Int j=idx;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            d_tab[i][param.d_nc+j]=precLigne[j];
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl>0)
            {GestionPntrEtComplexite::get().peutSupprPntr(precLigne, true); delete[] precLigne;}
    }
    d_nc+=param.d_nc;
    d_nl=param.d_nl;
}
// FIN EXO_CONCATENATION_MILIEU


// DEBUT EXO_CONCATENATION_FIN
void Tableau2DNonContigu_Other::concatenationFin(const Tableau2DNonContigu &param)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_nc==0)
        return;
    // Concatenation a un tableau vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl==0)
        d_tab=new Double*[param.d_nl];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<param.d_nl;i++)
    {
        // Allouer un nouveau tableau pour la ligne
        Double *precLigne=d_tab[i];
        d_tab[i]=new Double[d_nc+param.d_nc];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            d_tab[i][j]=precLigne[j];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<param.d_nc;j++)
            d_tab[i][j+d_nc]=param.d_tab[i][j];
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl>0)
            {GestionPntrEtComplexite::get().peutSupprPntr(precLigne, true); delete[] precLigne;}
    }
    d_nc+=param.d_nc;
    d_nl=param.d_nl;
}
// FIN EXO_CONCATENATION_FIN


// DEBUT EXO_OP_CONCATENATION
Tableau2DNonContigu &Tableau2DNonContigu_Other::operator+=(const Tableau2DNonContigu &param)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_nc==0)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
    // Concatenation a un tableau vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl==0)
        d_tab=new Double*[param.d_nl];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<param.d_nl;i++)
    {
        // Allouer un nouveau tableau pour la ligne
        Double *precLigne=d_tab[i];
        d_tab[i]=new Double[d_nc+param.d_nc];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            d_tab[i][j]=precLigne[j];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<param.d_nc;j++)
            d_tab[i][j+d_nc]=param.d_tab[i][j];
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl>0)
            {GestionPntrEtComplexite::get().peutSupprPntr(precLigne, true); delete[] precLigne;}
    }
    d_nc+=param.d_nc;
    d_nl=param.d_nl;

    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_CONCATENATION


// DEBUT EXO_OP_EGAL
bool Tableau2DNonContigu_Other::operator==(const Tableau2DNonContigu &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl!=param.d_nl || d_nc!=param.d_nc)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    Int i=0,j=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_nl &&  d_tab[i][j]==param.d_tab[i][j])
    {
        j++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&j==d_nc)
        {
            i++;
            j=0;
        }
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i==d_nl;}
}
// FIN EXO_OP_EGAL


// DEBUT EXO_OP_DIFFERENT
bool Tableau2DNonContigu_Other::operator!=(const Tableau2DNonContigu &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl!=param.d_nl || d_nc!=param.d_nc)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    Int i=0,j=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_nl &&  d_tab[i][j]==param.d_tab[i][j])
    {
        j++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&j==d_nc)
        {
            i++;
            j=0;
        }
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i!=d_nl;}
}
// FIN EXO_OP_DIFFERENT


// DEBUT EXO_OP_AFFECTATION
Tableau2DNonContigu &Tableau2DNonContigu_Other::operator=(const Tableau2DNonContigu &param)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}

    // Copie d'un tableau vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_nl==0 && param.d_nc==0)
    {
        for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
            delete[] d_tab[i];
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tab, true); delete[] d_tab;}
        d_tab=nullptr;
        d_nl=0;
        d_nc=0;
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
    }
    // Meme nombre de lignes
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl==param.d_nl)
    {
        for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
        {
            // Alloue un tableau pour la ligne si sa taille est differente
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc!=param.d_nc)
            {
                delete[] d_tab[i];
                d_tab[i]=new Double[param.d_nc];
            }
            for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<param.d_nc;j++)
                d_tab[i][j]=param.d_tab[i][j];
        }
        d_nc=param.d_nc;
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
    }

    // Nombre de ligne differents
    Double **prec_tab=d_tab;
    d_tab=new Double*[param.d_nl];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<param.d_nl;i++)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&i<d_nl && d_nc==param.d_nc)
            // Reutilise le tableau ligne si le nombre de colonnes est le meme
            d_tab[i]=prec_tab[i];
        else
        {
            // Alloue un nouveau tableau pour la ligne
            d_tab[i]=new Double[param.d_nc];
            // Libere le tableau si cette ligne existait
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&i<d_nl)
                delete[] prec_tab[i];
        }
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<param.d_nc;j++)
            d_tab[i][j]=param.d_tab[i][j];
    }
    // libere les tableaux ligne en trop
    for(Int i=param.d_nl;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
        delete[] prec_tab[i];
    {GestionPntrEtComplexite::get().peutSupprPntr(prec_tab, true); delete[] prec_tab;}

    d_nl=param.d_nl;
    d_nc=param.d_nc;
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_AFFECTATION


// DEBUT EXO_OP_PARENTHESE
Double& Tableau2DNonContigu_Other::operator()(Int i, Int j)
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_tab[i][j];}
}
// FIN EXO_OP_PARENTHESE


// DEBUT EXO_OP_PARENTHESE_CONST
Double Tableau2DNonContigu_Other::operator()(Int i, Int j) const
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_tab[i][j];}
}
// FIN EXO_OP_PARENTHESE_CONST


// DEBUT EXO_SETTER
void Tableau2DNonContigu_Other::set(Int i, Int j, Double val)
{
    d_tab[i][j]=val;
}
// FIN EXO_SETTER


// DEBUT EXO_GETTER
Double Tableau2DNonContigu_Other::get(Int i, Int j) const
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_tab[i][j];}
}
// FIN EXO_GETTER


// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
void Tableau2DNonContigu_Other::additionnerATousLesElts(Double val)
{
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            d_tab[i][j]+=val;
}
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS


// DEBUT EXO_INSERER_UN_ELT_DEBUT
void Tableau2DNonContigu_Other::insererUnEltDebut(Double* tab, Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl==0)
    {
        // Concatenation a un tableau vide
        assert(d_nc==0);
        d_tab=new Double*[nb];
    }
    // Allouer un nouveau tableau
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        Double *prec_ligne=d_tab[i];
        d_tab[i]=new Double[d_nc+1];
        d_tab[i][0]=tab[i];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            d_tab[i][j+1]=prec_ligne[j];
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl>0)
            {GestionPntrEtComplexite::get().peutSupprPntr(prec_ligne, true); delete[] prec_ligne;}
    }
    d_nc+=1;
    d_nl=nb;
}
// FIN EXO_INSERER_UN_ELT_DEBUT



// DEBUT EXO_INSERER_UN_ELT_MILIEU
void Tableau2DNonContigu_Other::insererUnEltMilieu(Int idx, Double* tab, Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx>d_nc)
        idx=d_nc;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl==0)
    {
        // Concatenation a un tableau vide
        assert(d_nc==0);
        d_tab=new Double*[nb];
    }
    // Allouer un nouveau tableau
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        Double *prec_ligne=d_tab[i];
        d_tab[i]=new Double[d_nc+1];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<idx;j++)
            d_tab[i][j]=prec_ligne[j];
        d_tab[i][idx]=tab[i];
        for(Int j=idx;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            d_tab[i][j+1]=prec_ligne[j];
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl>0)
            {GestionPntrEtComplexite::get().peutSupprPntr(prec_ligne, true); delete[] prec_ligne;}
    }
    d_nc+=1;
    d_nl=nb;
}
// FIN EXO_INSERER_UN_ELT_MILIEU


// DEBUT EXO_INSERER_UN_ELT_FIN
void Tableau2DNonContigu_Other::insererUnEltFin(Double* tab, Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl==0)
    {
        // Concatenation a un tableau vide
        assert(d_nc==0);
        d_tab=new Double*[nb];
    }
    // Allouer un nouveau tableau
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        Double *prec_ligne=d_tab[i];
        d_tab[i]=new Double[d_nc+1];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            d_tab[i][j]=prec_ligne[j];
        d_tab[i][d_nc]=tab[i];
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl>0)
            {GestionPntrEtComplexite::get().peutSupprPntr(prec_ligne, true); delete[] prec_ligne;}
    }
    d_nc+=1;
    d_nl=nb;
}
// FIN EXO_INSERER_UN_ELT_FIN


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_DEBUT
void Tableau2DNonContigu_Other::insererPlusieursEltsDebut(Double **tab, Int nbl, Int nbc)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nbc==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl==0)
    {
        // Concatenation a un tableau vide
        assert(d_nc==0);
        d_tab=new Double*[nbl];
    }
    // Allouer un nouveau tableau
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nbl;i++)
    {
        Double *prec_ligne=d_tab[i];
        d_tab[i]=new Double[d_nc+nbc];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<nbc;j++)
            d_tab[i][j]=tab[i][j];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            d_tab[i][j+nbc]=prec_ligne[j];
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl>0)
            {GestionPntrEtComplexite::get().peutSupprPntr(prec_ligne, true); delete[] prec_ligne;}
    }
    d_nc+=nbc;
    d_nl=nbl;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_MILIEU
void Tableau2DNonContigu_Other::insererPlusieursEltsMilieu(Int idx, Double **tab, Int nbl, Int nbc)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nbc==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx>d_nc)
        idx=d_nc;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl==0)
        // Concatenation a un tableau vide
        d_tab=new Double*[nbl];
    // Allouer un nouveau tableau
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nbl;i++)
    {
        Double *prec_ligne=d_tab[i];
        d_tab[i]=new Double[d_nc+nbc];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<idx;j++)
            d_tab[i][j]=prec_ligne[j];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<nbc;j++)
            d_tab[i][idx+j]=tab[i][j];
        for(Int j=idx;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            d_tab[i][j+nbc]=prec_ligne[j];
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl>0)
            {GestionPntrEtComplexite::get().peutSupprPntr(prec_ligne, true); delete[] prec_ligne;}
    }
    d_nc+=nbc;
    d_nl=nbl;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_FIN
void Tableau2DNonContigu_Other::insererPlusieursEltsFin(Double **tab, Int nbl, Int nbc)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nbc==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl==0)
    {
        // Concatenation a un tableau vide
        assert(d_nc==0);
        d_tab=new Double*[nbl];
    }
    // Allouer un nouveau tableau
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nbl;i++)
    {
        Double *prec_ligne=d_tab[i];
        d_tab[i]=new Double[d_nc+nbc];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc;j++)
            d_tab[i][j]=prec_ligne[j];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<nbc;j++)
            d_tab[i][d_nc+j]=tab[i][j];
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl>0)
            {GestionPntrEtComplexite::get().peutSupprPntr(prec_ligne, true); delete[] prec_ligne;}
    }
    d_nc+=nbc;
    d_nl=nbl;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ENLEVER_UN_ELT_DEBUT
void Tableau2DNonContigu_Other::enleverUnEltDebut()
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc==1)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        delete[] d_tab[0];
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tab, true); delete[] d_tab;}
        d_tab=nullptr;
        return;
    }

    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        Double *prec_ligne=d_tab[i];
        d_tab[i]=new Double[d_nc-1];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc-1;j++)
            d_tab[i][j]=prec_ligne[j+1];
        {GestionPntrEtComplexite::get().peutSupprPntr(prec_ligne, true); delete[] prec_ligne;}
    }
    d_nc--;
}
// FIN EXO_ENLEVER_UN_ELT_DEBUT


// DEBUT EXO_ENLEVER_UN_ELT_MILIEU
void Tableau2DNonContigu_Other::enleverUnEltMilieu(Int idx)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx>=d_nc)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc==1)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        delete[] d_tab[0];
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tab, true); delete[] d_tab;}
        d_tab=nullptr;
        return;
    }

    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        Double *prec_ligne=d_tab[i];
        d_tab[i]=new Double[d_nc-1];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<idx;j++)
            d_tab[i][j]=prec_ligne[j];
        for(Int j=idx;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc-1;j++)
            d_tab[i][j]=prec_ligne[j+1];
        {GestionPntrEtComplexite::get().peutSupprPntr(prec_ligne, true); delete[] prec_ligne;}
    }
    d_nc--;
}
// FIN EXO_ENLEVER_UN_ELT_MILIEU


// DEBUT EXO_ENLEVER_UN_ELT_FIN
void Tableau2DNonContigu_Other::enleverUnEltFin()
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc==1)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        delete[] d_tab[0];
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tab, true); delete[] d_tab;}
        d_tab=nullptr;
        return;
    }

    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        Double *prec_ligne=d_tab[i];
        d_tab[i]=new Double[d_nc-1];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc-1;j++)
            d_tab[i][j]=prec_ligne[j];
        {GestionPntrEtComplexite::get().peutSupprPntr(prec_ligne, true); delete[] prec_ligne;}
    }
    d_nc--;
}
// FIN EXO_ENLEVER_UN_ELT_FIN


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
void Tableau2DNonContigu_Other::enleverPlusieursEltsDebut(Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc<=nb)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        delete[] d_tab[0];
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tab, true); delete[] d_tab;}
        d_tab=nullptr;
        return;
    }

    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        Double *prec_ligne=d_tab[i];
        d_tab[i]=new Double[d_nc-nb];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc-nb;j++)
            d_tab[i][j]=prec_ligne[j+nb];
        {GestionPntrEtComplexite::get().peutSupprPntr(prec_ligne, true); delete[] prec_ligne;}
    }
    d_nc-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU
void Tableau2DNonContigu_Other::enleverPlusieursEltsMilieu(Int idx, Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx>=d_nc)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc<idx+nb)
        nb=d_nc-idx;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc<=nb)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        delete[] d_tab[0];
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tab, true); delete[] d_tab;}
        d_tab=nullptr;
        return;
    }

    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        Double *prec_ligne=d_tab[i];
        d_tab[i]=new Double[d_nc-nb];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<idx;j++)
            d_tab[i][j]=prec_ligne[j];
        for(Int j=idx;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc-nb;j++)
            d_tab[i][j]=prec_ligne[j+nb];
        {GestionPntrEtComplexite::get().peutSupprPntr(prec_ligne, true); delete[] prec_ligne;}
    }
    d_nc-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_FIN
void Tableau2DNonContigu_Other::enleverPlusieursEltsFin(Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nc<=nb)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        delete[] d_tab[0];
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tab, true); delete[] d_tab;}
        d_tab=nullptr;
        return;
    }

    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_nl;i++)
    {
        Double *prec_ligne=d_tab[i];
        d_tab[i]=new Double[d_nc-nb];
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<d_nc-nb;j++)
            d_tab[i][j]=prec_ligne[j];
        {GestionPntrEtComplexite::get().peutSupprPntr(prec_ligne, true); delete[] prec_ligne;}
    }
    d_nc-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ELTS_EN_ORDRE_DECROISSANT
bool Tableau2DNonContigu_Other::eltsEnOrdreDecroissant() const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_nl*d_nc==0 || d_nl*d_nc==1)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    Int i=0,j=0;
    Int nextI=i,nextJ=j+1;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nextJ==d_nc)
    {
        nextI++;
        nextJ=0;
    }
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&nextI<d_nl && d_tab[i][j]>d_tab[nextI][nextJ])
    {
        i=nextI;
        j=nextJ;
        nextJ++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nextJ==d_nc)
        {
            nextI++;
            nextJ=0;
        }
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return nextI==d_nl;}
}
// FIN EXO_ELTS_EN_ORDRE_DECROISSANT


// DEBUT EXO_CONTIENT_ELT
bool Tableau2DNonContigu_Other::contientElt(Double val) const
{
    Int i=0,j=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_nl && d_tab[i][j]!=val)
    {
        j++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&j==d_nc)
        {
            j=0;
            i++;
        }
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i<d_nl;}
}
// FIN EXO_CONTIENT_ELT


// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
Int Tableau2DNonContigu_Other::calculerNombreDOccurencesDUnElt(Double val) const
{
    Int nbOccurences=0;
    Int i=0,j=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_nl)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tab[i][j]==val)
            nbOccurences++;
        j++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&j==d_nc)
        {
            j=0;
            i++;
        }
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return nbOccurences;}
}
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT


// DEBUT EXO_OP_ADDITION
Tableau2DNonContigu operator_plus(const Tableau2DNonContigu& a, const Tableau2DNonContigu& b)
{
    Tableau2DNonContigu res(a);
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<res.d_nl;i++)
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<res.d_nc;j++)
            res.d_tab[i][j]+=b.d_tab[i][j];
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return res;}
}
// FIN EXO_OP_ADDITION


// DEBUT EXO_OP_SOUSTRACTION
Tableau2DNonContigu operator_moins(const Tableau2DNonContigu& a, const Tableau2DNonContigu& b)
{
    Tableau2DNonContigu res(a);
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<res.d_nl;i++)
        for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<res.d_nc;j++)
            res.d_tab[i][j]-=b.d_tab[i][j];
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return res;}
}
// FIN EXO_OP_SOUSTRACTION
