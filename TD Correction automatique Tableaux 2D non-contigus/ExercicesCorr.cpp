#include <iostream>
#include "ExercicesBase.h"

using namespace std;


/////////////////////////////////////////////////////////////////////////////
// Compare la structure de donnees de param'etudiant avec celle de la solution //
/////////////////////////////////////////////////////////////////////////////


// Verifie si la structure de donnees est correcte au niveau de sa sctructure et des poInteurs
bool Tableau2DNonContiguCorr::verifierIntegrite(const Tableau2DNonContigu &param)
{
    // Verifie si les valeurs sont dans les bons intervals pour eviter les crash
    if (param.d_nl<0)
        return false;
    if (param.d_nc<0)
        return false;
    if (param.d_nl==0 || param.d_nc==0)
    {
        if (param.d_nl!=0 || param.d_nc!=0)
            return false;
        if (param.d_tab!=nullptr)
            if (!GestionPntrEtComplexite::get().pntrValide(param.d_tab))
                return false;
        return true;
    }
    // Verifie l'allocation des pointeurs
    if (!GestionPntrEtComplexite::get().pntrValide(param.d_tab))
        return false;
    // Verifie la taille allouee
    if (GestionPntrEtComplexite::get().pntrTailleAllouee(param.d_tab)<param.d_nl)
        return false;
    for (int i=0;i<param.d_nl;i++)
    {
        if (param.d_tab[i]!=nullptr)
        {
            if (!GestionPntrEtComplexite::get().pntrValide(param.d_tab[i]))
                return false;
            if (GestionPntrEtComplexite::get().pntrTailleAllouee(param.d_tab[i])<param.d_nc)
                return false;
        }
    }
    return true;
}


// Compare la correction avec la struct de param'etudiant. Retourne false si different
bool Tableau2DNonContiguCorr::compareCorrAvecEtud(const Tableau2DNonContigu &student) const
{
    if (d_nl!=student.d_nl)
        return false;
    if (d_nc!=student.d_nc)
        return false;
    for (int i=0;i<d_nl;i++)
    {
        for (int j=0;j<d_nc;j++)
        {
            if (d_tab[i][j]!=student.d_tab[i][j])
                return false;
        }
    }
    return true;
}

// genere la structDeDonneesVerifCorr a partir de la liste
StructDeDonneesVerifCorr Tableau2DNonContiguCorr::getStructDeDonneesVerifCorr() const
{
    StructDeDonneesVerifCorr vect(d_nl,d_nc);

    for (int i=0;i<d_nl;i++)
        for (int j=0;j<d_nc;j++)
            vect.set(i,j,d_tab[i][j]);
    return vect;
}

// Constructeur avec la structDeDonneesVerifCorr
void Tableau2DNonContiguCorr::setStructDeDonneesVerifCorr(const StructDeDonneesVerifCorr &tab)
{
    d_nl=tab.getNbLignes();
    d_nc=tab.getNbColonnes();

    d_tab=new double*[d_nl];
    for (int i=0;i<d_nl;i++)
    {
        d_tab[i]=new double[d_nc];
        for (int j=0;j<d_nc;j++)
            d_tab[i][j]=tab.get(i,j);
    }
}

// DEBUT EXO_CONSTRUCTEUR
Tableau2DNonContiguCorr::Tableau2DNonContiguCorr():d_nl{0},d_nc{0},d_tab{nullptr}
{
}
// FIN EXO_CONSTRUCTEUR


// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
Tableau2DNonContiguCorr::Tableau2DNonContiguCorr(const Tableau2DNonContiguCorr &param):d_nl{param.d_nl},d_nc{param.d_nc},
                                                         d_tab{new double*[d_nl]}
{
    for (int i=0;i<d_nl;i++)
    {
        d_tab[i]=new double[d_nc];
        for (int j=0;j<d_nc;j++)
            d_tab[i][j]=param.d_tab[i][j];
    }
}
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE


// DEBUT EXO_CONSTRUCTEUR_AVEC_TABLEAU
Tableau2DNonContiguCorr::Tableau2DNonContiguCorr(double **tab, int nl, int nc):d_nl{nl},d_nc{nc},
                                                 d_tab{new double*[d_nl]}
{
    d_nl=nl;
    d_nc=nc;

    // Copie le tableau
    for (int i=0;i<d_nl;i++)
    {
        d_tab[i]=new double[d_nc];
        for (int j=0;j<d_nc;j++)
            d_tab[i][j]=tab[i][j];
    }
}
// FIN EXO_CONSTRUCTEUR_AVEC_TABLEAU


// DEBUT DESTRUCTEUR
Tableau2DNonContiguCorr::~Tableau2DNonContiguCorr()
{
    for (int i=0;i<d_nl;i++)
        delete[] d_tab[i];
    delete[] d_tab;
}
// FIN DESTRUCTEUR


// DEBUT EXO_CONCATENATION_DEBUT
void Tableau2DNonContiguCorr::concatenationDebut(const Tableau2DNonContiguCorr &param)
{
    if (param.d_nc==0)
        return;
    // Concatenation a un tableau vide
    if (d_nl==0)
        d_tab=new double*[param.d_nl];
    for (int i=0;i<param.d_nl;i++)
    {
        // Allouer un nouveau tableau pour la ligne
        double *precLigne=d_tab[i];
        d_tab[i]=new double[d_nc+param.d_nc];
        for (int j=0;j<param.d_nc;j++)
            d_tab[i][j]=param.d_tab[i][j];
        for (int j=0;j<d_nc;j++)
            d_tab[i][param.d_nc+j]=precLigne[j];
        if (d_nl>0)
            delete[] precLigne;
    }
    d_nc+=param.d_nc;
    d_nl=param.d_nl;
}
// FIN EXO_CONCATENATION_DEBUT


// DEBUT EXO_CONCATENATION_MILIEU
void Tableau2DNonContiguCorr::concatenationMilieu(int idx, const Tableau2DNonContiguCorr &param)
{
    if (param.d_nc==0)
        return;
    if (idx>d_nc)
        idx=d_nc;
    // Concatenation a un tableau vide
    if (d_nl==0)
        d_tab=new double*[param.d_nl];
    for (int i=0;i<param.d_nl;i++)
    {
        // Allouer un nouveau tableau pour la ligne
        double *precLigne=d_tab[i];
        d_tab[i]=new double[d_nc+param.d_nc];
        for (int j=0;j<idx;j++)
            d_tab[i][j]=precLigne[j];
        for (int j=0;j<param.d_nc;j++)
            d_tab[i][j+idx]=param.d_tab[i][j];
        for (int j=idx;j<d_nc;j++)
            d_tab[i][param.d_nc+j]=precLigne[j];
        if (d_nl>0)
            delete[] precLigne;
    }
    d_nc+=param.d_nc;
    d_nl=param.d_nl;
}
// FIN EXO_CONCATENATION_MILIEU


// DEBUT EXO_CONCATENATION_FIN
void Tableau2DNonContiguCorr::concatenationFin(const Tableau2DNonContiguCorr &param)
{
    if (param.d_nc==0)
        return;
    // Concatenation a un tableau vide
    if (d_nl==0)
        d_tab=new double*[param.d_nl];
    for (int i=0;i<param.d_nl;i++)
    {
        // Allouer un nouveau tableau pour la ligne
        double *precLigne=d_tab[i];
        d_tab[i]=new double[d_nc+param.d_nc];
        for (int j=0;j<d_nc;j++)
            d_tab[i][j]=precLigne[j];
        for (int j=0;j<param.d_nc;j++)
            d_tab[i][j+d_nc]=param.d_tab[i][j];
        if (d_nl>0)
            delete[] precLigne;
    }
    d_nc+=param.d_nc;
    d_nl=param.d_nl;
}
// FIN EXO_CONCATENATION_FIN


// DEBUT EXO_OP_CONCATENATION
Tableau2DNonContiguCorr &Tableau2DNonContiguCorr::operator+=(const Tableau2DNonContiguCorr &param)
{
    if (param.d_nc==0)
        return *this;
    // Concatenation a un tableau vide
    if (d_nl==0)
        d_tab=new double*[param.d_nl];
    for (int i=0;i<param.d_nl;i++)
    {
        // Allouer un nouveau tableau pour la ligne
        double *precLigne=d_tab[i];
        d_tab[i]=new double[d_nc+param.d_nc];
        for (int j=0;j<d_nc;j++)
            d_tab[i][j]=precLigne[j];
        for (int j=0;j<param.d_nc;j++)
            d_tab[i][j+d_nc]=param.d_tab[i][j];
        if (d_nl>0)
            delete[] precLigne;
    }
    d_nc+=param.d_nc;
    d_nl=param.d_nl;

    return *this;
}
// FIN EXO_OP_CONCATENATION


// DEBUT EXO_OP_EGAL
bool Tableau2DNonContiguCorr::operator==(const Tableau2DNonContiguCorr &param) const
{
    if (this==&param)
        return true;
    if (d_nl!=param.d_nl || d_nc!=param.d_nc)
        return false;
    int i=0,j=0;
    while(i<d_nl &&  d_tab[i][j]==param.d_tab[i][j])
    {
        j++;
        if (j==d_nc)
        {
            i++;
            j=0;
        }
    }
    return i==d_nl;
}
// FIN EXO_OP_EGAL


// DEBUT EXO_OP_DIFFERENT
bool Tableau2DNonContiguCorr::operator!=(const Tableau2DNonContiguCorr &param) const
{
    if (this==&param)
        return false;
    if (d_nl!=param.d_nl || d_nc!=param.d_nc)
        return true;
    int i=0,j=0;
    while(i<d_nl &&  d_tab[i][j]==param.d_tab[i][j])
    {
        j++;
        if (j==d_nc)
        {
            i++;
            j=0;
        }
    }
    return i!=d_nl;
}
// FIN EXO_OP_DIFFERENT


// DEBUT EXO_OP_AFFECTATION
Tableau2DNonContiguCorr &Tableau2DNonContiguCorr::operator=(const Tableau2DNonContiguCorr &param)
{
    if (this==&param)
        return *this;

    // Copie d'un tableau vide
    if (param.d_nl==0 && param.d_nc==0)
    {
        for (int i=0;i<d_nl;i++)
            delete[] d_tab[i];
        delete[] d_tab;
        d_tab=nullptr;
        d_nl=0;
        d_nc=0;
        return *this;
    }
    // Meme nombre de lignes
    if (d_nl==param.d_nl)
    {
        for (int i=0;i<d_nl;i++)
        {
            // Alloue un tableau pour la ligne si sa taille est differente
            if (d_nc!=param.d_nc)
            {
                delete[] d_tab[i];
                d_tab[i]=new double[param.d_nc];
            }
            for (int j=0;j<param.d_nc;j++)
                d_tab[i][j]=param.d_tab[i][j];
        }
        d_nc=param.d_nc;
        return *this;
    }

    // Nombre de ligne differents
    double **prec_tab=d_tab;
    d_tab=new double*[param.d_nl];
    for (int i=0;i<param.d_nl;i++)
    {
        if (i<d_nl && d_nc==param.d_nc)
            // Reutilise le tableau ligne si le nombre de colonnes est le meme
            d_tab[i]=prec_tab[i];
        else
        {
            // Alloue un nouveau tableau pour la ligne
            d_tab[i]=new double[param.d_nc];
            // Libere le tableau si cette ligne existait
            if (i<d_nl)
                delete[] prec_tab[i];
        }
        for (int j=0;j<param.d_nc;j++)
            d_tab[i][j]=param.d_tab[i][j];
    }
    // libere les tableaux ligne en trop
    for (int i=param.d_nl;i<d_nl;i++)
        delete[] prec_tab[i];
    delete[] prec_tab;

    d_nl=param.d_nl;
    d_nc=param.d_nc;
    return *this;
}
// FIN EXO_OP_AFFECTATION


// DEBUT EXO_OP_PARENTHESE
double& Tableau2DNonContiguCorr::operator()(int i, int j)
{
    return d_tab[i][j];
}
// FIN EXO_OP_PARENTHESE


// DEBUT EXO_OP_PARENTHESE_CONST
double Tableau2DNonContiguCorr::operator()(int i, int j) const
{
    return d_tab[i][j];
}
// FIN EXO_OP_PARENTHESE_CONST


// DEBUT EXO_SETTER
void Tableau2DNonContiguCorr::set(int i, int j, double val)
{
    d_tab[i][j]=val;
}
// FIN EXO_SETTER


// DEBUT EXO_GETTER
double Tableau2DNonContiguCorr::get(int i, int j) const
{
    return d_tab[i][j];
}
// FIN EXO_GETTER


// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
void Tableau2DNonContiguCorr::additionnerATousLesElts(double val)
{
    for (int i=0;i<d_nl;i++)
        for (int j=0;j<d_nc;j++)
            d_tab[i][j]+=val;
}
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS


// DEBUT EXO_INSERER_UN_ELT_DEBUT
void Tableau2DNonContiguCorr::insererUnEltDebut(double* tab, int nb)
{
    if (nb==0)
        return;
    if (d_nl==0)
    {
        // Concatenation a un tableau vide
        assert(d_nc==0);
        d_tab=new double*[nb];
    }
    // Allouer un nouveau tableau
    for (int i=0;i<nb;i++)
    {
        double *prec_ligne=d_tab[i];
        d_tab[i]=new double[d_nc+1];
        d_tab[i][0]=tab[i];
        for (int j=0;j<d_nc;j++)
            d_tab[i][j+1]=prec_ligne[j];
        if (d_nl>0)
            delete[] prec_ligne;
    }
    d_nc+=1;
    d_nl=nb;
}
// FIN EXO_INSERER_UN_ELT_DEBUT


// DEBUT EXO_INSERER_UN_ELT_MILIEU
void Tableau2DNonContiguCorr::insererUnEltMilieu(int idx, double* tab, int nb)
{
    if (nb==0)
        return;
    if (idx>d_nc)
        idx=d_nc;
    if (d_nl==0)
    {
        // Concatenation a un tableau vide
        assert(d_nc==0);
        d_tab=new double*[nb];
    }
    // Allouer un nouveau tableau
    for (int i=0;i<nb;i++)
    {
        double *prec_ligne=d_tab[i];
        d_tab[i]=new double[d_nc+1];
        for (int j=0;j<idx;j++)
            d_tab[i][j]=prec_ligne[j];
        d_tab[i][idx]=tab[i];
        for (int j=idx;j<d_nc;j++)
            d_tab[i][j+1]=prec_ligne[j];
        if (d_nl>0)
            delete[] prec_ligne;
    }
    d_nc+=1;
    d_nl=nb;
}
// FIN EXO_INSERER_UN_ELT_MILIEU


// DEBUT EXO_INSERER_UN_ELT_FIN
void Tableau2DNonContiguCorr::insererUnEltFin(double* tab, int nb)
{
    if (nb==0)
        return;
    if (d_nl==0)
    {
        // Concatenation a un tableau vide
        assert(d_nc==0);
        d_tab=new double*[nb];
    }
    // Allouer un nouveau tableau
    for (int i=0;i<nb;i++)
    {
        double *prec_ligne=d_tab[i];
        d_tab[i]=new double[d_nc+1];
        for (int j=0;j<d_nc;j++)
            d_tab[i][j]=prec_ligne[j];
        d_tab[i][d_nc]=tab[i];
        if (d_nl>0)
            delete[] prec_ligne;
    }
    d_nc+=1;
    d_nl=nb;
}
// FIN EXO_INSERER_UN_ELT_FIN


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_DEBUT
void Tableau2DNonContiguCorr::insererPlusieursEltsDebut(double **tab, int nbl, int nbc)
{
    if (nbc==0)
        return;
    if (d_nl==0)
    {
        // Concatenation a un tableau vide
        assert(d_nc==0);
        d_tab=new double*[nbl];
    }
    // Allouer un nouveau tableau
    for (int i=0;i<nbl;i++)
    {
        double *prec_ligne=d_tab[i];
        d_tab[i]=new double[d_nc+nbc];
        for (int j=0;j<nbc;j++)
            d_tab[i][j]=tab[i][j];
        for (int j=0;j<d_nc;j++)
            d_tab[i][j+nbc]=prec_ligne[j];
        if (d_nl>0)
            delete[] prec_ligne;
    }
    d_nc+=nbc;
    d_nl=nbl;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_MILIEU
void Tableau2DNonContiguCorr::insererPlusieursEltsMilieu(int idx, double **tab, int nbl, int nbc)
{
    if (nbc==0)
        return;
    if (idx>d_nc)
        idx=d_nc;
    if (d_nl==0)
    {
        // Concatenation a un tableau vide
        assert(d_nc==0);
        d_tab=new double*[nbl];
    }
    // Allouer un nouveau tableau
    for (int i=0;i<nbl;i++)
    {
        double *prec_ligne=d_tab[i];
        d_tab[i]=new double[d_nc+nbc];
        for (int j=0;j<idx;j++)
            d_tab[i][j]=prec_ligne[j];
        for (int j=0;j<nbc;j++)
            d_tab[i][idx+j]=tab[i][j];
        for (int j=idx;j<d_nc;j++)
            d_tab[i][j+nbc]=prec_ligne[j];
        if (d_nl>0)
            delete[] prec_ligne;
    }
    d_nc+=nbc;
    d_nl=nbl;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_FIN
void Tableau2DNonContiguCorr::insererPlusieursEltsFin(double **tab, int nbl, int nbc)
{
    if (nbc==0)
        return;
    if (d_nl==0)
    {
        // Concatenation a un tableau vide
        assert(d_nc==0);
        d_tab=new double*[nbl];
    }
    // Allouer un nouveau tableau
    for (int i=0;i<nbl;i++)
    {
        double *prec_ligne=d_tab[i];
        d_tab[i]=new double[d_nc+nbc];
        for (int j=0;j<d_nc;j++)
            d_tab[i][j]=prec_ligne[j];
        for (int j=0;j<nbc;j++)
            d_tab[i][d_nc+j]=tab[i][j];
        if (d_nl>0)
            delete[] prec_ligne;
    }
    d_nc+=nbc;
    d_nl=nbl;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ENLEVER_UN_ELT_DEBUT
void Tableau2DNonContiguCorr::enleverUnEltDebut()
{
    if (d_nc==0)
        return;
    if (d_nc==1)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        delete[] d_tab[0];
        delete[] d_tab;
        d_tab=nullptr;
        return;
    }

    for (int i=0;i<d_nl;i++)
    {
        double *prec_ligne=d_tab[i];
        d_tab[i]=new double[d_nc-1];
        for (int j=0;j<d_nc-1;j++)
            d_tab[i][j]=prec_ligne[j+1];
        delete[] prec_ligne;
    }
    d_nc--;
}
// FIN EXO_ENLEVER_UN_ELT_DEBUT


// DEBUT EXO_ENLEVER_UN_ELT_MILIEU
void Tableau2DNonContiguCorr::enleverUnEltMilieu(int idx)
{
    if (d_nc==0)
        return;
    if (idx>=d_nc)
        return;
    if (d_nc==1)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        delete[] d_tab[0];
        delete[] d_tab;
        d_tab=nullptr;
        return;
    }

    for (int i=0;i<d_nl;i++)
    {
        double *prec_ligne=d_tab[i];
        d_tab[i]=new double[d_nc-1];
        for (int j=0;j<idx;j++)
            d_tab[i][j]=prec_ligne[j];
        for (int j=idx;j<d_nc-1;j++)
            d_tab[i][j]=prec_ligne[j+1];
        delete[] prec_ligne;
    }
    d_nc--;
}
// FIN EXO_ENLEVER_UN_ELT_MILIEU


// DEBUT EXO_ENLEVER_UN_ELT_FIN
void Tableau2DNonContiguCorr::enleverUnEltFin()
{
    if (d_nc==0)
        return;
    if (d_nc==1)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        delete[] d_tab[0];
        delete[] d_tab;
        d_tab=nullptr;
        return;
    }

    for (int i=0;i<d_nl;i++)
    {
        double *prec_ligne=d_tab[i];
        d_tab[i]=new double[d_nc-1];
        for (int j=0;j<d_nc-1;j++)
            d_tab[i][j]=prec_ligne[j];
        delete[] prec_ligne;
    }
    d_nc--;
}
// FIN EXO_ENLEVER_UN_ELT_FIN


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
void Tableau2DNonContiguCorr::enleverPlusieursEltsDebut(int nb)
{
    if (d_nc==0)
        return;
    if (d_nc<=nb)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        delete[] d_tab[0];
        delete[] d_tab;
        d_tab=nullptr;
        return;
    }

    for (int i=0;i<d_nl;i++)
    {
        double *prec_ligne=d_tab[i];
        d_tab[i]=new double[d_nc-nb];
        for (int j=0;j<d_nc-nb;j++)
            d_tab[i][j]=prec_ligne[j+nb];
        delete[] prec_ligne;
    }
    d_nc-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU
void Tableau2DNonContiguCorr::enleverPlusieursEltsMilieu(int idx, int nb)
{
    if (d_nc==0)
        return;
    if (idx>=d_nc)
        return;
    if (d_nc<idx+nb)
        nb=d_nc-idx;
    if (d_nc<=nb)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        delete[] d_tab[0];
        delete[] d_tab;
        d_tab=nullptr;
        return;
    }

    for (int i=0;i<d_nl;i++)
    {
        double *prec_ligne=d_tab[i];
        d_tab[i]=new double[d_nc-nb];
        for (int j=0;j<idx;j++)
            d_tab[i][j]=prec_ligne[j];
        for (int j=idx;j<d_nc-nb;j++)
            d_tab[i][j]=prec_ligne[j+nb];
        delete[] prec_ligne;
    }
    d_nc-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_FIN
void Tableau2DNonContiguCorr::enleverPlusieursEltsFin(int nb)
{
    if (d_nc==0)
        return;
    if (d_nc<=nb)
    {
         // Le tableau devient vide
        d_nl=0;
        d_nc=0;
        delete[] d_tab[0];
        delete[] d_tab;
        d_tab=nullptr;
        return;
    }

    for (int i=0;i<d_nl;i++)
    {
        double *prec_ligne=d_tab[i];
        d_tab[i]=new double[d_nc-nb];
        for (int j=0;j<d_nc-nb;j++)
            d_tab[i][j]=prec_ligne[j];
        delete[] prec_ligne;
    }
    d_nc-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_FIN



// DEBUT EXO_ELTS_EN_ORDRE_DECROISSANT
bool Tableau2DNonContiguCorr::eltsEnOrdreDecroissant() const
{
    if (d_nl*d_nc==0 || d_nl*d_nc==1)
        return true;
    int i=0,j=0;
    int nextI=i,nextJ=j+1;
    if (nextJ==d_nc)
    {
        nextI++;
        nextJ=0;
    }
    while(nextI<d_nl && d_tab[i][j]>d_tab[nextI][nextJ])
    {
        i=nextI;
        j=nextJ;
        nextJ++;
        if (nextJ==d_nc)
        {
            nextI++;
            nextJ=0;
        }
    }
    return nextI==d_nl;
}
// FIN EXO_ELTS_EN_ORDRE_DECROISSANT


// DEBUT EXO_CONTIENT_ELT
bool Tableau2DNonContiguCorr::contientElt(double val) const
{
    int i=0,j=0;
    while(i<d_nl && d_tab[i][j]!=val)
    {
        j++;
        if (j==d_nc)
        {
            j=0;
            i++;
        }
    }
    return i<d_nl;
}
// FIN EXO_CONTIENT_ELT


// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
int Tableau2DNonContiguCorr::calculerNombreDOccurencesDUnElt(double val) const
{
    int nbOccurences=0;
    int i=0,j=0;
    while(i<d_nl)
    {
        if (d_tab[i][j]==val)
            nbOccurences++;
        j++;
        if (j==d_nc)
        {
            j=0;
            i++;
        }
    }
    return nbOccurences;
}
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT


// DEBUT EXO_OP_ADDITION
Tableau2DNonContiguCorr operator+(const Tableau2DNonContiguCorr& a, const Tableau2DNonContiguCorr& b)
{
    Tableau2DNonContiguCorr res(a);
    for (int i=0;i<res.d_nl;i++)
        for (int j=0;j<res.d_nc;j++)
            res.d_tab[i][j]+=b.d_tab[i][j];
    return res;
}
// FIN EXO_OP_ADDITION


// DEBUT EXO_OP_SOUSTRACTION
Tableau2DNonContiguCorr operator-(const Tableau2DNonContiguCorr& a, const Tableau2DNonContiguCorr& b)
{
    Tableau2DNonContiguCorr res(a);
    for (int i=0;i<res.d_nl;i++)
        for (int j=0;j<res.d_nc;j++)
            res.d_tab[i][j]-=b.d_tab[i][j];
    return res;
}
// FIN EXO_OP_SOUSTRACTION

