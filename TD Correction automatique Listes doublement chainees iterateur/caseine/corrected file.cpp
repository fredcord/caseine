#include "Exercices.h"



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le constructeur pour la structure de données ci-dessous
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
LDC::LDC():d_tete{nullptr}
{
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le destructeur pour la structure de données ci-dessous
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
LDC::~LDC()
{
    while(d_tete)
    {
        ChainonDC *tmp=d_tete->d_suiv;
        delete d_tete;
        d_tete=tmp;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le constructeur par recopie pour la structure de données ci-dessous
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
LDC::LDC(const LDC &l):d_tete{nullptr}
{
    ChainonDC* lc = l.d_tete, *prevC;
    while (lc)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        if (d_tete == nullptr)
            d_tete = nc;
        else
        {
            prevC->d_suiv = nc;
            nc->d_prec=prevC;
        }
        prevC = nc;
        lc = lc->d_suiv;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le constructeur qui prend comme paramètres un tableau avec sa
// taille: double *tabVal, int nbVal. Ce constructeur doit créer une
// structure de données contenant tous les éléments dans le tableau
// passé en paramètre
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
LDC::LDC(double *tabVal, int nbVal):d_tete(nullptr)
{
    ChainonDC *prevC=nullptr;
    for (int i=0;i<nbVal;i++)
    {
        ChainonDC *nc = new ChainonDC(tabVal[i]);
        if (d_tete == nullptr)
            d_tete = nc;
        else
        {
            prevC->d_suiv = nc;
            nc->d_prec=prevC;
        }
        prevC = nc;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode concatenationDebut(s) qui ajoute les elements de la
// structure de données s au début de la structure de données this. Cette
// structure de données s est passée en paramètre. L'ordre des éléments
// doit être gardé.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDC::concatenationDebut(const LDC &l)
{
    ChainonDC *c1=d_tete,*precC1=nullptr,*c2=l.d_tete;
    while(c2!=nullptr)
    {
        ChainonDC* nc=new ChainonDC(c2->d_v);
        if (precC1!=nullptr)
        {
            precC1->d_suiv=nc;
            nc->d_prec=precC1;
        }
        else
            d_tete=nc;
        if (c1!=nullptr)
        {
            c1->d_prec=nc;
            nc->d_suiv=c1;
        }
        precC1=nc;
        c2=c2->d_suiv;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode concatenationFin(s) qui ajoute les elements de la
// structure de données s à la fin de la structure de données this. Cette
// structure de données s est passée en paramètre. L'ordre des éléments
// doit être gardé.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDC::concatenationFin(const LDC &l)
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    ChainonDC *c2=l.d_tete;
    while(c2!=nullptr)
    {
        c1=new ChainonDC(c2->d_v);
        if (precC1!=nullptr)
        {
            precC1->d_suiv=c1;
            c1->d_prec=precC1;
        }
        else
            d_tete=c1;
        precC1=c1;
        c2=c2->d_suiv;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode operator+=(s) qui ajoute les elements de la structure de
// données s à la fin de la structure de données this. Cette structure de
// données s est passée en paramètre
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
LDC &LDC::operator+=(const LDC &l)
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    ChainonDC *c2=l.d_tete;
    while(c2!=nullptr)
    {
        c1=new ChainonDC(c2->d_v);
        if (precC1!=nullptr)
        {
            precC1->d_suiv=c1;
            c1->d_prec=precC1;
        }
        else
            d_tete=c1;
        precC1=c1;
        c2=c2->d_suiv;
    }

    return *this;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode operator==(s) qui retourne true si la structure de
// données s passée en paramètre est égale à la structure de données this.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
bool LDC::operator==(const LDC &l) const
{
    if (this==&l)
        return true;
    ChainonDC *c=d_tete, *lc=l.d_tete;
    while(c!=nullptr && lc!=nullptr && c->d_v== lc->d_v)
    {
        c=c->d_suiv;
        lc=lc->d_suiv;
    }
    return (c==nullptr && lc==nullptr);
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode operator!=(s) qui retourne true si la structure de
// données s passée en paramètre est différente à la structure de données this.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
bool LDC::operator!=(const LDC &l) const
{
    if (this==&l)
        return false;
    ChainonDC *c=d_tete, *lc=l.d_tete;
    while(c!=nullptr && lc!=nullptr && c->d_v== lc->d_v)
    {
        c=c->d_suiv;
        lc=lc->d_suiv;
    }
    return (c!=nullptr || lc!=nullptr);
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire l'opérateur d'affectation operator=(s). Après cette méthode, la structure
// de données this doit contenir les mêmes éléments que la structure de données s
// passée en paramètre.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
LDC &LDC::operator=(const LDC &l)
{
	if (this==&l)
		return *this;

	ChainonDC *c1=d_tete,*precC1=nullptr,*c2=l.d_tete;
	while (c1!=nullptr && c2!=nullptr)
	{
		c1->d_v=c2->d_v;
		precC1=c1;
		c1=c1->d_suiv;
		c2=c2->d_suiv;
	}
	if (c1!=nullptr)
	{
		if (precC1!=nullptr)
			precC1->d_suiv=nullptr;
		else
			d_tete=nullptr;
		while(c1)
		{
			precC1=c1;
			c1=c1->d_suiv;
			delete precC1;
		}
	}
	else if (c2!=nullptr)
	{
		while(c2!=nullptr)
		{
			c1=new ChainonDC(c2->d_v);
			if (precC1==nullptr)
				d_tete=c1;
			else
			{
				precC1->d_suiv=c1;
				c1->d_prec=precC1;
			}
			precC1=c1;
			c2=c2->d_suiv;
		}
	}
	return *this;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void additionnerATousLesElts(double val) qui ajoute la valeur
// val passée en paramètre à tous les éléments de la stucture de données.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDC::additionnerATousLesElts(double val)
{
    ChainonDC *c=d_tete;
    while(c!=nullptr)
    {
        c->d_v+=val;
        c=c->d_suiv;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererUnEltDebut(double val) qui insère un élément
// contenant la valeur val passée en paramètre. Cet élément est inséré au
// début de la stucture de données.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDC::insererUnEltDebut(double valeur)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(valeur);
    // Insertion dans une liste vide
    if (d_tete == nullptr) {
        d_tete = nc;
        return;
    }
    // Insertion en tete
    nc->d_suiv = d_tete;
    d_tete->d_prec=nc;
    d_tete = nc;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererUnEltMilieu(int idx, double val) qui
// insère un élément contenant la valeur val passée en paramètre. Cet élément
// est inséré à la position idx dans la stucture de données.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - L'index d'insertion peut être une valeur égale ou plus grande que la taille
//   de la structure de données. Dans ce cas, l'insertion se fait à la fin de la
//   structure de données.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDC::insererUnEltMilieu(int idx, double valeur)
{
    int i=0;
    ChainonDC *c=d_tete,*precC=nullptr;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }

    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(valeur);
    // Insertion entre precC et c
    nc->d_prec=precC;
    if (precC!=nullptr)
        precC->d_suiv=nc;
    else
        d_tete=nc;
    nc->d_suiv=c;
    if (c!=nullptr)
        c->d_prec=nc;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererUnEltFin(double val) qui insère un élément
// contenant la valeur val passée en paramètre. Cet élément est inséré à la
// fin de la stucture de données.
// Remarques:
// - La structure de données peut être vide
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDC::insererUnEltFin(double valeur)
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }

    c1=new ChainonDC(valeur);
    if (precC1!=nullptr)
    {
        precC1->d_suiv=c1;
        c1->d_prec=precC1;
    }
    else
        d_tete=c1;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererPlusieursEltsDebut(double *tabVal, int nbVal)
// qui insère nbVal éléments au début de la structures de données. Les éléments
// à ajouter sont le tableau tabVal. L'ordre des éléments dans le tableau doit
// être gardé dans la structure de données.
// Remarques:
// - Le paramètre nbVal peut être égal à zéro
// - La structure de données peut être vide
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDC::insererPlusieursEltsDebut(double *tabVal, int nbVal)
{
    ChainonDC *precC=nullptr,*c=d_tete;
    for (int i=0;i<nbVal;i++)
    {
        ChainonDC *nc=new ChainonDC(tabVal[i]);
        // Insertion entre precC et c
        if (precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;
        nc->d_prec=precC;

        nc->d_suiv=c;
        if (c!=nullptr)
            c->d_prec=nc;
        precC=nc;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererPlusieursEltsMilieu(int idx, double *tabVal, int nbVal)
// qui insère nbVal éléments à la position idx dans la structures de données. Les éléments
// à ajouter sont le tableau tabVal. L'ordre des éléments dans le tableau doit
// être gardé dans la structure de données.
// Remarques:
// - Le paramètre nbVal peut être égal à 0.
// - L'index du premier élément de la structure de données est 0.
// - L'index d'insertion idx peut être une valeur égale ou plus grande que la taille
//   de la structure de données. Dans ce cas, l'insertion se fait à la fin de la
//   structure de données.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDC::insererPlusieursEltsMilieu(int idx, double *tabVal, int nbVal)
{
    ChainonDC *precC=nullptr,*c=d_tete;
    int i=0;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }

    for (int j=0;j<nbVal;j++)
    {
        ChainonDC *nc=new ChainonDC(tabVal[j]);
        // Insertion entre precC et c
        if (precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;
        nc->d_prec=precC;

        nc->d_suiv=c;
        if (c!=nullptr)
            c->d_prec=nc;
        precC=nc;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererPlusieursEltsFin(double *tabVal, int nbVal)
// qui insère nbVal éléments à la fin de la structures de données. Les éléments
// à ajouter sont le tableau tabVal. L'ordre des éléments dans le tableau doit
// être gardé dans la structure de données.
// Remarques:
// - Le paramètre nbVal peut être égal à zéro
// - La structure de données peut être vide
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDC::insererPlusieursEltsFin(double *tabVal, int nbVal)
{
    ChainonDC *precC=nullptr,*c=d_tete;
    while(c!=nullptr)
    {
        precC=c;
        c=c->d_suiv;
    }

    for (int j=0;j<nbVal;j++)
    {
        ChainonDC *nc=new ChainonDC(tabVal[j]);
        // Insertion apres precC
        if (precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;
        nc->d_prec=precC;

        precC=nc;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverUnEltDebut() qui enlève le premier élément
// de la structure de données.
// Remarque:
// - La structure de données peut être vide
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDC::enleverUnEltDebut()
{
    ChainonDC *precC=nullptr;
    if(d_tete!=nullptr)
    {
        precC=d_tete;
        d_tete=d_tete->d_suiv;
        delete precC;
        if (d_tete!=nullptr)
            d_tete->d_prec=nullptr;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverUnEltMilieu(int idx) qui enlève
// l'élément d'index idx dans la structure de données.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - La structure de données peut être vide.
// - La valeur idx peut être plus grande ou égale à la taille de la structure
//   de données. Dans ce cas, l'élément n'est pas supprimé.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDC::enleverUnEltMilieu(int idx)
{
    ChainonDC *c=d_tete, *precC=nullptr;
    int i=0;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }
    if(c!=nullptr)
    {
        ChainonDC *nextC=c->d_suiv;
        delete c;
        if (precC)
            precC->d_suiv=nextC;
        else
            d_tete=nextC;
        if (nextC)
            nextC->d_prec=precC;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverUnEltFin() qui enlève le dernier élément
// de la structure de données.
// Remarque:
// - La structure de données peut être vide
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDC::enleverUnEltFin()
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    if(precC1!=nullptr)
    {
        c1=precC1;
        precC1=precC1->d_prec;
        delete c1;
    }
    if (precC1==nullptr)
        d_tete=nullptr;
    else
        precC1->d_suiv=nullptr;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverPlusieursEltsDebut(int nb) qui enlève nb
// éléments au début de la structures de données. Cette valeur nb est passée
// en paramètre.
// Remarque:
// - La valeur nb peut être plus grande que la taille de la structures de
// données.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDC::enleverPlusieursEltsDebut(int nb)
{
    ChainonDC *precC=nullptr;
    int i=0;
    while(d_tete!=nullptr && i<nb)
    {
        precC=d_tete;
        d_tete=d_tete->d_suiv;
        delete precC;
        if (d_tete!=nullptr)
            d_tete->d_prec=nullptr;
        i++;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverPlusieursEltsMilieu(int idx, int nb) qui
// enlève nb éléments à partir de l'élément d'index idx. Ces valeurs idx et
// nb sont passées en paramètre.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - La valeur nb peut être plus grande que la taille de la structures de
// données.
// - La valeur idx peut être plus grande ou égale à la taille de la structure
// de données. Dans ce cas, les éléments ne sont pas supprimés.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDC::enleverPlusieursEltsMilieu(int idx, int nb)
{
    ChainonDC *c=d_tete, *precC=nullptr;
    int i=0;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }
    if (c==nullptr)
        return;
    i=0;
    while(c!=nullptr && i<nb)
    {
        ChainonDC *nextC=c->d_suiv;
        delete c;
        if (precC)
            precC->d_suiv=nextC;
        else
            d_tete=nextC;
        if (nextC)
            nextC->d_prec=precC;

        c=nextC;
        i++;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverPlusieursEltsFin(int nb) qui enlève nb
// éléments à la fin de la structures de données. Cette valeur nb est passée
// en paramètre.
// Remarque:
// - La valeur nb peut être plus grande que la taille de la structures de
// données.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDC::enleverPlusieursEltsFin(int nb)
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }

    int i=0;
    while(precC1!=nullptr && i<nb)
    {
        c1=precC1;
        precC1=precC1->d_prec;
        delete c1;
        i++;
    }
    if (precC1==nullptr)
        d_tete=nullptr;
    else
        precC1->d_suiv=nullptr;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode eltsEnOrdreDecroissant() qui retourne true si les éléments
// dans la structures de données sont classés par ordre strictement décroissant.
// Remarque:
// - eltsEnOrdreDecroissant() doit retourner true pour une structure de données vide
// ou ne contenant qu'un seul élément.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
bool LDC::eltsEnOrdreDecroissant() const
{
    if (d_tete==nullptr || d_tete->d_suiv==nullptr)
        return true;
    ChainonDC *c=d_tete;
    while(c->d_suiv!=nullptr && c->d_v>c->d_suiv->d_v)
    {
        c=c->d_suiv;
    }
    return (c->d_suiv==nullptr);
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode contientElt(double val) qui retourne true si la structure
// de données contient au moins un élément avec la valeur val, val étant un
// paramètre de la méthode.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
bool LDC::contientElt(double val) const
{
    ChainonDC *c=d_tete;
    while(c!=nullptr && val!=c->d_v)
    {
        c=c->d_suiv;
    }
    return (c!=nullptr);
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverToutesLesOccurencesDUnElt(double val) qui
// enlève tous les éléments contenant la valeur val dans la structure de
// données, val étant un paramètre de la méthode.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LDC::enleverToutesLesOccurencesDUnElt(double val)
{
    ChainonDC *c=d_tete,*precC=nullptr;
    while(c!=nullptr)
    {
        if (c->d_v==val)
        {
            ChainonDC *nextC=c->d_suiv;
            delete c;
            if (precC)
                precC->d_suiv=nextC;
            else
                d_tete=nextC;
            if (nextC)
                nextC->d_prec=precC;
            c=nextC;
        }
        else
        {
            precC=c;
            c=c->d_suiv;
        }
    }
}



