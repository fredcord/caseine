#include <iostream>
#include <algorithm>
#include "ExercicesBase.h"


using namespace std;


//////////////////////////////////////////////////////////////////////////////////////////
// Verifie si la liste chainee est correcte au niveau de sa sctructure et des pointeurs //
//////////////////////////////////////////////////////////////////////////////////////////
bool LDCCorr::verifierIntegrite(const LDC &param)
{
    // Liste chainee vide
    if (param.d_ancre.d_suivant==&param.d_ancre)
        return param.d_ancre.d_precedent==&param.d_ancre;
    if (param.d_ancre.d_precedent==&param.d_ancre)
        return param.d_ancre.d_suivant==&param.d_ancre;

    ChainonDC *c=param.d_ancre.d_suivant;
    int compteur=0;
    vector<ChainonDC*> pntrUilise;
    while(c!=&param.d_ancre && compteur<StructDeDonneesVerifCorr::NB_MAX_COLONNES)
    {
        // Validite des pointeurs
        if (!GestionPntrEtComplexite::get().pntrValide(c))
            return false;
        if (c->d_precedent==nullptr)
            return false;
        if (c->d_suivant==nullptr)
            return false;
        if (c->d_suivant!=&param.d_ancre)
        {
            if (!GestionPntrEtComplexite::get().pntrValide(c->d_suivant))
                return false;
        }
        if (c->d_suivant->d_precedent!=c)
            return false;
        if (c->d_precedent!=&param.d_ancre)
        {
            if (!GestionPntrEtComplexite::get().pntrValide(c->d_precedent))
                return false;
        }
        if (c->d_precedent->d_suivant!=c)
            return false;
        // Verifier si le pointeur est bien unique
        if(std::find(pntrUilise.begin(), pntrUilise.end(), c)!=pntrUilise.end())
            return false;
        pntrUilise.push_back(c);

        c=c->d_suivant;
        compteur++;
    }
    return compteur<StructDeDonneesVerifCorr::NB_MAX_COLONNES;
}

// Compare la correction avec la struct de l'etudiant. Retourne false si different
bool LDCCorr::compareCorrAvecEtud(const LDC &etudiant) const
{
    ChainonDC *c1=this->d_ancre.d_suivant,*c2=etudiant.d_ancre.d_suivant;

    // Cas de la chaine vide
    if (c1==&d_ancre && c2!=&etudiant.d_ancre)
        return false;
    if (c1!=&d_ancre && c2==&etudiant.d_ancre)
        return false;

    vector<ChainonDC*> pntrCorr;
    while(c1!=&d_ancre)
    {
        // Verifie la longueur de la liste chainee
        if (c2==&etudiant.d_ancre)
            return false;

        // Verifier si le pointeur c2 n'est pas un pointeur de la liste correction
        if(std::find(pntrCorr.begin(), pntrCorr.end(), c2)!=pntrCorr.end())
            return false;
        pntrCorr.push_back(c1);

        // Verifie si les chainons contiennent la meme valeur
        if (c1->d_v!=c2->d_v)
            return false;

        c1=c1->d_suivant;
        c2=c2->d_suivant;
    }
    return true;
}

// Compare les iterateurs
bool LDCCorr::compareCorrAvecEtud(const Iterateur& corrIt, const LDC &etudLdc,
                                  const Iterateur& etudIt) const
{
    // Verifie le pointeur sur l'ancre pour l'iterateur de l'etudiant
    if (etudIt.d_ancre!=&etudLdc.d_ancre)
        return false;
    // Verifie la position du chainon courrant d_crt pour l'iterateur de l'etudiant
    int posCorr=0;
    ChainonDC *c=d_ancre.d_suivant;
    while(c!=corrIt.d_crt)
    {
        c=c->d_suivant;
        posCorr++;
    }
    int posEtud=0;
    c=etudLdc.d_ancre.d_suivant;
    while(c!=etudIt.d_crt && posEtud<posCorr)
    {
        c=c->d_suivant;
        posEtud++;
    }
    return (c==etudIt.d_crt);
}

// genere la structDeDonneesVerifCorr a partir de la liste
StructDeDonneesVerifCorr LDCCorr::getStructDeDonneesVerifCorr() const
{
    StructDeDonneesVerifCorr vect;
    ChainonDC *c=d_ancre.d_suivant;
    while(c!=&d_ancre)
    {
        vect.insererUnEltFin(c->d_v);
        c=c->d_suivant;
    }
    return vect;
}

// Constructeur avec la structDeDonneesVerifCorr
void LDCCorr::setStructDeDonneesVerifCorr(const StructDeDonneesVerifCorr &tab)
{
    // La liste est supposee etre vide
    d_ancre.d_suivant=&d_ancre;
    d_ancre.d_precedent=&d_ancre;

    if (tab.getNbColonnes()==0)
        return;
    // On insere les elements du tableau
    ChainonDC *precC=&d_ancre, *currC=nullptr;
    for (int i=0;i<tab.getNbColonnes();i++)
    {
        currC=new ChainonDC(tab[i]);
        precC->d_suivant=currC;
        currC->d_precedent=precC;
        precC=currC;
    }
    currC->d_suivant=&d_ancre;
    d_ancre.d_precedent=currC;
}


// DEBUT EXO_CONSTRUCTEUR
LDCCorr::LDCCorr():d_ancre{0}
{
    d_ancre.d_suivant=&d_ancre;
    d_ancre.d_precedent=&d_ancre;
}
// FIN EXO_CONSTRUCTEUR


// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
LDCCorr::LDCCorr(const LDCCorr &param):d_ancre{0}
{
    ChainonDC* lc = param.d_ancre.d_suivant, *precC=&d_ancre;
    while (lc!=&param.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        precC->d_suivant = nc;
        nc->d_precedent=precC;

        precC = nc;
        lc = lc->d_suivant;
    }
    precC->d_suivant=&d_ancre;
    d_ancre.d_precedent=precC;
}
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE


// DEBUT EXO_CONSTRUCTEUR_AVEC_TABLEAU
LDCCorr::LDCCorr(double *tab, int nb):d_ancre{0}
{
    ChainonDC* precC=&d_ancre;
    for (int i=0;i<nb;i++)
    {
        ChainonDC *nc = new ChainonDC(tab[i]);
        precC->d_suivant = nc;
        nc->d_precedent=precC;
        precC = nc;
    }
    precC->d_suivant=&d_ancre;
    d_ancre.d_precedent=precC;
}
// FIN EXO_CONSTRUCTEUR_AVEC_TABLEAU


// DEBUT EXO_DESTRUCTEUR
LDCCorr::~LDCCorr()
{
    while(d_ancre.d_suivant!=&d_ancre)
    {
        ChainonDC *tmp=d_ancre.d_suivant;
        d_ancre.d_suivant=d_ancre.d_suivant->d_suivant;
        delete tmp;
    }
}
// FIN EXO_DESTRUCTEUR


// DEBUT EXO_CONCATENATION_DEBUT
void LDCCorr::concatenationDebut(const LDCCorr &param)
{
    ChainonDC* lc = param.d_ancre.d_suivant, *precC=&d_ancre;
    ChainonDC* nextC=d_ancre.d_suivant;
    while (lc!=&param.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        precC->d_suivant = nc;
        nc->d_precedent=precC;

        precC = nc;
        lc = lc->d_suivant;
    }
    precC->d_suivant=nextC;
    nextC->d_precedent=precC;
}
// FIN EXO_CONCATENATION_DEBUT


// DEBUT EXO_CONCATENATION_MILIEU
void LDCCorr::concatenationMilieu(int idx, const LDCCorr &param)
{
    ChainonDC *precC=&d_ancre;
    int i=0;
    while(precC->d_suivant!=&d_ancre && i<idx)
    {
        precC=precC->d_suivant;
        i++;
    }

    ChainonDC* lc = param.d_ancre.d_suivant;
    ChainonDC* nextC=precC->d_suivant;
    while (lc!=&param.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        precC->d_suivant = nc;
        nc->d_precedent=precC;

        precC = nc;
        lc = lc->d_suivant;
    }
    precC->d_suivant=nextC;
    nextC->d_precedent=precC;
}
// FIN EXO_CONCATENATION_MILIEU


// DEBUT EXO_CONCATENATION_FIN
void LDCCorr::concatenationFin(const LDCCorr &param)
{
    ChainonDC* lc = param.d_ancre.d_suivant, *precC=d_ancre.d_precedent;
    ChainonDC* nextC=&d_ancre;
    while (lc!=&param.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        precC->d_suivant = nc;
        nc->d_precedent=precC;

        precC = nc;
        lc = lc->d_suivant;
    }
    precC->d_suivant=nextC;
    nextC->d_precedent=precC;
}
// FIN EXO_CONCATENATION_FIN


// DEBUT EXO_OP_CONCATENATION
LDCCorr &LDCCorr::operator+=(const LDCCorr &param)
{
    ChainonDC* lc = param.d_ancre.d_suivant, *precC=d_ancre.d_precedent;
    ChainonDC* nextC=&d_ancre;
    while (lc!=&param.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        precC->d_suivant = nc;
        nc->d_precedent=precC;

        precC = nc;
        lc = lc->d_suivant;
    }
    precC->d_suivant=nextC;
    nextC->d_precedent=precC;

    return *this;
}
// FIN EXO_OP_CONCATENATION


// DEBUT EXO_OP_EGAL
bool LDCCorr::operator==(const LDCCorr &param) const
{
    if (this==&param)
        return true;
    ChainonDC *c=d_ancre.d_suivant, *lc=param.d_ancre.d_suivant;
    while(c!=&d_ancre && lc!=&param.d_ancre && c->d_v== lc->d_v)
    {
        c=c->d_suivant;
        lc=lc->d_suivant;
    }
    return (c==&d_ancre && lc==&param.d_ancre);
}
// FIN EXO_OP_EGAL


// DEBUT EXO_OP_DIFFERENT
bool LDCCorr::operator!=(const LDCCorr &param) const
{
    if (this==&param)
        return true;
    ChainonDC *c=d_ancre.d_suivant, *lc=param.d_ancre.d_suivant;
    while(c!=&d_ancre && lc!=&param.d_ancre && c->d_v== lc->d_v)
    {
        c=c->d_suivant;
        lc=lc->d_suivant;
    }
    return (c!=&d_ancre && lc!=&param.d_ancre);
}
// FIN EXO_OP_DIFFERENT


// DEBUT EXO_OP_AFFECTATION
LDCCorr &LDCCorr::operator=(const LDCCorr &param)
{
	if (this==&param)
		return *this;

    ChainonDC *precC1=&d_ancre,*c1=d_ancre.d_suivant, *c2=param.d_ancre.d_suivant;
    while(c1!=&d_ancre && c2!=&param.d_ancre)
	{
		c1->d_v=c2->d_v;
		precC1=c1;
		c1=c1->d_suivant;
		c2=c2->d_suivant;
	}
	if (c1!=&d_ancre)
	{
		while(c1!=&d_ancre)
		{
			precC1->d_suivant=c1->d_suivant;
			c1->d_suivant->d_precedent=precC1;
			delete c1;
            c1=precC1->d_suivant;
		}
	}
	else if (c2!=&param.d_ancre)
	{
		while(c2!=&param.d_ancre)
		{
			c1=new ChainonDC(c2->d_v);
			precC1->d_suivant=c1;
			c1->d_precedent=precC1;
			precC1=c1;
			c2=c2->d_suivant;
		}
        d_ancre.d_precedent=c1;
        c1->d_suivant=&d_ancre;
	}
	return *this;
}
// FIN EXO_OP_AFFECTATION


// DEBUT EXO_OP_CROCHET
double &LDCCorr::operator[](int i)
{
    ChainonDC *c=d_ancre.d_suivant;
    int compteur=0;
    while(c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    return c->d_v;
}
// FIN EXO_OP_CROCHET


// DEBUT EXO_OP_CROCHET_CONST
double LDCCorr::operator[](int i) const
{
    ChainonDC *c=d_ancre.d_suivant;
    int compteur=0;
    while(c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    return c->d_v;
}
// FIN EXO_OP_CROCHET_CONST


// DEBUT EXO_OP_PARENTHESE
double &LDCCorr::operator()(int i)
{
    ChainonDC *c=d_ancre.d_suivant;
    int compteur=0;
    while(c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    return c->d_v;
}
// FIN EXO_OP_PARENTHESE


// DEBUT EXO_OP_PARENTHESE_CONST
double LDCCorr::operator()(int i) const
{
    ChainonDC *c=d_ancre.d_suivant;
    int compteur=0;
    while(c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    return c->d_v;
}
// FIN EXO_OP_PARENTHESE_CONST


// DEBUT EXO_SETTER
void LDCCorr::set(int i, double val)
{
    ChainonDC *c=d_ancre.d_suivant;
    int compteur=0;
    while(c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    c->d_v=val;
}
// FIN EXO_SETTER


// DEBUT EXO_GETTER
double LDCCorr::get(int i) const
{
    ChainonDC *c=d_ancre.d_suivant;
    int compteur=0;
    while(c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    return c->d_v;
}
// FIN EXO_GETTER


// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
void LDCCorr::additionnerATousLesElts(double val)
{
    ChainonDC *c=d_ancre.d_suivant;
    while(c!=&d_ancre)
    {
        c->d_v+=val;
        c=c->d_suivant;
    }
}
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS


// DEBUT EXO_INSERER_UN_ELT_DEBUT
void LDCCorr::insererUnEltDebut(double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion entre d_andre et d_ancre.d_suivant
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    precC->d_suivant = nc;
    nc->d_precedent=precC;
    nc->d_suivant = c;
    c->d_precedent=nc;
}
// FIN EXO_INSERER_UN_ELT_DEBUT


// DEBUT EXO_INSERER_UN_ELT_MILIEU
void LDCCorr::insererUnEltMilieu(int idx, double val)
{
    int i=0;
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    while(c!=&d_ancre && i<idx)
    {
        precC=c;
        c=c->d_suivant;
        i++;
    }

    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion entre precC et c
    precC->d_suivant = nc;
    nc->d_precedent=precC;
    nc->d_suivant = c;
    c->d_precedent=nc;
}
// FIN EXO_INSERER_UN_ELT_MILIEU


// DEBUT EXO_INSERER_UN_ELT_FIN
void LDCCorr::insererUnEltFin(double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion entre d_andre et d_ancre.d_suivant
    ChainonDC *precC=d_ancre.d_precedent,*c=&d_ancre;
    precC->d_suivant = nc;
    nc->d_precedent=precC;
    nc->d_suivant = c;
    c->d_precedent=nc;
}
// FIN EXO_INSERER_UN_ELT_FIN


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_DEBUT
void LDCCorr::insererPlusieursEltsDebut(double *tab, int nb)
{
    // Insertion entre d_andre et d_ancre.d_suivant
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    for (int i=0;i<nb;i++)
    {
        ChainonDC *nc=new ChainonDC(tab[i]);
        // Insertion entre precC et c
        precC->d_suivant=nc;
        nc->d_precedent=precC;
        nc->d_suivant=c;
        c->d_precedent=nc;
        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_MILIEU
void LDCCorr::insererPlusieursEltsMilieu(int idx, double *tab, int nb)
{
    int i=0;
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    while(c!=&d_ancre && i<idx)
    {
        precC=c;
        c=c->d_suivant;
        i++;
    }

    for (int i=0;i<nb;i++)
    {
        ChainonDC *nc=new ChainonDC(tab[i]);
        // Insertion entre precC et c
        precC->d_suivant=nc;
        nc->d_precedent=precC;
        nc->d_suivant=c;
        c->d_precedent=nc;
        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_FIN
void LDCCorr::insererPlusieursEltsFin(double *tab, int nb)
{
    // Insertion entre d_andre et d_ancre.d_suivant
    ChainonDC *precC=d_ancre.d_precedent,*c=&d_ancre;
    for (int i=0;i<nb;i++)
    {
        ChainonDC *nc=new ChainonDC(tab[i]);
        // Insertion entre precC et c
        precC->d_suivant=nc;
        nc->d_precedent=precC;
        nc->d_suivant=c;
        c->d_precedent=nc;
        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ENLEVER_UN_ELT_DEBUT
void LDCCorr::enleverUnEltDebut()
{
    if (d_ancre.d_suivant==&d_ancre)
        return;
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant->d_suivant;
    delete d_ancre.d_suivant;
    precC->d_suivant=c;
    c->d_precedent=precC;
}
// FIN EXO_ENLEVER_UN_ELT_DEBUT


// DEBUT EXO_ENLEVER_UN_ELT_MILIEU
void LDCCorr::enleverUnEltMilieu(int idx)
{
    int i=0;
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    while(c!=&d_ancre && i<idx)
    {
        precC=c;
        c=c->d_suivant;
        i++;
    }
    if (c==&d_ancre)
        return;
    c=c->d_suivant;
    delete precC->d_suivant;
    precC->d_suivant=c;
    c->d_precedent=precC;
}
// FIN EXO_ENLEVER_UN_ELT_MILIEU


// DEBUT EXO_ENLEVER_UN_ELT_FIN
void LDCCorr::enleverUnEltFin()
{
    if (d_ancre.d_precedent==&d_ancre)
        return;
    ChainonDC *precC=d_ancre.d_precedent->d_precedent,*c=&d_ancre;
    delete d_ancre.d_precedent;
    precC->d_suivant=c;
    c->d_precedent=precC;
}
// FIN EXO_ENLEVER_UN_ELT_FIN


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
void LDCCorr::enleverPlusieursEltsDebut(int nb)
{
    int i=0;
    while(d_ancre.d_suivant!=&d_ancre && i<nb)
    {
        ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant->d_suivant;
        delete d_ancre.d_suivant;
        precC->d_suivant=c;
        c->d_precedent=precC;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU
void LDCCorr::enleverPlusieursEltsMilieu(int idx, int nb)
{
    int i=0;
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    while(c!=&d_ancre && i<idx)
    {
        precC=c;
        c=c->d_suivant;
        i++;
    }
    i=0;
    while(precC->d_suivant!=&d_ancre && i<nb)
    {
        c=precC->d_suivant->d_suivant;
        delete precC->d_suivant;
        precC->d_suivant=c;
        c->d_precedent=precC;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_FIN
void LDCCorr::enleverPlusieursEltsFin(int nb)
{
    int i=0;
    while(d_ancre.d_precedent!=&d_ancre && i<nb)
    {
        ChainonDC *precC=d_ancre.d_precedent->d_precedent, *c=&d_ancre;
        delete d_ancre.d_precedent;
        precC->d_suivant=c;
        c->d_precedent=precC;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ELTS_EN_ORDRE_DECROISSANT
bool LDCCorr::eltsEnOrdreDecroissant() const
{
    if (d_ancre.d_suivant==&d_ancre || d_ancre.d_suivant->d_suivant==&d_ancre)
        return true;
    ChainonDC *c=d_ancre.d_suivant;
    while(c->d_suivant!=&d_ancre && c->d_v>c->d_suivant->d_v)
    {
        c=c->d_suivant;
    }
    return (c->d_suivant==&d_ancre);
}
// FIN EXO_ELTS_EN_ORDRE_DECROISSANT


// DEBUT EXO_CONTIENT_ELT
bool LDCCorr::contientElt(double val) const
{
    ChainonDC *c=d_ancre.d_suivant;
    while(c!=&d_ancre && c->d_v!=val)
    {
        c=c->d_suivant;
    }
    return c!=&d_ancre;
}
// FIN EXO_CONTIENT_ELT


// DEBUT EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT
void LDCCorr::enleverToutesLesOccurencesDUnElt(double val)
{
    ChainonDC *c=d_ancre.d_suivant;
    while(c!=&d_ancre)
    {
        if (c->d_v==val)
        {
            ChainonDC *precC=c->d_precedent;
            c=c->d_suivant;
            delete c->d_precedent;
            precC->d_suivant=c;
            c->d_precedent=precC;
        }
        else
            c=c->d_suivant;
    }
}
// FIN EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT
int LDCCorr::chercherLIndexDeLaPremiereOccurenceDUnElt(double val) const
{
    ChainonDC *c=d_ancre.d_suivant;
    int i=0;
    while(c!=&d_ancre && c->d_v!=val)
    {
        c=c->d_suivant;
        i++;
    }
    if (c!=&d_ancre)
        return i;
    return -1;
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT
int LDCCorr::chercherLIndexDeLaDerniereOccurenceDUnElt(double val) const
{
    ChainonDC *c=d_ancre.d_suivant;
    int i=0,idx=-1;
    while(c!=&d_ancre)
    {
        if (c->d_v==val)
            idx=i;
        c=c->d_suivant;
        i++;
    }
    return idx;
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
int LDCCorr::calculerNombreDOccurencesDUnElt(double val) const
{
    int idxNbOccurences=0;
    ChainonDC *c=d_ancre.d_suivant;
    while(c!=&d_ancre)
    {
        if (c->d_v==val)
            idxNbOccurences++;
        c=c->d_suivant;
    }
    return idxNbOccurences;
}
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT


// DEBUT EXO_OP_ADDITION
LDCCorr operator+(const LDCCorr& l1, const LDCCorr& l2)
{
    LDCCorr res;
    ChainonDC *c1=l1.d_ancre.d_suivant,*c2=l2.d_ancre.d_suivant;
    while(c1!=&l1.d_ancre && c2!=&l2.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(c1->d_v+c2->d_v);
        // Insertion entre d_andre et d_ancre.d_suivant
        ChainonDC *precC=res.d_ancre.d_precedent,*c=&res.d_ancre;
        precC->d_suivant = nc;
        nc->d_precedent=precC;
        nc->d_suivant = c;
        c->d_precedent=nc;

        c1=c1->d_suivant;
        c2=c2->d_suivant;
    }
    return res;
}
// FIN EXO_OP_ADDITION


// DEBUT EXO_OP_SOUSTRACTION
LDCCorr operator-(const LDCCorr& l1, const LDCCorr& l2)
{
    LDCCorr res;
    ChainonDC *c1=l1.d_ancre.d_suivant,*c2=l2.d_ancre.d_suivant;
    while(c1!=&l1.d_ancre && c2!=&l2.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(c1->d_v-c2->d_v);
        // Insertion entre d_andre et d_ancre.d_suivant
        ChainonDC *precC=res.d_ancre.d_precedent,*c=&res.d_ancre;
        precC->d_suivant = nc;
        nc->d_precedent=precC;
        nc->d_suivant = c;
        c->d_precedent=nc;

        c1=c1->d_suivant;
        c2=c2->d_suivant;
    }
    return res;
}
// FIN EXO_OP_SOUSTRACTION


// DEBUT EXO_CREER_ITERATEUR
LDCCorr::Iterateur::Iterateur(ChainonDC* ancre):d_ancre{ancre},d_crt{ancre}
{
}

LDCCorr::Iterateur LDCCorr::premier()
{
    return LDCCorr::Iterateur{&d_ancre};
}
// FIN EXO_CREER_ITERATEUR


// DEBUT EXO_OP_PLUS_PLUS_ITERATEUR
LDCCorr::Iterateur& LDCCorr::Iterateur::operator++() {
    d_crt = d_crt->d_suivant;
    return *this;
}
// FIN EXO_OP_PLUS_PLUS_ITERATEUR


// DEBUT EXO_OP_MOINS_MOINS_ITERATEUR
LDCCorr::Iterateur& LDCCorr::Iterateur::operator--() {
    d_crt = d_crt->d_precedent;
    return *this;
}
// FIN EXO_OP_MOINS_MOINS_ITERATEUR


// DEBUT EXO_OP_ETOILE_ITERATEUR
double& LDCCorr::Iterateur::operator*() {
    return d_crt->d_v;
}
// FIN EXO_OP_ETOILE_ITERATEUR


// DEBUT EXO_OP_ETOILE_CONST_ITERATEUR
double LDCCorr::Iterateur::operator*() const {
    return d_crt->d_v;
}
// FIN EXO_OP_ETOILE_CONST_ITERATEUR


// DEBUT EXO_OP_FIN_ITERATEUR
bool LDCCorr::Iterateur::fin() const {
    return d_crt==d_ancre;
}
// FIN EXO_OP_FIN_ITERATEUR
