#include <iostream>
#include "ExercicesBase.h"

using namespace std;


// DEBUT EXO_CONSTRUCTEUR
LDC::LDC():d_ancre{0}
{
    d_ancre.d_suivant=&d_ancre;
    d_ancre.d_precedent=&d_ancre;
}
// FIN EXO_CONSTRUCTEUR



// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
LDC::LDC(const LDC &param):d_ancre{0}
{
    ChainonDC* lc = param.d_ancre.d_suivant, *precC=&d_ancre;
    while (lc!=&param.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        precC->d_suivant = nc;
        nc->d_precedent=precC;

        precC = nc;
        lc = lc->d_suivant;
    }
    precC->d_suivant=&d_ancre;
    d_ancre.d_precedent=precC;
}
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE



// DEBUT EXO_CONSTRUCTEUR_AVEC_TABLEAU
LDC::LDC(double *tab, int nb):d_ancre{0}
{
    ChainonDC* precC=&d_ancre;
    for (int i=0;i<nb;i++)
    {
        ChainonDC *nc = new ChainonDC(tab[i]);
        precC->d_suivant = nc;
        nc->d_precedent=precC;
        precC = nc;
    }
    precC->d_suivant=&d_ancre;
    d_ancre.d_precedent=precC;
}
// FIN EXO_CONSTRUCTEUR_AVEC_TABLEAU


// DEBUT EXO_DESTRUCTEUR
LDC::~LDC()
{
    while(d_ancre.d_suivant!=&d_ancre)
    {
        ChainonDC *tmp=d_ancre.d_suivant;
        d_ancre.d_suivant=d_ancre.d_suivant->d_suivant;
        delete tmp;
    }
}
// FIN EXO_DESTRUCTEUR


// DEBUT EXO_CONCATENATION_DEBUT
void LDC::concatenationDebut(const LDC &param)
{
    ChainonDC* lc = param.d_ancre.d_suivant, *precC=&d_ancre;
    ChainonDC* nextC=d_ancre.d_suivant;
    while (lc!=&param.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        precC->d_suivant = nc;
        nc->d_precedent=precC;

        precC = nc;
        lc = lc->d_suivant;
    }
    precC->d_suivant=nextC;
    nextC->d_precedent=precC;
}
// FIN EXO_CONCATENATION_DEBUT


// DEBUT EXO_CONCATENATION_MILIEU
void LDC::concatenationMilieu(int idx, const LDC &param)
{
    ChainonDC *precC=&d_ancre;
    int i=0;
    while(precC->d_suivant!=&d_ancre && i<idx)
    {
        precC=precC->d_suivant;
        i++;
    }

    ChainonDC* lc = param.d_ancre.d_suivant;
    ChainonDC* nextC=precC->d_suivant;
    while (lc!=&param.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        precC->d_suivant = nc;
        nc->d_precedent=precC;

        precC = nc;
        lc = lc->d_suivant;
    }
    precC->d_suivant=nextC;
    nextC->d_precedent=precC;
}
// FIN EXO_CONCATENATION_MILIEU


// DEBUT EXO_CONCATENATION_FIN
void LDC::concatenationFin(const LDC &param)
{
    ChainonDC* lc = param.d_ancre.d_suivant, *precC=d_ancre.d_precedent;
    ChainonDC* nextC=&d_ancre;
    while (lc!=&param.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        precC->d_suivant = nc;
        nc->d_precedent=precC;

        precC = nc;
        lc = lc->d_suivant;
    }
    precC->d_suivant=nextC;
    nextC->d_precedent=precC;
}
// FIN EXO_CONCATENATION_FIN



// DEBUT EXO_OP_CONCATENATION
LDC &LDC::operator+=(const LDC &param)
{
    ChainonDC* lc = param.d_ancre.d_suivant, *precC=d_ancre.d_precedent;
    ChainonDC* nextC=&d_ancre;
    while (lc!=&param.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        precC->d_suivant = nc;
        nc->d_precedent=precC;

        precC = nc;
        lc = lc->d_suivant;
    }
    precC->d_suivant=nextC;
    nextC->d_precedent=precC;

    return *this;
}
// FIN EXO_OP_CONCATENATION



// DEBUT EXO_OP_EGAL
bool LDC::operator==(const LDC &param) const
{
    if (this==&param)
        return true;
    ChainonDC *c=d_ancre.d_suivant, *lc=param.d_ancre.d_suivant;
    while(c!=&d_ancre && lc!=&param.d_ancre && c->d_v== lc->d_v)
    {
        c=c->d_suivant;
        lc=lc->d_suivant;
    }
    return (c==&d_ancre && lc==&param.d_ancre);
}
// FIN EXO_OP_EGAL



// DEBUT EXO_OP_DIFFERENT
bool LDC::operator!=(const LDC &param) const
{
    if (this==&param)
        return true;
    ChainonDC *c=d_ancre.d_suivant, *lc=param.d_ancre.d_suivant;
    while(c!=&d_ancre && lc!=&param.d_ancre && c->d_v== lc->d_v)
    {
        c=c->d_suivant;
        lc=lc->d_suivant;
    }
    return (c!=&d_ancre && lc!=&param.d_ancre);
}
// FIN EXO_OP_DIFFERENT


// DEBUT EXO_OP_AFFECTATION
LDC &LDC::operator=(const LDC &param)
{
	if (this==&param)
		return *this;

    ChainonDC *precC1=&d_ancre,*c1=d_ancre.d_suivant, *c2=param.d_ancre.d_suivant;
    while(c1!=&d_ancre && c2!=&param.d_ancre)
	{
		c1->d_v=c2->d_v;
		precC1=c1;
		c1=c1->d_suivant;
		c2=c2->d_suivant;
	}
	if (c1!=&d_ancre)
	{
		while(c1!=&d_ancre)
		{
			precC1->d_suivant=c1->d_suivant;
			c1->d_suivant->d_precedent=precC1;
			delete c1;
            c1=precC1->d_suivant;
		}
	}
	else if (c2!=&param.d_ancre)
	{
		while(c2!=&param.d_ancre)
		{
			c1=new ChainonDC(c2->d_v);
			precC1->d_suivant=c1;
			c1->d_precedent=precC1;
			precC1=c1;
			c2=c2->d_suivant;
		}
        d_ancre.d_precedent=c1;
        c1->d_suivant=&d_ancre;
	}
	return *this;
}
// FIN EXO_OP_AFFECTATION


// DEBUT EXO_OP_CROCHET
double &LDC::operator[](int i)
{
    ChainonDC *c=d_ancre.d_suivant;
    int compteur=0;
    while(c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    return c->d_v;
}
// FIN EXO_OP_CROCHET


// DEBUT EXO_OP_PARENTHESE
double &LDC::operator()(int i)
{
    ChainonDC *c=d_ancre.d_suivant;
    int compteur=0;
    while(c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    return c->d_v;
}
// FIN EXO_OP_PARENTHESE


// DEBUT EXO_OP_PARENTHESE_CONST
double LDC::operator()(int i) const
{
    ChainonDC *c=d_ancre.d_suivant;
    int compteur=0;
    while(c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    return c->d_v;
}
// FIN EXO_OP_PARENTHESE_CONST


// DEBUT EXO_OP_CROCHET_CONST
double LDC::operator[](int i) const
{
    ChainonDC *c=d_ancre.d_suivant;
    int compteur=0;
    while(c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    return c->d_v;
}
// FIN EXO_OP_CROCHET_CONST


// DEBUT EXO_SETTER
void LDC::set(int i, double val)
{
    ChainonDC *c=d_ancre.d_suivant;
    int compteur=0;
    while(c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    c->d_v=val;
}
// FIN EXO_SETTER


// DEBUT EXO_GETTER
double LDC::get(int i) const
{
    ChainonDC *c=d_ancre.d_suivant;
    int compteur=0;
    while(c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    return c->d_v;
}
// FIN EXO_GETTER


// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
void LDC::additionnerATousLesElts(double val)
{
    ChainonDC *c=d_ancre.d_suivant;
    while(c!=&d_ancre)
    {
        c->d_v+=val;
        c=c->d_suivant;
    }
}
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS


// DEBUT EXO_INSERER_UN_ELT_DEBUT
void LDC::insererUnEltDebut(double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion entre d_andre et d_ancre.d_suivant
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    precC->d_suivant = nc;
    nc->d_precedent=precC;
    nc->d_suivant = c;
    c->d_precedent=nc;
}
// FIN EXO_INSERER_UN_ELT_DEBUT


// DEBUT EXO_INSERER_UN_ELT_MILIEU
void LDC::insererUnEltMilieu(int idx, double val)
{
    int i=0;
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    while(c!=&d_ancre && i<idx)
    {
        precC=c;
        c=c->d_suivant;
        i++;
    }

    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion entre precC et c
    precC->d_suivant = nc;
    nc->d_precedent=precC;
    nc->d_suivant = c;
    c->d_precedent=nc;
}
// FIN EXO_INSERER_UN_ELT_MILIEU


// DEBUT EXO_INSERER_UN_ELT_FIN
void LDC::insererUnEltFin(double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion entre d_andre et d_ancre.d_suivant
    ChainonDC *precC=d_ancre.d_precedent,*c=&d_ancre;
    precC->d_suivant = nc;
    nc->d_precedent=precC;
    nc->d_suivant = c;
    c->d_precedent=nc;
}
// FIN EXO_INSERER_UN_ELT_FIN


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_DEBUT
void LDC::insererPlusieursEltsDebut(double *tab, int nb)
{
    // Insertion entre d_andre et d_ancre.d_suivant
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    for (int i=0;i<nb;i++)
    {
        ChainonDC *nc=new ChainonDC(tab[i]);
        // Insertion entre precC et c
        precC->d_suivant=nc;
        nc->d_precedent=precC;
        nc->d_suivant=c;
        c->d_precedent=nc;
        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_MILIEU
void LDC::insererPlusieursEltsMilieu(int idx, double *tab, int nb)
{
    int i=0;
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    while(c!=&d_ancre && i<idx)
    {
        precC=c;
        c=c->d_suivant;
        i++;
    }

    for (int i=0;i<nb;i++)
    {
        ChainonDC *nc=new ChainonDC(tab[i]);
        // Insertion entre precC et c
        precC->d_suivant=nc;
        nc->d_precedent=precC;
        nc->d_suivant=c;
        c->d_precedent=nc;
        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_FIN
void LDC::insererPlusieursEltsFin(double *tab, int nb)
{
    // Insertion entre d_andre et d_ancre.d_suivant
    ChainonDC *precC=d_ancre.d_precedent,*c=&d_ancre;
    for (int i=0;i<nb;i++)
    {
        ChainonDC *nc=new ChainonDC(tab[i]);
        // Insertion entre precC et c
        precC->d_suivant=nc;
        nc->d_precedent=precC;
        nc->d_suivant=c;
        c->d_precedent=nc;
        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ENLEVER_UN_ELT_DEBUT
void LDC::enleverUnEltDebut()
{
    if (d_ancre.d_suivant==&d_ancre)
        return;
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant->d_suivant;
    delete d_ancre.d_suivant;
    precC->d_suivant=c;
    c->d_precedent=precC;
}
// FIN EXO_ENLEVER_UN_ELT_DEBUT


// DEBUT EXO_ENLEVER_UN_ELT_MILIEU
void LDC::enleverUnEltMilieu(int idx)
{
    int i=0;
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    while(c!=&d_ancre && i<idx)
    {
        precC=c;
        c=c->d_suivant;
        i++;
    }
    if (c==&d_ancre)
        return;
    c=c->d_suivant;
    delete precC->d_suivant;
    precC->d_suivant=c;
    c->d_precedent=precC;
}
// FIN EXO_ENLEVER_UN_ELT_MILIEU


// DEBUT EXO_ENLEVER_UN_ELT_FIN
void LDC::enleverUnEltFin()
{
    if (d_ancre.d_precedent==&d_ancre)
        return;
    ChainonDC *precC=d_ancre.d_precedent->d_precedent,*c=&d_ancre;
    delete d_ancre.d_precedent;
    precC->d_suivant=c;
    c->d_precedent=precC;
}
// FIN EXO_ENLEVER_UN_ELT_FIN


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
void LDC::enleverPlusieursEltsDebut(int nb)
{
    int i=0;
    while(d_ancre.d_suivant!=&d_ancre && i<nb)
    {
        ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant->d_suivant;
        delete d_ancre.d_suivant;
        precC->d_suivant=c;
        c->d_precedent=precC;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU
void LDC::enleverPlusieursEltsMilieu(int idx, int nb)
{
    int i=0;
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    while(c!=&d_ancre && i<idx)
    {
        precC=c;
        c=c->d_suivant;
        i++;
    }
    i=0;
    while(precC->d_suivant!=&d_ancre && i<nb)
    {
        c=precC->d_suivant->d_suivant;
        delete precC->d_suivant;
        precC->d_suivant=c;
        c->d_precedent=precC;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_FIN
void LDC::enleverPlusieursEltsFin(int nb)
{
    int i=0;
    while(d_ancre.d_precedent!=&d_ancre && i<nb)
    {
        ChainonDC *precC=d_ancre.d_precedent->d_precedent, *c=&d_ancre;
        delete d_ancre.d_precedent;
        precC->d_suivant=c;
        c->d_precedent=precC;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_FIN



// DEBUT EXO_ELTS_EN_ORDRE_DECROISSANT
bool LDC::eltsEnOrdreDecroissant() const
{
    if (d_ancre.d_suivant==&d_ancre || d_ancre.d_suivant->d_suivant==&d_ancre)
        return true;
    ChainonDC *c=d_ancre.d_suivant;
    while(c->d_suivant!=&d_ancre && c->d_v>c->d_suivant->d_v)
    {
        c=c->d_suivant;
    }
    return (c->d_suivant==&d_ancre);
}
// FIN EXO_ELTS_EN_ORDRE_DECROISSANT


// DEBUT EXO_CONTIENT_ELT
bool LDC::contientElt(double val) const
{
    ChainonDC *c=d_ancre.d_suivant;
    while(c!=&d_ancre && c->d_v!=val)
    {
        c=c->d_suivant;
    }
    return c!=&d_ancre;
}
// FIN EXO_CONTIENT_ELT


// DEBUT EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT
void LDC::enleverToutesLesOccurencesDUnElt(double val)
{
    ChainonDC *c=d_ancre.d_suivant;
    while(c!=&d_ancre)
    {
        if (c->d_v==val)
        {
            ChainonDC *precC=c->d_precedent;
            c=c->d_suivant;
            delete c->d_precedent;
            precC->d_suivant=c;
            c->d_precedent=precC;
        }
        else
            c=c->d_suivant;
    }
}
// FIN EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT
int LDC::chercherLIndexDeLaPremiereOccurenceDUnElt(double val) const
{
    ChainonDC *c=d_ancre.d_suivant;
    int i=0;
    while(c!=&d_ancre && c->d_v!=val)
    {
        c=c->d_suivant;
        i++;
    }
    if (c!=&d_ancre)
        return i;
    return -1;
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT
int LDC::chercherLIndexDeLaDerniereOccurenceDUnElt(double val) const
{
    ChainonDC *c=d_ancre.d_suivant;
    int i=0,idx=-1;
    while(c!=&d_ancre)
    {
        if (c->d_v==val)
            idx=i;
        c=c->d_suivant;
        i++;
    }
    return idx;
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
int LDC::calculerNombreDOccurencesDUnElt(double val) const
{
    int idxNbOccurences=0;
    ChainonDC *c=d_ancre.d_suivant;
    while(c!=&d_ancre)
    {
        if (c->d_v==val)
            idxNbOccurences++;
        c=c->d_suivant;
    }
    return idxNbOccurences;
}
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT


// DEBUT EXO_OP_ADDITION
LDC operator+(const LDC& l1, const LDC& l2)
{
    LDC res;
    ChainonDC *c1=l1.d_ancre.d_suivant,*c2=l2.d_ancre.d_suivant;
    while(c1!=&l1.d_ancre && c2!=&l2.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(c1->d_v+c2->d_v);
        // Insertion entre d_andre et d_ancre.d_suivant
        ChainonDC *precC=res.d_ancre.d_precedent,*c=&res.d_ancre;
        precC->d_suivant = nc;
        nc->d_precedent=precC;
        nc->d_suivant = c;
        c->d_precedent=nc;

        c1=c1->d_suivant;
        c2=c2->d_suivant;
    }
    return res;
}
// FIN EXO_OP_ADDITION


// DEBUT EXO_OP_SOUSTRACTION
LDC operator-(const LDC& l1, const LDC& l2)
{
    LDC res;
    ChainonDC *c1=l1.d_ancre.d_suivant,*c2=l2.d_ancre.d_suivant;
    while(c1!=&l1.d_ancre && c2!=&l2.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(c1->d_v-c2->d_v);
        // Insertion entre d_andre et d_ancre.d_suivant
        ChainonDC *precC=res.d_ancre.d_precedent,*c=&res.d_ancre;
        precC->d_suivant = nc;
        nc->d_precedent=precC;
        nc->d_suivant = c;
        c->d_precedent=nc;

        c1=c1->d_suivant;
        c2=c2->d_suivant;
    }
    return res;
}
// FIN EXO_OP_SOUSTRACTION


// DEBUT EXO_CREER_ITERATEUR
LDC::Iterateur::Iterateur(ChainonDC* ancre):d_ancre{ancre},d_crt{ancre}
{
}

LDC::Iterateur LDC::premier()
{
    return LDC::Iterateur{&d_ancre};
}
// FIN EXO_CREER_ITERATEUR


// DEBUT EXO_OP_PLUS_PLUS_ITERATEUR
LDC::Iterateur& LDC::Iterateur::operator++() {
    d_crt = d_crt->d_suivant;
    return *this;
}
// FIN EXO_OP_PLUS_PLUS_ITERATEUR


// DEBUT EXO_OP_MOINS_MOINS_ITERATEUR
LDC::Iterateur& LDC::Iterateur::operator--() {
    d_crt = d_crt->d_precedent;
    return *this;
}
// FIN EXO_OP_MOINS_MOINS_ITERATEUR


// DEBUT EXO_OP_ETOILE_ITERATEUR
double& LDC::Iterateur::operator*() {
    return d_crt->d_v;
}
// FIN EXO_OP_ETOILE_ITERATEUR


// DEBUT EXO_OP_ETOILE_CONST_ITERATEUR
double LDC::Iterateur::operator*() const {
    return d_crt->d_v;
}
// FIN EXO_OP_ETOILE_CONST_ITERATEUR


// DEBUT EXO_OP_FIN_ITERATEUR
bool LDC::Iterateur::fin() const {
    return d_crt==d_ancre;
}
// FIN EXO_OP_FIN_ITERATEUR
