////////////////////////////////////////////////////////////////////////////////////
// Fichier genere automatiquement pour l'instrumentation du code. Ne pas modifier //
////////////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <algorithm>
#include "ExercicesBase.h"


using namespace std;


//////////////////////////////////////////////////////////////////////////////////////////
// Verifie si la liste chainee est correcte au niveau de sa sctructure et des pointeurs //
//////////////////////////////////////////////////////////////////////////////////////////
bool LDCCorr_Other::verifierIntegrite(const LDC &param)
{
    // Liste chainee vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_ancre.d_suivant==&param.d_ancre)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return param.d_ancre.d_precedent==&param.d_ancre;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_ancre.d_precedent==&param.d_ancre)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return param.d_ancre.d_suivant==&param.d_ancre;}

    ChainonDC *c=param.d_ancre.d_suivant;
    Int compteur=0;
    vector<ChainonDC*> pntrUilise;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&param.d_ancre && compteur<StructDeDonneesVerifCorr::NB_MAX_COLONNES)
    {
        // Validite des pointeurs
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(c))
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_precedent==nullptr)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_suivant==nullptr)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_suivant!=&param.d_ancre)
        {
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(c->d_suivant))
                {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        }
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_suivant->d_precedent!=c)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_precedent!=&param.d_ancre)
        {
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(c->d_precedent))
                {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        }
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_precedent->d_suivant!=c)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        // Verifier si le pointeur est bien unique
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&std::find(pntrUilise.begin(), pntrUilise.end(), c)!=pntrUilise.end())
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        pntrUilise.push_back(c);

        c=c->d_suivant;
        compteur++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return compteur<StructDeDonneesVerifCorr::NB_MAX_COLONNES;}
}

// Compare la correction avec la struct de l'etudiant. Retourne false si different
bool LDCCorr_Other::compareCorrAvecEtud(const LDC &etudiant) const
{
    ChainonDC *c1=this->d_ancre.d_suivant,*c2=etudiant.d_ancre.d_suivant;

    // Cas de la chaine vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1==&d_ancre && c2!=&etudiant.d_ancre)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1!=&d_ancre && c2==&etudiant.d_ancre)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}

    vector<ChainonDC*> pntrCorr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=&d_ancre)
    {
        // Verifie la longueur de la liste chainee
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c2==&etudiant.d_ancre)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}

        // Verifier si le pointeur c2 n'est pas un pointeur de la liste correction
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&std::find(pntrCorr.begin(), pntrCorr.end(), c2)!=pntrCorr.end())
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        pntrCorr.push_back(c1);

        // Verifie si les chainons contiennent la meme valeur
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1->d_v!=c2->d_v)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}

        c1=c1->d_suivant;
        c2=c2->d_suivant;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
}

// Compare les iterateurs
bool LDCCorr_Other::compareCorrAvecEtud(const Iterateur& corrIt, const LDC &etudLdc,
                                  const Iterateur& etudIt) const
{
    // Verifie le pointeur sur l'ancre pour l'iterateur de l'etudiant
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&etudIt.d_ancre!=&etudLdc.d_ancre)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    // Verifie la position du chainon courrant d_crt pour l'iterateur de l'etudiant
    Int posCorr=0;
    ChainonDC *c=d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=corrIt.d_crt)
    {
        c=c->d_suivant;
        posCorr++;
    }
    Int posEtud=0;
    c=etudLdc.d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=etudIt.d_crt && posEtud<posCorr)
    {
        c=c->d_suivant;
        posEtud++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c==etudIt.d_crt);}
}

// genere la structDeDonneesVerifCorr a partir de la liste
StructDeDonneesVerifCorr LDCCorr_Other::getStructDeDonneesVerifCorr() const
{
    StructDeDonneesVerifCorr vect;
    ChainonDC *c=d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre)
    {
        vect.insererUnEltFin(c->d_v);
        c=c->d_suivant;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return vect;}
}

// Constructeur avec la structDeDonneesVerifCorr
void LDCCorr_Other::setStructDeDonneesVerifCorr(const StructDeDonneesVerifCorr &tab)
{
    // La liste est supposee etre vide
    d_ancre.d_suivant=&d_ancre;
    d_ancre.d_precedent=&d_ancre;

    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&tab.getNbColonnes()==0)
        return;
    // On insere les elements du tableau
    ChainonDC *precC=&d_ancre, *currC=nullptr;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<tab.getNbColonnes();i++)
    {
        currC=new ChainonDC(tab[i]);
        precC->d_suivant=currC;
        currC->d_precedent=precC;
        precC=currC;
    }
    currC->d_suivant=&d_ancre;
    d_ancre.d_precedent=currC;
}


// DEBUT EXO_CONSTRUCTEUR
LDCCorr_ConstructDestruct::LDCCorr_ConstructDestruct():d_ancre{0}
{
    d_ancre.d_suivant=&d_ancre;
    d_ancre.d_precedent=&d_ancre;
}
// FIN EXO_CONSTRUCTEUR


// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
LDCCorr_ConstructDestruct::LDCCorr_ConstructDestruct(const LDCCorr &param):d_ancre{0}
{
    ChainonDC* lc = param.d_ancre.d_suivant, *precC=&d_ancre;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&lc!=&param.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        precC->d_suivant = nc;
        nc->d_precedent=precC;

        precC = nc;
        lc = lc->d_suivant;
    }
    precC->d_suivant=&d_ancre;
    d_ancre.d_precedent=precC;
}
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE


// DEBUT EXO_CONSTRUCTEUR_AVEC_TABLEAU
LDCCorr_ConstructDestruct::LDCCorr_ConstructDestruct(Double *tab, Int nb):d_ancre{0}
{
    ChainonDC* precC=&d_ancre;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        ChainonDC *nc = new ChainonDC(tab[i]);
        precC->d_suivant = nc;
        nc->d_precedent=precC;
        precC = nc;
    }
    precC->d_suivant=&d_ancre;
    d_ancre.d_precedent=precC;
}
// FIN EXO_CONSTRUCTEUR_AVEC_TABLEAU


// DEBUT EXO_DESTRUCTEUR
LDCCorr_ConstructDestruct::~LDCCorr_ConstructDestruct()
{
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&d_ancre.d_suivant!=&d_ancre)
    {
        ChainonDC *tmp=d_ancre.d_suivant;
        d_ancre.d_suivant=d_ancre.d_suivant->d_suivant;
        {GestionPntrEtComplexite::get().peutSupprPntr(tmp, false); delete tmp;}
    }
}
// FIN EXO_DESTRUCTEUR


// DEBUT EXO_CONCATENATION_DEBUT
void LDCCorr_Other::concatenationDebut(const LDCCorr &param)
{
    ChainonDC* lc = param.d_ancre.d_suivant, *precC=&d_ancre;
    ChainonDC* nextC=d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&lc!=&param.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        precC->d_suivant = nc;
        nc->d_precedent=precC;

        precC = nc;
        lc = lc->d_suivant;
    }
    precC->d_suivant=nextC;
    nextC->d_precedent=precC;
}
// FIN EXO_CONCATENATION_DEBUT


// DEBUT EXO_CONCATENATION_MILIEU
void LDCCorr_Other::concatenationMilieu(Int idx, const LDCCorr &param)
{
    ChainonDC *precC=&d_ancre;
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&precC->d_suivant!=&d_ancre && i<idx)
    {
        precC=precC->d_suivant;
        i++;
    }

    ChainonDC* lc = param.d_ancre.d_suivant;
    ChainonDC* nextC=precC->d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&lc!=&param.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        precC->d_suivant = nc;
        nc->d_precedent=precC;

        precC = nc;
        lc = lc->d_suivant;
    }
    precC->d_suivant=nextC;
    nextC->d_precedent=precC;
}
// FIN EXO_CONCATENATION_MILIEU


// DEBUT EXO_CONCATENATION_FIN
void LDCCorr_Other::concatenationFin(const LDCCorr &param)
{
    ChainonDC* lc = param.d_ancre.d_suivant, *precC=d_ancre.d_precedent;
    ChainonDC* nextC=&d_ancre;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&lc!=&param.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        precC->d_suivant = nc;
        nc->d_precedent=precC;

        precC = nc;
        lc = lc->d_suivant;
    }
    precC->d_suivant=nextC;
    nextC->d_precedent=precC;
}
// FIN EXO_CONCATENATION_FIN


// DEBUT EXO_OP_CONCATENATION
LDCCorr &LDCCorr_Other::operator+=(const LDCCorr &param)
{
    ChainonDC* lc = param.d_ancre.d_suivant, *precC=d_ancre.d_precedent;
    ChainonDC* nextC=&d_ancre;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&lc!=&param.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        precC->d_suivant = nc;
        nc->d_precedent=precC;

        precC = nc;
        lc = lc->d_suivant;
    }
    precC->d_suivant=nextC;
    nextC->d_precedent=precC;

    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_CONCATENATION


// DEBUT EXO_OP_EGAL
bool LDCCorr_Other::operator==(const LDCCorr &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    ChainonDC *c=d_ancre.d_suivant, *lc=param.d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && lc!=&param.d_ancre && c->d_v== lc->d_v)
    {
        c=c->d_suivant;
        lc=lc->d_suivant;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c==&d_ancre && lc==&param.d_ancre);}
}
// FIN EXO_OP_EGAL


// DEBUT EXO_OP_DIFFERENT
bool LDCCorr_Other::operator!=(const LDCCorr &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    ChainonDC *c=d_ancre.d_suivant, *lc=param.d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && lc!=&param.d_ancre && c->d_v== lc->d_v)
    {
        c=c->d_suivant;
        lc=lc->d_suivant;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c!=&d_ancre && lc!=&param.d_ancre);}
}
// FIN EXO_OP_DIFFERENT


// DEBUT EXO_OP_AFFECTATION
LDCCorr &LDCCorr_Other::operator=(const LDCCorr &param)
{
	if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
		{GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}

    ChainonDC *precC1=&d_ancre,*c1=d_ancre.d_suivant, *c2=param.d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=&d_ancre && c2!=&param.d_ancre)
	{
		c1->d_v=c2->d_v;
		precC1=c1;
		c1=c1->d_suivant;
		c2=c2->d_suivant;
	}
	if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1!=&d_ancre)
	{
		while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=&d_ancre)
		{
			precC1->d_suivant=c1->d_suivant;
			c1->d_suivant->d_precedent=precC1;
			{GestionPntrEtComplexite::get().peutSupprPntr(c1, false); delete c1;}
            c1=precC1->d_suivant;
		}
	}
	else if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c2!=&param.d_ancre)
	{
		while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=&param.d_ancre)
		{
			c1=new ChainonDC(c2->d_v);
			precC1->d_suivant=c1;
			c1->d_precedent=precC1;
			precC1=c1;
			c2=c2->d_suivant;
		}
        d_ancre.d_precedent=c1;
        c1->d_suivant=&d_ancre;
	}
	{GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_AFFECTATION


// DEBUT EXO_OP_CROCHET
Double &LDCCorr_Other::operator[](Int i)
{
    ChainonDC *c=d_ancre.d_suivant;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c->d_v;}
}
// FIN EXO_OP_CROCHET


// DEBUT EXO_OP_CROCHET_CONST
Double LDCCorr_Other::operator[](Int i) const
{
    ChainonDC *c=d_ancre.d_suivant;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c->d_v;}
}
// FIN EXO_OP_CROCHET_CONST


// DEBUT EXO_OP_PARENTHESE
Double &LDCCorr_Other::operator()(Int i)
{
    ChainonDC *c=d_ancre.d_suivant;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c->d_v;}
}
// FIN EXO_OP_PARENTHESE


// DEBUT EXO_OP_PARENTHESE_CONST
Double LDCCorr_Other::operator()(Int i) const
{
    ChainonDC *c=d_ancre.d_suivant;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c->d_v;}
}
// FIN EXO_OP_PARENTHESE_CONST


// DEBUT EXO_SETTER
void LDCCorr_Other::set(Int i, Double val)
{
    ChainonDC *c=d_ancre.d_suivant;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    c->d_v=val;
}
// FIN EXO_SETTER


// DEBUT EXO_GETTER
Double LDCCorr_Other::get(Int i) const
{
    ChainonDC *c=d_ancre.d_suivant;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c->d_v;}
}
// FIN EXO_GETTER


// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
void LDCCorr_Other::additionnerATousLesElts(Double val)
{
    ChainonDC *c=d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre)
    {
        c->d_v+=val;
        c=c->d_suivant;
    }
}
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS


// DEBUT EXO_INSERER_UN_ELT_DEBUT
void LDCCorr_Other::insererUnEltDebut(Double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion entre d_andre et d_ancre.d_suivant
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    precC->d_suivant = nc;
    nc->d_precedent=precC;
    nc->d_suivant = c;
    c->d_precedent=nc;
}
// FIN EXO_INSERER_UN_ELT_DEBUT


// DEBUT EXO_INSERER_UN_ELT_MILIEU
void LDCCorr_Other::insererUnEltMilieu(Int idx, Double val)
{
    Int i=0;
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && i<idx)
    {
        precC=c;
        c=c->d_suivant;
        i++;
    }

    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion entre precC et c
    precC->d_suivant = nc;
    nc->d_precedent=precC;
    nc->d_suivant = c;
    c->d_precedent=nc;
}
// FIN EXO_INSERER_UN_ELT_MILIEU


// DEBUT EXO_INSERER_UN_ELT_FIN
void LDCCorr_Other::insererUnEltFin(Double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion entre d_andre et d_ancre.d_suivant
    ChainonDC *precC=d_ancre.d_precedent,*c=&d_ancre;
    precC->d_suivant = nc;
    nc->d_precedent=precC;
    nc->d_suivant = c;
    c->d_precedent=nc;
}
// FIN EXO_INSERER_UN_ELT_FIN


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_DEBUT
void LDCCorr_Other::insererPlusieursEltsDebut(Double *tab, Int nb)
{
    // Insertion entre d_andre et d_ancre.d_suivant
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        ChainonDC *nc=new ChainonDC(tab[i]);
        // Insertion entre precC et c
        precC->d_suivant=nc;
        nc->d_precedent=precC;
        nc->d_suivant=c;
        c->d_precedent=nc;
        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_MILIEU
void LDCCorr_Other::insererPlusieursEltsMilieu(Int idx, Double *tab, Int nb)
{
    Int i=0;
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && i<idx)
    {
        precC=c;
        c=c->d_suivant;
        i++;
    }

    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        ChainonDC *nc=new ChainonDC(tab[i]);
        // Insertion entre precC et c
        precC->d_suivant=nc;
        nc->d_precedent=precC;
        nc->d_suivant=c;
        c->d_precedent=nc;
        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_FIN
void LDCCorr_Other::insererPlusieursEltsFin(Double *tab, Int nb)
{
    // Insertion entre d_andre et d_ancre.d_suivant
    ChainonDC *precC=d_ancre.d_precedent,*c=&d_ancre;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        ChainonDC *nc=new ChainonDC(tab[i]);
        // Insertion entre precC et c
        precC->d_suivant=nc;
        nc->d_precedent=precC;
        nc->d_suivant=c;
        c->d_precedent=nc;
        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ENLEVER_UN_ELT_DEBUT
void LDCCorr_Other::enleverUnEltDebut()
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_ancre.d_suivant==&d_ancre)
        return;
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant->d_suivant;
    delete d_ancre.d_suivant;
    precC->d_suivant=c;
    c->d_precedent=precC;
}
// FIN EXO_ENLEVER_UN_ELT_DEBUT


// DEBUT EXO_ENLEVER_UN_ELT_MILIEU
void LDCCorr_Other::enleverUnEltMilieu(Int idx)
{
    Int i=0;
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && i<idx)
    {
        precC=c;
        c=c->d_suivant;
        i++;
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==&d_ancre)
        return;
    c=c->d_suivant;
    delete precC->d_suivant;
    precC->d_suivant=c;
    c->d_precedent=precC;
}
// FIN EXO_ENLEVER_UN_ELT_MILIEU


// DEBUT EXO_ENLEVER_UN_ELT_FIN
void LDCCorr_Other::enleverUnEltFin()
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_ancre.d_precedent==&d_ancre)
        return;
    ChainonDC *precC=d_ancre.d_precedent->d_precedent,*c=&d_ancre;
    delete d_ancre.d_precedent;
    precC->d_suivant=c;
    c->d_precedent=precC;
}
// FIN EXO_ENLEVER_UN_ELT_FIN


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
void LDCCorr_Other::enleverPlusieursEltsDebut(Int nb)
{
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&d_ancre.d_suivant!=&d_ancre && i<nb)
    {
        ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant->d_suivant;
        delete d_ancre.d_suivant;
        precC->d_suivant=c;
        c->d_precedent=precC;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU
void LDCCorr_Other::enleverPlusieursEltsMilieu(Int idx, Int nb)
{
    Int i=0;
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && i<idx)
    {
        precC=c;
        c=c->d_suivant;
        i++;
    }
    i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&precC->d_suivant!=&d_ancre && i<nb)
    {
        c=precC->d_suivant->d_suivant;
        delete precC->d_suivant;
        precC->d_suivant=c;
        c->d_precedent=precC;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_FIN
void LDCCorr_Other::enleverPlusieursEltsFin(Int nb)
{
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&d_ancre.d_precedent!=&d_ancre && i<nb)
    {
        ChainonDC *precC=d_ancre.d_precedent->d_precedent, *c=&d_ancre;
        delete d_ancre.d_precedent;
        precC->d_suivant=c;
        c->d_precedent=precC;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ELTS_EN_ORDRE_DECROISSANT
bool LDCCorr_Other::eltsEnOrdreDecroissant() const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_ancre.d_suivant==&d_ancre || d_ancre.d_suivant->d_suivant==&d_ancre)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    ChainonDC *c=d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c->d_suivant!=&d_ancre && c->d_v>c->d_suivant->d_v)
    {
        c=c->d_suivant;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c->d_suivant==&d_ancre);}
}
// FIN EXO_ELTS_EN_ORDRE_DECROISSANT


// DEBUT EXO_CONTIENT_ELT
bool LDCCorr_Other::contientElt(Double val) const
{
    ChainonDC *c=d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && c->d_v!=val)
    {
        c=c->d_suivant;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c!=&d_ancre;}
}
// FIN EXO_CONTIENT_ELT


// DEBUT EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT
void LDCCorr_Other::enleverToutesLesOccurencesDUnElt(Double val)
{
    ChainonDC *c=d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_v==val)
        {
            ChainonDC *precC=c->d_precedent;
            c=c->d_suivant;
            delete c->d_precedent;
            precC->d_suivant=c;
            c->d_precedent=precC;
        }
        else
            c=c->d_suivant;
    }
}
// FIN EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT
Int LDCCorr_Other::chercherLIndexDeLaPremiereOccurenceDUnElt(Double val) const
{
    ChainonDC *c=d_ancre.d_suivant;
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && c->d_v!=val)
    {
        c=c->d_suivant;
        i++;
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c!=&d_ancre)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i;}
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return -1;}
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT
Int LDCCorr_Other::chercherLIndexDeLaDerniereOccurenceDUnElt(Double val) const
{
    ChainonDC *c=d_ancre.d_suivant;
    Int i=0,idx=-1;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_v==val)
            idx=i;
        c=c->d_suivant;
        i++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return idx;}
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
Int LDCCorr_Other::calculerNombreDOccurencesDUnElt(Double val) const
{
    Int idxNbOccurences=0;
    ChainonDC *c=d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_v==val)
            idxNbOccurences++;
        c=c->d_suivant;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return idxNbOccurences;}
}
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT


// DEBUT EXO_OP_ADDITION
LDCCorr operator_plus(const LDCCorr& l1, const LDCCorr& l2)
{
    LDCCorr res;
    ChainonDC *c1=l1.d_ancre.d_suivant,*c2=l2.d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=&l1.d_ancre && c2!=&l2.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(c1->d_v+c2->d_v);
        // Insertion entre d_andre et d_ancre.d_suivant
        ChainonDC *precC=res.d_ancre.d_precedent,*c=&res.d_ancre;
        precC->d_suivant = nc;
        nc->d_precedent=precC;
        nc->d_suivant = c;
        c->d_precedent=nc;

        c1=c1->d_suivant;
        c2=c2->d_suivant;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return res;}
}
// FIN EXO_OP_ADDITION


// DEBUT EXO_OP_SOUSTRACTION
LDCCorr operator_moins(const LDCCorr& l1, const LDCCorr& l2)
{
    LDCCorr res;
    ChainonDC *c1=l1.d_ancre.d_suivant,*c2=l2.d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=&l1.d_ancre && c2!=&l2.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(c1->d_v-c2->d_v);
        // Insertion entre d_andre et d_ancre.d_suivant
        ChainonDC *precC=res.d_ancre.d_precedent,*c=&res.d_ancre;
        precC->d_suivant = nc;
        nc->d_precedent=precC;
        nc->d_suivant = c;
        c->d_precedent=nc;

        c1=c1->d_suivant;
        c2=c2->d_suivant;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return res;}
}
// FIN EXO_OP_SOUSTRACTION


// DEBUT EXO_CREER_ITERATEUR
LDCCorr_Other::Iterateur::Iterateur(ChainonDC* ancre):d_ancre{ancre},d_crt{ancre}
{
}

LDCCorr_Other::Iterateur LDCCorr_Other::premier()
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return LDCCorr_Other::Iterateur{&d_ancre};}
}
// FIN EXO_CREER_ITERATEUR


// DEBUT EXO_OP_PLUS_PLUS_ITERATEUR
LDCCorr_Other::Iterateur& LDCCorr_Other::Iterateur::operator++() {
    d_crt = d_crt->d_suivant;
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_PLUS_PLUS_ITERATEUR


// DEBUT EXO_OP_MOINS_MOINS_ITERATEUR
LDCCorr_Other::Iterateur& LDCCorr_Other::Iterateur::operator--() {
    d_crt = d_crt->d_precedent;
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_MOINS_MOINS_ITERATEUR


// DEBUT EXO_OP_ETOILE_ITERATEUR
Double& LDCCorr_Other::Iterateur::operator*() {
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_crt->d_v;}
}
// FIN EXO_OP_ETOILE_ITERATEUR


// DEBUT EXO_OP_ETOILE_CONST_ITERATEUR
Double LDCCorr_Other::Iterateur::operator*() const {
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_crt->d_v;}
}
// FIN EXO_OP_ETOILE_CONST_ITERATEUR


// DEBUT EXO_OP_FIN_ITERATEUR
bool LDCCorr_Other::Iterateur::fin() const {
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_crt==d_ancre;}
}
// FIN EXO_OP_FIN_ITERATEUR
