////////////////////////////////////////////////////////////////////////////////////
// Fichier genere automatiquement pour l'instrumentation du code. Ne pas modifier //
////////////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include "ExercicesBase.h"

using namespace std;


// DEBUT EXO_CONSTRUCTEUR
LDC_ConstructDestruct::LDC_ConstructDestruct():d_ancre{0}
{
    d_ancre.d_suivant=&d_ancre;
    d_ancre.d_precedent=&d_ancre;
}
// FIN EXO_CONSTRUCTEUR



// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
LDC_ConstructDestruct::LDC_ConstructDestruct(const LDC &param):d_ancre{0}
{
    ChainonDC* lc = param.d_ancre.d_suivant, *precC=&d_ancre;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&lc!=&param.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        precC->d_suivant = nc;
        nc->d_precedent=precC;

        precC = nc;
        lc = lc->d_suivant;
    }
    precC->d_suivant=&d_ancre;
    d_ancre.d_precedent=precC;
}
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE



// DEBUT EXO_CONSTRUCTEUR_AVEC_TABLEAU
LDC_ConstructDestruct::LDC_ConstructDestruct(Double *tab, Int nb):d_ancre{0}
{
    ChainonDC* precC=&d_ancre;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        ChainonDC *nc = new ChainonDC(tab[i]);
        precC->d_suivant = nc;
        nc->d_precedent=precC;
        precC = nc;
    }
    precC->d_suivant=&d_ancre;
    d_ancre.d_precedent=precC;
}
// FIN EXO_CONSTRUCTEUR_AVEC_TABLEAU


// DEBUT EXO_DESTRUCTEUR
LDC_ConstructDestruct::~LDC_ConstructDestruct()
{
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&d_ancre.d_suivant!=&d_ancre)
    {
        ChainonDC *tmp=d_ancre.d_suivant;
        d_ancre.d_suivant=d_ancre.d_suivant->d_suivant;
        {GestionPntrEtComplexite::get().peutSupprPntr(tmp, false); delete tmp;}
    }
}
// FIN EXO_DESTRUCTEUR


// DEBUT EXO_CONCATENATION_DEBUT
void LDC_Other::concatenationDebut(const LDC &param)
{
    ChainonDC* lc = param.d_ancre.d_suivant, *precC=&d_ancre;
    ChainonDC* nextC=d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&lc!=&param.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        precC->d_suivant = nc;
        nc->d_precedent=precC;

        precC = nc;
        lc = lc->d_suivant;
    }
    precC->d_suivant=nextC;
    nextC->d_precedent=precC;
}
// FIN EXO_CONCATENATION_DEBUT


// DEBUT EXO_CONCATENATION_MILIEU
void LDC_Other::concatenationMilieu(Int idx, const LDC &param)
{
    ChainonDC *precC=&d_ancre;
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&precC->d_suivant!=&d_ancre && i<idx)
    {
        precC=precC->d_suivant;
        i++;
    }

    ChainonDC* lc = param.d_ancre.d_suivant;
    ChainonDC* nextC=precC->d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&lc!=&param.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        precC->d_suivant = nc;
        nc->d_precedent=precC;

        precC = nc;
        lc = lc->d_suivant;
    }
    precC->d_suivant=nextC;
    nextC->d_precedent=precC;
}
// FIN EXO_CONCATENATION_MILIEU


// DEBUT EXO_CONCATENATION_FIN
void LDC_Other::concatenationFin(const LDC &param)
{
    ChainonDC* lc = param.d_ancre.d_suivant, *precC=d_ancre.d_precedent;
    ChainonDC* nextC=&d_ancre;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&lc!=&param.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        precC->d_suivant = nc;
        nc->d_precedent=precC;

        precC = nc;
        lc = lc->d_suivant;
    }
    precC->d_suivant=nextC;
    nextC->d_precedent=precC;
}
// FIN EXO_CONCATENATION_FIN



// DEBUT EXO_OP_CONCATENATION
LDC &LDC_Other::operator+=(const LDC &param)
{
    ChainonDC* lc = param.d_ancre.d_suivant, *precC=d_ancre.d_precedent;
    ChainonDC* nextC=&d_ancre;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&lc!=&param.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        precC->d_suivant = nc;
        nc->d_precedent=precC;

        precC = nc;
        lc = lc->d_suivant;
    }
    precC->d_suivant=nextC;
    nextC->d_precedent=precC;

    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_CONCATENATION



// DEBUT EXO_OP_EGAL
bool LDC_Other::operator==(const LDC &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    ChainonDC *c=d_ancre.d_suivant, *lc=param.d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && lc!=&param.d_ancre && c->d_v== lc->d_v)
    {
        c=c->d_suivant;
        lc=lc->d_suivant;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c==&d_ancre && lc==&param.d_ancre);}
}
// FIN EXO_OP_EGAL



// DEBUT EXO_OP_DIFFERENT
bool LDC_Other::operator!=(const LDC &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    ChainonDC *c=d_ancre.d_suivant, *lc=param.d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && lc!=&param.d_ancre && c->d_v== lc->d_v)
    {
        c=c->d_suivant;
        lc=lc->d_suivant;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c!=&d_ancre && lc!=&param.d_ancre);}
}
// FIN EXO_OP_DIFFERENT


// DEBUT EXO_OP_AFFECTATION
LDC &LDC_Other::operator=(const LDC &param)
{
	if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
		{GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}

    ChainonDC *precC1=&d_ancre,*c1=d_ancre.d_suivant, *c2=param.d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=&d_ancre && c2!=&param.d_ancre)
	{
		c1->d_v=c2->d_v;
		precC1=c1;
		c1=c1->d_suivant;
		c2=c2->d_suivant;
	}
	if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1!=&d_ancre)
	{
		while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=&d_ancre)
		{
			precC1->d_suivant=c1->d_suivant;
			c1->d_suivant->d_precedent=precC1;
			{GestionPntrEtComplexite::get().peutSupprPntr(c1, false); delete c1;}
            c1=precC1->d_suivant;
		}
	}
	else if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c2!=&param.d_ancre)
	{
		while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=&param.d_ancre)
		{
			c1=new ChainonDC(c2->d_v);
			precC1->d_suivant=c1;
			c1->d_precedent=precC1;
			precC1=c1;
			c2=c2->d_suivant;
		}
        d_ancre.d_precedent=c1;
        c1->d_suivant=&d_ancre;
	}
	{GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_AFFECTATION


// DEBUT EXO_OP_CROCHET
Double &LDC_Other::operator[](Int i)
{
    ChainonDC *c=d_ancre.d_suivant;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c->d_v;}
}
// FIN EXO_OP_CROCHET


// DEBUT EXO_OP_PARENTHESE
Double &LDC_Other::operator()(Int i)
{
    ChainonDC *c=d_ancre.d_suivant;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c->d_v;}
}
// FIN EXO_OP_PARENTHESE


// DEBUT EXO_OP_PARENTHESE_CONST
Double LDC_Other::operator()(Int i) const
{
    ChainonDC *c=d_ancre.d_suivant;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c->d_v;}
}
// FIN EXO_OP_PARENTHESE_CONST


// DEBUT EXO_OP_CROCHET_CONST
Double LDC_Other::operator[](Int i) const
{
    ChainonDC *c=d_ancre.d_suivant;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c->d_v;}
}
// FIN EXO_OP_CROCHET_CONST


// DEBUT EXO_SETTER
void LDC_Other::set(Int i, Double val)
{
    ChainonDC *c=d_ancre.d_suivant;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    c->d_v=val;
}
// FIN EXO_SETTER


// DEBUT EXO_GETTER
Double LDC_Other::get(Int i) const
{
    ChainonDC *c=d_ancre.d_suivant;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && compteur<i)
    {
        c=c->d_suivant;
        compteur++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c->d_v;}
}
// FIN EXO_GETTER


// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
void LDC_Other::additionnerATousLesElts(Double val)
{
    ChainonDC *c=d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre)
    {
        c->d_v+=val;
        c=c->d_suivant;
    }
}
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS


// DEBUT EXO_INSERER_UN_ELT_DEBUT
void LDC_Other::insererUnEltDebut(Double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion entre d_andre et d_ancre.d_suivant
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    precC->d_suivant = nc;
    nc->d_precedent=precC;
    nc->d_suivant = c;
    c->d_precedent=nc;
}
// FIN EXO_INSERER_UN_ELT_DEBUT


// DEBUT EXO_INSERER_UN_ELT_MILIEU
void LDC_Other::insererUnEltMilieu(Int idx, Double val)
{
    Int i=0;
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && i<idx)
    {
        precC=c;
        c=c->d_suivant;
        i++;
    }

    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion entre precC et c
    precC->d_suivant = nc;
    nc->d_precedent=precC;
    nc->d_suivant = c;
    c->d_precedent=nc;
}
// FIN EXO_INSERER_UN_ELT_MILIEU


// DEBUT EXO_INSERER_UN_ELT_FIN
void LDC_Other::insererUnEltFin(Double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion entre d_andre et d_ancre.d_suivant
    ChainonDC *precC=d_ancre.d_precedent,*c=&d_ancre;
    precC->d_suivant = nc;
    nc->d_precedent=precC;
    nc->d_suivant = c;
    c->d_precedent=nc;
}
// FIN EXO_INSERER_UN_ELT_FIN


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_DEBUT
void LDC_Other::insererPlusieursEltsDebut(Double *tab, Int nb)
{
    // Insertion entre d_andre et d_ancre.d_suivant
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        ChainonDC *nc=new ChainonDC(tab[i]);
        // Insertion entre precC et c
        precC->d_suivant=nc;
        nc->d_precedent=precC;
        nc->d_suivant=c;
        c->d_precedent=nc;
        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_MILIEU
void LDC_Other::insererPlusieursEltsMilieu(Int idx, Double *tab, Int nb)
{
    Int i=0;
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && i<idx)
    {
        precC=c;
        c=c->d_suivant;
        i++;
    }

    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        ChainonDC *nc=new ChainonDC(tab[i]);
        // Insertion entre precC et c
        precC->d_suivant=nc;
        nc->d_precedent=precC;
        nc->d_suivant=c;
        c->d_precedent=nc;
        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_FIN
void LDC_Other::insererPlusieursEltsFin(Double *tab, Int nb)
{
    // Insertion entre d_andre et d_ancre.d_suivant
    ChainonDC *precC=d_ancre.d_precedent,*c=&d_ancre;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        ChainonDC *nc=new ChainonDC(tab[i]);
        // Insertion entre precC et c
        precC->d_suivant=nc;
        nc->d_precedent=precC;
        nc->d_suivant=c;
        c->d_precedent=nc;
        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ENLEVER_UN_ELT_DEBUT
void LDC_Other::enleverUnEltDebut()
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_ancre.d_suivant==&d_ancre)
        return;
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant->d_suivant;
    delete d_ancre.d_suivant;
    precC->d_suivant=c;
    c->d_precedent=precC;
}
// FIN EXO_ENLEVER_UN_ELT_DEBUT


// DEBUT EXO_ENLEVER_UN_ELT_MILIEU
void LDC_Other::enleverUnEltMilieu(Int idx)
{
    Int i=0;
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && i<idx)
    {
        precC=c;
        c=c->d_suivant;
        i++;
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==&d_ancre)
        return;
    c=c->d_suivant;
    delete precC->d_suivant;
    precC->d_suivant=c;
    c->d_precedent=precC;
}
// FIN EXO_ENLEVER_UN_ELT_MILIEU


// DEBUT EXO_ENLEVER_UN_ELT_FIN
void LDC_Other::enleverUnEltFin()
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_ancre.d_precedent==&d_ancre)
        return;
    ChainonDC *precC=d_ancre.d_precedent->d_precedent,*c=&d_ancre;
    delete d_ancre.d_precedent;
    precC->d_suivant=c;
    c->d_precedent=precC;
}
// FIN EXO_ENLEVER_UN_ELT_FIN


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
void LDC_Other::enleverPlusieursEltsDebut(Int nb)
{
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&d_ancre.d_suivant!=&d_ancre && i<nb)
    {
        ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant->d_suivant;
        delete d_ancre.d_suivant;
        precC->d_suivant=c;
        c->d_precedent=precC;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU
void LDC_Other::enleverPlusieursEltsMilieu(Int idx, Int nb)
{
    Int i=0;
    ChainonDC *precC=&d_ancre,*c=d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && i<idx)
    {
        precC=c;
        c=c->d_suivant;
        i++;
    }
    i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&precC->d_suivant!=&d_ancre && i<nb)
    {
        c=precC->d_suivant->d_suivant;
        delete precC->d_suivant;
        precC->d_suivant=c;
        c->d_precedent=precC;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_FIN
void LDC_Other::enleverPlusieursEltsFin(Int nb)
{
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&d_ancre.d_precedent!=&d_ancre && i<nb)
    {
        ChainonDC *precC=d_ancre.d_precedent->d_precedent, *c=&d_ancre;
        delete d_ancre.d_precedent;
        precC->d_suivant=c;
        c->d_precedent=precC;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_FIN



// DEBUT EXO_ELTS_EN_ORDRE_DECROISSANT
bool LDC_Other::eltsEnOrdreDecroissant() const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_ancre.d_suivant==&d_ancre || d_ancre.d_suivant->d_suivant==&d_ancre)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    ChainonDC *c=d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c->d_suivant!=&d_ancre && c->d_v>c->d_suivant->d_v)
    {
        c=c->d_suivant;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c->d_suivant==&d_ancre);}
}
// FIN EXO_ELTS_EN_ORDRE_DECROISSANT


// DEBUT EXO_CONTIENT_ELT
bool LDC_Other::contientElt(Double val) const
{
    ChainonDC *c=d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && c->d_v!=val)
    {
        c=c->d_suivant;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c!=&d_ancre;}
}
// FIN EXO_CONTIENT_ELT


// DEBUT EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT
void LDC_Other::enleverToutesLesOccurencesDUnElt(Double val)
{
    ChainonDC *c=d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_v==val)
        {
            ChainonDC *precC=c->d_precedent;
            c=c->d_suivant;
            delete c->d_precedent;
            precC->d_suivant=c;
            c->d_precedent=precC;
        }
        else
            c=c->d_suivant;
    }
}
// FIN EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT
Int LDC_Other::chercherLIndexDeLaPremiereOccurenceDUnElt(Double val) const
{
    ChainonDC *c=d_ancre.d_suivant;
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre && c->d_v!=val)
    {
        c=c->d_suivant;
        i++;
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c!=&d_ancre)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i;}
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return -1;}
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT
Int LDC_Other::chercherLIndexDeLaDerniereOccurenceDUnElt(Double val) const
{
    ChainonDC *c=d_ancre.d_suivant;
    Int i=0,idx=-1;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_v==val)
            idx=i;
        c=c->d_suivant;
        i++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return idx;}
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
Int LDC_Other::calculerNombreDOccurencesDUnElt(Double val) const
{
    Int idxNbOccurences=0;
    ChainonDC *c=d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=&d_ancre)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_v==val)
            idxNbOccurences++;
        c=c->d_suivant;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return idxNbOccurences;}
}
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT


// DEBUT EXO_OP_ADDITION
LDC operator_plus(const LDC& l1, const LDC& l2)
{
    LDC res;
    ChainonDC *c1=l1.d_ancre.d_suivant,*c2=l2.d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=&l1.d_ancre && c2!=&l2.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(c1->d_v+c2->d_v);
        // Insertion entre d_andre et d_ancre.d_suivant
        ChainonDC *precC=res.d_ancre.d_precedent,*c=&res.d_ancre;
        precC->d_suivant = nc;
        nc->d_precedent=precC;
        nc->d_suivant = c;
        c->d_precedent=nc;

        c1=c1->d_suivant;
        c2=c2->d_suivant;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return res;}
}
// FIN EXO_OP_ADDITION


// DEBUT EXO_OP_SOUSTRACTION
LDC operator_moins(const LDC& l1, const LDC& l2)
{
    LDC res;
    ChainonDC *c1=l1.d_ancre.d_suivant,*c2=l2.d_ancre.d_suivant;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=&l1.d_ancre && c2!=&l2.d_ancre)
    {
        ChainonDC *nc = new ChainonDC(c1->d_v-c2->d_v);
        // Insertion entre d_andre et d_ancre.d_suivant
        ChainonDC *precC=res.d_ancre.d_precedent,*c=&res.d_ancre;
        precC->d_suivant = nc;
        nc->d_precedent=precC;
        nc->d_suivant = c;
        c->d_precedent=nc;

        c1=c1->d_suivant;
        c2=c2->d_suivant;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return res;}
}
// FIN EXO_OP_SOUSTRACTION


// DEBUT EXO_CREER_ITERATEUR
LDC_Other::Iterateur::Iterateur(ChainonDC* ancre):d_ancre{ancre},d_crt{ancre}
{
}

LDC_Other::Iterateur LDC_Other::premier()
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return LDC_Other::Iterateur{&d_ancre};}
}
// FIN EXO_CREER_ITERATEUR


// DEBUT EXO_OP_PLUS_PLUS_ITERATEUR
LDC_Other::Iterateur& LDC_Other::Iterateur::operator++() {
    d_crt = d_crt->d_suivant;
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_PLUS_PLUS_ITERATEUR


// DEBUT EXO_OP_MOINS_MOINS_ITERATEUR
LDC_Other::Iterateur& LDC_Other::Iterateur::operator--() {
    d_crt = d_crt->d_precedent;
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_MOINS_MOINS_ITERATEUR


// DEBUT EXO_OP_ETOILE_ITERATEUR
Double& LDC_Other::Iterateur::operator*() {
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_crt->d_v;}
}
// FIN EXO_OP_ETOILE_ITERATEUR


// DEBUT EXO_OP_ETOILE_CONST_ITERATEUR
Double LDC_Other::Iterateur::operator*() const {
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_crt->d_v;}
}
// FIN EXO_OP_ETOILE_CONST_ITERATEUR


// DEBUT EXO_OP_FIN_ITERATEUR
bool LDC_Other::Iterateur::fin() const {
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_crt==d_ancre;}
}
// FIN EXO_OP_FIN_ITERATEUR
