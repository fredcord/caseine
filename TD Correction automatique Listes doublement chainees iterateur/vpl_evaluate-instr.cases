////////////////////////////////////////////////////////////////////////////////////
// Fichier genere automatiquement pour l'instrumentation du code. Ne pas modifier //
////////////////////////////////////////////////////////////////////////////////////


#ifndef VPL_EVALUATE_CASES
#define VPL_EVALUATE_CASES

////////////////////////////////////////////////////////////////////////////////////////
// Fichier de configuration pour les exercices. Il contient:                          //
// - Pour chaque exercice, le coefficient (nombre de points) pour le calcul de la     //
//   note. Si le coefficent n'est pas spécifié ou s'il est égal à 0, l'exercices est  //
//   ignoré.                                                                          //
// - Un macro pour indiquer si les commentaires doivent être affichés pour l'etudiant //
//                                                                                    //
// Ce fichier doit respecter la syntaxe C++.                                          //
////////////////////////////////////////////////////////////////////////////////////////

// Le nom des classes pour l'etudiant et la correction
#define STRUCT_DE_DONNEES_ETUD LDC
#define STRUCT_DE_DONNEES_CORR LDCCorr


class ChainonDC {
public:
    ChainonDC() : d_v(0), d_precedent(nullptr), d_suivant(nullptr){}
    ChainonDC(Double val) : d_v(val), d_precedent(nullptr), d_suivant(nullptr){}
    Double d_v;
    ChainonDC *d_precedent;
    ChainonDC *d_suivant;
};


// Les membres de la classe
#define MEMBRES_STRUCT_DE_DONNEES  ChainonDC d_ancre;


#define ITERATEUR                  Iterateur
#define ITERATEUR_TYPE_ELEMENT     ChainonDC



#define MONTRER_COMMENTAIRES_ETUDIANT true


// Renommage des methodes

#define ajouterUnEltFin push
#define enleverPlusieursEltsDebut pull


// Le format des macros pour chaque exercice:
//   coefficient, ID_EXO_METHODE,                                              booleen pour montrer les commentaires


#define Q01 {1.0, ID_EXO_CONSTRUCTEUR,                                         MONTRER_COMMENTAIRES_ETUDIANT },
#define Q02 {1.0, ID_EXO_DESTRUCTEUR,                                          MONTRER_COMMENTAIRES_ETUDIANT },
#define Q03 {1.0, ID_EXO_CONSTRUCTEUR_PAR_RECOPIE,                             MONTRER_COMMENTAIRES_ETUDIANT },
#define Q04 {1.0, ID_EXO_CONSTRUCTEUR_AVEC_TABLEAU,                            MONTRER_COMMENTAIRES_ETUDIANT },
#define Q05 {1.0, ID_EXO_CONCATENATION_DEBUT,                                  MONTRER_COMMENTAIRES_ETUDIANT },
#define Q06 {1.0, ID_EXO_CONCATENATION_MILIEU,                                 MONTRER_COMMENTAIRES_ETUDIANT },
#define Q07 {1.0, ID_EXO_CONCATENATION_FIN,                                    MONTRER_COMMENTAIRES_ETUDIANT },
#define Q08 {1.0, ID_EXO_OP_CONCATENATION,                                     MONTRER_COMMENTAIRES_ETUDIANT },
#define Q09 {1.0, ID_EXO_OP_EGAL,                                              MONTRER_COMMENTAIRES_ETUDIANT },
#define Q10 {1.0, ID_EXO_OP_DIFFERENT,                                         MONTRER_COMMENTAIRES_ETUDIANT },
#define Q11 {1.0, ID_EXO_OP_AFFECTATION,                                       MONTRER_COMMENTAIRES_ETUDIANT },
#define Q12 {1.0, ID_EXO_OP_CROCHET,                                           MONTRER_COMMENTAIRES_ETUDIANT },
#define Q13 {1.0, ID_EXO_OP_CROCHET_CONST,                                     MONTRER_COMMENTAIRES_ETUDIANT },
#define Q14 {1.0, ID_EXO_OP_PARENTHESE,                                        MONTRER_COMMENTAIRES_ETUDIANT },
#define Q15 {1.0, ID_EXO_OP_PARENTHESE_CONST,                                  MONTRER_COMMENTAIRES_ETUDIANT },
#define Q16 {1.0, ID_EXO_SETTER,                                               MONTRER_COMMENTAIRES_ETUDIANT },
#define Q17 {1.0, ID_EXO_GETTER,                                               MONTRER_COMMENTAIRES_ETUDIANT },
#define Q18 {1.0, ID_EXO_ADDITIONNER_A_TOUS_LES_ELTS,                          MONTRER_COMMENTAIRES_ETUDIANT },
#define Q19 {1.0, ID_EXO_INSERER_UN_ELT_DEBUT,                                 MONTRER_COMMENTAIRES_ETUDIANT },
#define Q20 {1.0, ID_EXO_INSERER_UN_ELT_MILIEU,                                MONTRER_COMMENTAIRES_ETUDIANT },
#define Q21 {1.0, ID_EXO_INSERER_UN_ELT_FIN,                                   MONTRER_COMMENTAIRES_ETUDIANT },
#define Q22 {1.0, ID_EXO_INSERER_PLUSIEURS_ELTS_DEBUT,                         MONTRER_COMMENTAIRES_ETUDIANT },
#define Q23 {1.0, ID_EXO_INSERER_PLUSIEURS_ELTS_MILIEU,                        MONTRER_COMMENTAIRES_ETUDIANT },
#define Q24 {1.0, ID_EXO_INSERER_PLUSIEURS_ELTS_FIN,                           MONTRER_COMMENTAIRES_ETUDIANT },
#define Q25 {1.0, ID_EXO_ENLEVER_UN_ELT_DEBUT,                                 MONTRER_COMMENTAIRES_ETUDIANT },
#define Q26 {1.0, ID_EXO_ENLEVER_UN_ELT_MILIEU,                                MONTRER_COMMENTAIRES_ETUDIANT },
#define Q27 {1.0, ID_EXO_ENLEVER_UN_ELT_FIN,                                   MONTRER_COMMENTAIRES_ETUDIANT },
#define Q28 {1.0, ID_EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT,                         MONTRER_COMMENTAIRES_ETUDIANT },
#define Q29 {1.0, ID_EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU,                        MONTRER_COMMENTAIRES_ETUDIANT },
#define Q30 {1.0, ID_EXO_ENLEVER_PLUSIEURS_ELTS_FIN,                           MONTRER_COMMENTAIRES_ETUDIANT },
#define Q31 {1.0, ID_EXO_ELTS_EN_ORDRE_DECROISSANT,                            MONTRER_COMMENTAIRES_ETUDIANT },
#define Q32 {1.0, ID_EXO_CONTIENT_ELT,                                         MONTRER_COMMENTAIRES_ETUDIANT },
#define Q33 {1.0, ID_EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT,               MONTRER_COMMENTAIRES_ETUDIANT },
#define Q34 {1.0, ID_EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT,   MONTRER_COMMENTAIRES_ETUDIANT },
#define Q35 {1.0, ID_EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT,   MONTRER_COMMENTAIRES_ETUDIANT },
#define Q36 {1.0, ID_EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT,                MONTRER_COMMENTAIRES_ETUDIANT },
#define Q37 {1.0, ID_EXO_OP_ADDITION,                                          MONTRER_COMMENTAIRES_ETUDIANT },
#define Q38 {1.0, ID_EXO_OP_SOUSTRACTION,                                      MONTRER_COMMENTAIRES_ETUDIANT },


#ifdef ITERATEUR
#define Q41 {1.0, ID_EXO_CREER_ITERATEUR,                                      MONTRER_COMMENTAIRES_ETUDIANT },
#define Q42 {1.0, ID_EXO_OP_PLUS_PLUS_ITERATEUR,                               MONTRER_COMMENTAIRES_ETUDIANT },
#define Q43 {1.0, ID_EXO_OP_MOINS_MOINS_ITERATEUR,                             MONTRER_COMMENTAIRES_ETUDIANT },
#define Q44 {1.0, ID_EXO_OP_ETOILE_ITERATEUR,                                  MONTRER_COMMENTAIRES_ETUDIANT },
#define Q45 {1.0, ID_EXO_OP_ETOILE_CONST_ITERATEUR,                            MONTRER_COMMENTAIRES_ETUDIANT },
#define Q46 {1.0, ID_EXO_OP_FIN_ITERATEUR,                                     MONTRER_COMMENTAIRES_ETUDIANT },
#endif

// DEBUT_ENONCE_STRUCT_DE_DONNEES
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(Double val) : d_v(val), d_precedent(nullptr), d_suivant(nullptr){}
//     Double d_v;
//     ChainonDC *d_precedent;
//     ChainonDC *d_suivant;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
// FIN_ENONCE_STRUCT_DE_DONNEES


#endif

