////////////////////////////////////////////////////////////////////////////////////
// Fichier genere automatiquement pour l'instrumentation du code. Ne pas modifier //
////////////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <algorithm>
#include "ExercicesBase.h"

using namespace std;


/////////////////////////////////////////////////////////////////////////////
// Compare la structure de donnees de l'etudiant avec celle de la solution //
/////////////////////////////////////////////////////////////////////////////
bool LCSCorr_Other::verifierIntegrite(const LCS &param)
{
    // Verifie si la liste chainee est correcte au niveau de sa sctructure et des pointeurs
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!param.d_tete)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(param.d_tete))
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}

    ChainonCS *c=param.d_tete;
    Int compteur=0;
    vector<ChainonCS*> pntrUilise;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && compteur<StructDeDonneesVerifCorr::NB_MAX_COLONNES)
    {
        // Validite des pointeurs
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(c))
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_suiv!=nullptr)
        {
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(c->d_suiv))
                {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        }
        // Verifier si le pointeur est bien unique
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&std::find(pntrUilise.begin(), pntrUilise.end(), c)!=pntrUilise.end())
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        pntrUilise.push_back(c);

        c=c->d_suiv;
        compteur++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c==nullptr;}
}

// Compare la correction avec la struct de l'etudiant. Retourne false si different
bool LCSCorr_Other::compareCorrAvecEtud(const LCS &student) const
{
    ChainonCS *c1=this->d_tete,*c2=student.d_tete;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1==nullptr && c2!=nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}

    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=nullptr)
    {
        // Verifie si le pointeur c2 est valide
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c2==nullptr)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(c2))
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}

        // Verifie le precedent du suivant est correct
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1->d_suiv!=nullptr)
        {
            // Pour la correction aussi
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c2->d_suiv==nullptr)
                {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(c2->d_suiv))
                {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        }
        // Verifier si ce pointeur c2 n'est pas utilise dans la liste correction
        ChainonCS *tmpC=this->d_tete;
        while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&tmpC!=nullptr && c2!=tmpC)
            tmpC=tmpC->d_suiv;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&tmpC!=nullptr)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        // Vérifie s'il contient la meme valeur
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1->d_v!=c2->d_v)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        c1=c1->d_suiv;
        c2=c2->d_suiv;
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c2!=nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}

    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
}

// genere la structDeDonneesVerifCorr a partir de la liste
StructDeDonneesVerifCorr LCSCorr_Other::getStructDeDonneesVerifCorr() const
{
    StructDeDonneesVerifCorr vect;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!d_tete)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return vect;}

    ChainonCS *c=d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr)
    {
        vect.insererUnEltFin(c->d_v);
        c=c->d_suiv;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return vect;}
}

// Constructeur avec la structDeDonneesVerifCorr
void LCSCorr_Other::setStructDeDonneesVerifCorr(const StructDeDonneesVerifCorr &tab)
{
    // Supprime d'abord tous les elements
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete)
    {
        ChainonCS *tete=d_tete;
        do
        {
            ChainonCS *tmp=d_tete->d_suiv;
            {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
            d_tete=tmp;
        }
        while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&d_tete!=tete);
        d_tete=nullptr;
    }
    // On insere les elements du tableau
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<tab.getNbColonnes();i++)
        insererUnEltFin(tab[i]);
}

LCSCorr_ConstructDestruct::LCSCorr_ConstructDestruct():d_tete(nullptr)
{
}

// Constructeur par recopie
LCSCorr_ConstructDestruct::LCSCorr_ConstructDestruct(const LCSCorr &param):d_tete(nullptr)
{
    ChainonCS *lc = param.d_tete, *prevC=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&lc) {
        ChainonCS *nc = new ChainonCS(lc->d_v);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr)
            d_tete = nc;
        else
        {
            prevC->d_suiv = nc;
        }
        prevC = nc;
        lc = lc->d_suiv;
    }
}

// Constructeur avec un tableau
LCSCorr_ConstructDestruct::LCSCorr_ConstructDestruct(Double *tab, Int nb):d_tete(nullptr)
{
    ChainonCS *prevC=nullptr;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        ChainonCS *nc = new ChainonCS(tab[i]);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr)
            d_tete = nc;
        else
        {
            prevC->d_suiv = nc;
        }
        prevC = nc;
    }
}

LCSCorr_ConstructDestruct::~LCSCorr_ConstructDestruct()
{
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&d_tete)
    {
        ChainonCS *tmp=d_tete->d_suiv;
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
        d_tete=tmp;
    }
}


// DEBUT EXO_CONCATENATION_DEBUT
void LCSCorr_Other::concatenationDebut(const LCSCorr &param)
{
    ChainonCS *c1=d_tete,*precC1=nullptr,*c2=param.d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=nullptr)
    {
        ChainonCS* nc=new ChainonCS(c2->d_v);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
        {
            precC1->d_suiv=nc;
        }
        else
            d_tete=nc;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1!=nullptr)
        {
            nc->d_suiv=c1;
        }
        precC1=nc;
        c2=c2->d_suiv;
    }
}
// FIN EXO_CONCATENATION_DEBUT


// DEBUT EXO_CONCATENATION_MILIEU
void LCSCorr_Other::concatenationMilieu(Int idx,const LCSCorr &param)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_tete==nullptr)
        return;
    ChainonCS *c1=d_tete,*precC1=nullptr;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=nullptr && compteur<idx)
    {
        precC1=c1;
        c1=c1->d_suiv;
        compteur++;
    }
    ChainonCS *suivC1=c1;
    ChainonCS *c2=param.d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=nullptr)
    {
        c1=new ChainonCS(c2->d_v);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
        {
            precC1->d_suiv=c1;
        }
        else
        {
            d_tete=c1;
        }
        precC1=c1;
        c2=c2->d_suiv;
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1)
        precC1->d_suiv=suivC1;
}
// FIN EXO_CONCATENATION_MILIEU


// DEBUT EXO_CONCATENATION_FIN
void LCSCorr_Other::concatenationFin(const LCSCorr &param)
{
    ChainonCS *c1=d_tete,*precC1=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    ChainonCS *c2=param.d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=nullptr)
    {
        c1=new ChainonCS(c2->d_v);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
        {
            precC1->d_suiv=c1;
        }
        else
            d_tete=c1;
        precC1=c1;
        c2=c2->d_suiv;
    }
}
// FIN EXO_CONCATENATION_FIN



LCSCorr &LCSCorr_Other::operator+=(const LCSCorr &param)
{
    ChainonCS *c1=d_tete,*precC1=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    ChainonCS *c2=param.d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=nullptr)
    {
        c1=new ChainonCS(c2->d_v);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
        {
            precC1->d_suiv=c1;
        }
        else
            d_tete=c1;
        precC1=c1;
        c2=c2->d_suiv;
    }

    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}

bool LCSCorr_Other::operator==(const LCSCorr &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    ChainonCS *c=d_tete, *lc=param.d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && lc!=nullptr && c->d_v== lc->d_v)
    {
        c=c->d_suiv;
        lc=lc->d_suiv;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c==nullptr && lc==nullptr);}
}


bool LCSCorr_Other::operator!=(const LCSCorr &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    ChainonCS *c=d_tete, *lc=param.d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && lc!=nullptr && c->d_v== lc->d_v)
    {
        c=c->d_suiv;
        lc=lc->d_suiv;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c!=nullptr || lc!=nullptr);}
}

LCSCorr &LCSCorr_Other::operator=(const LCSCorr &param)
{
	if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
		{GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}

	ChainonCS *c1=d_tete,*precC1=nullptr,*c2=param.d_tete;
	while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=nullptr && c2!=nullptr)
	{
		c1->d_v=c2->d_v;
		precC1=c1;
		c1=c1->d_suiv;
		c2=c2->d_suiv;
	}
	if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1!=nullptr)
	{
		if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
			precC1->d_suiv=nullptr;
		else
			d_tete=nullptr;
		while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1)
		{
			precC1=c1;
			c1=c1->d_suiv;
			{GestionPntrEtComplexite::get().peutSupprPntr(precC1, false); delete precC1;}
		}
	}
	else if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c2!=nullptr)
	{
		while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=nullptr)
		{
			c1=new ChainonCS(c2->d_v);
			if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1==nullptr)
				d_tete=c1;
			else
			{
				precC1->d_suiv=c1;
			}
			precC1=c1;
			c2=c2->d_suiv;
		}
	}
	{GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}


// DEBUT EXO_OP_CROCHET
Double &LCSCorr_Other::operator[](Int i)
{
    ChainonCS *c=d_tete;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c->d_v;}
}
// FIN EXO_OP_CROCHET


// DEBUT EXO_OP_CROCHET_CONST
Double LCSCorr_Other::operator[](Int i) const
{
    ChainonCS *c=d_tete;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c->d_v;}
}
// FIN EXO_OP_CROCHET_CONST


// DEBUT EXO_SETTER
void LCSCorr_Other::set(Int i, Double val)
{
    ChainonCS *c=d_tete;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    c->d_v=val;
}
// FIN EXO_SETTER


// DEBUT EXO_GETTER
Double LCSCorr_Other::get(Int i) const
{
    ChainonCS *c=d_tete;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c->d_v;}
}
// FIN EXO_GETTER


void LCSCorr_Other::additionnerATousLesElts(Double val)
{
    ChainonCS *c=d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr)
    {
        c->d_v+=val;
        c=c->d_suiv;
    }
}


void LCSCorr_Other::insererUnEltDebut(Double val)
{
    // Creation du nouveau chainon
    ChainonCS *nc = new ChainonCS(val);
    // Insertion dans une liste vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr) {
        d_tete = nc;
        return;
    }
    // Insertion en tete
    nc->d_suiv = d_tete;
    d_tete = nc;
}


void LCSCorr_Other::insererUnEltMilieu(Int idx, Double val)
{
    Int i=0;
    ChainonCS *c=d_tete,*precC=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }

    // Creation du nouveau chainon
    ChainonCS *nc = new ChainonCS(val);
    // Insertion entre precC et c
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC!=nullptr)
        precC->d_suiv=nc;
    else
        d_tete=nc;
    nc->d_suiv=c;
}


void LCSCorr_Other::insererUnEltFin(Double val)
{
    ChainonCS *c1=d_tete,*precC1=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }

    c1=new ChainonCS(val);
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
    {
        precC1->d_suiv=c1;
    }
    else
        d_tete=c1;
}


void LCSCorr_Other::insererPlusieursEltsDebut(Double *tab, Int nb)
{
    ChainonCS *precC=nullptr,*c=d_tete;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        ChainonCS *nc=new ChainonCS(tab[i]);
        // Insertion entre precC et c
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;

        nc->d_suiv=c;
        precC=nc;
    }
}


void LCSCorr_Other::insererPlusieursEltsMilieu(Int idx, Double *tab, Int nb)
{
    ChainonCS *precC=nullptr,*c=d_tete;
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }

    for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<nb;j++)
    {
        ChainonCS *nc=new ChainonCS(tab[j]);
        // Insertion entre precC et c
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;

        nc->d_suiv=c;
        precC=nc;
    }
}


void LCSCorr_Other::insererPlusieursEltsFin(Double *tab, Int nb)
{
    ChainonCS *precC=nullptr,*c=d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr)
    {
        precC=c;
        c=c->d_suiv;
    }

    for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<nb;j++)
    {
        ChainonCS *nc=new ChainonCS(tab[j]);
        // Insertion apres precC
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;

        precC=nc;
    }
}


void LCSCorr_Other::enleverUnEltDebut()
{
    ChainonCS *precC=nullptr;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete!=nullptr)
    {
        precC=d_tete;
        d_tete=d_tete->d_suiv;
        {GestionPntrEtComplexite::get().peutSupprPntr(precC, false); delete precC;}
    }
}

void LCSCorr_Other::enleverUnEltMilieu(Int idx)
{
    ChainonCS *c=d_tete, *precC=nullptr;
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c!=nullptr)
    {
        ChainonCS *nextC=c->d_suiv;
        {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC)
            precC->d_suiv=nextC;
        else
            d_tete=nextC;
    }
}

void LCSCorr_Other::enleverUnEltFin()
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete->d_suiv==nullptr)
    {
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
        d_tete=nullptr;
        return;
    }
    ChainonCS *c1=d_tete->d_suiv,*precC1=d_tete, *precPrec1=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=nullptr)
    {
        precPrec1=precC1;
        precC1=c1;
        c1=c1->d_suiv;
    }
    {GestionPntrEtComplexite::get().peutSupprPntr(precC1, false); delete precC1;}
    precPrec1->d_suiv=nullptr;
}


void LCSCorr_Other::enleverPlusieursEltsDebut(Int nb)
{
    ChainonCS *precC=nullptr;
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&d_tete!=nullptr && i<nb)
    {
        precC=d_tete;
        d_tete=d_tete->d_suiv;
        {GestionPntrEtComplexite::get().peutSupprPntr(precC, false); delete precC;}
        i++;
    }
}


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU
void LCSCorr_Other::enleverPlusieursEltsMilieu(Int idx, Int nb)
{
    ChainonCS *c=d_tete, *precC=nullptr;
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==nullptr)
        return;
    i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && i<nb)
    {
        ChainonCS *nextC=c->d_suiv;
        {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC)
            precC->d_suiv=nextC;
        else
            d_tete=nextC;
        c=nextC;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_FIN
void LCSCorr_Other::enleverPlusieursEltsFin(Int nb)
{
    // Un premier parcour est necessaire pour connaitre
    // la taille de la liste chainee
    ChainonCS *c=d_tete, *precC=nullptr;
    Int taille=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr)
    {
        precC=c;
        c=c->d_suiv;
        taille++;
    }
    // On se deplace sur le chainon a partir duquel on supprime
    Int i=0;
    c=d_tete;
    precC=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && i<taille-nb)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }
    // Met a jour le dernier chainon, celui juste avant celui qui est supprime
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC!=nullptr)
        precC->d_suiv=nullptr;
    else
        d_tete=nullptr;
    // Supprime les chainon
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr)
    {
        ChainonCS *nextC=c->d_suiv;
        {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
        c=nextC;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_FIN


bool LCSCorr_Other::eltsEnOrdreDecroissant() const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr || d_tete->d_suiv==nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    ChainonCS *c=d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c->d_suiv!=nullptr && c->d_v>c->d_suiv->d_v)
    {
        c=c->d_suiv;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c->d_suiv==nullptr);}
}

bool LCSCorr_Other::contientElt(Double val) const
{
    ChainonCS *c=d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && val!=c->d_v)
    {
        c=c->d_suiv;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c!=nullptr);}
}

void LCSCorr_Other::enleverToutesLesOccurencesDUnElt(Double val)
{
    ChainonCS *c=d_tete,*precC=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_v==val)
        {
            ChainonCS *nextC=c->d_suiv;
            {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC)
                precC->d_suiv=nextC;
            else
                d_tete=nextC;
            c=nextC;
        }
        else
        {
            precC=c;
            c=c->d_suiv;
        }
    }
}


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT
Int LCSCorr_Other::chercherLIndexDeLaPremiereOccurenceDUnElt(Double val) const
{
    ChainonCS *c=d_tete;
    Int idx=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && val!=c->d_v)
    {
        c=c->d_suiv;
        idx++;
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return -1;}
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return idx;}
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT
Int LCSCorr_Other::chercherLIndexDeLaDerniereOccurenceDUnElt(Double val) const
{
    ChainonCS *c=d_tete;
    Int idx=0,idxDerniereOccurence=-1;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&val==c->d_v)
            idxDerniereOccurence=idx;
        c=c->d_suiv;
        idx++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return idxDerniereOccurence;}
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
Int LCSCorr_Other::calculerNombreDOccurencesDUnElt(Double val) const
{
    ChainonCS *c=d_tete;
    Int nbOccurence=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&val==c->d_v)
            nbOccurence++;
        c=c->d_suiv;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return nbOccurence;}
}
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
