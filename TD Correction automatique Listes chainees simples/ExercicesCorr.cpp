#include <iostream>
#include <algorithm>
#include "ExercicesBase.h"

using namespace std;


/////////////////////////////////////////////////////////////////////////////
// Compare la structure de donnees de l'etudiant avec celle de la solution //
/////////////////////////////////////////////////////////////////////////////
bool LCSCorr::verifierIntegrite(const LCS &param)
{
    // Verifie si la liste chainee est correcte au niveau de sa sctructure et des pointeurs
    if (!param.d_tete)
        return true;
    if (!GestionPntrEtComplexite::get().pntrValide(param.d_tete))
        return false;

    ChainonCS *c=param.d_tete;
    int compteur=0;
    vector<ChainonCS*> pntrUilise;
    while(c!=nullptr && compteur<StructDeDonneesVerifCorr::NB_MAX_COLONNES)
    {
        // Validite des pointeurs
        if (!GestionPntrEtComplexite::get().pntrValide(c))
            return false;
        if (c->d_suiv!=nullptr)
        {
            if (!GestionPntrEtComplexite::get().pntrValide(c->d_suiv))
                return false;
        }
        // Verifier si le pointeur est bien unique
        if(std::find(pntrUilise.begin(), pntrUilise.end(), c)!=pntrUilise.end())
            return false;
        pntrUilise.push_back(c);

        c=c->d_suiv;
        compteur++;
    }
    return c==nullptr;
}

// Compare la correction avec la struct de l'etudiant. Retourne false si different
bool LCSCorr::compareCorrAvecEtud(const LCS &student) const
{
    ChainonCS *c1=this->d_tete,*c2=student.d_tete;
    if (c1==nullptr && c2!=nullptr)
        return false;

    while(c1!=nullptr)
    {
        // Verifie si le pointeur c2 est valide
        if (c2==nullptr)
            return false;
        if (!GestionPntrEtComplexite::get().pntrValide(c2))
            return false;

        // Verifie le precedent du suivant est correct
        if (c1->d_suiv!=nullptr)
        {
            // Pour la correction aussi
            if (c2->d_suiv==nullptr)
                return false;
            if (!GestionPntrEtComplexite::get().pntrValide(c2->d_suiv))
                return false;
        }
        // Verifier si ce pointeur c2 n'est pas utilise dans la liste correction
        ChainonCS *tmpC=this->d_tete;
        while(tmpC!=nullptr && c2!=tmpC)
            tmpC=tmpC->d_suiv;
        if (tmpC!=nullptr)
            return false;
        // Vérifie s'il contient la meme valeur
        if (c1->d_v!=c2->d_v)
            return false;
        c1=c1->d_suiv;
        c2=c2->d_suiv;
    }
    if (c2!=nullptr)
        return false;

    return true;
}

// genere la structDeDonneesVerifCorr a partir de la liste
StructDeDonneesVerifCorr LCSCorr::getStructDeDonneesVerifCorr() const
{
    StructDeDonneesVerifCorr vect;
    if (!d_tete)
        return vect;

    ChainonCS *c=d_tete;
    while(c!=nullptr)
    {
        vect.insererUnEltFin(c->d_v);
        c=c->d_suiv;
    }
    return vect;
}

// Constructeur avec la structDeDonneesVerifCorr
void LCSCorr::setStructDeDonneesVerifCorr(const StructDeDonneesVerifCorr &tab)
{
    // Supprime d'abord tous les elements
    if (d_tete)
    {
        ChainonCS *tete=d_tete;
        do
        {
            ChainonCS *tmp=d_tete->d_suiv;
            delete d_tete;
            d_tete=tmp;
        }
        while (d_tete!=tete);
        d_tete=nullptr;
    }
    // On insere les elements du tableau
    for (int i=0;i<tab.getNbColonnes();i++)
        insererUnEltFin(tab[i]);
}

LCSCorr::LCSCorr():d_tete(nullptr)
{
}

// Constructeur par recopie
LCSCorr::LCSCorr(const LCSCorr &param):d_tete(nullptr)
{
    ChainonCS *lc = param.d_tete, *prevC=nullptr;
    while(lc) {
        ChainonCS *nc = new ChainonCS(lc->d_v);
        if(d_tete == nullptr)
            d_tete = nc;
        else
        {
            prevC->d_suiv = nc;
        }
        prevC = nc;
        lc = lc->d_suiv;
    }
}

// Constructeur avec un tableau
LCSCorr::LCSCorr(double *tab, int nb):d_tete(nullptr)
{
    ChainonCS *prevC=nullptr;
    for(int i=0;i<nb;i++)
    {
        ChainonCS *nc = new ChainonCS(tab[i]);
        if(d_tete == nullptr)
            d_tete = nc;
        else
        {
            prevC->d_suiv = nc;
        }
        prevC = nc;
    }
}

LCSCorr::~LCSCorr()
{
    while(d_tete)
    {
        ChainonCS *tmp=d_tete->d_suiv;
        delete d_tete;
        d_tete=tmp;
    }
}


// DEBUT EXO_CONCATENATION_DEBUT
void LCSCorr::concatenationDebut(const LCSCorr &param)
{
    ChainonCS *c1=d_tete,*precC1=nullptr,*c2=param.d_tete;
    while(c2!=nullptr)
    {
        ChainonCS* nc=new ChainonCS(c2->d_v);
        if(precC1!=nullptr)
        {
            precC1->d_suiv=nc;
        }
        else
            d_tete=nc;
        if(c1!=nullptr)
        {
            nc->d_suiv=c1;
        }
        precC1=nc;
        c2=c2->d_suiv;
    }
}
// FIN EXO_CONCATENATION_DEBUT


// DEBUT EXO_CONCATENATION_MILIEU
void LCSCorr::concatenationMilieu(int idx,const LCSCorr &param)
{
    if (param.d_tete==nullptr)
        return;
    ChainonCS *c1=d_tete,*precC1=nullptr;
    int compteur=0;
    while(c1!=nullptr && compteur<idx)
    {
        precC1=c1;
        c1=c1->d_suiv;
        compteur++;
    }
    ChainonCS *suivC1=c1;
    ChainonCS *c2=param.d_tete;
    while(c2!=nullptr)
    {
        c1=new ChainonCS(c2->d_v);
        if(precC1!=nullptr)
        {
            precC1->d_suiv=c1;
        }
        else
        {
            d_tete=c1;
        }
        precC1=c1;
        c2=c2->d_suiv;
    }
    if (precC1)
        precC1->d_suiv=suivC1;
}
// FIN EXO_CONCATENATION_MILIEU


// DEBUT EXO_CONCATENATION_FIN
void LCSCorr::concatenationFin(const LCSCorr &param)
{
    ChainonCS *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    ChainonCS *c2=param.d_tete;
    while(c2!=nullptr)
    {
        c1=new ChainonCS(c2->d_v);
        if(precC1!=nullptr)
        {
            precC1->d_suiv=c1;
        }
        else
            d_tete=c1;
        precC1=c1;
        c2=c2->d_suiv;
    }
}
// FIN EXO_CONCATENATION_FIN



LCSCorr &LCSCorr::operator+=(const LCSCorr &param)
{
    ChainonCS *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    ChainonCS *c2=param.d_tete;
    while(c2!=nullptr)
    {
        c1=new ChainonCS(c2->d_v);
        if(precC1!=nullptr)
        {
            precC1->d_suiv=c1;
        }
        else
            d_tete=c1;
        precC1=c1;
        c2=c2->d_suiv;
    }

    return *this;
}

bool LCSCorr::operator==(const LCSCorr &param) const
{
    if(this==&param)
        return true;
    ChainonCS *c=d_tete, *lc=param.d_tete;
    while(c!=nullptr && lc!=nullptr && c->d_v== lc->d_v)
    {
        c=c->d_suiv;
        lc=lc->d_suiv;
    }
    return (c==nullptr && lc==nullptr);
}


bool LCSCorr::operator!=(const LCSCorr &param) const
{
    if(this==&param)
        return false;
    ChainonCS *c=d_tete, *lc=param.d_tete;
    while(c!=nullptr && lc!=nullptr && c->d_v== lc->d_v)
    {
        c=c->d_suiv;
        lc=lc->d_suiv;
    }
    return (c!=nullptr || lc!=nullptr);
}

LCSCorr &LCSCorr::operator=(const LCSCorr &param)
{
	if(this==&param)
		return *this;

	ChainonCS *c1=d_tete,*precC1=nullptr,*c2=param.d_tete;
	while(c1!=nullptr && c2!=nullptr)
	{
		c1->d_v=c2->d_v;
		precC1=c1;
		c1=c1->d_suiv;
		c2=c2->d_suiv;
	}
	if(c1!=nullptr)
	{
		if(precC1!=nullptr)
			precC1->d_suiv=nullptr;
		else
			d_tete=nullptr;
		while(c1)
		{
			precC1=c1;
			c1=c1->d_suiv;
			delete precC1;
		}
	}
	else if(c2!=nullptr)
	{
		while(c2!=nullptr)
		{
			c1=new ChainonCS(c2->d_v);
			if(precC1==nullptr)
				d_tete=c1;
			else
			{
				precC1->d_suiv=c1;
			}
			precC1=c1;
			c2=c2->d_suiv;
		}
	}
	return *this;
}


// DEBUT EXO_OP_CROCHET
double &LCSCorr::operator[](int i)
{
    ChainonCS *c=d_tete;
    int compteur=0;
    while(c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    return c->d_v;
}
// FIN EXO_OP_CROCHET


// DEBUT EXO_OP_CROCHET_CONST
double LCSCorr::operator[](int i) const
{
    ChainonCS *c=d_tete;
    int compteur=0;
    while(c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    return c->d_v;
}
// FIN EXO_OP_CROCHET_CONST


// DEBUT EXO_SETTER
void LCSCorr::set(int i, double val)
{
    ChainonCS *c=d_tete;
    int compteur=0;
    while(c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    c->d_v=val;
}
// FIN EXO_SETTER


// DEBUT EXO_GETTER
double LCSCorr::get(int i) const
{
    ChainonCS *c=d_tete;
    int compteur=0;
    while(c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    return c->d_v;
}
// FIN EXO_GETTER


void LCSCorr::additionnerATousLesElts(double val)
{
    ChainonCS *c=d_tete;
    while(c!=nullptr)
    {
        c->d_v+=val;
        c=c->d_suiv;
    }
}


void LCSCorr::insererUnEltDebut(double val)
{
    // Creation du nouveau chainon
    ChainonCS *nc = new ChainonCS(val);
    // Insertion dans une liste vide
    if(d_tete == nullptr) {
        d_tete = nc;
        return;
    }
    // Insertion en tete
    nc->d_suiv = d_tete;
    d_tete = nc;
}


void LCSCorr::insererUnEltMilieu(int idx, double val)
{
    int i=0;
    ChainonCS *c=d_tete,*precC=nullptr;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }

    // Creation du nouveau chainon
    ChainonCS *nc = new ChainonCS(val);
    // Insertion entre precC et c
    if(precC!=nullptr)
        precC->d_suiv=nc;
    else
        d_tete=nc;
    nc->d_suiv=c;
}


void LCSCorr::insererUnEltFin(double val)
{
    ChainonCS *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }

    c1=new ChainonCS(val);
    if(precC1!=nullptr)
    {
        precC1->d_suiv=c1;
    }
    else
        d_tete=c1;
}


void LCSCorr::insererPlusieursEltsDebut(double *tab, int nb)
{
    ChainonCS *precC=nullptr,*c=d_tete;
    for(int i=0;i<nb;i++)
    {
        ChainonCS *nc=new ChainonCS(tab[i]);
        // Insertion entre precC et c
        if(precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;

        nc->d_suiv=c;
        precC=nc;
    }
}


void LCSCorr::insererPlusieursEltsMilieu(int idx, double *tab, int nb)
{
    ChainonCS *precC=nullptr,*c=d_tete;
    int i=0;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }

    for(int j=0;j<nb;j++)
    {
        ChainonCS *nc=new ChainonCS(tab[j]);
        // Insertion entre precC et c
        if(precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;

        nc->d_suiv=c;
        precC=nc;
    }
}


void LCSCorr::insererPlusieursEltsFin(double *tab, int nb)
{
    ChainonCS *precC=nullptr,*c=d_tete;
    while(c!=nullptr)
    {
        precC=c;
        c=c->d_suiv;
    }

    for(int j=0;j<nb;j++)
    {
        ChainonCS *nc=new ChainonCS(tab[j]);
        // Insertion apres precC
        if(precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;

        precC=nc;
    }
}


void LCSCorr::enleverUnEltDebut()
{
    ChainonCS *precC=nullptr;
    if(d_tete!=nullptr)
    {
        precC=d_tete;
        d_tete=d_tete->d_suiv;
        delete precC;
    }
}

void LCSCorr::enleverUnEltMilieu(int idx)
{
    ChainonCS *c=d_tete, *precC=nullptr;
    int i=0;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }
    if(c!=nullptr)
    {
        ChainonCS *nextC=c->d_suiv;
        delete c;
        if(precC)
            precC->d_suiv=nextC;
        else
            d_tete=nextC;
    }
}

void LCSCorr::enleverUnEltFin()
{
    if (d_tete==nullptr)
        return;
    if (d_tete->d_suiv==nullptr)
    {
        delete d_tete;
        d_tete=nullptr;
        return;
    }
    ChainonCS *c1=d_tete->d_suiv,*precC1=d_tete, *precPrec1=nullptr;
    while(c1!=nullptr)
    {
        precPrec1=precC1;
        precC1=c1;
        c1=c1->d_suiv;
    }
    delete precC1;
    precPrec1->d_suiv=nullptr;
}


void LCSCorr::enleverPlusieursEltsDebut(int nb)
{
    ChainonCS *precC=nullptr;
    int i=0;
    while(d_tete!=nullptr && i<nb)
    {
        precC=d_tete;
        d_tete=d_tete->d_suiv;
        delete precC;
        i++;
    }
}


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU
void LCSCorr::enleverPlusieursEltsMilieu(int idx, int nb)
{
    ChainonCS *c=d_tete, *precC=nullptr;
    int i=0;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }
    if(c==nullptr)
        return;
    i=0;
    while(c!=nullptr && i<nb)
    {
        ChainonCS *nextC=c->d_suiv;
        delete c;
        if(precC)
            precC->d_suiv=nextC;
        else
            d_tete=nextC;
        c=nextC;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_FIN
void LCSCorr::enleverPlusieursEltsFin(int nb)
{
    // Un premier parcour est necessaire pour connaitre
    // la taille de la liste chainee
    ChainonCS *c=d_tete, *precC=nullptr;
    int taille=0;
    while(c!=nullptr)
    {
        precC=c;
        c=c->d_suiv;
        taille++;
    }
    // On se deplace sur le chainon a partir duquel on supprime
    int i=0;
    c=d_tete;
    precC=nullptr;
    while(c!=nullptr && i<taille-nb)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }
    // Met a jour le dernier chainon, celui juste avant celui qui est supprime
    if (precC!=nullptr)
        precC->d_suiv=nullptr;
    else
        d_tete=nullptr;
    // Supprime les chainon
    while(c!=nullptr)
    {
        ChainonCS *nextC=c->d_suiv;
        delete c;
        c=nextC;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_FIN


bool LCSCorr::eltsEnOrdreDecroissant() const
{
    if(d_tete==nullptr || d_tete->d_suiv==nullptr)
        return true;
    ChainonCS *c=d_tete;
    while(c->d_suiv!=nullptr && c->d_v>c->d_suiv->d_v)
    {
        c=c->d_suiv;
    }
    return (c->d_suiv==nullptr);
}

bool LCSCorr::contientElt(double val) const
{
    ChainonCS *c=d_tete;
    while(c!=nullptr && val!=c->d_v)
    {
        c=c->d_suiv;
    }
    return (c!=nullptr);
}

void LCSCorr::enleverToutesLesOccurencesDUnElt(double val)
{
    ChainonCS *c=d_tete,*precC=nullptr;
    while(c!=nullptr)
    {
        if(c->d_v==val)
        {
            ChainonCS *nextC=c->d_suiv;
            delete c;
            if(precC)
                precC->d_suiv=nextC;
            else
                d_tete=nextC;
            c=nextC;
        }
        else
        {
            precC=c;
            c=c->d_suiv;
        }
    }
}


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT
int LCSCorr::chercherLIndexDeLaPremiereOccurenceDUnElt(double val) const
{
    ChainonCS *c=d_tete;
    int idx=0;
    while(c!=nullptr && val!=c->d_v)
    {
        c=c->d_suiv;
        idx++;
    }
    if (c==nullptr)
        return -1;
    return idx;
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT
int LCSCorr::chercherLIndexDeLaDerniereOccurenceDUnElt(double val) const
{
    ChainonCS *c=d_tete;
    int idx=0,idxDerniereOccurence=-1;
    while(c!=nullptr)
    {
        if (val==c->d_v)
            idxDerniereOccurence=idx;
        c=c->d_suiv;
        idx++;
    }
    return idxDerniereOccurence;
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
int LCSCorr::calculerNombreDOccurencesDUnElt(double val) const
{
    ChainonCS *c=d_tete;
    int nbOccurence=0;
    while(c!=nullptr)
    {
        if (val==c->d_v)
            nbOccurence++;
        c=c->d_suiv;
    }
    return nbOccurence;
}
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
