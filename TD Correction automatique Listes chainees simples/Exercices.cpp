#include <iostream>
#include "ExercicesBase.h"

using namespace std;


// DEBUT EXO_CONSTRUCTEUR
LCS::LCS():d_tete{nullptr}
{
}
// FIN EXO_CONSTRUCTEUR


// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
LCS::LCS(const LCS &param):d_tete{nullptr}
{
    ChainonCS* lc = param.d_tete, *prevC;
    while(lc)
    {
        ChainonCS *nc = new ChainonCS(lc->d_v);
        if(d_tete == nullptr)
            d_tete = nc;
        else
            prevC->d_suiv = nc;
        prevC = nc;
        lc = lc->d_suiv;
    }
}
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE



// DEBUT EXO_CONSTRUCTEUR_AVEC_TABLEAU
LCS::LCS(double *tab, int nb):d_tete(nullptr)
{
    ChainonCS *prevC=nullptr;
    for(int i=0;i<nb;i++)
    {
        ChainonCS *nc = new ChainonCS(tab[i]);
        if(d_tete == nullptr)
            d_tete = nc;
        else
            prevC->d_suiv = nc;
        prevC = nc;
    }
}
// FIN EXO_CONSTRUCTEUR_AVEC_TABLEAU



// DEBUT EXO_DESTRUCTEUR
LCS::~LCS()
{
    while(d_tete)
    {
        ChainonCS *tmp=d_tete->d_suiv;
        delete d_tete;
        d_tete=tmp;
    }
}
// FIN EXO_DESTRUCTEUR



// DEBUT EXO_CONCATENATION_DEBUT
void LCS::concatenationDebut(const LCS &param)
{
    ChainonCS *c1=d_tete,*precC1=nullptr,*c2=param.d_tete;
    while(c2!=nullptr)
    {
        ChainonCS* nc=new ChainonCS(c2->d_v);
        if(precC1!=nullptr)
        {
            precC1->d_suiv=nc;
        }
        else
            d_tete=nc;
        if(c1!=nullptr)
        {
            nc->d_suiv=c1;
        }
        precC1=nc;
        c2=c2->d_suiv;
    }
}
// FIN EXO_CONCATENATION_DEBUT


// DEBUT EXO_CONCATENATION_MILIEU
void LCS::concatenationMilieu(int idx,const LCS &param)
{
    if (param.d_tete==nullptr)
        return;
    ChainonCS *c1=d_tete,*precC1=nullptr;
    int compteur=0;
    while(c1!=nullptr && compteur<idx)
    {
        precC1=c1;
        c1=c1->d_suiv;
        compteur++;
    }
    ChainonCS *suivC1=c1;
    ChainonCS *c2=param.d_tete;
    while(c2!=nullptr)
    {
        c1=new ChainonCS(c2->d_v);
        if(precC1!=nullptr)
        {
            precC1->d_suiv=c1;
        }
        else
        {
            d_tete=c1;
        }
        precC1=c1;
        c2=c2->d_suiv;
    }
    if (precC1)
        precC1->d_suiv=suivC1;
}
// FIN EXO_CONCATENATION_MILIEU


// DEBUT EXO_CONCATENATION_FIN
void LCS::concatenationFin(const LCS &param)
{
    ChainonCS *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    ChainonCS *c2=param.d_tete;
    while(c2!=nullptr)
    {
        c1=new ChainonCS(c2->d_v);
        if(precC1!=nullptr)
        {
            precC1->d_suiv=c1;
        }
        else
            d_tete=c1;
        precC1=c1;
        c2=c2->d_suiv;
    }
}
// FIN EXO_CONCATENATION_FIN




// DEBUT EXO_OP_CONCATENATION
LCS &LCS::operator+=(const LCS &param)
{
    ChainonCS *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    ChainonCS *c2=param.d_tete;
    while(c2!=nullptr)
    {
        c1=new ChainonCS(c2->d_v);
        if(precC1!=nullptr)
        {
            precC1->d_suiv=c1;
        }
        else
            d_tete=c1;
        precC1=c1;
        c2=c2->d_suiv;
    }

    return *this;
}
// FIN EXO_OP_CONCATENATION



// DEBUT EXO_OP_EGAL
bool LCS::operator==(const LCS &param) const
{
    if(this==&param)
        return true;
    ChainonCS *c=d_tete, *lc=param.d_tete;
    while(c!=nullptr && lc!=nullptr && c->d_v== lc->d_v)
    {
        c=c->d_suiv;
        lc=lc->d_suiv;
    }
    return (c==nullptr && lc==nullptr);
}
// FIN EXO_OP_EGAL




// DEBUT EXO_OP_DIFFERENT
bool LCS::operator!=(const LCS &param) const
{
    if(this==&param)
        return false;
    ChainonCS *c=d_tete, *lc=param.d_tete;
    while(c!=nullptr && lc!=nullptr && c->d_v== lc->d_v)
    {
        c=c->d_suiv;
        lc=lc->d_suiv;
    }
    return (c!=nullptr || lc!=nullptr);
}
// FIN EXO_OP_DIFFERENT



// DEBUT EXO_OP_AFFECTATION
LCS &LCS::operator=(const LCS &param)
{
	if(this==&param)
		return *this;

	ChainonCS *c1=d_tete,*precC1=nullptr,*c2=param.d_tete;
	while(c1!=nullptr && c2!=nullptr)
	{
		c1->d_v=c2->d_v;
		precC1=c1;
		c1=c1->d_suiv;
		c2=c2->d_suiv;
	}
	if(c1!=nullptr)
	{
		if(precC1!=nullptr)
			precC1->d_suiv=nullptr;
		else
			d_tete=nullptr;
		while(c1)
		{
			precC1=c1;
			c1=c1->d_suiv;
			delete precC1;
		}
	}
	else if(c2!=nullptr)
	{
		while(c2!=nullptr)
		{
			c1=new ChainonCS(c2->d_v);
			if(precC1==nullptr)
				d_tete=c1;
			else
			{
				precC1->d_suiv=c1;
			}
			precC1=c1;
			c2=c2->d_suiv;
		}
	}
	return *this;
}
// FIN EXO_OP_AFFECTATION



// DEBUT EXO_OP_CROCHET
double &LCS::operator[](int i)
{
    ChainonCS *c=d_tete;
    int compteur=0;
    while(c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    return c->d_v;
}
// FIN EXO_OP_CROCHET



// DEBUT EXO_OP_CROCHET_CONST
double LCS::operator[](int i) const
{
    ChainonCS *c=d_tete;
    int compteur=0;
    while(c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    return c->d_v;
}
// FIN EXO_OP_CROCHET_CONST


// DEBUT EXO_SETTER
void LCS::set(int i, double val)
{
    ChainonCS *c=d_tete;
    int compteur=0;
    while(c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    c->d_v=val;
}
// FIN EXO_SETTER


// DEBUT EXO_GETTER
double LCS::get(int i) const
{
    ChainonCS *c=d_tete;
    int compteur=0;
    while(c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    return c->d_v;
}
// FIN EXO_GETTER


// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
void LCS::additionnerATousLesElts(double val)
{
    ChainonCS *c=d_tete;
    while(c!=nullptr)
    {
        c->d_v+=val;
        c=c->d_suiv;
    }
}
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS



// DEBUT EXO_INSERER_UN_ELT_DEBUT
void LCS::insererUnEltDebut(double val)
{
    // Creation du nouveau chainon
    ChainonCS *nc = new ChainonCS(val);
    // Insertion dans une liste vide
    if(d_tete == nullptr) {
        d_tete = nc;
        return;
    }
    // Insertion en tete
    nc->d_suiv = d_tete;
    d_tete = nc;
}
// FIN EXO_INSERER_UN_ELT_DEBUT



// DEBUT EXO_INSERER_UN_ELT_MILIEU
void LCS::insererUnEltMilieu(int idx, double val)
{
    int i=0;
    ChainonCS *c=d_tete,*precC=nullptr;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }

    // Creation du nouveau chainon
    ChainonCS *nc = new ChainonCS(val);
    // Insertion entre precC et c
    if(precC!=nullptr)
        precC->d_suiv=nc;
    else
        d_tete=nc;
    nc->d_suiv=c;
}
// FIN EXO_INSERER_UN_ELT_MILIEU



// DEBUT EXO_INSERER_UN_ELT_FIN
void LCS::insererUnEltFin(double val)
{
    ChainonCS *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }

    c1=new ChainonCS(val);
    if(precC1!=nullptr)
    {
        precC1->d_suiv=c1;
    }
    else
        d_tete=c1;
}
// FIN EXO_INSERER_UN_ELT_FIN




// DEBUT EXO_INSERER_PLUSIEURS_ELTS_DEBUT
void LCS::insererPlusieursEltsDebut(double *tab, int nb)
{
    ChainonCS *precC=nullptr,*c=d_tete;
    for(int i=0;i<nb;i++)
    {
        ChainonCS *nc=new ChainonCS(tab[i]);
        // Insertion entre precC et c
        if(precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;

        nc->d_suiv=c;
        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_DEBUT



// DEBUT EXO_INSERER_PLUSIEURS_ELTS_MILIEU
void LCS::insererPlusieursEltsMilieu(int idx, double *tab, int nb)
{
    ChainonCS *precC=nullptr,*c=d_tete;
    int i=0;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }

    for(int j=0;j<nb;j++)
    {
        ChainonCS *nc=new ChainonCS(tab[j]);
        // Insertion entre precC et c
        if(precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;

        nc->d_suiv=c;
        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_MILIEU



// DEBUT EXO_INSERER_PLUSIEURS_ELTS_FIN
void LCS::insererPlusieursEltsFin(double *tab, int nb)
{
    ChainonCS *precC=nullptr,*c=d_tete;
    while(c!=nullptr)
    {
        precC=c;
        c=c->d_suiv;
    }

    for(int j=0;j<nb;j++)
    {
        ChainonCS *nc=new ChainonCS(tab[j]);
        // Insertion apres precC
        if(precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;

        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ENLEVER_UN_ELT_DEBUT
void LCS::enleverUnEltDebut()
{
    ChainonCS *precC=nullptr;
    if(d_tete!=nullptr)
    {
        precC=d_tete;
        d_tete=d_tete->d_suiv;
        delete precC;
    }
}
// FIN EXO_ENLEVER_UN_ELT_DEBUT



// DEBUT EXO_ENLEVER_UN_ELT_MILIEU
void LCS::enleverUnEltMilieu(int idx)
{
    ChainonCS *c=d_tete, *precC=nullptr;
    int i=0;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }
    if(c!=nullptr)
    {
        ChainonCS *nextC=c->d_suiv;
        delete c;
        if(precC)
            precC->d_suiv=nextC;
        else
            d_tete=nextC;
    }
}
// FIN EXO_ENLEVER_UN_ELT_MILIEU




// DEBUT EXO_ENLEVER_UN_ELT_FIN
void LCS::enleverUnEltFin()
{
    if (d_tete==nullptr)
        return;
    if (d_tete->d_suiv==nullptr)
    {
        delete d_tete;
        d_tete=nullptr;
        return;
    }
    ChainonCS *c1=d_tete->d_suiv,*precC1=d_tete, *precPrec1=nullptr;
    while(c1!=nullptr)
    {
        precPrec1=precC1;
        precC1=c1;
        c1=c1->d_suiv;
    }
    delete precC1;
    precPrec1->d_suiv=nullptr;
}
// FIN EXO_ENLEVER_UN_ELT_FIN



// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
void LCS::enleverPlusieursEltsDebut(int nb)
{
    ChainonCS *precC=nullptr;
    int i=0;
    while(d_tete!=nullptr && i<nb)
    {
        precC=d_tete;
        d_tete=d_tete->d_suiv;
        delete precC;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT



// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU
void LCS::enleverPlusieursEltsMilieu(int idx, int nb)
{
    ChainonCS *c=d_tete, *precC=nullptr;
    int i=0;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }
    if(c==nullptr)
        return;
    i=0;
    while(c!=nullptr && i<nb)
    {
        ChainonCS *nextC=c->d_suiv;
        delete c;
        if(precC)
            precC->d_suiv=nextC;
        else
            d_tete=nextC;
        c=nextC;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_FIN
void LCS::enleverPlusieursEltsFin(int nb)
{
    // Un premier parcour est necessaire pour connaitre
    // la taille de la liste chainee
    ChainonCS *c=d_tete, *precC=nullptr;
    int taille=0;
    while(c!=nullptr)
    {
        precC=c;
        c=c->d_suiv;
        taille++;
    }
    // On se deplace sur le chainon a partir duquel on supprime
    int i=0;
    c=d_tete;
    precC=nullptr;
    while(c!=nullptr && i<taille-nb)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }
    // Met a jour le dernier chainon, celui juste avant celui qui est supprime
    if (precC!=nullptr)
        precC->d_suiv=nullptr;
    else
        d_tete=nullptr;
    // Supprime les chainon
    while(c!=nullptr)
    {
        ChainonCS *nextC=c->d_suiv;
        delete c;
        c=nextC;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ELTS_EN_ORDRE_DECROISSANT
bool LCS::eltsEnOrdreDecroissant() const
{
    if(d_tete==nullptr || d_tete->d_suiv==nullptr)
        return true;
    ChainonCS *c=d_tete;
    while(c->d_suiv!=nullptr && c->d_v>c->d_suiv->d_v)
    {
        c=c->d_suiv;
    }
    return (c->d_suiv==nullptr);
}
// FIN EXO_ELTS_EN_ORDRE_DECROISSANT



// DEBUT EXO_CONTIENT_ELT
bool LCS::contientElt(double val) const
{
    ChainonCS *c=d_tete;
    while(c!=nullptr && val!=c->d_v)
    {
        c=c->d_suiv;
    }
    return (c!=nullptr);
}
// FIN EXO_CONTIENT_ELT



// DEBUT EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT
void LCS::enleverToutesLesOccurencesDUnElt(double val)
{
    ChainonCS *c=d_tete,*precC=nullptr;
    while(c!=nullptr)
    {
        if(c->d_v==val)
        {
            ChainonCS *nextC=c->d_suiv;
            delete c;
            if(precC)
                precC->d_suiv=nextC;
            else
                d_tete=nextC;
            c=nextC;
        }
        else
        {
            precC=c;
            c=c->d_suiv;
        }
    }
}
// FIN EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT
int LCS::chercherLIndexDeLaPremiereOccurenceDUnElt(double val) const
{
    ChainonCS *c=d_tete;
    int idx=0;
    while(c!=nullptr && val!=c->d_v)
    {
        c=c->d_suiv;
        idx++;
    }
    if (c==nullptr)
        return -1;
    return idx;
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT
int LCS::chercherLIndexDeLaDerniereOccurenceDUnElt(double val) const
{
    ChainonCS *c=d_tete;
    int idx=0,idxDerniereOccurence=-1;
    while(c!=nullptr)
    {
        if (val==c->d_v)
            idxDerniereOccurence=idx;
        c=c->d_suiv;
        idx++;
    }
    return idxDerniereOccurence;
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
int LCS::calculerNombreDOccurencesDUnElt(double val) const
{
    ChainonCS *c=d_tete;
    int nbOccurence=0;
    while(c!=nullptr)
    {
        if (val==c->d_v)
            nbOccurence++;
        c=c->d_suiv;
    }
    return nbOccurence;
}
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT

