#include "Exercices.h"



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le constructeur pour la structure de données ci-dessous
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
LCS::LCS():d_tete{nullptr}
{
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le destructeur pour la structure de données ci-dessous
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
LCS::~LCS()
{
    while(d_tete)
    {
        ChainonCS *tmp=d_tete->d_suiv;
        delete d_tete;
        d_tete=tmp;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le constructeur par recopie pour la structure de données ci-dessous
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
LCS::LCS(const LCS &param):d_tete{nullptr}
{
    ChainonCS* lc = param.d_tete, *prevC;
    while(lc)
    {
        ChainonCS *nc = new ChainonCS(lc->d_v);
        if(d_tete == nullptr)
            d_tete = nc;
        else
            prevC->d_suiv = nc;
        prevC = nc;
        lc = lc->d_suiv;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le constructeur qui prend comme paramètres un tableau avec sa
// taille: double *tab, int val. Ce constructeur doit créer une
// structure de données contenant tous les éléments dans le tableau
// passé en paramètre
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
LCS::LCS(double *tab, int nb):d_tete(nullptr)
{
    ChainonCS *prevC=nullptr;
    for(int i=0;i<nb;i++)
    {
        ChainonCS *nc = new ChainonCS(tab[i]);
        if(d_tete == nullptr)
            d_tete = nc;
        else
            prevC->d_suiv = nc;
        prevC = nc;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode concatenationDebut(param) qui ajoute les elements de la
// structure de données param au début de la structure de données this. Cette
// structure de données param est passée en paramètre. L'ordre des éléments
// doit être gardé.
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LCS::concatenationDebut(const LCS &param)
{
    ChainonCS *c1=d_tete,*precC1=nullptr,*c2=param.d_tete;
    while(c2!=nullptr)
    {
        ChainonCS* nc=new ChainonCS(c2->d_v);
        if(precC1!=nullptr)
        {
            precC1->d_suiv=nc;
        }
        else
            d_tete=nc;
        if(c1!=nullptr)
        {
            nc->d_suiv=c1;
        }
        precC1=nc;
        c2=c2->d_suiv;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode concatenationMilieu(int idx, param) qui insère les
// éléments de la structure de données param en paramètre. Ces éléments
// sont insérés à la position idx dans la stucture de données.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - L'index d'insertion peut être une valeur égale ou plus grande que la taille
//   de la structure de données. Dans ce cas, l'insertion se fait à la fin de la
//   structure de données.
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LCS::concatenationMilieu(int idx,const LCS &param)
{
    if (param.d_tete==nullptr)
        return;
    ChainonCS *c1=d_tete,*precC1=nullptr;
    int compteur=0;
    while(c1!=nullptr && compteur<idx)
    {
        precC1=c1;
        c1=c1->d_suiv;
        compteur++;
    }
    ChainonCS *suivC1=c1;
    ChainonCS *c2=param.d_tete;
    while(c2!=nullptr)
    {
        c1=new ChainonCS(c2->d_v);
        if(precC1!=nullptr)
        {
            precC1->d_suiv=c1;
        }
        else
        {
            d_tete=c1;
        }
        precC1=c1;
        c2=c2->d_suiv;
    }
    if (precC1)
        precC1->d_suiv=suivC1;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode concatenationFin(param) qui ajoute les elements de la
// structure de données param à la fin de la structure de données this. Cette
// structure de données param est passée en paramètre. L'ordre des éléments
// doit être gardé.
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LCS::concatenationFin(const LCS &param)
{
    ChainonCS *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    ChainonCS *c2=param.d_tete;
    while(c2!=nullptr)
    {
        c1=new ChainonCS(c2->d_v);
        if(precC1!=nullptr)
        {
            precC1->d_suiv=c1;
        }
        else
            d_tete=c1;
        precC1=c1;
        c2=c2->d_suiv;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode operator+=(param) qui ajoute les elements de la structure de
// données param à la fin de la structure de données this. Cette structure de
// données param est passée en paramètre
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
LCS &LCS::operator+=(const LCS &param)
{
    ChainonCS *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    ChainonCS *c2=param.d_tete;
    while(c2!=nullptr)
    {
        c1=new ChainonCS(c2->d_v);
        if(precC1!=nullptr)
        {
            precC1->d_suiv=c1;
        }
        else
            d_tete=c1;
        precC1=c1;
        c2=c2->d_suiv;
    }

    return *this;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode operator==(param) qui retourne true si la structure de
// données param passée en paramètre est égale à la structure de données this.
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
bool LCS::operator==(const LCS &param) const
{
    if(this==&param)
        return true;
    ChainonCS *c=d_tete, *lc=param.d_tete;
    while(c!=nullptr && lc!=nullptr && c->d_v== lc->d_v)
    {
        c=c->d_suiv;
        lc=lc->d_suiv;
    }
    return (c==nullptr && lc==nullptr);
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode operator!=(param) qui retourne true si la structure de
// données param passée en paramètre est différente à la structure de données this.
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
bool LCS::operator!=(const LCS &param) const
{
    if(this==&param)
        return false;
    ChainonCS *c=d_tete, *lc=param.d_tete;
    while(c!=nullptr && lc!=nullptr && c->d_v== lc->d_v)
    {
        c=c->d_suiv;
        lc=lc->d_suiv;
    }
    return (c!=nullptr || lc!=nullptr);
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire l'opérateur d'affectation operator=(param). Après cette méthode, la structure
// de données this doit contenir les mêmes éléments que la structure de données param
// passée en paramètre.
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
LCS &LCS::operator=(const LCS &param)
{
	if(this==&param)
		return *this;

	ChainonCS *c1=d_tete,*precC1=nullptr,*c2=param.d_tete;
	while(c1!=nullptr && c2!=nullptr)
	{
		c1->d_v=c2->d_v;
		precC1=c1;
		c1=c1->d_suiv;
		c2=c2->d_suiv;
	}
	if(c1!=nullptr)
	{
		if(precC1!=nullptr)
			precC1->d_suiv=nullptr;
		else
			d_tete=nullptr;
		while(c1)
		{
			precC1=c1;
			c1=c1->d_suiv;
			delete precC1;
		}
	}
	else if(c2!=nullptr)
	{
		while(c2!=nullptr)
		{
			c1=new ChainonCS(c2->d_v);
			if(precC1==nullptr)
				d_tete=c1;
			else
			{
				precC1->d_suiv=c1;
			}
			precC1=c1;
			c2=c2->d_suiv;
		}
	}
	return *this;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void additionnerATousLesElts(double val) qui ajoute la valeur
// val passée en paramètre à tous les éléments de la stucture de données.
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LCS::additionnerATousLesElts(double val)
{
    ChainonCS *c=d_tete;
    while(c!=nullptr)
    {
        c->d_v+=val;
        c=c->d_suiv;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererUnEltDebut(double val) qui insère un élément
// contenant la valeur val passée en paramètre. Cet élément est inséré au
// début de la stucture de données.
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LCS::insererUnEltDebut(double val)
{
    // Creation du nouveau chainon
    ChainonCS *nc = new ChainonCS(val);
    // Insertion dans une liste vide
    if(d_tete == nullptr) {
        d_tete = nc;
        return;
    }
    // Insertion en tete
    nc->d_suiv = d_tete;
    d_tete = nc;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererUnEltMilieu(int idx, double val) qui
// insère un élément contenant la valeur val passée en paramètre. Cet élément
// est inséré à la position idx dans la stucture de données.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - L'index d'insertion peut être une valeur égale ou plus grande que la taille
//   de la structure de données. Dans ce cas, l'insertion se fait à la fin de la
//   structure de données.
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LCS::insererUnEltMilieu(int idx, double val)
{
    int i=0;
    ChainonCS *c=d_tete,*precC=nullptr;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }

    // Creation du nouveau chainon
    ChainonCS *nc = new ChainonCS(val);
    // Insertion entre precC et c
    if(precC!=nullptr)
        precC->d_suiv=nc;
    else
        d_tete=nc;
    nc->d_suiv=c;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererUnEltFin(double val) qui insère un élément
// contenant la valeur val passée en paramètre. Cet élément est inséré à la
// fin de la stucture de données.
// Remarques:
// - La structure de données peut être vide
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LCS::insererUnEltFin(double val)
{
    ChainonCS *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }

    c1=new ChainonCS(val);
    if(precC1!=nullptr)
    {
        precC1->d_suiv=c1;
    }
    else
        d_tete=c1;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererPlusieursEltsDebut(double *tab, int nb)
// qui insère nb éléments au début de la structures de données. Les éléments
// à inserer sont le tableau tab. L'ordre des éléments dans le tableau doit
// être gardé dans la structure de données.
// Remarques:
// - Le paramètre nb peut être égal à zéro
// - La structure de données peut être vide
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LCS::insererPlusieursEltsDebut(double *tab, int nb)
{
    ChainonCS *precC=nullptr,*c=d_tete;
    for(int i=0;i<nb;i++)
    {
        ChainonCS *nc=new ChainonCS(tab[i]);
        // Insertion entre precC et c
        if(precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;

        nc->d_suiv=c;
        precC=nc;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererPlusieursEltsMilieu(int idx, double *tab, int nb)
// qui insère nb éléments à la position idx dans la structures de données. Les éléments
// à inserer sont le tableau tab. L'ordre des éléments dans le tableau doit
// être gardé dans la structure de données.
// Remarques:
// - Le paramètre nb peut être égal à 0.
// - L'index du premier élément de la structure de données est 0.
// - L'index d'insertion idx peut être une valeur égale ou plus grande que la taille
//   de la structure de données. Dans ce cas, l'insertion se fait à la fin de la
//   structure de données.
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LCS::insererPlusieursEltsMilieu(int idx, double *tab, int nb)
{
    ChainonCS *precC=nullptr,*c=d_tete;
    int i=0;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }

    for(int j=0;j<nb;j++)
    {
        ChainonCS *nc=new ChainonCS(tab[j]);
        // Insertion entre precC et c
        if(precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;

        nc->d_suiv=c;
        precC=nc;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererPlusieursEltsFin(double *tab, int nb)
// qui insère nb éléments à la fin de la structures de données. Les éléments
// à inserer sont le tableau tab. L'ordre des éléments dans le tableau doit
// être gardé dans la structure de données.
// Remarques:
// - Le paramètre nb peut être égal à zéro
// - La structure de données peut être vide
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LCS::insererPlusieursEltsFin(double *tab, int nb)
{
    ChainonCS *precC=nullptr,*c=d_tete;
    while(c!=nullptr)
    {
        precC=c;
        c=c->d_suiv;
    }

    for(int j=0;j<nb;j++)
    {
        ChainonCS *nc=new ChainonCS(tab[j]);
        // Insertion apres precC
        if(precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;

        precC=nc;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverUnEltDebut() qui enlève le premier élément
// de la structure de données.
// Remarque:
// - La structure de données peut être vide
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LCS::enleverUnEltDebut()
{
    ChainonCS *precC=nullptr;
    if(d_tete!=nullptr)
    {
        precC=d_tete;
        d_tete=d_tete->d_suiv;
        delete precC;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverUnEltMilieu(int idx) qui enlève
// l'élément d'index idx dans la structure de données.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - La structure de données peut être vide.
// - La valeur idx peut être plus grande ou égale à la taille de la structure
//   de données. Dans ce cas, l'élément n'est pas supprimé.
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LCS::enleverUnEltMilieu(int idx)
{
    ChainonCS *c=d_tete, *precC=nullptr;
    int i=0;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }
    if(c!=nullptr)
    {
        ChainonCS *nextC=c->d_suiv;
        delete c;
        if(precC)
            precC->d_suiv=nextC;
        else
            d_tete=nextC;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverUnEltFin() qui enlève le dernier élément
// de la structure de données.
// Remarque:
// - La structure de données peut être vide
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LCS::enleverUnEltFin()
{
    if (d_tete==nullptr)
        return;
    if (d_tete->d_suiv==nullptr)
    {
        delete d_tete;
        d_tete=nullptr;
        return;
    }
    ChainonCS *c1=d_tete->d_suiv,*precC1=d_tete, *precPrec1=nullptr;
    while(c1!=nullptr)
    {
        precPrec1=precC1;
        precC1=c1;
        c1=c1->d_suiv;
    }
    delete precC1;
    precPrec1->d_suiv=nullptr;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverPlusieursEltsDebut(int nb) qui enlève nb
// éléments au début de la structures de données. Cette valeur nb est passée
// en paramètre.
// Remarque:
// - La valeur nb peut être plus grande que la taille de la structures de
// données. Dans ce cas, tous les éléments doivent être supprimés.
// - La valeur nb peut être égale à 0. Dans ce cas, rien n'est supprimé.
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LCS::enleverPlusieursEltsDebut(int nb)
{
    ChainonCS *precC=nullptr;
    int i=0;
    while(d_tete!=nullptr && i<nb)
    {
        precC=d_tete;
        d_tete=d_tete->d_suiv;
        delete precC;
        i++;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverPlusieursEltsMilieu(int idx, int nb) qui
// enlève nb éléments à partir de l'élément d'index idx. Ces valeurs idx et
// nb sont passées en paramètre.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - La valeur nb peut être plus grande que la taille de la structures de
// données.
// - La valeur idx peut être plus grande ou égale à la taille de la structure
// de données. Dans ce cas, les éléments ne sont pas supprimés.
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LCS::enleverPlusieursEltsMilieu(int idx, int nb)
{
    ChainonCS *c=d_tete, *precC=nullptr;
    int i=0;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }
    if(c==nullptr)
        return;
    i=0;
    while(c!=nullptr && i<nb)
    {
        ChainonCS *nextC=c->d_suiv;
        delete c;
        if(precC)
            precC->d_suiv=nextC;
        else
            d_tete=nextC;
        c=nextC;
        i++;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverPlusieursEltsFin(int nb) qui enlève nb
// éléments à la fin de la structures de données. Cette valeur nb est passée
// en paramètre.
// Remarque:
// - La valeur nb peut être plus grande que la taille de la structures de
// données.
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LCS::enleverPlusieursEltsFin(int nb)
{
    // Un premier parcour est necessaire pour connaitre
    // la taille de la liste chainee
    ChainonCS *c=d_tete, *precC=nullptr;
    int taille=0;
    while(c!=nullptr)
    {
        precC=c;
        c=c->d_suiv;
        taille++;
    }
    // On se deplace sur le chainon a partir duquel on supprime
    int i=0;
    c=d_tete;
    precC=nullptr;
    while(c!=nullptr && i<taille-nb)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }
    // Met a jour le dernier chainon, celui juste avant celui qui est supprime
    if (precC!=nullptr)
        precC->d_suiv=nullptr;
    else
        d_tete=nullptr;
    // Supprime les chainon
    while(c!=nullptr)
    {
        ChainonCS *nextC=c->d_suiv;
        delete c;
        c=nextC;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode eltsEnOrdreDecroissant() qui retourne true si les éléments
// dans la structures de données sont classés par ordre strictement décroissant.
// Remarque:
// - eltsEnOrdreDecroissant() doit retourner true pour une structure de données vide
// ou ne contenant qu'un seul élément.
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
bool LCS::eltsEnOrdreDecroissant() const
{
    if(d_tete==nullptr || d_tete->d_suiv==nullptr)
        return true;
    ChainonCS *c=d_tete;
    while(c->d_suiv!=nullptr && c->d_v>c->d_suiv->d_v)
    {
        c=c->d_suiv;
    }
    return (c->d_suiv==nullptr);
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode contientElt(double val) qui retourne true si la structure
// de données contient au moins un élément avec la valeur val, val étant un
// paramètre de la méthode.
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
bool LCS::contientElt(double val) const
{
    ChainonCS *c=d_tete;
    while(c!=nullptr && val!=c->d_v)
    {
        c=c->d_suiv;
    }
    return (c!=nullptr);
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverToutesLesOccurencesDUnElt(double val) qui
// enlève tous les éléments contenant la valeur val dans la structure de
// données, val étant un paramètre de la méthode.
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void LCS::enleverToutesLesOccurencesDUnElt(double val)
{
    ChainonCS *c=d_tete,*precC=nullptr;
    while(c!=nullptr)
    {
        if(c->d_v==val)
        {
            ChainonCS *nextC=c->d_suiv;
            delete c;
            if(precC)
                precC->d_suiv=nextC;
            else
                d_tete=nextC;
            c=nextC;
        }
        else
        {
            precC=c;
            c=c->d_suiv;
        }
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode int chercherLIndexDeLaPremiereOccurenceDUnElt(double val)
// qui retourne l'index du premier l'élément contenant la valeur val dans la
// structure de données, val étant un paramètre de la méthode.
// Remarques:
// - La structure de données ne contient pas nécessairement la valeur val. Dans ce
//   cas, la méthode doit retourner -1
// - La structure de données peut être vide
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
int LCS::chercherLIndexDeLaPremiereOccurenceDUnElt(double val) const
{
    ChainonCS *c=d_tete;
    int idx=0;
    while(c!=nullptr && val!=c->d_v)
    {
        c=c->d_suiv;
        idx++;
    }
    if (c==nullptr)
        return -1;
    return idx;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode int chercherLIndexDeLaDerniereOccurenceDUnElt(double val)
// qui retourne l'index du dernier l'élément contenant la valeur val dans la
// structure de données, val étant un paramètre de la méthode.
// Remarques:
// - La structure de données ne contient pas nécessairement la valeur val. Dans ce
//   cas, la méthode doit retourner -1
// - La structure de données peut être vide
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
int LCS::chercherLIndexDeLaDerniereOccurenceDUnElt(double val) const
{
    ChainonCS *c=d_tete;
    int idx=0,idxDerniereOccurence=-1;
    while(c!=nullptr)
    {
        if (val==c->d_v)
            idxDerniereOccurence=idx;
        c=c->d_suiv;
        idx++;
    }
    return idxDerniereOccurence;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode int calculerNombreDOccurencesDUnElt(double val) qui
// retourne le nombre d'éléments contenant la valeur val dans la
// structure de données, val étant un paramètre de la méthode.
// Remarques:
// - La structure de données ne contient pas nécessairement la valeur val. Dans ce
//   cas, la méthode doit retourner 0
// - La structure de données peut être vide
//
// class ChainonCS {
//   friend class LCS;
//   private:
//     ChainonCS(double val) : d_v(val), d_suiv(nullptr){}
//     double d_v;
//     ChainonCS *d_suiv;
// };
//
// class LCS {
//   public:
//     LCS();
//     ~LCS();
//     ...
//
//  private:
//    ChainonCS *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
int LCS::calculerNombreDOccurencesDUnElt(double val) const
{
    ChainonCS *c=d_tete;
    int nbOccurence=0;
    while(c!=nullptr)
    {
        if (val==c->d_v)
            nbOccurence++;
        c=c->d_suiv;
    }
    return nbOccurence;
}



