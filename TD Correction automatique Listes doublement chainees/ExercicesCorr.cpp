#include <iostream>
#include <algorithm>
#include "ExercicesBase.h"


using namespace std;


/////////////////////////////////////////////////////////////////////////////
// Compare la structure de donnees de l'etudiant avec celle de la solution //
/////////////////////////////////////////////////////////////////////////////
bool LDCCorr::verifierIntegrite(const LDC &param)
{
    // Verifie si la liste chainee est correcte au niveau de sa sctructure et des pointeurs
    if (!param.d_tete)
        return true;
    if (!GestionPntrEtComplexite::get().pntrValide(param.d_tete))
        return false;
    if (param.d_tete->d_prec!=nullptr)
        return false;

    ChainonDC *c=param.d_tete;
    int compteur=0;
    vector<ChainonDC*> pntrUilise;
    while(c!=nullptr && compteur<StructDeDonneesVerifCorr::NB_MAX_COLONNES)
    {
        // Validite des pointeurs
        if (!GestionPntrEtComplexite::get().pntrValide(c))
            return false;
        if (c->d_prec!=nullptr)
        {
            if (!GestionPntrEtComplexite::get().pntrValide(c->d_prec))
                return false;
            if (c->d_prec->d_suiv!=c)
                return false;
        }
        if (c->d_suiv!=nullptr)
        {
            if (!GestionPntrEtComplexite::get().pntrValide(c->d_suiv))
                return false;
            if (c->d_suiv->d_prec!=c)
                return false;
        }
        // Verifier si le pointeur est bien unique
        if(std::find(pntrUilise.begin(), pntrUilise.end(), c)!=pntrUilise.end())
            return false;
        pntrUilise.push_back(c);

        c=c->d_suiv;
        compteur++;
    }
    return c==nullptr;
}

// Compare la correction avec la struct de l'etudiant. Retourne false si different
bool LDCCorr::compareCorrAvecEtud(const LDC &student) const
{
    ChainonDC *c1=this->d_tete,*c2=student.d_tete;
    if (c1==nullptr && c2!=nullptr)
        return false;

    while(c1!=nullptr)
    {
        // Verifie si le pointeur c2 est valide
        if (c2==nullptr)
            return false;
        if (!GestionPntrEtComplexite::get().pntrValide(c2))
            return false;

        // Verifie le precedent du suivant est correct
        if (c1->d_suiv!=nullptr)
        {
            // Pour la correction aussi
            if (!GestionPntrEtComplexite::get().pntrValide(c1->d_suiv) || c1->d_suiv->d_prec!=c1)
            {
                cout<<"Erreur sur la correction"<<endl;
                return false;
            }
            if (c2->d_suiv==nullptr)
                return false;
            if (!GestionPntrEtComplexite::get().pntrValide(c2->d_suiv) || c2->d_suiv->d_prec!=c2)
                return false;
        }
        // Verifier si ce pointeur c2 n'est pas utilise dans la liste correction
        ChainonDC *tmpC=this->d_tete;
        while(tmpC!=nullptr && c2!=tmpC)
            tmpC=tmpC->d_suiv;
        if (tmpC!=nullptr)
            return false;
        // Vérifie s'il contient la meme valeur
        if (c1->d_v!=c2->d_v)
            return false;
        // Verifie le chainon precedent
        if (c1->d_prec==nullptr)
        {
            if (c2->d_prec!=nullptr) return false;
        }
        else
        {
            if (!GestionPntrEtComplexite::get().pntrValide(c2->d_prec))
                return false;
            if (c1->d_prec->d_v!=c2->d_prec->d_v)
                return false;
        }
        c1=c1->d_suiv;
        c2=c2->d_suiv;
    }
    if (c2!=nullptr)
        return false;

    return true;
}

// genere la structDeDonneesVerifCorr a partir de la liste
StructDeDonneesVerifCorr LDCCorr::getStructDeDonneesVerifCorr() const
{
    StructDeDonneesVerifCorr vect;
    if (!d_tete)
        return vect;

    ChainonDC *c=d_tete;
    while(c!=nullptr)
    {
        vect.insererUnEltFin(c->d_v);
        c=c->d_suiv;
    }
    return vect;
}

// Constructeur avec la structDeDonneesVerifCorr
void LDCCorr::setStructDeDonneesVerifCorr(const StructDeDonneesVerifCorr &tab)
{
    // Supprime d'abord tous les elements
    if (d_tete)
    {
        ChainonDC *tete=d_tete;
        do
        {
            ChainonDC *tmp=d_tete->d_suiv;
            delete d_tete;
            d_tete=tmp;
        }
        while (d_tete!=tete);
        d_tete=nullptr;
    }
    // On insere les elements du tableau
    for (int i=0;i<tab.getNbColonnes();i++)
        insererUnEltFin(tab[i]);
}

// Constructeur par recopie
LDCCorr::LDCCorr():d_tete(nullptr)
{
}

// Constructeur par recopie
LDCCorr::LDCCorr(const LDCCorr &param):d_tete(nullptr)
{
    ChainonDC *lc = param.d_tete, *prevC=nullptr;
    while(lc) {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        if(d_tete == nullptr)
            d_tete = nc;
        else
        {
            prevC->d_suiv = nc;
            nc->d_prec=prevC;
        }
        prevC = nc;
        lc = lc->d_suiv;
    }
}

// Constructeur avec un tableau
LDCCorr::LDCCorr(double *tab, int nb):d_tete(nullptr)
{
    ChainonDC *prevC=nullptr;
    for(int i=0;i<nb;i++)
    {
        ChainonDC *nc = new ChainonDC(tab[i]);
        if(d_tete == nullptr)
            d_tete = nc;
        else
        {
            prevC->d_suiv = nc;
            nc->d_prec=prevC;
        }
        prevC = nc;
    }
}

LDCCorr::~LDCCorr()
{
    while(d_tete)
    {
        ChainonDC *tmp=d_tete->d_suiv;
        delete d_tete;
        d_tete=tmp;
    }
}


void LDCCorr::insererUnEltDebut(double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion dans une liste vide
    if(d_tete == nullptr) {
        d_tete = nc;
        return;
    }
    // Insertion en tete
    nc->d_suiv = d_tete;
    d_tete->d_prec=nc;
    d_tete = nc;
}


void LDCCorr::insererUnEltMilieu(int idx, double val)
{
    int i=0;
    ChainonDC *c=d_tete,*precC=nullptr;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }

    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion entre precC et c
    nc->d_prec=precC;
    if(precC!=nullptr)
        precC->d_suiv=nc;
    else
        d_tete=nc;
    nc->d_suiv=c;
    if(c!=nullptr)
        c->d_prec=nc;
}


void LDCCorr::insererUnEltFin(double val)
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }

    c1=new ChainonDC(val);
    if(precC1!=nullptr)
    {
        precC1->d_suiv=c1;
        c1->d_prec=precC1;
    }
    else
        d_tete=c1;
}


void LDCCorr::insererPlusieursEltsDebut(double *tab, int nb)
{
    ChainonDC *precC=nullptr,*c=d_tete;
    for(int i=0;i<nb;i++)
    {
        ChainonDC *nc=new ChainonDC(tab[i]);
        // Insertion entre precC et c
        if(precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;
        nc->d_prec=precC;

        nc->d_suiv=c;
        if(c!=nullptr)
            c->d_prec=nc;
        precC=nc;
    }
}

void LDCCorr::insererPlusieursEltsMilieu(int idx, double *tab, int nb)
{
    ChainonDC *precC=nullptr,*c=d_tete;
    int i=0;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }

    for(int j=0;j<nb;j++)
    {
        ChainonDC *nc=new ChainonDC(tab[j]);
        // Insertion entre precC et c
        if(precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;
        nc->d_prec=precC;

        nc->d_suiv=c;
        if(c!=nullptr)
            c->d_prec=nc;
        precC=nc;
    }
}


void LDCCorr::insererPlusieursEltsFin(double *tab, int nb)
{
    ChainonDC *precC=nullptr,*c=d_tete;
    while(c!=nullptr)
    {
        precC=c;
        c=c->d_suiv;
    }

    for(int j=0;j<nb;j++)
    {
        ChainonDC *nc=new ChainonDC(tab[j]);
        // Insertion apres precC
        if(precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;
        nc->d_prec=precC;

        precC=nc;
    }
}


void LDCCorr::enleverUnEltDebut()
{
    ChainonDC *precC=nullptr;
    if(d_tete!=nullptr)
    {
        precC=d_tete;
        d_tete=d_tete->d_suiv;
        delete precC;
        if(d_tete!=nullptr)
            d_tete->d_prec=nullptr;
    }
}

void LDCCorr::enleverUnEltMilieu(int idx)
{
    ChainonDC *c=d_tete, *precC=nullptr;
    int i=0;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }
    if(c!=nullptr)
    {
        ChainonDC *nextC=c->d_suiv;
        delete c;
        if(precC)
            precC->d_suiv=nextC;
        else
            d_tete=nextC;
        if(nextC)
            nextC->d_prec=precC;
    }
}

void LDCCorr::enleverUnEltFin()
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    if(precC1!=nullptr)
    {
        c1=precC1;
        precC1=precC1->d_prec;
        delete c1;
    }
    if(precC1==nullptr)
        d_tete=nullptr;
    else
        precC1->d_suiv=nullptr;
}

void LDCCorr::enleverPlusieursEltsDebut(int nb)
{
    ChainonDC *precC=nullptr;
    int i=0;
    while(d_tete!=nullptr && i<nb)
    {
        precC=d_tete;
        d_tete=d_tete->d_suiv;
        delete precC;
        if(d_tete!=nullptr)
            d_tete->d_prec=nullptr;
        i++;
    }
}

void LDCCorr::enleverPlusieursEltsMilieu(int idx, int nb)
{
    ChainonDC *c=d_tete, *precC=nullptr;
    int i=0;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }
    if(c==nullptr)
        return;
    i=0;
    while(c!=nullptr && i<nb)
    {
        ChainonDC *nextC=c->d_suiv;
        delete c;
        if(precC)
            precC->d_suiv=nextC;
        else
            d_tete=nextC;
        if(nextC)
            nextC->d_prec=precC;

        c=nextC;
        i++;
    }
}

void LDCCorr::enleverPlusieursEltsFin(int nb)
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }

    int i=0;
    while(precC1!=nullptr && i<nb)
    {
        c1=precC1;
        precC1=precC1->d_prec;
        delete c1;
        i++;
    }
    if(precC1==nullptr)
        d_tete=nullptr;
    else
        precC1->d_suiv=nullptr;
}


// DEBUT EXO_CONCATENATION_DEBUT
void LDCCorr::concatenationDebut(const LDCCorr &param)
{
    ChainonDC *c1=d_tete,*precC1=nullptr,*c2=param.d_tete;
    while(c2!=nullptr)
    {
        ChainonDC* nc=new ChainonDC(c2->d_v);
        if(precC1!=nullptr)
        {
            precC1->d_suiv=nc;
            nc->d_prec=precC1;
        }
        else
            d_tete=nc;
        if(c1!=nullptr)
        {
            c1->d_prec=nc;
            nc->d_suiv=c1;
        }
        precC1=nc;
        c2=c2->d_suiv;
    }
}
// FIN EXO_CONCATENATION_DEBUT


// DEBUT EXO_CONCATENATION_MILIEU
void LDCCorr::concatenationMilieu(int idx, const LDCCorr &param)
{
    ChainonDC *precC=nullptr,*c=d_tete;
    int i=0;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }

    ChainonDC *c2=param.d_tete;
    while(c2!=nullptr)
    {
        ChainonDC *nc=new ChainonDC(c2->d_v);
        // Insertion entre precC et c
        if (precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;
        nc->d_prec=precC;

        nc->d_suiv=c;
        if (c!=nullptr)
            c->d_prec=nc;
        precC=nc;
        c2=c2->d_suiv;
    }
}
// FIN EXO_CONCATENATION_MILIEU


// DEBUT EXO_CONCATENATION_FIN
void LDCCorr::concatenationFin(const LDCCorr &param)
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    ChainonDC *c2=param.d_tete;
    while(c2!=nullptr)
    {
        c1=new ChainonDC(c2->d_v);
        if(precC1!=nullptr)
        {
            precC1->d_suiv=c1;
            c1->d_prec=precC1;
        }
        else
            d_tete=c1;
        precC1=c1;
        c2=c2->d_suiv;
    }
}
// FIN EXO_CONCATENATION_FIN



LDCCorr &LDCCorr::operator+=(const LDCCorr &param)
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    ChainonDC *c2=param.d_tete;
    while(c2!=nullptr)
    {
        c1=new ChainonDC(c2->d_v);
        if(precC1!=nullptr)
        {
            precC1->d_suiv=c1;
            c1->d_prec=precC1;
        }
        else
            d_tete=c1;
        precC1=c1;
        c2=c2->d_suiv;
    }

    return *this;
}


bool LDCCorr::operator==(const LDCCorr &param) const
{
    if(this==&param)
        return true;
    ChainonDC *c=d_tete, *lc=param.d_tete;
    while(c!=nullptr && lc!=nullptr && c->d_v== lc->d_v)
    {
        c=c->d_suiv;
        lc=lc->d_suiv;
    }
    return (c==nullptr && lc==nullptr);
}


bool LDCCorr::operator!=(const LDCCorr &param) const
{
    if(this==&param)
        return false;
    ChainonDC *c=d_tete, *lc=param.d_tete;
    while(c!=nullptr && lc!=nullptr && c->d_v== lc->d_v)
    {
        c=c->d_suiv;
        lc=lc->d_suiv;
    }
    return (c!=nullptr || lc!=nullptr);
}

LDCCorr &LDCCorr::operator=(const LDCCorr &param)
{
	if(this==&param)
		return *this;

	ChainonDC *c1=d_tete,*precC1=nullptr,*c2=param.d_tete;
	while(c1!=nullptr && c2!=nullptr)
	{
		c1->d_v=c2->d_v;
		precC1=c1;
		c1=c1->d_suiv;
		c2=c2->d_suiv;
	}
	if(c1!=nullptr)
	{
		if(precC1!=nullptr)
			precC1->d_suiv=nullptr;
		else
			d_tete=nullptr;
		while(c1)
		{
			precC1=c1;
			c1=c1->d_suiv;
			delete precC1;
		}
	}
	else if(c2!=nullptr)
	{
		while(c2!=nullptr)
		{
			c1=new ChainonDC(c2->d_v);
			if(precC1==nullptr)
				d_tete=c1;
			else
			{
				precC1->d_suiv=c1;
				c1->d_prec=precC1;
			}
			precC1=c1;
			c2=c2->d_suiv;
		}
	}
	return *this;
}


// DEBUT EXO_OP_CROCHET
double &LDCCorr::operator[](int i)
{
    ChainonDC *c=d_tete;
    int compteur=0;
    while(c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    return c->d_v;
}
// FIN EXO_OP_CROCHET


// DEBUT EXO_OP_CROCHET_CONST
double LDCCorr::operator[](int i) const
{
    ChainonDC *c=d_tete;
    int compteur=0;
    while(c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    return c->d_v;
}
// FIN EXO_OP_CROCHET_CONST


// DEBUT EXO_SETTER
void LDCCorr::set(int i, double val)
{
    ChainonDC *c=d_tete;
    int compteur=0;
    while(c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    c->d_v=val;
}
// FIN EXO_SETTER


// DEBUT EXO_GETTER
double LDCCorr::get(int i) const
{
    ChainonDC *c=d_tete;
    int compteur=0;
    while(c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    return c->d_v;
}
// FIN EXO_GETTER


void LDCCorr::additionnerATousLesElts(double val)
{
    ChainonDC *c=d_tete;
    while(c!=nullptr)
    {
        c->d_v+=val;
        c=c->d_suiv;
    }
}

bool LDCCorr::eltsEnOrdreDecroissant() const
{
    if(d_tete==nullptr || d_tete->d_suiv==nullptr)
        return true;
    ChainonDC *c=d_tete;
    while(c->d_suiv!=nullptr && c->d_v>c->d_suiv->d_v)
    {
        c=c->d_suiv;
    }
    return (c->d_suiv==nullptr);
}

bool LDCCorr::contientElt(double val) const
{
    ChainonDC *c=d_tete;
    while(c!=nullptr && val!=c->d_v)
    {
        c=c->d_suiv;
    }
    return (c!=nullptr);
}

void LDCCorr::enleverToutesLesOccurencesDUnElt(double val)
{
    ChainonDC *c=d_tete,*precC=nullptr;
    while(c!=nullptr)
    {
        if(c->d_v==val)
        {
            ChainonDC *nextC=c->d_suiv;
            delete c;
            if(precC)
                precC->d_suiv=nextC;
            else
                d_tete=nextC;
            if(nextC)
                nextC->d_prec=precC;
            c=nextC;
        }
        else
        {
            precC=c;
            c=c->d_suiv;
        }
    }
}


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT
int LDCCorr::chercherLIndexDeLaPremiereOccurenceDUnElt(double val) const
{
    ChainonDC *c=d_tete;
    int idxPremiereOccurence=0;
    while(c!=nullptr && val!=c->d_v)
    {
        idxPremiereOccurence++;
        c=c->d_suiv;
    }
    if (c==nullptr)
        return -1;
    return idxPremiereOccurence;
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT
int LDCCorr::chercherLIndexDeLaDerniereOccurenceDUnElt(double val) const
{
    ChainonDC *c=d_tete;
    int idx=0;
    int idxDerniereOccurence=-1;
    while(c!=nullptr)
    {
        if (val==c->d_v)
            idxDerniereOccurence=idx;
        idx++;
        c=c->d_suiv;
    }
    return idxDerniereOccurence;
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
int LDCCorr::calculerNombreDOccurencesDUnElt(double val) const
{
    ChainonDC *c=d_tete;
    int idxNbOccurences=0;
    while(c!=nullptr)
    {
        if (val==c->d_v)
            idxNbOccurences++;
        c=c->d_suiv;
    }
    return idxNbOccurences;
}
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT


