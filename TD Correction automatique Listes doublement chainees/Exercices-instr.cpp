////////////////////////////////////////////////////////////////////////////////////
// Fichier genere automatiquement pour l'instrumentation du code. Ne pas modifier //
////////////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include "ExercicesBase.h"

using namespace std;


// DEBUT EXO_CONSTRUCTEUR
LDC_ConstructDestruct::LDC_ConstructDestruct():d_tete{nullptr}
{
}
// FIN EXO_CONSTRUCTEUR



// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
LDC_ConstructDestruct::LDC_ConstructDestruct(const LDC &param):d_tete{nullptr}
{
    ChainonDC* lc = param.d_tete, *prevC;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&lc)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr)
            d_tete = nc;
        else
        {
            prevC->d_suiv = nc;
            nc->d_prec=prevC;
        }
        prevC = nc;
        lc = lc->d_suiv;
    }
}
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE



// DEBUT EXO_CONSTRUCTEUR_AVEC_TABLEAU
LDC_ConstructDestruct::LDC_ConstructDestruct(Double *tab, Int nb):d_tete(nullptr)
{
    ChainonDC *prevC=nullptr;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        ChainonDC *nc = new ChainonDC(tab[i]);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr)
            d_tete = nc;
        else
        {
            prevC->d_suiv = nc;
            nc->d_prec=prevC;
        }
        prevC = nc;
    }
}
// FIN EXO_CONSTRUCTEUR_AVEC_TABLEAU


// DEBUT EXO_DESTRUCTEUR
LDC_ConstructDestruct::~LDC_ConstructDestruct()
{
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&d_tete)
    {
        ChainonDC *tmp=d_tete->d_suiv;
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
        d_tete=tmp;
    }
}
// FIN EXO_DESTRUCTEUR


// DEBUT EXO_CONCATENATION_DEBUT
void LDC_Other::concatenationDebut(const LDC &param)
{
    ChainonDC *c1=d_tete,*precC1=nullptr,*c2=param.d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=nullptr)
    {
        ChainonDC* nc=new ChainonDC(c2->d_v);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
        {
            precC1->d_suiv=nc;
            nc->d_prec=precC1;
        }
        else
            d_tete=nc;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1!=nullptr)
        {
            c1->d_prec=nc;
            nc->d_suiv=c1;
        }
        precC1=nc;
        c2=c2->d_suiv;
    }
}
// FIN EXO_CONCATENATION_DEBUT


// DEBUT EXO_CONCATENATION_MILIEU
void LDC_Other::concatenationMilieu(Int idx, const LDC &param)
{
    ChainonDC *precC=nullptr,*c=d_tete;
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }

    ChainonDC *c2=param.d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=nullptr)
    {
        ChainonDC *nc=new ChainonDC(c2->d_v);
        // Insertion entre precC et c
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;
        nc->d_prec=precC;

        nc->d_suiv=c;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c!=nullptr)
            c->d_prec=nc;
        precC=nc;
        c2=c2->d_suiv;
    }
}
// FIN EXO_CONCATENATION_MILIEU


// DEBUT EXO_CONCATENATION_FIN
void LDC_Other::concatenationFin(const LDC &param)
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    ChainonDC *c2=param.d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=nullptr)
    {
        c1=new ChainonDC(c2->d_v);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
        {
            precC1->d_suiv=c1;
            c1->d_prec=precC1;
        }
        else
            d_tete=c1;
        precC1=c1;
        c2=c2->d_suiv;
    }
}
// FIN EXO_CONCATENATION_FIN



// DEBUT EXO_OP_CONCATENATION
LDC &LDC_Other::operator+=(const LDC &param)
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    ChainonDC *c2=param.d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=nullptr)
    {
        c1=new ChainonDC(c2->d_v);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
        {
            precC1->d_suiv=c1;
            c1->d_prec=precC1;
        }
        else
            d_tete=c1;
        precC1=c1;
        c2=c2->d_suiv;
    }

    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_CONCATENATION



// DEBUT EXO_OP_EGAL
bool LDC_Other::operator==(const LDC &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    ChainonDC *c=d_tete, *lc=param.d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && lc!=nullptr && c->d_v== lc->d_v)
    {
        c=c->d_suiv;
        lc=lc->d_suiv;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c==nullptr && lc==nullptr);}
}
// FIN EXO_OP_EGAL



// DEBUT EXO_OP_DIFFERENT
bool LDC_Other::operator!=(const LDC &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    ChainonDC *c=d_tete, *lc=param.d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && lc!=nullptr && c->d_v== lc->d_v)
    {
        c=c->d_suiv;
        lc=lc->d_suiv;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c!=nullptr || lc!=nullptr);}
}
// FIN EXO_OP_DIFFERENT



// DEBUT EXO_OP_AFFECTATION
LDC &LDC_Other::operator=(const LDC &param)
{
	if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
		{GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}

	ChainonDC *c1=d_tete,*precC1=nullptr,*c2=param.d_tete;
	while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=nullptr && c2!=nullptr)
	{
		c1->d_v=c2->d_v;
		precC1=c1;
		c1=c1->d_suiv;
		c2=c2->d_suiv;
	}
	if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1!=nullptr)
	{
		if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
			precC1->d_suiv=nullptr;
		else
			d_tete=nullptr;
		while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1)
		{
			precC1=c1;
			c1=c1->d_suiv;
			{GestionPntrEtComplexite::get().peutSupprPntr(precC1, false); delete precC1;}
		}
	}
	else if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c2!=nullptr)
	{
		while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=nullptr)
		{
			c1=new ChainonDC(c2->d_v);
			if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1==nullptr)
				d_tete=c1;
			else
			{
				precC1->d_suiv=c1;
				c1->d_prec=precC1;
			}
			precC1=c1;
			c2=c2->d_suiv;
		}
	}
	{GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_AFFECTATION


// DEBUT EXO_OP_CROCHET
Double &LDC_Other::operator[](Int i)
{
    ChainonDC *c=d_tete;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c->d_v;}
}
// FIN EXO_OP_CROCHET


// DEBUT EXO_OP_CROCHET_CONST
Double LDC_Other::operator[](Int i) const
{
    ChainonDC *c=d_tete;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c->d_v;}
}
// FIN EXO_OP_CROCHET_CONST


// DEBUT EXO_SETTER
void LDC_Other::set(Int i, Double val)
{
    ChainonDC *c=d_tete;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    c->d_v=val;
}
// FIN EXO_SETTER


// DEBUT EXO_GETTER
Double LDC_Other::get(Int i) const
{
    ChainonDC *c=d_tete;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c->d_v;}
}
// FIN EXO_GETTER


// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
void LDC_Other::additionnerATousLesElts(Double val)
{
    ChainonDC *c=d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr)
    {
        c->d_v+=val;
        c=c->d_suiv;
    }
}
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS



// DEBUT EXO_INSERER_UN_ELT_DEBUT
void LDC_Other::insererUnEltDebut(Double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion dans une liste vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr) {
        d_tete = nc;
        return;
    }
    // Insertion en tete
    nc->d_suiv = d_tete;
    d_tete->d_prec=nc;
    d_tete = nc;
}
// FIN EXO_INSERER_UN_ELT_DEBUT



// DEBUT EXO_INSERER_UN_ELT_MILIEU
void LDC_Other::insererUnEltMilieu(Int idx, Double val)
{
    Int i=0;
    ChainonDC *c=d_tete,*precC=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }

    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion entre precC et c
    nc->d_prec=precC;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC!=nullptr)
        precC->d_suiv=nc;
    else
        d_tete=nc;
    nc->d_suiv=c;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c!=nullptr)
        c->d_prec=nc;
}
// FIN EXO_INSERER_UN_ELT_MILIEU



// DEBUT EXO_INSERER_UN_ELT_FIN
void LDC_Other::insererUnEltFin(Double val)
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }

    c1=new ChainonDC(val);
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
    {
        precC1->d_suiv=c1;
        c1->d_prec=precC1;
    }
    else
        d_tete=c1;
}
// FIN EXO_INSERER_UN_ELT_FIN



// DEBUT EXO_INSERER_PLUSIEURS_ELTS_DEBUT
void LDC_Other::insererPlusieursEltsDebut(Double *tab, Int nb)
{
    ChainonDC *precC=nullptr,*c=d_tete;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        ChainonDC *nc=new ChainonDC(tab[i]);
        // Insertion entre precC et c
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;
        nc->d_prec=precC;

        nc->d_suiv=c;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c!=nullptr)
            c->d_prec=nc;
        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_DEBUT



// DEBUT EXO_INSERER_PLUSIEURS_ELTS_MILIEU
void LDC_Other::insererPlusieursEltsMilieu(Int idx, Double *tab, Int nb)
{
    ChainonDC *precC=nullptr,*c=d_tete;
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }

    for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<nb;j++)
    {
        ChainonDC *nc=new ChainonDC(tab[j]);
        // Insertion entre precC et c
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;
        nc->d_prec=precC;

        nc->d_suiv=c;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c!=nullptr)
            c->d_prec=nc;
        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_MILIEU



// DEBUT EXO_INSERER_PLUSIEURS_ELTS_FIN
void LDC_Other::insererPlusieursEltsFin(Double *tab, Int nb)
{
    ChainonDC *precC=nullptr,*c=d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr)
    {
        precC=c;
        c=c->d_suiv;
    }

    for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<nb;j++)
    {
        ChainonDC *nc=new ChainonDC(tab[j]);
        // Insertion apres precC
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;
        nc->d_prec=precC;

        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_FIN



// DEBUT EXO_ENLEVER_UN_ELT_DEBUT
void LDC_Other::enleverUnEltDebut()
{
    ChainonDC *precC=nullptr;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete!=nullptr)
    {
        precC=d_tete;
        d_tete=d_tete->d_suiv;
        {GestionPntrEtComplexite::get().peutSupprPntr(precC, false); delete precC;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete!=nullptr)
            d_tete->d_prec=nullptr;
    }
}
// FIN EXO_ENLEVER_UN_ELT_DEBUT



// DEBUT EXO_ENLEVER_UN_ELT_MILIEU
void LDC_Other::enleverUnEltMilieu(Int idx)
{
    ChainonDC *c=d_tete, *precC=nullptr;
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c!=nullptr)
    {
        ChainonDC *nextC=c->d_suiv;
        {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC)
            precC->d_suiv=nextC;
        else
            d_tete=nextC;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nextC)
            nextC->d_prec=precC;
    }
}
// FIN EXO_ENLEVER_UN_ELT_MILIEU



// DEBUT EXO_ENLEVER_UN_ELT_FIN
void LDC_Other::enleverUnEltFin()
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
    {
        c1=precC1;
        precC1=precC1->d_prec;
        {GestionPntrEtComplexite::get().peutSupprPntr(c1, false); delete c1;}
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1==nullptr)
        d_tete=nullptr;
    else
        precC1->d_suiv=nullptr;
}
// FIN EXO_ENLEVER_UN_ELT_FIN



// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
void LDC_Other::enleverPlusieursEltsDebut(Int nb)
{
    ChainonDC *precC=nullptr;
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&d_tete!=nullptr && i<nb)
    {
        precC=d_tete;
        d_tete=d_tete->d_suiv;
        {GestionPntrEtComplexite::get().peutSupprPntr(precC, false); delete precC;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete!=nullptr)
            d_tete->d_prec=nullptr;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT



// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU
void LDC_Other::enleverPlusieursEltsMilieu(Int idx, Int nb)
{
    ChainonDC *c=d_tete, *precC=nullptr;
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==nullptr)
        return;
    i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && i<nb)
    {
        ChainonDC *nextC=c->d_suiv;
        {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC)
            precC->d_suiv=nextC;
        else
            d_tete=nextC;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nextC)
            nextC->d_prec=precC;

        c=nextC;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU



// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_FIN
void LDC_Other::enleverPlusieursEltsFin(Int nb)
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }

    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&precC1!=nullptr && i<nb)
    {
        c1=precC1;
        precC1=precC1->d_prec;
        {GestionPntrEtComplexite::get().peutSupprPntr(c1, false); delete c1;}
        i++;
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1==nullptr)
        d_tete=nullptr;
    else
        precC1->d_suiv=nullptr;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_FIN



// DEBUT EXO_ELTS_EN_ORDRE_DECROISSANT
bool LDC_Other::eltsEnOrdreDecroissant() const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr || d_tete->d_suiv==nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    ChainonDC *c=d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c->d_suiv!=nullptr && c->d_v>c->d_suiv->d_v)
    {
        c=c->d_suiv;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c->d_suiv==nullptr);}
}
// FIN EXO_ELTS_EN_ORDRE_DECROISSANT



// DEBUT EXO_CONTIENT_ELT
bool LDC_Other::contientElt(Double val) const
{
    ChainonDC *c=d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && val!=c->d_v)
    {
        c=c->d_suiv;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c!=nullptr);}
}
// FIN EXO_CONTIENT_ELT



// DEBUT EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT
void LDC_Other::enleverToutesLesOccurencesDUnElt(Double val)
{
    ChainonDC *c=d_tete,*precC=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_v==val)
        {
            ChainonDC *nextC=c->d_suiv;
            {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC)
                precC->d_suiv=nextC;
            else
                d_tete=nextC;
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nextC)
                nextC->d_prec=precC;
            c=nextC;
        }
        else
        {
            precC=c;
            c=c->d_suiv;
        }
    }
}
// FIN EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT
Int LDC_Other::chercherLIndexDeLaPremiereOccurenceDUnElt(Double val) const
{
    ChainonDC *c=d_tete;
    Int idxPremiereOccurence=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && val!=c->d_v)
    {
        idxPremiereOccurence++;
        c=c->d_suiv;
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return -1;}
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return idxPremiereOccurence;}
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT
Int LDC_Other::chercherLIndexDeLaDerniereOccurenceDUnElt(Double val) const
{
    ChainonDC *c=d_tete;
    Int idx=0;
    Int idxDerniereOccurence=-1;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&val==c->d_v)
            idxDerniereOccurence=idx;
        idx++;
        c=c->d_suiv;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return idxDerniereOccurence;}
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
Int LDC_Other::calculerNombreDOccurencesDUnElt(Double val) const
{
    ChainonDC *c=d_tete;
    Int idxNbOccurences=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&val==c->d_v)
            idxNbOccurences++;
        c=c->d_suiv;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return idxNbOccurences;}
}
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT

