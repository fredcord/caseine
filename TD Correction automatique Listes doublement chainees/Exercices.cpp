#include <iostream>
#include "ExercicesBase.h"

using namespace std;


// DEBUT EXO_CONSTRUCTEUR
LDC::LDC():d_tete{nullptr}
{
}
// FIN EXO_CONSTRUCTEUR



// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
LDC::LDC(const LDC &param):d_tete{nullptr}
{
    ChainonDC* lc = param.d_tete, *prevC;
    while (lc)
    {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        if (d_tete == nullptr)
            d_tete = nc;
        else
        {
            prevC->d_suiv = nc;
            nc->d_prec=prevC;
        }
        prevC = nc;
        lc = lc->d_suiv;
    }
}
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE



// DEBUT EXO_CONSTRUCTEUR_AVEC_TABLEAU
LDC::LDC(double *tab, int nb):d_tete(nullptr)
{
    ChainonDC *prevC=nullptr;
    for (int i=0;i<nb;i++)
    {
        ChainonDC *nc = new ChainonDC(tab[i]);
        if (d_tete == nullptr)
            d_tete = nc;
        else
        {
            prevC->d_suiv = nc;
            nc->d_prec=prevC;
        }
        prevC = nc;
    }
}
// FIN EXO_CONSTRUCTEUR_AVEC_TABLEAU


// DEBUT EXO_DESTRUCTEUR
LDC::~LDC()
{
    while(d_tete)
    {
        ChainonDC *tmp=d_tete->d_suiv;
        delete d_tete;
        d_tete=tmp;
    }
}
// FIN EXO_DESTRUCTEUR


// DEBUT EXO_CONCATENATION_DEBUT
void LDC::concatenationDebut(const LDC &param)
{
    ChainonDC *c1=d_tete,*precC1=nullptr,*c2=param.d_tete;
    while(c2!=nullptr)
    {
        ChainonDC* nc=new ChainonDC(c2->d_v);
        if (precC1!=nullptr)
        {
            precC1->d_suiv=nc;
            nc->d_prec=precC1;
        }
        else
            d_tete=nc;
        if (c1!=nullptr)
        {
            c1->d_prec=nc;
            nc->d_suiv=c1;
        }
        precC1=nc;
        c2=c2->d_suiv;
    }
}
// FIN EXO_CONCATENATION_DEBUT


// DEBUT EXO_CONCATENATION_MILIEU
void LDC::concatenationMilieu(int idx, const LDC &param)
{
    ChainonDC *precC=nullptr,*c=d_tete;
    int i=0;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }

    ChainonDC *c2=param.d_tete;
    while(c2!=nullptr)
    {
        ChainonDC *nc=new ChainonDC(c2->d_v);
        // Insertion entre precC et c
        if (precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;
        nc->d_prec=precC;

        nc->d_suiv=c;
        if (c!=nullptr)
            c->d_prec=nc;
        precC=nc;
        c2=c2->d_suiv;
    }
}
// FIN EXO_CONCATENATION_MILIEU


// DEBUT EXO_CONCATENATION_FIN
void LDC::concatenationFin(const LDC &param)
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    ChainonDC *c2=param.d_tete;
    while(c2!=nullptr)
    {
        c1=new ChainonDC(c2->d_v);
        if (precC1!=nullptr)
        {
            precC1->d_suiv=c1;
            c1->d_prec=precC1;
        }
        else
            d_tete=c1;
        precC1=c1;
        c2=c2->d_suiv;
    }
}
// FIN EXO_CONCATENATION_FIN



// DEBUT EXO_OP_CONCATENATION
LDC &LDC::operator+=(const LDC &param)
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    ChainonDC *c2=param.d_tete;
    while(c2!=nullptr)
    {
        c1=new ChainonDC(c2->d_v);
        if (precC1!=nullptr)
        {
            precC1->d_suiv=c1;
            c1->d_prec=precC1;
        }
        else
            d_tete=c1;
        precC1=c1;
        c2=c2->d_suiv;
    }

    return *this;
}
// FIN EXO_OP_CONCATENATION



// DEBUT EXO_OP_EGAL
bool LDC::operator==(const LDC &param) const
{
    if (this==&param)
        return true;
    ChainonDC *c=d_tete, *lc=param.d_tete;
    while(c!=nullptr && lc!=nullptr && c->d_v== lc->d_v)
    {
        c=c->d_suiv;
        lc=lc->d_suiv;
    }
    return (c==nullptr && lc==nullptr);
}
// FIN EXO_OP_EGAL



// DEBUT EXO_OP_DIFFERENT
bool LDC::operator!=(const LDC &param) const
{
    if (this==&param)
        return false;
    ChainonDC *c=d_tete, *lc=param.d_tete;
    while(c!=nullptr && lc!=nullptr && c->d_v== lc->d_v)
    {
        c=c->d_suiv;
        lc=lc->d_suiv;
    }
    return (c!=nullptr || lc!=nullptr);
}
// FIN EXO_OP_DIFFERENT



// DEBUT EXO_OP_AFFECTATION
LDC &LDC::operator=(const LDC &param)
{
	if (this==&param)
		return *this;

	ChainonDC *c1=d_tete,*precC1=nullptr,*c2=param.d_tete;
	while (c1!=nullptr && c2!=nullptr)
	{
		c1->d_v=c2->d_v;
		precC1=c1;
		c1=c1->d_suiv;
		c2=c2->d_suiv;
	}
	if (c1!=nullptr)
	{
		if (precC1!=nullptr)
			precC1->d_suiv=nullptr;
		else
			d_tete=nullptr;
		while(c1)
		{
			precC1=c1;
			c1=c1->d_suiv;
			delete precC1;
		}
	}
	else if (c2!=nullptr)
	{
		while(c2!=nullptr)
		{
			c1=new ChainonDC(c2->d_v);
			if (precC1==nullptr)
				d_tete=c1;
			else
			{
				precC1->d_suiv=c1;
				c1->d_prec=precC1;
			}
			precC1=c1;
			c2=c2->d_suiv;
		}
	}
	return *this;
}
// FIN EXO_OP_AFFECTATION


// DEBUT EXO_OP_CROCHET
double &LDC::operator[](int i)
{
    ChainonDC *c=d_tete;
    int compteur=0;
    while(c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    return c->d_v;
}
// FIN EXO_OP_CROCHET


// DEBUT EXO_OP_CROCHET_CONST
double LDC::operator[](int i) const
{
    ChainonDC *c=d_tete;
    int compteur=0;
    while(c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    return c->d_v;
}
// FIN EXO_OP_CROCHET_CONST


// DEBUT EXO_SETTER
void LDC::set(int i, double val)
{
    ChainonDC *c=d_tete;
    int compteur=0;
    while(c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    c->d_v=val;
}
// FIN EXO_SETTER


// DEBUT EXO_GETTER
double LDC::get(int i) const
{
    ChainonDC *c=d_tete;
    int compteur=0;
    while(c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    return c->d_v;
}
// FIN EXO_GETTER


// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
void LDC::additionnerATousLesElts(double val)
{
    ChainonDC *c=d_tete;
    while(c!=nullptr)
    {
        c->d_v+=val;
        c=c->d_suiv;
    }
}
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS



// DEBUT EXO_INSERER_UN_ELT_DEBUT
void LDC::insererUnEltDebut(double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion dans une liste vide
    if (d_tete == nullptr) {
        d_tete = nc;
        return;
    }
    // Insertion en tete
    nc->d_suiv = d_tete;
    d_tete->d_prec=nc;
    d_tete = nc;
}
// FIN EXO_INSERER_UN_ELT_DEBUT



// DEBUT EXO_INSERER_UN_ELT_MILIEU
void LDC::insererUnEltMilieu(int idx, double val)
{
    int i=0;
    ChainonDC *c=d_tete,*precC=nullptr;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }

    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion entre precC et c
    nc->d_prec=precC;
    if (precC!=nullptr)
        precC->d_suiv=nc;
    else
        d_tete=nc;
    nc->d_suiv=c;
    if (c!=nullptr)
        c->d_prec=nc;
}
// FIN EXO_INSERER_UN_ELT_MILIEU



// DEBUT EXO_INSERER_UN_ELT_FIN
void LDC::insererUnEltFin(double val)
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }

    c1=new ChainonDC(val);
    if (precC1!=nullptr)
    {
        precC1->d_suiv=c1;
        c1->d_prec=precC1;
    }
    else
        d_tete=c1;
}
// FIN EXO_INSERER_UN_ELT_FIN



// DEBUT EXO_INSERER_PLUSIEURS_ELTS_DEBUT
void LDC::insererPlusieursEltsDebut(double *tab, int nb)
{
    ChainonDC *precC=nullptr,*c=d_tete;
    for (int i=0;i<nb;i++)
    {
        ChainonDC *nc=new ChainonDC(tab[i]);
        // Insertion entre precC et c
        if (precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;
        nc->d_prec=precC;

        nc->d_suiv=c;
        if (c!=nullptr)
            c->d_prec=nc;
        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_DEBUT



// DEBUT EXO_INSERER_PLUSIEURS_ELTS_MILIEU
void LDC::insererPlusieursEltsMilieu(int idx, double *tab, int nb)
{
    ChainonDC *precC=nullptr,*c=d_tete;
    int i=0;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }

    for (int j=0;j<nb;j++)
    {
        ChainonDC *nc=new ChainonDC(tab[j]);
        // Insertion entre precC et c
        if (precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;
        nc->d_prec=precC;

        nc->d_suiv=c;
        if (c!=nullptr)
            c->d_prec=nc;
        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_MILIEU



// DEBUT EXO_INSERER_PLUSIEURS_ELTS_FIN
void LDC::insererPlusieursEltsFin(double *tab, int nb)
{
    ChainonDC *precC=nullptr,*c=d_tete;
    while(c!=nullptr)
    {
        precC=c;
        c=c->d_suiv;
    }

    for (int j=0;j<nb;j++)
    {
        ChainonDC *nc=new ChainonDC(tab[j]);
        // Insertion apres precC
        if (precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;
        nc->d_prec=precC;

        precC=nc;
    }
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_FIN



// DEBUT EXO_ENLEVER_UN_ELT_DEBUT
void LDC::enleverUnEltDebut()
{
    ChainonDC *precC=nullptr;
    if(d_tete!=nullptr)
    {
        precC=d_tete;
        d_tete=d_tete->d_suiv;
        delete precC;
        if (d_tete!=nullptr)
            d_tete->d_prec=nullptr;
    }
}
// FIN EXO_ENLEVER_UN_ELT_DEBUT



// DEBUT EXO_ENLEVER_UN_ELT_MILIEU
void LDC::enleverUnEltMilieu(int idx)
{
    ChainonDC *c=d_tete, *precC=nullptr;
    int i=0;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }
    if(c!=nullptr)
    {
        ChainonDC *nextC=c->d_suiv;
        delete c;
        if (precC)
            precC->d_suiv=nextC;
        else
            d_tete=nextC;
        if (nextC)
            nextC->d_prec=precC;
    }
}
// FIN EXO_ENLEVER_UN_ELT_MILIEU



// DEBUT EXO_ENLEVER_UN_ELT_FIN
void LDC::enleverUnEltFin()
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    if(precC1!=nullptr)
    {
        c1=precC1;
        precC1=precC1->d_prec;
        delete c1;
    }
    if (precC1==nullptr)
        d_tete=nullptr;
    else
        precC1->d_suiv=nullptr;
}
// FIN EXO_ENLEVER_UN_ELT_FIN



// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
void LDC::enleverPlusieursEltsDebut(int nb)
{
    ChainonDC *precC=nullptr;
    int i=0;
    while(d_tete!=nullptr && i<nb)
    {
        precC=d_tete;
        d_tete=d_tete->d_suiv;
        delete precC;
        if (d_tete!=nullptr)
            d_tete->d_prec=nullptr;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT



// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU
void LDC::enleverPlusieursEltsMilieu(int idx, int nb)
{
    ChainonDC *c=d_tete, *precC=nullptr;
    int i=0;
    while(c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }
    if (c==nullptr)
        return;
    i=0;
    while(c!=nullptr && i<nb)
    {
        ChainonDC *nextC=c->d_suiv;
        delete c;
        if (precC)
            precC->d_suiv=nextC;
        else
            d_tete=nextC;
        if (nextC)
            nextC->d_prec=precC;

        c=nextC;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU



// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_FIN
void LDC::enleverPlusieursEltsFin(int nb)
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }

    int i=0;
    while(precC1!=nullptr && i<nb)
    {
        c1=precC1;
        precC1=precC1->d_prec;
        delete c1;
        i++;
    }
    if (precC1==nullptr)
        d_tete=nullptr;
    else
        precC1->d_suiv=nullptr;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_FIN



// DEBUT EXO_ELTS_EN_ORDRE_DECROISSANT
bool LDC::eltsEnOrdreDecroissant() const
{
    if (d_tete==nullptr || d_tete->d_suiv==nullptr)
        return true;
    ChainonDC *c=d_tete;
    while(c->d_suiv!=nullptr && c->d_v>c->d_suiv->d_v)
    {
        c=c->d_suiv;
    }
    return (c->d_suiv==nullptr);
}
// FIN EXO_ELTS_EN_ORDRE_DECROISSANT



// DEBUT EXO_CONTIENT_ELT
bool LDC::contientElt(double val) const
{
    ChainonDC *c=d_tete;
    while(c!=nullptr && val!=c->d_v)
    {
        c=c->d_suiv;
    }
    return (c!=nullptr);
}
// FIN EXO_CONTIENT_ELT



// DEBUT EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT
void LDC::enleverToutesLesOccurencesDUnElt(double val)
{
    ChainonDC *c=d_tete,*precC=nullptr;
    while(c!=nullptr)
    {
        if (c->d_v==val)
        {
            ChainonDC *nextC=c->d_suiv;
            delete c;
            if (precC)
                precC->d_suiv=nextC;
            else
                d_tete=nextC;
            if (nextC)
                nextC->d_prec=precC;
            c=nextC;
        }
        else
        {
            precC=c;
            c=c->d_suiv;
        }
    }
}
// FIN EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT
int LDC::chercherLIndexDeLaPremiereOccurenceDUnElt(double val) const
{
    ChainonDC *c=d_tete;
    int idxPremiereOccurence=0;
    while(c!=nullptr && val!=c->d_v)
    {
        idxPremiereOccurence++;
        c=c->d_suiv;
    }
    if (c==nullptr)
        return -1;
    return idxPremiereOccurence;
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT
int LDC::chercherLIndexDeLaDerniereOccurenceDUnElt(double val) const
{
    ChainonDC *c=d_tete;
    int idx=0;
    int idxDerniereOccurence=-1;
    while(c!=nullptr)
    {
        if (val==c->d_v)
            idxDerniereOccurence=idx;
        idx++;
        c=c->d_suiv;
    }
    return idxDerniereOccurence;
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
int LDC::calculerNombreDOccurencesDUnElt(double val) const
{
    ChainonDC *c=d_tete;
    int idxNbOccurences=0;
    while(c!=nullptr)
    {
        if (val==c->d_v)
            idxNbOccurences++;
        c=c->d_suiv;
    }
    return idxNbOccurences;
}
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT

