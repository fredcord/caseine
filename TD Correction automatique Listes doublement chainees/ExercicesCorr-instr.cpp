////////////////////////////////////////////////////////////////////////////////////
// Fichier genere automatiquement pour l'instrumentation du code. Ne pas modifier //
////////////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <algorithm>
#include "ExercicesBase.h"


using namespace std;


/////////////////////////////////////////////////////////////////////////////
// Compare la structure de donnees de l'etudiant avec celle de la solution //
/////////////////////////////////////////////////////////////////////////////
bool LDCCorr_Other::verifierIntegrite(const LDC &param)
{
    // Verifie si la liste chainee est correcte au niveau de sa sctructure et des pointeurs
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!param.d_tete)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(param.d_tete))
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_tete->d_prec!=nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}

    ChainonDC *c=param.d_tete;
    Int compteur=0;
    vector<ChainonDC*> pntrUilise;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && compteur<StructDeDonneesVerifCorr::NB_MAX_COLONNES)
    {
        // Validite des pointeurs
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(c))
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_prec!=nullptr)
        {
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(c->d_prec))
                {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_prec->d_suiv!=c)
                {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        }
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_suiv!=nullptr)
        {
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(c->d_suiv))
                {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_suiv->d_prec!=c)
                {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        }
        // Verifier si le pointeur est bien unique
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&std::find(pntrUilise.begin(), pntrUilise.end(), c)!=pntrUilise.end())
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        pntrUilise.push_back(c);

        c=c->d_suiv;
        compteur++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c==nullptr;}
}

// Compare la correction avec la struct de l'etudiant. Retourne false si different
bool LDCCorr_Other::compareCorrAvecEtud(const LDC &student) const
{
    ChainonDC *c1=this->d_tete,*c2=student.d_tete;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1==nullptr && c2!=nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}

    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=nullptr)
    {
        // Verifie si le pointeur c2 est valide
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c2==nullptr)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(c2))
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}

        // Verifie le precedent du suivant est correct
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1->d_suiv!=nullptr)
        {
            // Pour la correction aussi
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(c1->d_suiv) || c1->d_suiv->d_prec!=c1)
            {
                cout<<"Erreur sur la correction"<<endl;
                {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
            }
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c2->d_suiv==nullptr)
                {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(c2->d_suiv) || c2->d_suiv->d_prec!=c2)
                {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        }
        // Verifier si ce pointeur c2 n'est pas utilise dans la liste correction
        ChainonDC *tmpC=this->d_tete;
        while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&tmpC!=nullptr && c2!=tmpC)
            tmpC=tmpC->d_suiv;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&tmpC!=nullptr)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        // Vérifie s'il contient la meme valeur
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1->d_v!=c2->d_v)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        // Verifie le chainon precedent
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1->d_prec==nullptr)
        {
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c2->d_prec!=nullptr) {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        }
        else
        {
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!GestionPntrEtComplexite::get().pntrValide(c2->d_prec))
                {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1->d_prec->d_v!=c2->d_prec->d_v)
                {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
        }
        c1=c1->d_suiv;
        c2=c2->d_suiv;
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c2!=nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}

    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
}

// genere la structDeDonneesVerifCorr a partir de la liste
StructDeDonneesVerifCorr LDCCorr_Other::getStructDeDonneesVerifCorr() const
{
    StructDeDonneesVerifCorr vect;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&!d_tete)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return vect;}

    ChainonDC *c=d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr)
    {
        vect.insererUnEltFin(c->d_v);
        c=c->d_suiv;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return vect;}
}

// Constructeur avec la structDeDonneesVerifCorr
void LDCCorr_Other::setStructDeDonneesVerifCorr(const StructDeDonneesVerifCorr &tab)
{
    // Supprime d'abord tous les elements
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete)
    {
        ChainonDC *tete=d_tete;
        do
        {
            ChainonDC *tmp=d_tete->d_suiv;
            {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
            d_tete=tmp;
        }
        while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&d_tete!=tete);
        d_tete=nullptr;
    }
    // On insere les elements du tableau
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<tab.getNbColonnes();i++)
        insererUnEltFin(tab[i]);
}

// Constructeur par recopie
LDCCorr_ConstructDestruct::LDCCorr_ConstructDestruct():d_tete(nullptr)
{
}

// Constructeur par recopie
LDCCorr_ConstructDestruct::LDCCorr_ConstructDestruct(const LDCCorr &param):d_tete(nullptr)
{
    ChainonDC *lc = param.d_tete, *prevC=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&lc) {
        ChainonDC *nc = new ChainonDC(lc->d_v);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr)
            d_tete = nc;
        else
        {
            prevC->d_suiv = nc;
            nc->d_prec=prevC;
        }
        prevC = nc;
        lc = lc->d_suiv;
    }
}

// Constructeur avec un tableau
LDCCorr_ConstructDestruct::LDCCorr_ConstructDestruct(Double *tab, Int nb):d_tete(nullptr)
{
    ChainonDC *prevC=nullptr;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        ChainonDC *nc = new ChainonDC(tab[i]);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr)
            d_tete = nc;
        else
        {
            prevC->d_suiv = nc;
            nc->d_prec=prevC;
        }
        prevC = nc;
    }
}

LDCCorr_ConstructDestruct::~LDCCorr_ConstructDestruct()
{
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&d_tete)
    {
        ChainonDC *tmp=d_tete->d_suiv;
        {GestionPntrEtComplexite::get().peutSupprPntr(d_tete, false); delete d_tete;}
        d_tete=tmp;
    }
}


void LDCCorr_Other::insererUnEltDebut(Double val)
{
    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion dans une liste vide
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete == nullptr) {
        d_tete = nc;
        return;
    }
    // Insertion en tete
    nc->d_suiv = d_tete;
    d_tete->d_prec=nc;
    d_tete = nc;
}


void LDCCorr_Other::insererUnEltMilieu(Int idx, Double val)
{
    Int i=0;
    ChainonDC *c=d_tete,*precC=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }

    // Creation du nouveau chainon
    ChainonDC *nc = new ChainonDC(val);
    // Insertion entre precC et c
    nc->d_prec=precC;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC!=nullptr)
        precC->d_suiv=nc;
    else
        d_tete=nc;
    nc->d_suiv=c;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c!=nullptr)
        c->d_prec=nc;
}


void LDCCorr_Other::insererUnEltFin(Double val)
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }

    c1=new ChainonDC(val);
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
    {
        precC1->d_suiv=c1;
        c1->d_prec=precC1;
    }
    else
        d_tete=c1;
}


void LDCCorr_Other::insererPlusieursEltsDebut(Double *tab, Int nb)
{
    ChainonDC *precC=nullptr,*c=d_tete;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
    {
        ChainonDC *nc=new ChainonDC(tab[i]);
        // Insertion entre precC et c
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;
        nc->d_prec=precC;

        nc->d_suiv=c;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c!=nullptr)
            c->d_prec=nc;
        precC=nc;
    }
}

void LDCCorr_Other::insererPlusieursEltsMilieu(Int idx, Double *tab, Int nb)
{
    ChainonDC *precC=nullptr,*c=d_tete;
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }

    for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<nb;j++)
    {
        ChainonDC *nc=new ChainonDC(tab[j]);
        // Insertion entre precC et c
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;
        nc->d_prec=precC;

        nc->d_suiv=c;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c!=nullptr)
            c->d_prec=nc;
        precC=nc;
    }
}


void LDCCorr_Other::insererPlusieursEltsFin(Double *tab, Int nb)
{
    ChainonDC *precC=nullptr,*c=d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr)
    {
        precC=c;
        c=c->d_suiv;
    }

    for(Int j=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),j<nb;j++)
    {
        ChainonDC *nc=new ChainonDC(tab[j]);
        // Insertion apres precC
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;
        nc->d_prec=precC;

        precC=nc;
    }
}


void LDCCorr_Other::enleverUnEltDebut()
{
    ChainonDC *precC=nullptr;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete!=nullptr)
    {
        precC=d_tete;
        d_tete=d_tete->d_suiv;
        {GestionPntrEtComplexite::get().peutSupprPntr(precC, false); delete precC;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete!=nullptr)
            d_tete->d_prec=nullptr;
    }
}

void LDCCorr_Other::enleverUnEltMilieu(Int idx)
{
    ChainonDC *c=d_tete, *precC=nullptr;
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c!=nullptr)
    {
        ChainonDC *nextC=c->d_suiv;
        {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC)
            precC->d_suiv=nextC;
        else
            d_tete=nextC;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nextC)
            nextC->d_prec=precC;
    }
}

void LDCCorr_Other::enleverUnEltFin()
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
    {
        c1=precC1;
        precC1=precC1->d_prec;
        {GestionPntrEtComplexite::get().peutSupprPntr(c1, false); delete c1;}
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1==nullptr)
        d_tete=nullptr;
    else
        precC1->d_suiv=nullptr;
}

void LDCCorr_Other::enleverPlusieursEltsDebut(Int nb)
{
    ChainonDC *precC=nullptr;
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&d_tete!=nullptr && i<nb)
    {
        precC=d_tete;
        d_tete=d_tete->d_suiv;
        {GestionPntrEtComplexite::get().peutSupprPntr(precC, false); delete precC;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete!=nullptr)
            d_tete->d_prec=nullptr;
        i++;
    }
}

void LDCCorr_Other::enleverPlusieursEltsMilieu(Int idx, Int nb)
{
    ChainonDC *c=d_tete, *precC=nullptr;
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==nullptr)
        return;
    i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && i<nb)
    {
        ChainonDC *nextC=c->d_suiv;
        {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC)
            precC->d_suiv=nextC;
        else
            d_tete=nextC;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nextC)
            nextC->d_prec=precC;

        c=nextC;
        i++;
    }
}

void LDCCorr_Other::enleverPlusieursEltsFin(Int nb)
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }

    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&precC1!=nullptr && i<nb)
    {
        c1=precC1;
        precC1=precC1->d_prec;
        {GestionPntrEtComplexite::get().peutSupprPntr(c1, false); delete c1;}
        i++;
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1==nullptr)
        d_tete=nullptr;
    else
        precC1->d_suiv=nullptr;
}


// DEBUT EXO_CONCATENATION_DEBUT
void LDCCorr_Other::concatenationDebut(const LDCCorr &param)
{
    ChainonDC *c1=d_tete,*precC1=nullptr,*c2=param.d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=nullptr)
    {
        ChainonDC* nc=new ChainonDC(c2->d_v);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
        {
            precC1->d_suiv=nc;
            nc->d_prec=precC1;
        }
        else
            d_tete=nc;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1!=nullptr)
        {
            c1->d_prec=nc;
            nc->d_suiv=c1;
        }
        precC1=nc;
        c2=c2->d_suiv;
    }
}
// FIN EXO_CONCATENATION_DEBUT


// DEBUT EXO_CONCATENATION_MILIEU
void LDCCorr_Other::concatenationMilieu(Int idx, const LDCCorr &param)
{
    ChainonDC *precC=nullptr,*c=d_tete;
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && i<idx)
    {
        precC=c;
        c=c->d_suiv;
        i++;
    }

    ChainonDC *c2=param.d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=nullptr)
    {
        ChainonDC *nc=new ChainonDC(c2->d_v);
        // Insertion entre precC et c
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC!=nullptr)
            precC->d_suiv=nc;
        else
            d_tete=nc;
        nc->d_prec=precC;

        nc->d_suiv=c;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c!=nullptr)
            c->d_prec=nc;
        precC=nc;
        c2=c2->d_suiv;
    }
}
// FIN EXO_CONCATENATION_MILIEU


// DEBUT EXO_CONCATENATION_FIN
void LDCCorr_Other::concatenationFin(const LDCCorr &param)
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    ChainonDC *c2=param.d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=nullptr)
    {
        c1=new ChainonDC(c2->d_v);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
        {
            precC1->d_suiv=c1;
            c1->d_prec=precC1;
        }
        else
            d_tete=c1;
        precC1=c1;
        c2=c2->d_suiv;
    }
}
// FIN EXO_CONCATENATION_FIN



LDCCorr &LDCCorr_Other::operator+=(const LDCCorr &param)
{
    ChainonDC *c1=d_tete,*precC1=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=nullptr)
    {
        precC1=c1;
        c1=c1->d_suiv;
    }
    ChainonDC *c2=param.d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=nullptr)
    {
        c1=new ChainonDC(c2->d_v);
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
        {
            precC1->d_suiv=c1;
            c1->d_prec=precC1;
        }
        else
            d_tete=c1;
        precC1=c1;
        c2=c2->d_suiv;
    }

    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}


bool LDCCorr_Other::operator==(const LDCCorr &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    ChainonDC *c=d_tete, *lc=param.d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && lc!=nullptr && c->d_v== lc->d_v)
    {
        c=c->d_suiv;
        lc=lc->d_suiv;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c==nullptr && lc==nullptr);}
}


bool LDCCorr_Other::operator!=(const LDCCorr &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    ChainonDC *c=d_tete, *lc=param.d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && lc!=nullptr && c->d_v== lc->d_v)
    {
        c=c->d_suiv;
        lc=lc->d_suiv;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c!=nullptr || lc!=nullptr);}
}

LDCCorr &LDCCorr_Other::operator=(const LDCCorr &param)
{
	if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
		{GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}

	ChainonDC *c1=d_tete,*precC1=nullptr,*c2=param.d_tete;
	while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1!=nullptr && c2!=nullptr)
	{
		c1->d_v=c2->d_v;
		precC1=c1;
		c1=c1->d_suiv;
		c2=c2->d_suiv;
	}
	if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c1!=nullptr)
	{
		if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1!=nullptr)
			precC1->d_suiv=nullptr;
		else
			d_tete=nullptr;
		while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c1)
		{
			precC1=c1;
			c1=c1->d_suiv;
			{GestionPntrEtComplexite::get().peutSupprPntr(precC1, false); delete precC1;}
		}
	}
	else if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c2!=nullptr)
	{
		while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c2!=nullptr)
		{
			c1=new ChainonDC(c2->d_v);
			if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC1==nullptr)
				d_tete=c1;
			else
			{
				precC1->d_suiv=c1;
				c1->d_prec=precC1;
			}
			precC1=c1;
			c2=c2->d_suiv;
		}
	}
	{GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}


// DEBUT EXO_OP_CROCHET
Double &LDCCorr_Other::operator[](Int i)
{
    ChainonDC *c=d_tete;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c->d_v;}
}
// FIN EXO_OP_CROCHET


// DEBUT EXO_OP_CROCHET_CONST
Double LDCCorr_Other::operator[](Int i) const
{
    ChainonDC *c=d_tete;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c->d_v;}
}
// FIN EXO_OP_CROCHET_CONST


// DEBUT EXO_SETTER
void LDCCorr_Other::set(Int i, Double val)
{
    ChainonDC *c=d_tete;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    c->d_v=val;
}
// FIN EXO_SETTER


// DEBUT EXO_GETTER
Double LDCCorr_Other::get(Int i) const
{
    ChainonDC *c=d_tete;
    Int compteur=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && compteur<i)
    {
        c=c->d_suiv;
        compteur++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return c->d_v;}
}
// FIN EXO_GETTER


void LDCCorr_Other::additionnerATousLesElts(Double val)
{
    ChainonDC *c=d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr)
    {
        c->d_v+=val;
        c=c->d_suiv;
    }
}

bool LDCCorr_Other::eltsEnOrdreDecroissant() const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_tete==nullptr || d_tete->d_suiv==nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    ChainonDC *c=d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c->d_suiv!=nullptr && c->d_v>c->d_suiv->d_v)
    {
        c=c->d_suiv;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c->d_suiv==nullptr);}
}

bool LDCCorr_Other::contientElt(Double val) const
{
    ChainonDC *c=d_tete;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && val!=c->d_v)
    {
        c=c->d_suiv;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return (c!=nullptr);}
}

void LDCCorr_Other::enleverToutesLesOccurencesDUnElt(Double val)
{
    ChainonDC *c=d_tete,*precC=nullptr;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c->d_v==val)
        {
            ChainonDC *nextC=c->d_suiv;
            {GestionPntrEtComplexite::get().peutSupprPntr(c, false); delete c;}
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&precC)
                precC->d_suiv=nextC;
            else
                d_tete=nextC;
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nextC)
                nextC->d_prec=precC;
            c=nextC;
        }
        else
        {
            precC=c;
            c=c->d_suiv;
        }
    }
}


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT
Int LDCCorr_Other::chercherLIndexDeLaPremiereOccurenceDUnElt(Double val) const
{
    ChainonDC *c=d_tete;
    Int idxPremiereOccurence=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr && val!=c->d_v)
    {
        idxPremiereOccurence++;
        c=c->d_suiv;
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&c==nullptr)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return -1;}
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return idxPremiereOccurence;}
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT
Int LDCCorr_Other::chercherLIndexDeLaDerniereOccurenceDUnElt(Double val) const
{
    ChainonDC *c=d_tete;
    Int idx=0;
    Int idxDerniereOccurence=-1;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&val==c->d_v)
            idxDerniereOccurence=idx;
        idx++;
        c=c->d_suiv;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return idxDerniereOccurence;}
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
Int LDCCorr_Other::calculerNombreDOccurencesDUnElt(Double val) const
{
    ChainonDC *c=d_tete;
    Int idxNbOccurences=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&c!=nullptr)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&val==c->d_v)
            idxNbOccurences++;
        c=c->d_suiv;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return idxNbOccurences;}
}
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT


