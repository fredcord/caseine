#include "Exercices.h"



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le constructeur pour la structure de données ci-dessous
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le destructeur pour la structure de données ci-dessous
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le constructeur par recopie pour la structure de données ci-dessous
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le constructeur qui prend comme paramètres un tableau avec sa
// taille: double *tabVal, int nbVal. Ce constructeur doit créer une
// structure de données contenant tous les éléments dans le tableau
// passé en paramètre
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode concatenationDebut(s) qui ajoute les elements de la
// structure de données s au début de la structure de données this. Cette
// structure de données s est passée en paramètre. L'ordre des éléments
// doit être gardé.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode concatenationFin(s) qui ajoute les elements de la
// structure de données s à la fin de la structure de données this. Cette
// structure de données s est passée en paramètre. L'ordre des éléments
// doit être gardé.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode operator+=(s) qui ajoute les elements de la structure de
// données s à la fin de la structure de données this. Cette structure de
// données s est passée en paramètre
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode operator==(s) qui retourne true si la structure de
// données s passée en paramètre est égale à la structure de données this.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode operator!=(s) qui retourne true si la structure de
// données s passée en paramètre est différente à la structure de données this.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire l'opérateur d'affectation operator=(s). Après cette méthode, la structure
// de données this doit contenir les mêmes éléments que la structure de données s
// passée en paramètre.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void additionnerATousLesElts(double val) qui ajoute la valeur
// val passée en paramètre à tous les éléments de la stucture de données.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererUnEltDebut(double val) qui insère un élément
// contenant la valeur val passée en paramètre. Cet élément est inséré au
// début de la stucture de données.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererUnEltMilieu(int idx, double val) qui
// insère un élément contenant la valeur val passée en paramètre. Cet élément
// est inséré à la position idx dans la stucture de données.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - L'index d'insertion peut être une valeur égale ou plus grande que la taille
//   de la structure de données. Dans ce cas, l'insertion se fait à la fin de la
//   structure de données.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererUnEltFin(double val) qui insère un élément
// contenant la valeur val passée en paramètre. Cet élément est inséré à la
// fin de la stucture de données.
// Remarques:
// - La structure de données peut être vide
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererPlusieursEltsDebut(double *tabVal, int nbVal)
// qui insère nbVal éléments au début de la structures de données. Les éléments
// à ajouter sont le tableau tabVal. L'ordre des éléments dans le tableau doit
// être gardé dans la structure de données.
// Remarques:
// - Le paramètre nbVal peut être égal à zéro
// - La structure de données peut être vide
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererPlusieursEltsMilieu(int idx, double *tabVal, int nbVal)
// qui insère nbVal éléments à la position idx dans la structures de données. Les éléments
// à ajouter sont le tableau tabVal. L'ordre des éléments dans le tableau doit
// être gardé dans la structure de données.
// Remarques:
// - Le paramètre nbVal peut être égal à 0.
// - L'index du premier élément de la structure de données est 0.
// - L'index d'insertion idx peut être une valeur égale ou plus grande que la taille
//   de la structure de données. Dans ce cas, l'insertion se fait à la fin de la
//   structure de données.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererPlusieursEltsFin(double *tabVal, int nbVal)
// qui insère nbVal éléments à la fin de la structures de données. Les éléments
// à ajouter sont le tableau tabVal. L'ordre des éléments dans le tableau doit
// être gardé dans la structure de données.
// Remarques:
// - Le paramètre nbVal peut être égal à zéro
// - La structure de données peut être vide
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverUnEltDebut() qui enlève le premier élément
// de la structure de données.
// Remarque:
// - La structure de données peut être vide
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverUnEltMilieu(int idx) qui enlève
// l'élément d'index idx dans la structure de données.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - La structure de données peut être vide.
// - La valeur idx peut être plus grande ou égale à la taille de la structure
//   de données. Dans ce cas, l'élément n'est pas supprimé.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverUnEltFin() qui enlève le dernier élément
// de la structure de données.
// Remarque:
// - La structure de données peut être vide
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverPlusieursEltsDebut(int nb) qui enlève nb
// éléments au début de la structures de données. Cette valeur nb est passée
// en paramètre.
// Remarque:
// - La valeur nb peut être plus grande que la taille de la structures de
// données.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverPlusieursEltsMilieu(int idx, int nb) qui
// enlève nb éléments à partir de l'élément d'index idx. Ces valeurs idx et
// nb sont passées en paramètre.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - La valeur nb peut être plus grande que la taille de la structures de
// données.
// - La valeur idx peut être plus grande ou égale à la taille de la structure
// de données. Dans ce cas, les éléments ne sont pas supprimés.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverPlusieursEltsFin(int nb) qui enlève nb
// éléments à la fin de la structures de données. Cette valeur nb est passée
// en paramètre.
// Remarque:
// - La valeur nb peut être plus grande que la taille de la structures de
// données.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode eltsEnOrdreDecroissant() qui retourne true si les éléments
// dans la structures de données sont classés par ordre strictement décroissant.
// Remarque:
// - eltsEnOrdreDecroissant() doit retourner true pour une structure de données vide
// ou ne contenant qu'un seul élément.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode contientElt(double val) qui retourne true si la structure
// de données contient au moins un élément avec la valeur val, val étant un
// paramètre de la méthode.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverToutesLesOccurencesDUnElt(double val) qui
// enlève tous les éléments contenant la valeur val dans la structure de
// données, val étant un paramètre de la méthode.
//
// class ChainonDC {
//   friend class LDC;
//   private:
//     ChainonDC(double val) : d_v(val), d_prec(nullptr), d_suiv(nullptr){}
//     double d_v;
//     ChainonDC *d_prec;
//     ChainonDC *d_suiv;
// };
//
// class LDC {
//   public:
//     LDC();
//     ~LDC();
//     ...
//
//  private:
//    ChainonDC *d_tete;
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



