#ifndef STRUCTDEDONNEESVERIFCORR_H_INCLUDED
#define STRUCTDEDONNEESVERIFCORR_H_INCLUDED

#include <iomanip>
#include <sstream>
#include <vector>

#include "GestionPntrEtComplexite.h"

using namespace std;

class Tableau2DContiguCorr;

class StructDeDonneesVerifCorr {

public:
    class ITERATEUR
    {
    public:
        Double operator *() const { return (*structDeDonnees)[idx];}
        Double& operator *() { return (*structDeDonnees)[idx];}
        ITERATEUR& operator++() {
            idx++;
            if (idx>=structDeDonnees->nbColonnes)
                idx=-1;
            return *this;
        }
        ITERATEUR& operator--() {
            idx--;
            if (idx==-2)
                idx=structDeDonnees->nbColonnes-1;
            return *this;
        }
        bool fin() {return idx==-1;}
        ITERATEUR(StructDeDonneesVerifCorr *s):structDeDonnees(s),idx(-1){}
    private:
        StructDeDonneesVerifCorr *structDeDonnees;
        int idx;
    };
    ITERATEUR premier()
    {
        return ITERATEUR(this);
    }
    operator StructDeDonneesVerifCorr*() {return this;}

    StructDeDonneesVerifCorr():nbLignes{0},nbColonnes{0}
    {
        estVecteur=false;
    }
    StructDeDonneesVerifCorr(Int nbl, Int nbc)
    {
        estVecteur=false;
        nbLignes=nbl;
        nbColonnes=nbc;
    }
    StructDeDonneesVerifCorr(const StructDeDonneesVerifCorr &param)
    {
        nbLignes=param.nbLignes;
        nbColonnes=param.nbColonnes;
        estVecteur=param.estVecteur;
        for(int i=0;i<param.nbLignes;i++)
            for(int j=0;j<param.nbColonnes;j++)
                array[i][j]=param.array[i][j];
        if (nbLignes==0 || nbColonnes==0)
            if(nbLignes!=0 || nbColonnes!=0)
                assert(0);
    }
    StructDeDonneesVerifCorr(Double *tab, Int nb)
    {
        nbLignes=1;
        nbColonnes=nb;
        estVecteur=true;
        if (nbColonnes==0)
            nbLignes=0;
        for(int i=0;i<nb;i++)
            array[0][i]=tab[i];
        if (nbLignes==0 || nbColonnes==0)
            if(nbLignes!=0 || nbColonnes!=0)
                assert(0);
    }
    StructDeDonneesVerifCorr(Double **tab, Int nbl, Int nbc)
    {
        nbLignes=nbl;
        nbColonnes=nbc;
        estVecteur=false;
        if (nbColonnes==0)
            nbLignes=0;
        for(int i=0;i<nbl;i++)
            for(int j=0;j<nbc;j++)
                array[i][j]=tab[i][j];
        if (nbLignes==0 || nbColonnes==0)
            if(nbLignes!=0 || nbColonnes!=0)
                assert(0);
    }
    StructDeDonneesVerifCorr(Double *tab, Int nbl, Int nbc)
    {
        // Pour construire un vecteur ligne ou colonne
        nbLignes=nbl;
        nbColonnes=nbc;
        estVecteur=false;
        if (nbColonnes==0)
            nbLignes=0;
        int c=0;
        for(int i=0;i<nbl;i++)
            for(int j=0;j<nbc;j++)
            {
                array[i][j]=tab[c];
                c++;
            }
        if (nbLignes==0 || nbColonnes==0)
            if(nbLignes!=0 || nbColonnes!=0)
                assert(0);
    }
    StructDeDonneesVerifCorr(vector<vector<double>> liste):StructDeDonneesVerifCorr()
    {
        int i=0;
        nbLignes=liste.size();
        estVecteur=false;
        for (auto& each : liste)
        {
            if (i==0)
                nbColonnes=each.size();
            else
                assert(nbColonnes==(int)each.size());
            for(int j=0;j<nbColonnes;j++)
                array[i][j]=*(each.begin() + j);
            i++;
        }
        if (nbLignes==0 || nbColonnes==0)
            if(nbLignes!=0 || nbColonnes!=0)
                assert(0);
    }
    ~StructDeDonneesVerifCorr()
    {
    }
    void copieColonne0VersLigne0()
    {
        assert(nbColonnes<=1);
        for (int i=0;i<nbLignes;i++)
            array[0][i]=array[i][0];
    }
    void concatenationDebut(const StructDeDonneesVerifCorr &param)
    {
        concatenationMilieu(Int(0),param);
    }
    void concatenationMilieu(Int idx, const StructDeDonneesVerifCorr &param)
    {
        if (param.nbColonnes==0)
            return;
        if (idx>nbColonnes)
            idx=nbColonnes;

        StructDeDonneesVerifCorr tmp(*this);

        // Deplace les donnees dans le tableau
        for(int i=0;i<param.nbLignes;i++)
        {
            for (int j=0;j<idx;j++)
                array[i][j]=tmp.array[i][j];
            for (int j=0;j<param.nbColonnes;j++)
                array[i][j+idx]=param.array[i][j];
            for (int j=idx;j<nbColonnes;j++)
                array[i][j+param.nbColonnes]=tmp.array[i][j];
        }
        if (nbLignes<param.nbLignes)
            nbLignes=param.nbLignes;
        nbColonnes+=param.nbColonnes;
    }
    void concatenationFin(const StructDeDonneesVerifCorr &param)
    {
        concatenationMilieu(Int(nbColonnes),param);
    }
    StructDeDonneesVerifCorr &operator+=(const StructDeDonneesVerifCorr &param)
    {
        concatenationFin(param);
        return *this;
    }
    bool operator==(const StructDeDonneesVerifCorr &param) const
    {
        if (this==&param)
            return true;
        if (estVecteur==false && param.estVecteur==false)
            if (nbLignes!=param.nbLignes)
                return false;
        if (nbColonnes!=param.nbColonnes)
            return false;
        int i=0;
        int j=0;
        while(i<nbLignes && array[i][j]==param.array[i][j])
        {
            if (estVecteur || param.estVecteur)
                i++;
            else
            {
                j++;
                if (j==nbColonnes)
                {
                    j=0;
                    i++;
                }
            }
        }
        return i==nbLignes;
    }
    bool operator!=(const StructDeDonneesVerifCorr &param) const
    {
        return !(operator==(param));
    }
    StructDeDonneesVerifCorr &operator=(const StructDeDonneesVerifCorr &param)
    {
        if (this==&param)
            return *this;
        for (int i=0;i<param.nbLignes;i++)
            for(int j=0;j<param.nbColonnes;j++)
                array[i][j]=param.array[i][j];
        nbLignes=param.nbLignes;
        nbColonnes=param.nbColonnes;
        return *this;
    }
    void additionnerATousLesElts(Double val)
    {
        for (int i=0;i<nbLignes;i++)
            for(int j=0;j<nbColonnes;j++)
                array[i][j]+=val;
    }
    void insererUnEltDebut(Double val)
    {
        StructDeDonneesVerifCorr tmp(&val, 1, 1);
        concatenationMilieu(Int(0),tmp);
    }
    void insererUnEltDebut(Double* val, Int nb)
    {
        // Cree un vecteur colonne
        StructDeDonneesVerifCorr tmp(val, nb, 1);
        concatenationMilieu(Int(0),tmp);
    }
    void insererUnEltMilieu(Int idx, Double val)
    {
        StructDeDonneesVerifCorr tmp(&val, 1, 1);
        concatenationMilieu(Int(idx),tmp);
    }
    void insererUnEltMilieu(Int idx, Double *tab, Int nb)
    {
        StructDeDonneesVerifCorr tmp(tab, nb, 1);
        concatenationMilieu(Int(idx),tmp);
    }
    void insererUnEltFin(Double val)
    {
        StructDeDonneesVerifCorr tmp(&val, 1, 1);
        concatenationMilieu(Int(nbColonnes),tmp);
    }
    void insererUnEltFin(Double* val, Int nb)
    {
        // Cree un vecteur colonne
        StructDeDonneesVerifCorr tmp(val, nb, 1);
        concatenationMilieu(Int(nbColonnes),tmp);

    }
    void insererPlusieursEltsDebut(Double *tab, Int nb)
    {
        StructDeDonneesVerifCorr tmp(tab, 1, nb);
        concatenationMilieu(Int(0),tmp);
    }
    void insererPlusieursEltsDebut(Double **tab, Int nbl, Int nbc)
    {
        StructDeDonneesVerifCorr tmp(tab, nbl, nbc);
        concatenationMilieu(Int(0),tmp);
    }
    void insererPlusieursEltsMilieu(Int idx, Double *tab, Int nb)
    {
        // Cree un vecteur colonne
        StructDeDonneesVerifCorr tmp(tab, 1, nb);
        concatenationMilieu(idx,tmp);
    }
    void insererPlusieursEltsMilieu(Int idx, Double **tab, Int nbl, Int nbc)
    {
        StructDeDonneesVerifCorr tmp(tab, nbl, nbc);
        concatenationMilieu(idx,tmp);
    }
    void insererPlusieursEltsFin(Double *tab, Int nb)
    {
        // Cree un vecteur colonne
        StructDeDonneesVerifCorr tmp(tab, 1, nb);
        concatenationMilieu(nbColonnes,tmp);
    }
    void insererPlusieursEltsFin(Double **tab, Int nbl, Int nbc)
    {
        StructDeDonneesVerifCorr tmp(tab, nbl, nbc);
        concatenationMilieu(nbColonnes,tmp);
    }
    void enleverUnEltDebut()
    {
        enleverPlusieursEltsDebut(Int(1));
    }
    void enleverUnEltMilieu(Int idx)
    {
        enleverPlusieursEltsMilieu(idx,Int(1));
    }
    void enleverUnEltFin()
    {
        enleverPlusieursEltsFin(Int(1));
    }
    void enleverPlusieursEltsDebut(Int nb)
    {
        if (nb>nbColonnes)
            nb=nbColonnes;
        enleverPlusieursEltsMilieu(Int(0),nb);
    }
    void enleverPlusieursEltsMilieu(Int idx, Int nb)
    {
        if (nb<=0)
            return;
        if (idx>=nbColonnes)
            return;
        if (idx+nb>nbColonnes)
            nb=nbColonnes-idx;

        for(int i=0;i<nbLignes;i++)
            for (int j=idx+nb;j<nbColonnes;j++)
                array[i][j-nb]=array[i][j];
        nbColonnes-=nb;
        if (nbColonnes==0)
            nbLignes=0;
    }
    void enleverPlusieursEltsFin(Int nb)
    {
        if (nb>nbColonnes)
            nb=nbColonnes;
        enleverPlusieursEltsMilieu(nbColonnes-nb,nb);
    }
    void enleverToutesLesOccurencesDUnElt(Double val)
    {
        assert(nbLignes<=1);
        for (int j=0;j<nbColonnes;j++)
        {
            if (array[0][j]==val)
            {
                for (int k=j;k<nbColonnes-1;k++)
                    array[0][k]=array[0][k+1];
                nbColonnes--;
                j--;
            }
        }
        if (nbColonnes==0)
            nbLignes=0;
    }
    bool eltsEnOrdreDecroissant() const
    {
        if (nbLignes*nbColonnes==0 || nbLignes*nbColonnes==1)
            return true;
        int i=0,j=0;
        while(true)
        {
            int nextI=i,nextJ=j+1;
            if (nextJ==nbColonnes)
            {
                nextI++;
                nextJ=0;
            }
            if (nextI==nbLignes)
                return true;
            if (array[i][j]<array[nextI][nextJ])
                return false;
            i=nextI;
            j=nextJ;
        }
        return true;
    }
    bool contientElt(Double val) const
    {
        int i=0,j=0;
        while(i<nbLignes)
        {
            if (array[i][j]==val)
                return true;
            j++;
            if (j==nbColonnes)
            {
                j=0;
                i++;
            }
        }
        return false;
    }
    Int chercherLIndexDeLaPremiereOccurenceDUnElt(Double val) const
    {
        assert(nbLignes<=1);
        int j=0;
        while(j<nbColonnes && array[0][j]!=val)
        {
            j++;
        }
        if (j<nbColonnes)
            return j;
        else
            return -1;
    }
    Int chercherLIndexDeLaDerniereOccurenceDUnElt(Double val) const
    {
        assert(nbLignes<=1);
        int j=nbColonnes-1;
        while(j>=0 && array[0][j]!=val)
            j--;
        return j;
    }
    Int calculerNombreDOccurencesDUnElt(Double val) const
    {
        int nb=0;
        int i=0,j=0;
        while(i<nbLignes)
        {
            if (array[i][j]==val)
                nb++;
            j++;
            if (j==nbColonnes)
            {
                j=0;
                i++;
            }
        }
        return nb;
    }
    string toString() const
    {
        std::ostringstream stream;
        stream<<"{ ";
        for (int i=0;i<nbLignes;i++)
        {
            for(int j=0;j<nbColonnes;j++)
            {
                if (array[i][j]<10)
                    stream<<" ";
                stream << std::fixed << std::setprecision(0) << array[i][j];
                if (j<nbColonnes-1)
                    stream<<", ";
            }
            if (i+1<nbLignes)
                stream<<"; ";
        }
        stream<<" }";
        return stream.str();
    }
    Int getNbLignes() const
    {
        return nbLignes;
    }
    Int& getNbLignes()
    {
        return nbLignes;
    }
    Int getNbColonnes() const
    {
        return nbColonnes;
    }
    Int& getNbColonnes()
    {
        return nbColonnes;
    }
    Int getSize()
    {
        return nbLignes*nbColonnes;
    }
    Double** getArray()
    {
        return array2D;
    }

    // Acces aux elements de la premiere ligne
    Double& operator [](int idx) {
        return get(0,idx);
    }

    // Acces aux elements de la premiere ligne
    Double operator [](int idx) const {
        return get(0,idx);
    }

    Double& operator()(int idx) {
        return get(0,idx);
    }

    Double operator()(int idx) const {
        return get(0,idx);
    }

    Double& operator()(int i, int j) {
        return get(i,j);
    }

    Double operator()(int i, int j) const {
        return get(i,j);
    }

    void set(int i, int j, Double val) {
        assert(i>=0 && i<nbLignes);
        assert(i>=0 && j<nbColonnes);
        array[i][j]=val;
    }
    void set(int idx, Double val) {
        set(0,idx,val);
    }
    Double get(int i, int j) const {
        assert(i<nbLignes);
        assert(j<nbColonnes);
        return array[i][j];
    }
    Double get(int idx) const {
        return get(0,idx);
    }
    Double& get(int i, int j) {
        assert(i<nbLignes);
        assert(j<nbColonnes);
        return array[i][j];
    }
    Double& get(int idx) {
        return get(0,idx);
    }
public:
    bool estVecteur=false;
public:
    static const int NB_MAX_LIGNES=3;
    static const int NB_MAX_COLONNES=8;

    // Tableau 2D statique
    Double array[NB_MAX_LIGNES][NB_MAX_COLONNES];
    // Pour avoir un tableau de type Double**
    Double* array2D[NB_MAX_LIGNES]={array[0],array[1],array[2]};
    Int nbLignes,nbColonnes;
};

// Le corps de cette fonction se trouve dans ExercicesBase.cpp
StructDeDonneesVerifCorr operator+(const StructDeDonneesVerifCorr& a, const StructDeDonneesVerifCorr& b);
StructDeDonneesVerifCorr operator-(const StructDeDonneesVerifCorr& a, const StructDeDonneesVerifCorr& b);


#endif // STRUCTDEDONNEESVERIFCORR_H_INCLUDED
