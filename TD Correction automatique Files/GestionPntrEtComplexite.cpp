#include "GestionPntrEtComplexite.h"
#include <iostream>
#include <execinfo.h>
#include <memory>

#include <cxxabi.h>   // Pour demangling
#include <dlfcn.h>    // Pour dladdr

#include <elf.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

using namespace std;


void* GestionPntrEtComplexite::safe_malloc(size_t taille)
{
    // Si la taille est trop grande, c'est peut-etre un new avec une taille negative
    // Cette methode ne marche qu'avec l'option -fno-exceptions pour eviter que le programme
    // soit arrete par une exception avant d'avoir atteint cette methode
    if (taille>1000000)     // 1 MByte
    {
        // Toujours afficher l'erreur sur la console
        cout<<"void* safe_malloc avec taille trop grande :"<<taille<<endl;
        if (msgErreur!=nullptr && strlen(msgErreur)==0)
        {
            char* str=recupereFichierEtNumeroLigneCourants(nomFichierSourceEtudiant.c_str());
#ifdef DEBUG
            // Si ca ne vient pas du fichier de l'etudiant, ce bug est une consequence d'une erreur
            // plus haut dans le code, par exemple, une fonction sans return
            if (str==nullptr)
                str=recupereFichierEtNumeroLigneCourants();
#endif
            if (str!=nullptr)
                sprintf(msgErreur,"Erreur: allocation d'un pointeur avec une taille non valide\n  %s\n",str);
#ifdef DEBUG
            else
                sprintf(msgErreur,"Erreur: allocation d'un pointeur avec une taille non valide\n");
#endif
            free(str);
        }

        // Erreur impossible a rattraper. On quitte toujours.
        doExit(__LINE__);
        return 0x00;
    }
    return malloc(taille);
}


//#define DEBUG
void* operator new (size_t size)
{
    void* ptr = GestionPntrEtComplexite::get().safe_malloc(size);
//    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::NEW, ptr);
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::NEW, ptr);
#ifdef DEBUG
    cout<<"void* operator new (size_t size) "<<(void*)ptr<<endl;
#endif
    // Pour eviter d'avoir des valeur nullptr par defaut
    // C'est pour verifier si les constructeurs sont bien implementes
    memset(ptr,0xAA,size);
    GestionPntrEtComplexite::get().ajoutPntr(ptr, size, false);
    return ptr;
}

void *operator new[](size_t size)
{
    void* ptr = GestionPntrEtComplexite::get().safe_malloc(size);

    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::NEW, ptr);
#ifdef DEBUG
    cout<<"void *operator new[](size_t size) "<<ptr<<endl;
#endif
    // Pour eviter d'avoir des valeur nullptr par defaut
    // C'est pour verifier si les constructeurs sont bien implementes
    memset(ptr,0xAA,size);
    GestionPntrEtComplexite::get().ajoutPntr(ptr, size, true);
    return ptr;
}

void operator delete(void* ptr, size_t size)
{
#ifdef DEBUG
    cout<<"void operator delete(void* ptr, size_t size) "<<ptr<<endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DELETE, ptr);
    if (ptr==nullptr)
        return;
    GestionPntrEtComplexite::get().supprPntr(ptr, false);
}

void operator delete(void* ptr) _GLIBCXX_USE_NOEXCEPT
{
#ifdef DEBUG
    cout<<"void operator delete(void* ptr) "<<ptr<<endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DELETE, ptr);
    if (ptr==nullptr)
        return;
    GestionPntrEtComplexite::get().supprPntr(ptr, false);
}


void operator delete[](void* ptr, size_t taille)
{
#ifdef DEBUG
    cout<<"void operator delete[](void* ptr, size_t taille) "<<ptr<<endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DELETE, ptr);
    if (ptr==nullptr)
        return;
    GestionPntrEtComplexite::get().supprPntr(ptr, true);
}

void operator delete[](void* ptr) _GLIBCXX_USE_NOEXCEPT
{
#ifdef DEBUG
    cout<<"void operator delete[](void* ptr) "<<ptr<<endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DELETE, ptr);
    if (ptr==nullptr)
        return;
    GestionPntrEtComplexite::get().supprPntr(ptr, true);
}


//////////////////////////////////
// Methodes de la classe Double //
//////////////////////////////////
Double::Double()
{
#ifdef DEBUG
    cout<<"Double::Double()"<<endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DOUBLE_CONSTRUCTEUR, this);
    // Ajout de l'instance
    ajoutInst();
}

void Double::ajoutInst()
{
    GestionPntrEtComplexite::get().ajoutInst(this);
}

void Double::supprInst()
{
    GestionPntrEtComplexite::get().supprInst(this,sizeof(Double));
}

bool Double::instValide() const
{
    return GestionPntrEtComplexite::get().instValide(this);
}

/*
constexpr Double::Double(double val):d_v(val)
{
#ifdef DEBUG
    cout<<"Double::Double(double val)"<<endl;
#endif
    GestionPntrEtComplexite::get().compterNbAppelsDoubleInt();
    // Ajout de l'instance
    ajoutInst(this);
}*/

Double::Double(Double &val)
{
#ifdef DEBUG
    cout<<"Double::Double(Double &val)"<<endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DOUBLE_CONSTRUCTEUR, this, &val);
    // Verifier si l'allocation a été faite
    if (val.instValide())
        d_v=val.d_v;

    // Ajout de l'instance
    ajoutInst();
}

Double::Double(const Double &val)
{
#ifdef DEBUG
    cout<<"Double::Double(const Double &val)"<<endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DOUBLE_CONSTRUCTEUR, this, &val);
    // Verifier si l'allocation a été faite
    if (val.instValide())
        d_v=val.d_v;

    // Ajout de l'instance
    ajoutInst();
}

Double::Double(const Int &val)
{
#ifdef DEBUG
    cout<<"Double::Double(const Int &val)"<<endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DOUBLE_CONSTRUCTEUR, this, &val);
    // Verifier si l'allocation a été faite
    if (val.instValide())
        d_v=val.d_v;

    // Ajout de l'instance
    ajoutInst();

}

/*
// TRES IMPORTANT: ne pas mettre le destructeur
// Autrement la taille allouee est augmentee de 8 octets
// et l'adresse du pntr est differente pour new et GestionPntrEtComplexite::get().safe_malloc
// Ceci rend le tracage des allocations plus compliquees
Double::~Double()
{
#ifdef DEBUG
    cout<<"Double::~Double()"<<endl;
#endif
    GestionPntrEtComplexite::get().compterNbAppelsDoubleInt();
    if (instValide())
        supprInst();
}
*/

/*
Double &Double::operator=(double val)
{
#ifdef DEBUG
    cout<<"Double::operator="<<endl;
#endif
    GestionPntrEtComplexite::get().compterNbAppelsDoubleInt();
    // Verifier si l'allocation a été faite
    if (instValide())
        d_v=val;
    return *this;
}
*/

Double &Double::operator=(const Double &val)
{
#ifdef DEBUG
    cout<<"Double::operator="<<endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DOUBLE_AFFECTATION, this, &val);
    if (instValide() && val.instValide())
    {
        d_v=val.d_v;
    }

    return *this;
}

Double::operator double() const
{
#ifdef DEBUG
    cout<<"Double::operator double()"<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DOUBLE_CAST, this);
    if (!instValide())
        return 0;
    return d_v;
}

// Ce deuxieme operateur de conversion ne semble pas necessaire. S'il est absent, c'est le
// premier qui est appele. En tout, clang ne compile pas avec.
/*
& Double::operator double()
{
#ifdef DEBUG
    cout<<"&Double::operator double()"<< endl;
#endif
    GestionPntrEtComplexite::get().compterNbAppelsDoubleInt();
    if (!instValide())
    {
        // Instance pas valide, on retourne une variable statique pour eviter le plantage
        static Double temp;
        return temp.d_v;
    }
    return d_v;
}
*/

Double &Double::operator+=(const Double &val)
{
#ifdef DEBUG
    cout<<"Double &Double::operator+=(const Double &c)"<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DOUBLE_PLUS_EGAL, this, &val);
    if (instValide() && val.instValide())
        d_v += val.d_v;
    return *this;
}

Double &Double::operator-=(const Double &val)
{
#ifdef DEBUG
    cout<<"Double &Double::operator-=(const Double &d)"<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DOUBLE_MOINS_EGAL, this, &val);
    if (instValide() && val.instValide())
        d_v -= val.d_v;
    return *this;
}

Double &Double::operator*=(const Double &val)
{
#ifdef DEBUG
    cout<<"Double &Double::operator*=(const Double &c)"<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DOUBLE_MULT_EGAL, this, &val);
    if (instValide() && val.instValide())
        d_v *= val.d_v;
    return *this;
}

Double &Double::operator/=(const Double &val)
{
#ifdef DEBUG
    cout<<"Double &Double::operator/=(const Double &c)"<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DOUBLE_DIV_EGAL, this, &val);
    if (instValide() && val.instValide())
        d_v /= val.d_v;
    return *this;
}

// Prefix increment operator, c'est-a-dire ++i
Double &Double::operator++()
{
#ifdef DEBUG
    cout<<"Double &Double::operator++()"<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DOUBLE_PRE_PLUS_PLUS, this);
    if (instValide())
        d_v++;
    return *this;
}

// Postfix increment operator, c'est-a-dire i++
Double Double::operator++(int i)
{
#ifdef DEBUG
    cout<<"Double Double::operator++(int)"<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DOUBLE_POST_PLUS_PLUS, this);
    if (!instValide())
        return Double(0);

    Double temp(d_v);
    if (i==0)
        d_v++;
    else
        d_v+=i;
    return temp;
}

// Prefix decrement operator, c'est-a-dire --i
Double& Double::operator--()
{
#ifdef DEBUG
    cout<<"Double &Double::operator--()"<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DOUBLE_PRE_MOINS_MOINS, this);
    if (instValide())
        d_v--;
    return *this;
}

// Postfix decrement operator, c'est-a-dire i--
Double Double::operator--(int i)
{
#ifdef DEBUG
    cout<<"Double Double::operator--(int)"<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DOUBLE_POST_MOINS_MOINS, this);
    if (!instValide())
        return Double(0);

    Double temp(d_v);
    if (i==0)
        d_v--;
    else
        d_v-=i;
    return temp;
}


Double* Double::operator&()
{
#ifdef DEBUG
    cout<<"Double* Double::operator&()"<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DOUBLE_ADRESSE, this);
    if (!instValide())
    {
        static Double temp(0);
        return &temp;
    }
    return this;
}

void* Double::operator new(size_t size)
{
#ifdef DEBUG
    cout<<"Double::operator new avec pointeur "<< endl;
#endif
    void* ptr = GestionPntrEtComplexite::get().safe_malloc(size);
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DOUBLE_NEW, ptr);
    // Pour eviter d'avoir des valeur nullptr par defaut
    // C'est pour verifier si les constructeurs sont bien implementes
    memset(ptr,0xAA,size);
    GestionPntrEtComplexite::get().ajoutPntr(ptr, size, false);
    return ptr;
}

void* Double::operator new[](size_t size)
{
#ifdef DEBUG
    cout<<"void* Double::operator new[](size_t size) "<< endl;
#endif
    void* ptr = GestionPntrEtComplexite::get().safe_malloc(size);

    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DOUBLE_NEW, ptr);
    // Pour eviter d'avoir des valeur nullptr par defaut
    // C'est pour verifier si les constructeurs sont bien implementes
    memset(ptr,0xAA,size);
    GestionPntrEtComplexite::get().ajoutPntr(ptr, size,true);
    return ptr;
}

void Double::operator delete(void* ptr)
{
#ifdef DEBUG
    cout<<"static void Double::operator delete(void* ptr) "<<ptr<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DOUBLE_DELETE, ptr);
    if (ptr==nullptr)
        return;
    GestionPntrEtComplexite::get().supprPntr(ptr, false);
}

void Double::operator delete[](void* ptr, size_t taille)
{
#ifdef DEBUG
    cout<<"static void Double::operator delete[](void *ptr, size_t taille) " <<ptr<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DOUBLE_DELETE, ptr);
    if (ptr==nullptr)
        return;
    GestionPntrEtComplexite::get().supprPntr(ptr, true);
}


void Double::operator delete[](void* ptr)
{
#ifdef DEBUG
    cout<<"static void Double::operator delete[](void* ptr) " <<ptr<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DOUBLE_DELETE, ptr);
    if (ptr==nullptr)
        return;
    GestionPntrEtComplexite::get().supprPntr(ptr, true);
}


///////////////////////////////
// Methodes de la classe Int //
///////////////////////////////

Int::Int()
{
#ifdef DEBUG
    cout<<"Int::Int()"<<endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::INT_CONSTRUCTEUR, this);
    // Ajout de l'instance
    ajoutInst();
}

Int::Int(Int &val)
{
#ifdef DEBUG
    cout<<"Int::Int(Int &val)"<<endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::INT_CONSTRUCTEUR, this, &val);
    // Verifier si l'allocation a été faite
    if (val.instValide())
        d_v=val.d_v;

    // Ajout de l'instance
    ajoutInst();
}

Int::Int(const Int &val)
{
#ifdef DEBUG
    cout<<"Int::Int(const Int &val)"<<endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::INT_CONSTRUCTEUR, this, &val);
    // Verifier si l'allocation a été faite
    if (val.instValide())
        d_v=val.d_v;

    // Ajout de l'instance
    ajoutInst();
}

Int::Int(const Double &val)
{
#ifdef DEBUG
    cout<<"Int::Int(const Int &val)"<<endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::INT_CONSTRUCTEUR, this, &val);
    // Verifier si l'allocation a été faite
    if (val.instValide())
        d_v=val.d_v;

    // Ajout de l'instance
    ajoutInst();
}

/*
// TRES IMPORTANT: ne pas mettre le destructeur
// Autrement la taille allouee est augmentee de 8 octets
// et l'adresse du pntr est differente pour new et GestionPntrEtComplexite::get().safe_malloc
// Ceci rend le tracage des allocations plus compliquees
Int::~Int()
{
#ifdef DEBUG
    cout<<"Int::~Int()"<<endl;
#endif
    GestionPntrEtComplexite::get().compterNbAppelsDoubleInt();
    if (instValide())
        supprInst();
}
*/

/*
Int &Int::operator=(int val)
{
#ifdef DEBUG
    cout<<"Int &Int::operator=(int val)"<<endl;
#endif
    GestionPntrEtComplexite::get().compterNbAppelsDoubleInt();
    // Verifier si l'allocation a été faite
    if (instValide())
        d_v=val;
    return *this;
}
*/

Int &Int::operator=(const Int &val)
{
#ifdef DEBUG
    cout<<"Int &Int::operator=(const Int &val)"<<endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::INT_AFFECTATION, this, &val);
    if (instValide() && val.instValide())
    {
        d_v=val.d_v;
    }
    return *this;
}

Int::operator int() const
{
#ifdef DEBUG
    cout<<"Int::operator int() const"<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::INT_CAST, this);
    if (!instValide())
        return 0;
    return d_v;
}

// Ce deuxieme operateur de conversion ne semble pas necessaire. S'il est absent, c'est le
// premier qui est appele. En tout, clang ne compile pas avec.
/*
&Int::operator int()
{
#ifdef DEBUG
    cout<<"&Int::operator int()"<< endl;
#endif
    GestionPntrEtComplexite::get().compterNbAppelsDoubleInt();
    if (!instValide())
    {
        // Instance pas valide, on retourne une variable statique pour eviter le plantage
        static Int temp;
        return temp.d_v;
    }
    return d_v;
}
*/

Int &Int::operator+=(const Int &val)
{
#ifdef DEBUG
    cout<<"Int &Int::operator+=(const Int &val)"<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::INT_PLUS_EGAL, this, &val);
    if (instValide() && val.instValide())
        d_v += val.d_v;
    return *this;
}

Int &Int::operator-=(const Int &val)
{
#ifdef DEBUG
    cout<<"Int &Int::operator-=(const Int &val)"<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::INT_MOINS_EGAL, this, &val);
    if (instValide() && val.instValide())
        d_v -= val.d_v;
    return *this;
}

Int &Int::operator*=(const Int &val)
{
#ifdef DEBUG
    cout<<"Int &Int::operator*=(const Int &c)"<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::INT_MULT_EGAL, this, &val);
    if (instValide() && val.instValide())
        d_v *= val.d_v;
    return *this;
}

Int &Int::operator/=(const Int &val)
{
#ifdef DEBUG
    cout<<"Int &Int::operator/=(const Int &c)"<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::INT_DIV_EGAL, this, &val);
    if (instValide() && val.instValide())
        d_v /= val.d_v;
    return *this;
}

Int &Int::operator%=(const Int &val)
{
#ifdef DEBUG
    cout<<"Int &Int::operator%=(const Int &c)"<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::INT_POURCENT_EGAL, this, &val);
    if (instValide() && val.instValide())
        d_v %= val.d_v;
    return *this;
}

// Prefix increment operator, c'est-a-dire ++i
Int &Int::operator++()
{
#ifdef DEBUG
    cout<<"Int &Int::operator++()"<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::INT_PRE_PLUS_PLUS, this);
    if (instValide())
        d_v++;
    return *this;
}

// Postfix increment operator, c'est-a-dire i++
Int Int::operator++(int i)
{
#ifdef DEBUG
    cout<<"Int Int::operator++(int)"<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::INT_POST_PLUS_PLUS, this);
    if (!instValide())
        return Int(0);

    Int temp(d_v);
    if (i==0)
        d_v++;
    else
        d_v+=i;
    return temp;
}

// Prefix decrement operator, c'est-a-dire --i
Int& Int::operator--()
{
#ifdef DEBUG
    cout<<"Int &Int::operator--()"<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::INT_PRE_MOINS_MOINS, this);
    if (instValide())
        d_v--;
    return *this;
}

// Postfix decrement operator, c'est-a-dire i--
Int Int::operator--(int i)
{
#ifdef DEBUG
    cout<<"Int Int::operator--(int)"<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::INT_POST_MOINS_MOINS, this);
    if (!instValide())
        return Int(0);

    Int temp(d_v);
    if (i==0)
        d_v--;
    else
        d_v-=i;
    return temp;
}

Int* Int::operator&()
{
#ifdef DEBUG
    cout<<"Int* Int::operator&()"<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::INT_ADRESSE, this);
    if (!instValide())
    {
        static Int temp(0);
        return &temp;
    }
    return this;
}

void* Int::operator new(size_t size)
{
#ifdef DEBUG
    cout<<"void* Int::operator new(size_t size) "<< endl;
#endif
    void* ptr = GestionPntrEtComplexite::get().safe_malloc(size);

    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::INT_NEW, ptr);
    // Pour eviter d'avoir des valeur nullptr par defaut
    // C'est pour verifier si les constructeurs sont bien implementes
    memset(ptr,0xAA,size);
    GestionPntrEtComplexite::get().ajoutPntr(ptr, size,false);
    return ptr;
}

void* Int::operator new[](size_t size)
{
#ifdef DEBUG
    cout<<"void* Int::operator new[](size_t size) "<< endl;
#endif
    void* ptr = GestionPntrEtComplexite::get().safe_malloc(size);

    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::INT_NEW, ptr);
    // Pour eviter d'avoir des valeur nullptr par defaut
    // C'est pour verifier si les constructeurs sont bien implementes
    memset(ptr,0xAA,size);
    GestionPntrEtComplexite::get().ajoutPntr(ptr, size,true);
    return ptr;
}

void Int::operator delete(void* ptr)
{
#ifdef DEBUG
    cout<<"void Int::operator delete(void* ptr) "<<ptr<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::INT_DELETE, ptr);
    if (ptr==nullptr)
        return;
    GestionPntrEtComplexite::get().supprPntr(ptr, false);
}

void Int::operator delete[](void* ptr, size_t taille)
{
#ifdef DEBUG
    cout<<"void Int::operator delete[](void *ptr, size_t taille) " <<ptr<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::INT_DELETE, ptr);
    if (ptr==nullptr)
        return;
    GestionPntrEtComplexite::get().supprPntr(ptr, true);
}

void Int::operator delete[](void* ptr)
{
#ifdef DEBUG
    cout<<"void Int::operator delete[](void* ptr) " <<ptr<< endl;
#endif
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::INT_DELETE, ptr);
    if (ptr==nullptr)
        return;
    GestionPntrEtComplexite::get().supprPntr(ptr, true);
}

void Int::ajoutInst()
{
    GestionPntrEtComplexite::get().ajoutInst(this);
}

void Int::supprInst()
{
    GestionPntrEtComplexite::get().supprInst(this,sizeof(Int));
}

bool Int::instValide() const
{
    return GestionPntrEtComplexite::get().instValide(this);
}

///////////////////////////////////////////////////////
// Fonctions pour recuperer le nom du fichier source //
// et la ligne a partir du point d'execution courant //
///////////////////////////////////////////////////////
char* exec(const char* cmd)
{
    const int buff_size=128;
    array<char, buff_size> buffer;
    char* result=nullptr;
    int size=0;

    // Partie du code qui prend du temps
    unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe)
        return nullptr;
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr)
    {
        if (size%buff_size==0)
            result=(char*)realloc(result,size+buff_size);
        memcpy(&result[size],buffer.data(),buffer.size());
        size+=buffer.size();
    }
    return result;
}


// Recupere la liste de toutes les fonctions dans le fichier de l'executable
// Equivalent a readelf -s --wide TestDouble
void GestionPntrEtComplexite::TabFonctionsExecutable::init()
{
    void  *data;
    Elf64_Ehdr *elf;
    int fd;

    // Recupere le nom du fichier executable
    // Une autre facon de faire: argv[0]
    char ename[1024]={};
    int l=readlink("/proc/self/exe",ename,sizeof(ename)-1);
    if (l == -1)
        return;
    ename[l]=0x00;          // Absolument necessaire

    fd = open(ename, O_RDONLY);
    int filesize = lseek(fd, 0, SEEK_END);
    data = mmap(NULL, filesize, PROT_READ, MAP_SHARED, fd, 0);
    elf = (Elf64_Ehdr *) data;

    Elf64_Shdr      *shdr = (Elf64_Shdr *) ((uint8_t*)data + elf->e_shoff);
    char            *str = (char *) ((uint8_t*)data + shdr[elf->e_shstrndx].sh_offset);

    Elf64_Shdr      *symtab;
    Elf64_Shdr      *strtab;
    for (int i = 0; i < elf->e_shnum; i++)
    {
        if (shdr[i].sh_size)
        {
            if (strcmp(&str[shdr[i].sh_name], ".symtab") == 0)
                symtab = (Elf64_Shdr *) &shdr[i];
            if (strcmp(&str[shdr[i].sh_name], ".strtab") == 0)
                strtab = (Elf64_Shdr *) &shdr[i];
          }
    }

    Elf64_Sym *sym = (Elf64_Sym*) ((uint8_t*)data + symtab->sh_offset);
    str = (char*) ((uint8_t*)data + strtab->sh_offset);

    for (size_t i = 0; i < symtab->sh_size / sizeof(Elf64_Sym); i++)
    {
        char* name = str + sym[i].st_name;
        //printf("%s\n", name);
        //printf("0x%08x\n", sym[i].st_value);
        //cout<<name<<endl;

        int status = -1;
        char *demangledName = abi::__cxa_demangle(name, NULL, 0, &status);
        if (demangledName)
        {
            // N'ajoute le nom que si celui-ci n'est pas trop long
            // Certains noms de fonction sont très long (>1024).
            // Ce sont des fonctions systeme
            if (strlen(demangledName)+1<=sizeof(FonctionExecutable::nom))
                ajout(demangledName,(void*)sym[i].st_value);
            free(demangledName);
        }
        // N'ajoute pas les methodes pas demanglable
        // Ce sont des fonctions systeme
        /*
        else if (strlen(name)>0)
        {
            ajout(name,(void*)sym[i].st_value);
        }
        */
    }
    close(fd);
}

char* GestionPntrEtComplexite::recupereFichierEtNumeroTraceSymbol(void* ptrFonction, const char* fichier_specifique)
{
    char** trace_symbols = backtrace_symbols(&ptrFonction, 1);
    char trace_symbol[1024]={};
    memcpy(trace_symbol,trace_symbols[0],strlen(trace_symbols[0])+1);
    free(trace_symbols);
    if (strlen(trace_symbol)<10)
    {
        return nullptr;
    }

	char offset[1024]={};
    char* file_and_line_num=nullptr;

    // Extract file name and address from trace_symbol
    size_t p2 = strlen(trace_symbol)-1;
    while(p2>0 && trace_symbol[p2] != ')')
        p2--;

    size_t p1 = p2-1;
    while(p1>0 && trace_symbol[p1] != '(')
        p1--;
    char tmpAddr[256]={0x00};
    for (unsigned int j=p1+1;j<p2;j++)
        tmpAddr[j-(p1+1)]=trace_symbol[j];
    trace_symbol[p1]=0x00;

    char fichierExec[1024]={};          // Doit etre initialise a 0x00 avec ={}
    memcpy(fichierExec,trace_symbol,strlen(trace_symbol)+1);

    assert(sizeof(offset)>=strlen(tmpAddr)+1);
    memset(offset,0x00,sizeof(offset));
    memcpy(offset,tmpAddr,strlen(tmpAddr)+1);

    // offset contient le nom de la methode et l'offset par rapport au pointer de cette methode
    char nomFonction[1024]={};
    size_t j=0;
    while (j<strlen(offset) && offset[j]!='+')
    {
        nomFonction[j]=offset[j];
        j++;
    }
    if (j+1<strlen(offset))
        memmove(offset,&offset[j+1],strlen(offset)-j);
    int offsetLocal = (int)strtol(offset, nullptr, 0);
    int offsetFonction;
    int status=-1;
    char *demangledName = abi::__cxa_demangle(nomFonction, NULL, 0, &status);
    if (demangledName)
    {
        offsetFonction = (unsigned long)tabFonctionsExecutable.getAddr(demangledName);
        free(demangledName);
    }
    else
        offsetFonction = (unsigned long)tabFonctionsExecutable.getAddr(nomFonction);
    if (offsetFonction!=-1)
        sprintf(offset, "%#08x", offsetFonction+offsetLocal);
    else
        return nullptr;

    char syscom[4096];
    sprintf(syscom,"addr2line %s -e \"%s\"", offset, fichierExec);
    //printf("%s\n",syscom);
    // Passe au suivant si c'est ce fichier
    file_and_line_num=exec(syscom);

    // Verifie que le chaine file_and_line_num contient bien le chemin d'un fichier
    if (file_and_line_num==nullptr)
        return nullptr;
    if (strstr(file_and_line_num,"/")==nullptr)
    {
        free(file_and_line_num);
        return nullptr;
    }
    // Si un fichier_specifique est donne, verifier si c'est celui-la
    if (fichier_specifique!=nullptr)
    {
        if (strstr(file_and_line_num,fichier_specifique) == nullptr)
        {
            // Si ce n'est pas le cas, on continue
            free(file_and_line_num);
            return nullptr;
        }
    }
    // Verifie que le fichier n'est pas ce fichier qui contient cette fonction
    char nom_de_ce_fichier_source[]=__FILE__;
    // Enleve l'extension pour aussi detecter le fichier .h
    nom_de_ce_fichier_source[strlen(nom_de_ce_fichier_source)-3]=0x00;
    if (strstr(file_and_line_num,nom_de_ce_fichier_source) == nullptr)
    {
        char* tmp=strstr(file_and_line_num," (discriminator ");
        if (tmp != nullptr)
            *tmp=0x00;
        else
            // Supprime le retour a la ligne
            file_and_line_num[strlen(file_and_line_num)-1]=0x00;
        // Extrait le nom du fichier seul et le numero de la ligne
        int c1=strlen(file_and_line_num)-1;
        while(c1>0 && file_and_line_num[c1]!=':')
            c1--;
        char ligne[128]={0x00};
        memcpy(ligne,&file_and_line_num[c1+1],strlen(file_and_line_num)-c1-1);
        int numero=stoi(ligne);
        int c2=c1-1;
        while(c2>0 && file_and_line_num[c2]!='/')
            c2--;
        char fileName[128]={0x00};
        memcpy(fileName,&file_and_line_num[c2+1],c1-c2-1);
        //*GestionPntrEtComplexite::get().msgErreur << "Found: "<< file_and_line_num << endl;
        sprintf(file_and_line_num,"%s:%d",fileName,numero);
        return file_and_line_num;
    }
    return nullptr;
}

char* GestionPntrEtComplexite::recupereFichierEtNumeroLigneCourants(const char* fichier_specifique)
{
    void *trace[16];
	int trace_size = backtrace(trace, 16);

    // Doit trouver la premiere ligne apres celle contenant le fichier Double
    for (int i=1; i<trace_size && i<16; ++i)
    {
        char *file_and_line_num=recupereFichierEtNumeroTraceSymbol(trace[i], fichier_specifique);
        if (file_and_line_num!=nullptr)
            return file_and_line_num;
    }
    return nullptr;
}


////////////////////////////////////////////////////////////////////////////
// Le destructeur de la classe GestionPntrEtComplexite pour verifier les fuites //
// memoires sans devoir appeler fuiteMemoire() a la fin du main()         //
////////////////////////////////////////////////////////////////////////////


// Methode pour ajouter un pointeur qui vient d'etre alloue, au tableau
void GestionPntrEtComplexite::ajoutPntr(void* ptr, int taille, bool allocPourTab)
{
    if (tabPntr.nb%MARGE==0)
    {
        tabPntr.tab=(TabPntr::Pntr*)realloc(tabPntr.tab,sizeof(TabPntr::Pntr)*(tabPntr.nb+MARGE));
        memset(&tabPntr.tab[tabPntr.nb],0x00,sizeof(TabPntr::Pntr)*MARGE);
    }
    tabPntr.tab[tabPntr.nb].adr=ptr;
    // Genere un nombre aleatoire unique
    int j=0;
    do
    {
        tabPntr[tabPntr.nb].tag=rand();
        j=0;
        while(j<tabPntr.nb && tabPntr[tabPntr.nb].tag!=tabPntr[j].tag)
            j++;
    }
    while(j<tabPntr.nb);

    tabPntr[tabPntr.nb].taille=taille;
    tabPntr[tabPntr.nb].allocPourTab=allocPourTab;
    tabPntr[tabPntr.nb].libere=false;
    tabPntr.nb++;
}

// Retourne true si le pointeur est valide. On suppose qu'il est different de nullptr
int GestionPntrEtComplexite::chercherPntrIdx(const void* ptr)
{
    int i=tabPntr.nb-1;
    while(i>=0)
    {
        if ( tabPntr[i].adr==ptr && tabPntr[i].libere==false)
            break;
        i--;
    }
    return i;
}

bool GestionPntrEtComplexite::pntrDansTableauValide(const void* ptr)
{
    int i=tabPntr.nb-1;
    while(i>=0)
    {
        if ( ptr>=tabPntr[i].adr && ptr<(((char*)tabPntr[i].adr)+tabPntr[i].taille) && tabPntr[i].libere==false)
            break;
        i--;
    }
    if (i>=0)
        return true;

    i=tabInstance.nb-1;
    while(i>=0)
    {
        if ( ptr==tabInstance[i].adr && tabInstance[i].libere==false)
            break;
        i--;
    }
    return i>=0;
}

int GestionPntrEtComplexite::pntrTailleAllouee(void* ptr)
{
    int idx=chercherPntrIdx(ptr);
    // On suppose le pointeur valide
    assert(idx>=0);
    return tabPntr[idx].taille;
}

// Arrete le programme si l'etudiant essaie de supprimer un mauvais pointeur
bool GestionPntrEtComplexite::peutSupprPntr(void* ptr, bool allocPourTab)
{
    if (ptr==nullptr)
        return true;
    // Verifie si le pointeur est valide, c'est-a-dire, precedemment alloue
    int idxPtr=chercherPntrIdx(ptr);
    if (idxPtr==-1)
    {
        if (msgErreur!=nullptr && strlen(msgErreur)==0)
        {
            char* str=recupereFichierEtNumeroLigneCourants(nomFichierSourceEtudiant.c_str());
#ifdef DEBUG
            // Si ca ne vient pas du fichier de l'etudiant, ce bug est une consequence d'une erreur
            // plus haut dans le code, par exemple, une fonction sans return
            if (str==nullptr)
                str=recupereFichierEtNumeroLigneCourants();
#endif
            if (str!=nullptr)
                sprintf(msgErreur,"Erreur: delete d'un pointeur qui n'a pas été alloué ou déjà libéré\n  %s\n",str);
#ifdef DEBUG
            else
                sprintf(msgErreur,"Erreur: Pointeur qui n'a pas été alloué ou déjà libéré\n");
#endif
            free(str);
        }

        if (exitApresErreur)
            doExit(__LINE__);
        return false;
    }
    // Verifie si new[] correspond au delete[]
    if (tabPntr[idxPtr].allocPourTab!=allocPourTab)
    {
        if (msgErreur!=nullptr && strlen(msgErreur)==0)
        {
            // Si ca ne vient pas du fichier de l'etudiant, ce bug est une consequence d'une erreur
            // plus haut dans le code
            char* str=recupereFichierEtNumeroLigneCourants(nomFichierSourceEtudiant.c_str());
#ifdef DEBUG
            if (str==nullptr)
                str=recupereFichierEtNumeroLigneCourants();
#endif
            if (str!=nullptr)
            {
                if (allocPourTab)
                    sprintf(msgErreur,"Erreur: Utilisation de delete[] au lieu de delete à la ligne suivante:\n  %s\n",str);
                else
                    sprintf(msgErreur,"Erreur: Utilisation de delete au lieu de delete[] à la ligne suivante:\n  %s\n",str);
                free(str);
            }
#ifdef DEBUG
            else
            {
                if (allocPourTab)
                    sprintf(msgErreur,"Erreur: Utilisation de delete[] au lieu de delete\n");
                else
                    sprintf(msgErreur,"Erreur: Utilisation de delete au lieu de delete[]\n");
            }
#endif
            free(str);
        }

        if (exitApresErreur)
            doExit(__LINE__);
        return false;
    }
    return true;
}

void GestionPntrEtComplexite::supprPntr(void* ptr, bool allocPourTab)
{
    if (ptr==nullptr)
        return;

    if (!peutSupprPntr(ptr,allocPourTab))
        return;

    int idxPtr=chercherPntrIdx(ptr);

    // Suppression du pointeur
    assert(idxPtr<tabPntr.nb);
    assert(tabPntr[idxPtr].libere==false);
    assert(tabPntr[idxPtr].allocPourTab==allocPourTab);

    // Supprime toutes les instances dans ce tableau dynamique
    supprInst(tabPntr[idxPtr].adr, tabPntr[idxPtr].taille);
    // libere la memoire
    free(tabPntr[idxPtr].adr);
    // On ne supprime pas les pointeurs du tableau de pointeurs
    // Indique seuelement que le pointeur a ete libere
    // Comme cela, on garde un historique des allocations
    tabPntr[idxPtr].libere=true;
}


void GestionPntrEtComplexite::ajoutInst(void* ptr)
{
    // Ajout de l'instance
    // Augmente la taille du tableau si necessaire
    if (tabInstance.nb%MARGE==0)
    {
        tabInstance.tab=(TabInstance::Inst*)realloc(tabInstance.tab,sizeof(TabInstance::Inst)*(tabInstance.nb+MARGE));
        memset(&tabInstance.tab[tabInstance.nb],0x00,sizeof(TabInstance::Inst)*MARGE);
    }

    tabInstance[tabInstance.nb].adr=ptr;
    // Verifie si c'est instance creee par allocation dynamique (avec new ou new[])
    int i=0;
    while(i<tabPntr.nb&&(ptr<tabPntr[i].adr||
                (char*)ptr>=(char*)tabPntr[i].adr+tabPntr[i].taille))
        i++;
    tabInstance[tabInstance.nb].dyn=(i<tabPntr.nb);
    tabInstance[tabInstance.nb].libere=false;
    tabInstance.nb++;
}

// Supprime toutes les instances dont l'adr est entre ptr et ptr+taille
void GestionPntrEtComplexite::supprInst(void* ptr, int taille)
{
    for(int i=0;i<tabInstance.nb;i++)
    {
        if(tabInstance[i].libere!=false)
            continue;

        if(tabInstance[i].adr>=ptr && (char*)tabInstance[i].adr<(char*)ptr+taille)
        {
            tabInstance[i].libere=true;
        }
    }
}

// Verifie si l'instance a ete allouee et qu'on peut donc l'utiliser sans planter le programme
// Eventuellement affiche un message d'erreur
bool GestionPntrEtComplexite::instValide(const void* instAdr)
{
    int i=0;
    while(i<tabInstance.nb)
    {
        if (tabInstance[i].adr==instAdr && tabInstance[i].libere==false)
            break;
        i++;
    }

    if (i==tabInstance.nb)
    {
        if (msgErreur!=nullptr && strlen(msgErreur)==0)
        {
            char* str=recupereFichierEtNumeroLigneCourants(nomFichierSourceEtudiant.c_str());
            if (str!=nullptr)
                sprintf(msgErreur,"Erreur: Utilisation d'un pointeur pas alloué ou déjà libéré à la ligne suivante:\n  %s\n",str);
#ifdef DEBUG
            else
            {
                // Si ca ne vient pas du fichier de l'etudiant, ce bug est une consequence d'une erreur
                // plus haut dans le code
                str=recupereFichierEtNumeroLigneCourants();
                if (str!=nullptr)
                    sprintf(msgErreur,"Erreur: Utilisation d'un pointeur pas alloué ou déjà libéré à la ligne suivante:\n  %s\n",str);
                else
                    sprintf(msgErreur,"Erreur: Utilisation d'un pointeur pas alloué ou déjà libéré\n");
            }
#endif
            free(str);
        }

        if (exitApresErreur)
            doExit(__LINE__);
        return false;
    }
    return true;
}


void GestionPntrEtComplexite::TabAppelsInstructions::supprInstruction(const void *ptr)
{
    // Supprime toutes les instruction de la methode dont l'id est passee en parametre
    for (int idInstruct=0;idInstruct<nbInstructions;idInstruct++)
    {
        // Si une instruction dont la methode est celle passee en parametre est trouvee, on la supprime
        if (tabInstructions[idInstruct].ptr==ptr)
        {
            // Supprime les appels a l'instruction
            for (int j=0;j<nbAppels;j++)
            {
                if (tabAppels[j].idInstruct==idInstruct && (nbAppels-j-1)>0)
                {
                    memmove(&tabAppels[j],&tabAppels[j+1],(nbAppels-j-1)*sizeof(Appel));
                    j--;
                    nbAppels--;
                }
            }
            for (int j=0;j<nbAppels;j++)
                if (tabAppels[j].idInstruct>idInstruct)
                    tabAppels[j].idInstruct--;
            // Supprime l'instruction
            if (tabInstructions[idInstruct].nomMethode)
                free(tabInstructions[idInstruct].nomMethode);
            memmove(&tabInstructions[idInstruct],&tabInstructions[idInstruct+1],sizeof(Instruction)*(nbInstructions-idInstruct-1));
            nbInstructions--;
            idInstruct--;
        }
    }
}

bool GestionPntrEtComplexite::extraitNomClasseEtMethode(const char* str, char *nomClasse, char *nomMethode)
{
    int len=strlen(str);
    const char *adr=strstr(str,"::");
    if (adr==nullptr)
        return false;
    // Extrait le nom de la classe
    int idx = (adr-str)-1;
    int idx0 = idx;
    // Pour les classes template
    while(idx0>=0 && str[idx0]!='<')
        idx0--;
    if (idx0==-1)
        idx0=idx;
    // Cherche le caractere espace a partir de la fin
    while(idx0>=0 && str[idx0]!=' ')
        idx0--;
    idx0++;
    int l=idx-idx0+1;
    memcpy(nomClasse,&str[idx0],l*sizeof(char));
    nomClasse[l]=0x00;

    // Extrait le nom de la methode
    if (nomMethode!=nullptr)
    {
        idx = (adr-str)+2;
        l = 0;
        while(idx+l<len && str[idx+l]!=' ' && str[idx+l]!='(')
            l++;
        memcpy(nomMethode,&str[idx],l*sizeof(char));
        nomMethode[l]=0x00;
    }

    return true;
}


// Supprime dans this et t, toutes instructions appartenant aux memes methodes de classe
void GestionPntrEtComplexite::TabAppelsInstructions::supprInstructionsIdentiques(TabAppelsInstructions &t)
{
    for (int i=0;i<nbInstructions;i++)
    {
        int j=0;
        while(j<t.nbInstructions && tabInstructions[i].ptr!=t.tabInstructions[j].ptr)
            j++;
        // Si une instruction avec le meme pointeur est trouvee, on l'a supprime
        if (j<t.nbInstructions)
        {
            // Supprime la methode en commun et toutes les instructions qui lui sont associees
            const void* pntrASuppr=tabInstructions[i].ptr;
            supprInstruction(pntrASuppr);
            t.supprInstruction(pntrASuppr);
            i--;
        }
    }
}

void GestionPntrEtComplexite::TabAppelsInstructions::supprInstructionsDUneClasse(const char* nomClasseASuppr)
{
    char nomClasse[255];
    for (int i=0;i<nbInstructions;i++)
    {
        extraitNomClasseEtMethode(tabInstructions[i].nomMethode, nomClasse);
        if (strcmp(nomClasse,nomClasseASuppr)==0)
        {
            supprInstruction(tabInstructions[i].ptr);
            i--;
        }
    }
}

// Calcule le nombre de classes differentes utilisees
int GestionPntrEtComplexite::TabAppelsInstructions::getNbClasses()
{
    char tabNomsClasses[255][255];
    memset(tabNomsClasses,0x00,sizeof(tabNomsClasses));
    int nbNomClasses=0;
    for (int i=0;i<nbInstructions;i++)
    {
        if (tabInstructions[i].nomMethode==nullptr)
            continue;
        char nomClasse[255],nomMethode[255];
        // Extrait le nom de la classe
        extraitNomClasseEtMethode(tabInstructions[i].nomMethode, nomClasse, nomMethode);
        int j=0;
        while(j<nbNomClasses && strcmp(tabNomsClasses[j],nomClasse)!=0)
            j++;
        if (j==nbNomClasses)
        {
            memcpy(tabNomsClasses[nbNomClasses],nomClasse,strlen(nomClasse)+1);
            nbNomClasses++;
        }
    }
    return nbNomClasses;
}

// Calcule le nombre de methodes differentes utilisees
int GestionPntrEtComplexite::TabAppelsInstructions::getNbMethodes()
{
    char tabNomsMethodes[255][255];
    memset(tabNomsMethodes,0x00,sizeof(tabNomsMethodes));
    int nbNomMethodes=0;
    for (int i=0;i<nbInstructions;i++)
    {
        if (tabNomsMethodes[i]==nullptr)
            continue;
        int j=0;
        while(j<nbNomMethodes && strcmp(tabNomsMethodes[j],tabInstructions[i].nomMethode)!=0)
            j++;
        if (j==nbNomMethodes)
        {
            assert((strlen(tabInstructions[i].nomMethode)+1)<=sizeof(tabNomsMethodes[nbNomMethodes]));
            memcpy(tabNomsMethodes[nbNomMethodes],tabInstructions[i].nomMethode,strlen(tabInstructions[i].nomMethode)+1);
            nbNomMethodes++;
        }
    }
    return nbNomMethodes;
}

void GestionPntrEtComplexite::TabAppelsInstructions::getNomsClasseEtMethode(char* nomClasse,char* nomMethode)
{
    nomClasse[0]=0x00;
    nomMethode[0]=0x00;
    // Parcours des appels en partant du dernier
    for (int i=nbAppels-1;i>=0;i--)
    {
        // Recupere l'id de l'instruction
        int idInstruct=tabAppels[i].idInstruct;
        // Verifie si c'est une sortie de fonction
        if (tabInstructions[idInstruct].type!=SORTIE_FONCT)
            continue;
        // Si l'extraction du nom de la classe a ete fait correctement, on retourne
        if (extraitNomClasseEtMethode(tabInstructions[idInstruct].nomMethode, nomClasse, nomMethode))
            return;
    }
}

void GestionPntrEtComplexite::TabAppelsInstructions::getNomMethodeOuFonction(char* nomMethode)
{
    nomMethode[0]=0x00;
    // Parcours des appels en partant du dernier
    for (int i=nbAppels-1;i>=0;i--)
    {
        // Recupere l'id de l'instruction
        int idInstruct=tabAppels[i].idInstruct;
        // Verifie si c'est une sortie de fonction
        if (tabInstructions[idInstruct].type!=SORTIE_FONCT)
            continue;
        // Si l'extraction du nom de la classe a ete fait correctement, on retourne
        assert(tabInstructions[idInstruct].nomMethode!=nullptr &&
               strlen(tabInstructions[idInstruct].nomMethode)>0 &&
               strlen(tabInstructions[idInstruct].nomMethode)+1<255);
        // Si l'extraction du nom de la classe a ete fait correctement, on retourne
        memcpy(nomMethode,tabInstructions[idInstruct].nomMethode,strlen(tabInstructions[idInstruct].nomMethode)+1);
        return;
    }
}

// Retourne le nombre total de pointeur alloues pour la liste d'instructions
int GestionPntrEtComplexite::TabAppelsInstructions::getNbTotalPntr()
{
    int nbPntr=0;
    for (int i=0;i<nbInstructions;i++)
    {
        if (tabInstructions[i].type!=NEW && tabInstructions[i].type!=DOUBLE_NEW && tabInstructions[i].type!=INT_NEW)
            continue;
        nbPntr+=tabInstructions[i].nbAppels;
    }
    return nbPntr;
}

// Retourne le nombre total de pointeurs alloues et pas encore liberes
int GestionPntrEtComplexite::TabAppelsInstructions::getNbTotalPntrPasLiberes()
{
    int nbPntr=0;
    for (int i=0;i<nbAppels;i++)
    {
        int idInstruct=tabAppels[i].idInstruct;
        if (tabInstructions[idInstruct].type!=NEW && \
            tabInstructions[idInstruct].type!=DOUBLE_NEW && \
            tabInstructions[idInstruct].type!=INT_NEW)
            continue;
        // Verifie s'il existe une instruction pour la liberation du pointeur
        // Ne pas utiliser la methode pointeur valide parce que une meme adresse peut
        // etre reutilisee plusieurs fois
        int j=i+1;
        for(;j<nbAppels;j++)
        {
            int idInstruct2=tabAppels[j].idInstruct;
            if (tabInstructions[idInstruct2].type!=DELETE && \
                tabInstructions[idInstruct2].type!=DOUBLE_DELETE && \
                tabInstructions[idInstruct2].type!=INT_DELETE)
                continue;
            if (tabAppels[i].pthis==tabAppels[j].pthis)
                break;
        }
        if (j==nbAppels)
            nbPntr++;
    }
    return nbPntr;
}

// Retourne le nombre total de pointeurs liberes
int GestionPntrEtComplexite::TabAppelsInstructions::getNbTotalPntrLiberes()
{
    int nbPntr=0;
    for (int i=0;i<nbAppels;i++)
    {
        int idInstruct=tabAppels[i].idInstruct;
        if (tabInstructions[idInstruct].type!=DELETE && \
            tabInstructions[idInstruct].type!=DOUBLE_DELETE && \
            tabInstructions[idInstruct].type!=INT_DELETE)
            continue;
        nbPntr++;
    }
    return nbPntr;
}

int GestionPntrEtComplexite::TabAppelsInstructions::ajoutPntrInstruction(TabAppelsInstructions::Type type,
                                                                         const void* ptrInstruction,
                                                                         const char* nomMethode,
                                                                         const void* pThis, const void* param)
{
    // Verifie si l'instruction n'est pas deja dans le tableau.
    // Le pointeur de l'instruction et son type doivent etre different
    int i=0;
    while(i<nbInstructions && (tabInstructions[i].ptr!=ptrInstruction || tabInstructions[i].type!=type))
        i++;

    // Nouvelle instruction
    if (i==nbInstructions)
    {
        // Redimmensionne le tableau dynamique si necessaire
        if (nbInstructions%MARGE==0)
        {
            tabInstructions=(Instruction*)realloc(tabInstructions,sizeof(Instruction)*(nbInstructions+MARGE));
            memset(&tabInstructions[nbInstructions],0x00,sizeof(Instruction)*MARGE);
        }

        // Definit le type
        tabInstructions[nbInstructions].type=type;

        // Definit le pointeur vers l'instruction
        tabInstructions[nbInstructions].ptr=ptrInstruction;
        // Si le nom de la methode n'est pas deja donne, on le calcule
        if (nomMethode==nullptr)
        {
            // Met le nom de la methode dans la donnée de l'instruction
            // Ce nom est celui de la methode sur le haut de la pile d'execution
            assert(strlen(tabPileMethodes[nbPileMethodes-1].nom)+1<255);
            tabInstructions[nbInstructions].nomMethode=(char*)calloc(strlen(tabPileMethodes[nbPileMethodes-1].nom)+1,
                                                                     sizeof(char));
            memcpy(tabInstructions[nbInstructions].nomMethode,
                   tabPileMethodes[nbPileMethodes-1].nom,
                   strlen(tabPileMethodes[nbPileMethodes-1].nom)+1);
        }
        else
        {
            assert(strlen(nomMethode)+1<255);
            tabInstructions[nbInstructions].nomMethode=(char*)calloc(strlen(nomMethode)+1,sizeof(char));
            memcpy(tabInstructions[nbInstructions].nomMethode,nomMethode,strlen(nomMethode)+1);
        }
        nbInstructions++;
    }


/*    if (type==NEW || type==DOUBLE_NEW || type==INT_NEW)
        int kk=0;
    if (type==DELETE || type==DOUBLE_DELETE || type==INT_DELETE)
        int kk=0;
    if (type==ENTREE_FONCT || type==SORTIE_FONCT)
        int kk=0;
    if (type==DELETE || type==DOUBLE_DELETE || type==INT_DELETE)
        int kk=0;*/

    // Enregistre l'appel
    tabInstructions[i].nbAppels++;
    // Redimmensionne le tableau dynamique des appels si necessaire
    if (nbAppels%MARGE==0)
    {
        tabAppels=(Appel*)realloc(tabAppels,sizeof(Appel)*(nbAppels+MARGE));
        memset(&tabAppels[nbAppels],0x00,sizeof(Appel)*MARGE);
    }
    tabAppels[nbAppels].idInstruct=i;
    tabAppels[nbAppels].pthis=pThis;
    tabAppels[nbAppels].param=param;
    nbAppels++;

    return i;
}

// Ajout d'une instruction
int GestionPntrEtComplexite::TabAppelsInstructions::ajoutInstruction(TabAppelsInstructions::Type type,
                                                                     const void* pThis, const void* param)
{
    if (!GestionPntrEtComplexite::get().compterInstructions)
        return -1;
    // Ignore les appels en-dehors d'une methode de la correction ou de l'etudiant
    if(nbPileMethodes==0)
        return -1;

    // Recupere le pointeur vers l'instruction
	const int nMaxFrames = 3;
    void *callstack[nMaxFrames];
	int nFrames = backtrace(callstack, nMaxFrames);
	assert(nFrames==nMaxFrames);
    void* ptrInstruction=callstack[2];
    // Autre solution (legerement plus rapide)
    //void *ptrInstruction=__builtin_return_address(0x01);
    return ajoutPntrInstruction(type, ptrInstruction, nullptr, pThis, param);
}

void GestionPntrEtComplexite::TabAppelsInstructions::ajoutPileMethode(const void *ptr)
{
    // Avec dladdr et __cxa_demangle: 111 secondes
    // Sans: 7 secondes
    // Filtre les methodes
    int j=0;
    while(j<nbFiltresPile && tabFiltresPile[j]!=ptr)
        j++;
    if (j==nbFiltresPile)
        return;

    char* nomMethode=GestionPntrEtComplexite::get().tabFonctionsExecutable[ptr].nom;
    //cout<<"Enter: "<<nomMethode<<endl;
    ajoutPntrInstruction(ENTREE_FONCT, ptr, nomMethode);
    // Ajout de la methode dans la pile d'executions
    tabPileMethodes[nbPileMethodes].ptr=ptr;
    memset(tabPileMethodes[nbPileMethodes].nom, 0x00, sizeof(tabPileMethodes[nbPileMethodes].nom));
    memcpy(tabPileMethodes[nbPileMethodes].nom, nomMethode, strlen(nomMethode)+1);
    nbPileMethodes++;
}

void GestionPntrEtComplexite::TabAppelsInstructions::supprPileMethode(const void *ptr)
{
    // Avec dladdr et __cxa_demangle: 111 secondes
    // Sans: 7 secondes

    // Filtre les methodes
    int j=0;
    while(j<nbFiltresPile && tabFiltresPile[j]!=ptr)
        j++;
    if (j==nbFiltresPile)
        return;

    // Sortie de l'instruction "entree dans une methode"
    char* nomMethode=GestionPntrEtComplexite::get().tabFonctionsExecutable[ptr].nom;
    //cout<<"Exit: "<<nomMethode<<endl;

    ajoutPntrInstruction(SORTIE_FONCT, ptr, nomMethode);
    // A une entree correspond toujours une sortie
    assert(tabPileMethodes[nbPileMethodes-1].ptr==ptr);
    // Suppression de la methode de la pile d'executions
    memset(tabPileMethodes[nbPileMethodes-1].nom, 0x00, sizeof(tabPileMethodes[nbPileMethodes-1].nom));
    tabPileMethodes[nbPileMethodes-1].ptr=nullptr;
    nbPileMethodes--;
}

bool GestionPntrEtComplexite::ajoutInstruction(const char* typeInstruction, const char* fichier, int numLigne)
{
    if (!compterInstructions)
        return true;

    // Definit le type
    TabAppelsInstructions::Type type;
    if(strcmp(typeInstruction,"if")==0)
        type=TabAppelsInstructions::IF;
    else if(strcmp(typeInstruction,"while")==0)
        type=TabAppelsInstructions::WHILE;
    else if(strcmp(typeInstruction,"for")==0)
        type=TabAppelsInstructions::FOR;
    else if(strcmp(typeInstruction,"return")==0)
        type=TabAppelsInstructions::RETURN;
    else
        assert(0);

    // Ajoute l'instruction
    int instructIdx=tabAppelsInstructions.ajoutInstruction(type);
    if (instructIdx==-1)
        return true;

    // Si le nombre d'appels devient trop grand pour l'instruction, il y a une boucle infinies
    if ((tabAppelsInstructions[instructIdx].type==TabAppelsInstructions::WHILE ||
         tabAppelsInstructions[instructIdx].type==TabAppelsInstructions::FOR) &&
        tabAppelsInstructions[instructIdx].nbAppels>TabAppelsInstructions::NB_MAX_APPELS_BOUCLE_INFINIE)
    {
        // On desactive temporairement le compteur d'appels
        // Les lignes suivantes peuvent utiliser l'operateur new et donc provoquer un appel recursif
        compterInstructions=false;
        // On affiche un message d'erreur
        if (msgErreur!=nullptr && strlen(msgErreur)==0)
            sprintf(msgErreur, "Erreur: boucle infinie à la ligne suivante\n  %s:%d\n", fichier, numLigne);
        doExit(__LINE__);
    }
    return true;
}

void GestionPntrEtComplexite::resetEtStartCompterInstructions()
{
    tabAppelsInstructions.reset();
    compterInstructions=true;
}

void GestionPntrEtComplexite::stopCompterInstructions()
{
    compterInstructions=false;
}

void GestionPntrEtComplexite::doExit(int numLigne)
{
    // Remet le compteur a zero pour eviter de generer des exceptions
    compterInstructions=false;
    exitApresErreur=false;
    // On arrete le programme
    cout<<"GestionPntrEtComplexite, exit() a la ligne "<<numLigne<<endl;
    char* str=recupereFichierEtNumeroLigneCourants(nomFichierSourceEtudiant.c_str());
    if (str==nullptr)
        str=recupereFichierEtNumeroLigneCourants();
    if (str!=nullptr)
        printf("Origine de l'exit:\n  %s\n",str);
    else
        printf("Origine de l'exit inconnue\n");
    free(str);
    exit(0);
}


extern "C"
{

// -finstrument-functions
// -finstrument-functions-exclude-file-list=Evaluate.cpp,GestionPntrEtComplexite.cpp,main.cpp,Evaluate.h,Exercices.h,GestionPntrEtComplexite.h,StructDeDonnees.h,StructDeDonneesVerifCorr.h,initializer,list,vector,iterator,alloc,trait,stream,string,base,memory,uninitialized,new

void __cyg_profile_func_enter(void *this_fn, void *call_site)
{
    if (!GestionPntrEtComplexite::get().compterInstructions)
        return;
    // Ajoute la methode appelee dans la pile
    GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutPileMethode(this_fn);
}

void __cyg_profile_func_exit(void *this_fn, void *call_site)
{
    if (!GestionPntrEtComplexite::get().compterInstructions)
        return;
    if (GestionPntrEtComplexite::get().tabAppelsInstructions.nbPileMethodes==0)
        return;
    GestionPntrEtComplexite::get().tabAppelsInstructions.supprPileMethode(this_fn);
}
}

// Fonction execute en tout dernier
__attribute__((destructor))
void libere_memoire() {
    // Libere ici toute la memoire pour simplifier la detection des fuites memoire
    //GestionPntrEtComplexite::get().tabAppelsInstructions.reset();
    //cout<<"__attribute__((destructor))"<<endl;
}

