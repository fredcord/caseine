# On modifie les fichiers Exercices.h et Exercices.cpp pour utiliser la classe Double au lieu de double*
import re
import sys

def extraitNomClasseEtudiant(fileName):
    f=open(fileName,'r',encoding='utf-8')
    fileContent = f.read()
    f.close()
    nomClasseEtud=None
    nomClasseCorr=None
    m = re.search(r'^#define\s+STRUCT_DE_DONNEES_ETUD\s+(\w+)\s*', fileContent, flags=re.MULTILINE)
    if m:
        nomClasseEtud = m.groups()[0]
    m = re.search(r'^#define\s+STRUCT_DE_DONNEES_CORR\s+(\w+)\s*', fileContent, flags=re.MULTILINE)
    if m:
        nomClasseCorr = m.groups()[0]
    return nomClasseCorr,nomClasseEtud


class ProcessIf(object):
    def __call__(self, match):
        return 'if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&'


class ProcessWhile(object):
    def __call__(self, match):
        return 'while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&'


class ProcessFor(object):
    def __call__(self, match):
        # Si le groupe(2) est vide ou que des caracteres separateurs ou des commentaires
        if len(match.group(2))==0 or match.group(2).isspace() or re.match(r'\s*/\*(.*)\*/\s*$', match.group(2)):
            return 'for('+match.group(1)+';GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__)'+match.group(2)+';'
        else:
            return 'for('+match.group(1)+';GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),'+match.group(2)+';'


class ProcessReturn(object):
    def __call__(self, match):
        return '{GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return '+match.group(1)+';}'


def processFile(fileName, fileNameToWrite, nomClasse=None):
    # Lit le fichier
    f=open(fileName,'r',encoding='utf-8')
    fileContent = f.read()
    f.close()

    # Code pour isoler les constructeurs de l'etudiant
    if nomClasse is not None:
        fileContent = re.sub(nomClasse+r"\s*::\s*"+nomClasse+"\s*\(",nomClasse+"_ConstructDestruct::"+nomClasse+"_ConstructDestruct(",fileContent)
        fileContent = re.sub(nomClasse+r"\s*::\s*~\s*"+nomClasse+"\s*\(",nomClasse+"_ConstructDestruct::~"+nomClasse+"_ConstructDestruct(",fileContent)
        fileContent = re.sub(nomClasse+r"\s*::",nomClasse+"_Other::",fileContent)
        fileContent = re.sub(nomClasse+r"\s+operator\s*\+\s*\(", nomClasse+" operator_plus(", fileContent)
        fileContent = re.sub(nomClasse+r"\s+operator\s*\-\s*\(", nomClasse+" operator_moins(", fileContent)
        
    # On remplace le type double par la classe Double
    fileContent = re.sub(r"\bdouble\b","Double",fileContent)
    fileContent = re.sub(r"\bint\b","Int",fileContent)
    # Pour eviter de remplacer les const int
    fileContent = re.sub('const\s+Int', 'const int', fileContent)
    # Pour eviter de remplacer les const static int
    fileContent = re.sub('const\s+static\s+Int', 'const static int', fileContent)

    # Insere du code dans les instruction if(...
    fileContent = re.sub(r'if\s*\(', ProcessIf(), fileContent)

    # Insere du code dans les boucles while pour detecter les boucles infinies
    fileContent = re.sub(r'while\s*\(', ProcessWhile(), fileContent)

    # Insere du code dans les boucles while pour detecter les boucles infinies
    # \s*      --> zero or several separators
    # [^,;]+   --> at least one char (any char except ;)
    # \(       --> match paranthesis
    # ;        --> match ;
    # ()       --> define group(1). Use \1 to keep group(1) for the substitution
    fileContent = re.sub(r'for\s*\(([^,;]*);([^,;]*);', ProcessFor(), fileContent)

    # Pour les return
    fileContent = re.sub(r'return\s+([^,;]+);', ProcessReturn(), fileContent)

    # Pour les operateur delete[]
    fileContent = re.sub(r'delete\s*\[\s*\]\s*(\w+)\s*;', r'{GestionPntrEtComplexite::get().peutSupprPntr(\1, true); delete[] \1;}', fileContent)

    # Pour les operateur delete
    fileContent = re.sub(r'delete\s+(\w+)\s*;', r'{GestionPntrEtComplexite::get().peutSupprPntr(\1, false); delete \1;}', fileContent)

    # Ecrit le fichier
    f=open(fileNameToWrite,'w',encoding='utf-8')
    f.write("////////////////////////////////////////////////////////////////////////////////////\n")
    f.write("// Fichier genere automatiquement pour l'instrumentation du code. Ne pas modifier //\n")
    f.write("////////////////////////////////////////////////////////////////////////////////////\n\n\n")
    f.write(fileContent)
    f.close()

# Instrumentation des fichiers
nomClasseCorr,nomClasseEtud = extraitNomClasseEtudiant('vpl_evaluate.cases')
processFile('Exercices.cpp', 'Exercices-instr.cpp', nomClasseEtud)
processFile('ExercicesCorr.cpp', 'ExercicesCorr-instr.cpp', nomClasseCorr)
processFile('vpl_evaluate.cases', 'vpl_evaluate-instr.cases')

