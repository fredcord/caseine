////////////////////////////////////////////////////////////////////////////////////
// Fichier genere automatiquement pour l'instrumentation du code. Ne pas modifier //
////////////////////////////////////////////////////////////////////////////////////


#include "ExercicesBase.h"

using namespace std;

// DEBUT EXO_CONSTRUCTEUR
File_ConstructDestruct::File_ConstructDestruct():d_premier{0},d_dernier{0},d_quantite{0}
{
}
// FIN EXO_CONSTRUCTEUR


// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
File_ConstructDestruct::File_ConstructDestruct(const File &param):d_premier{param.d_premier},d_dernier{param.d_dernier},d_quantite{param.d_quantite}
{
    // Copie le tableau
    Int i=d_premier,q=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&q<d_quantite)
    {
        d_file[i]=param.d_file[i];
        q++;
        i++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&i==max_elt)
            i=0;
    }
}
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE


// DEBUT EXO_CONCATENATION_FIN
void File_Other::concatenationFin(const File &param)
{
    Int id2=param.d_premier;
    Int quant=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&quant<param.d_quantite)
    {
        d_file[d_dernier]=param.d_file[id2];
        d_dernier++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_dernier==max_elt)
            d_dernier=0;
        id2++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&id2==max_elt)
            id2=0;
        quant++;
        d_quantite++;
    }
}
// FIN EXO_CONCATENATION_FIN

// DEBUT EXO_OP_CONCATENATION
File &File_Other::operator+=(const File &param)
{
    Int id2=param.d_premier;
    Int quant=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&quant<param.d_quantite)
    {
        d_file[d_dernier]=param.d_file[id2];
        d_dernier++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_dernier==max_elt)
            d_dernier=0;
        id2++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&id2==max_elt)
            id2=0;
        quant++;
        d_quantite++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_CONCATENATION


// DEBUT EXO_OP_EGAL
bool File_Other::operator==(const File &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_quantite!=param.d_quantite)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    Int i=d_premier,j=param.d_premier;
    Int quant=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&quant<d_quantite &&  d_file[i]==param.d_file[j])
    {
        i++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&i==max_elt)
            i=0;
        j++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&j==max_elt)
            j=0;
        quant++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return quant==d_quantite;}
}
// FIN EXO_OP_EGAL


// DEBUT EXO_OP_AFFECTATION
File &File_Other::operator=(const File &param)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
    d_premier=param.d_premier;
    d_dernier=param.d_dernier;
    d_quantite=param.d_quantite;

    Int id=param.d_premier;
    Int quant=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&quant<param.d_quantite)
    {
        d_file[id]=param.d_file[id];
        id++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&id==max_elt)
            id=0;
        quant++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_AFFECTATION


// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
void File_Other::additionnerATousLesElts(Double val)
{
    Int id=d_premier;
    Int quant=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&quant<d_quantite)
    {
        d_file[id]+=val;
        id++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&id==max_elt)
            id=0;
        quant++;
    }
}
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS


// DEBUT EXO_INSERER_UN_ELT_FIN
void File_Other::insererUnEltFin(Double val)
{
    d_file[d_dernier++] = val;
    d_dernier %= max_elt;
    d_quantite++;
}
// FIN EXO_INSERER_UN_ELT_FIN


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
void File_Other::enleverPlusieursEltsDebut(Int nb)
{
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<nb && d_quantite>0)
    {
        d_premier++;
        d_premier %= max_elt;
        d_quantite--;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT


