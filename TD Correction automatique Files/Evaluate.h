#ifndef EVALUATE_H_INCLUDED
#define EVALUATE_H_INCLUDED

#include <signal.h>
#include <vector>

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

#include "StructDeDonneesVerifCorr.h"
#include "GestionPntrEtComplexite.h"
#include "ExercicesBase.h"



#ifndef Q01
#define	Q01
#endif
#ifndef Q02
#define	Q02
#endif
#ifndef Q03
#define	Q03
#endif
#ifndef Q04
#define	Q04
#endif
#ifndef Q05
#define	Q05
#endif
#ifndef Q06
#define	Q06
#endif
#ifndef Q07
#define	Q07
#endif
#ifndef Q08
#define	Q08
#endif
#ifndef Q09
#define	Q09
#endif
#ifndef Q10
#define	Q10
#endif
#ifndef Q11
#define	Q11
#endif
#ifndef Q12
#define	Q12
#endif
#ifndef Q13
#define	Q13
#endif
#ifndef Q14
#define	Q14
#endif
#ifndef Q15
#define	Q15
#endif
#ifndef Q16
#define	Q16
#endif
#ifndef Q17
#define	Q17
#endif
#ifndef Q18
#define	Q18
#endif
#ifndef Q19
#define	Q19
#endif
#ifndef Q20
#define	Q20
#endif
#ifndef Q21
#define	Q21
#endif
#ifndef Q22
#define	Q22
#endif
#ifndef Q23
#define	Q23
#endif
#ifndef Q24
#define	Q24
#endif
#ifndef Q25
#define	Q25
#endif
#ifndef Q26
#define	Q26
#endif
#ifndef Q27
#define	Q27
#endif
#ifndef Q28
#define	Q28
#endif
#ifndef Q29
#define	Q29
#endif
#ifndef Q30
#define	Q30
#endif
#ifndef Q31
#define	Q31
#endif
#ifndef Q32
#define	Q32
#endif
#ifndef Q33
#define	Q33
#endif
#ifndef Q34
#define	Q34
#endif
#ifndef Q35
#define	Q35
#endif
#ifndef Q36
#define	Q36
#endif
#ifndef Q37
#define	Q37
#endif
#ifndef Q38
#define	Q38
#endif
#ifndef Q39
#define	Q39
#endif
#ifndef Q40
#define	Q40
#endif
#ifndef Q41
#define	Q41
#endif
#ifndef Q42
#define	Q42
#endif
#ifndef Q43
#define	Q43
#endif
#ifndef Q44
#define	Q44
#endif
#ifndef Q45
#define	Q45
#endif
#ifndef Q46
#define	Q46
#endif
#ifndef Q47
#define	Q47
#endif
#ifndef Q48
#define	Q48
#endif
#ifndef Q49
#define	Q49
#endif
#ifndef Q50
#define	Q50
#endif

#define	ID_EXO_CONSTRUCTEUR                                          1
#define	ID_EXO_DESTRUCTEUR                                           2
#define	ID_EXO_CONSTRUCTEUR_PAR_RECOPIE	                             3
#define	ID_EXO_CONSTRUCTEUR_AVEC_TABLEAU          	                 4
#define	ID_EXO_CONCATENATION_DEBUT	                                 5
#define	ID_EXO_CONCATENATION_MILIEU	                                 6
#define	ID_EXO_CONCATENATION_FIN    	                             7
#define	ID_EXO_OP_CONCATENATION	                                     8
#define	ID_EXO_OP_EGAL	                                             9
#define	ID_EXO_OP_DIFFERENT	                                        10
#define	ID_EXO_OP_AFFECTATION	                                    11
#define	ID_EXO_OP_CROCHET	                                        12
#define	ID_EXO_OP_CROCHET_CONST	                                    13
#define	ID_EXO_OP_PARENTHESE                                        14
#define	ID_EXO_OP_PARENTHESE_CONST	                                15
#define	ID_EXO_GETTER	                                            16
#define	ID_EXO_SETTER	                                            17
#define	ID_EXO_ADDITIONNER_A_TOUS_LES_ELTS    	                    18
#define	ID_EXO_INSERER_UN_ELT_DEBUT	                                19
#define	ID_EXO_INSERER_UN_ELT_MILIEU	                            20
#define	ID_EXO_INSERER_UN_ELT_FIN          	                        21
#define	ID_EXO_INSERER_PLUSIEURS_ELTS_DEBUT	                        22
#define	ID_EXO_INSERER_PLUSIEURS_ELTS_MILIEU	                    23
#define	ID_EXO_INSERER_PLUSIEURS_ELTS_FIN      	                    24
#define	ID_EXO_ENLEVER_UN_ELT_DEBUT	                                25
#define	ID_EXO_ENLEVER_UN_ELT_MILIEU	                            26
#define	ID_EXO_ENLEVER_UN_ELT_FIN          	                        27
#define	ID_EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT          	            28
#define	ID_EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU	                    29
#define	ID_EXO_ENLEVER_PLUSIEURS_ELTS_FIN      	                    20
#define	ID_EXO_ELTS_EN_ORDRE_DECROISSANT	                        31
#define	ID_EXO_CONTIENT_ELT                    	                    32
#define	ID_EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT               33
#define	ID_EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT   34
#define	ID_EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT   35
#define	ID_EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT                36
#define	ID_EXO_OP_ADDITION	                                        37
#define	ID_EXO_OP_SOUSTRACTION	                                    38

#define	ID_EXO_CREER_ITERATEUR	                                    41
#define	ID_EXO_OP_PLUS_PLUS_ITERATEUR                               42
#define	ID_EXO_OP_MOINS_MOINS_ITERATEUR                             43
#define	ID_EXO_OP_ETOILE_ITERATEUR                                  44
#define	ID_EXO_OP_ETOILE_CONST_ITERATEUR                            45
#define	ID_EXO_OP_FIN_ITERATEUR                                     46

struct EvaluationExercice
{
    double coeff;
    int idExo;
    bool montrerCommentaires;
};

class ResultatEvaluation
{
public:
    ResultatEvaluation():note{0.0},coeff{0.0},correct{true},manquante{false},
                            enteteIncorrect{true},idExo{-1},ptrInstructCrash{nullptr},nomMethodeExo{0x00}{}
    ResultatEvaluation(double c):note{0.0},coeff{c},correct{true},manquante{false},
                            enteteIncorrect{true},idExo{-1},ptrInstructCrash{nullptr},nomMethodeExo{0x00}{}
    double note,coeff;          // Pour la note et le coeff
    bool correct,manquante,enteteIncorrect;     // Pour l'etat de l'evaluation
    int idExo;
    void *ptrInstructCrash;      // Pointeur sur l'instruction qui crash
    char nomMethodeExo[255];
};


/////////////////////////////////////////////////
// Classes to evaluate the dataStructEtud work //
/////////////////////////////////////////////////
template <class StructDeDonneesCorr, class StructDeDonneesEtud>
class Evaluer {

    friend void __stack_chk_fail();
    friend StructDeDonneesCorr;
    friend StructDeDonneesEtud;

    class JeuDeDonnees {
      static const int NB_MAX_JEUX_DE_DONNEES=4;
      public:
        JeuDeDonnees(int type)
        {
            this->type=type;
            inclureMemePntr=false;
            nbThis=0;
            nbParam=0;
            memset(dataStructThis,0x00,sizeof(dataStructThis));
            memset(dataStructParam,0x00,sizeof(dataStructParam));
            memset(tabParam,0x00,sizeof(tabParam));
        }
        void inclureThisEgalThis()
        {
            inclureMemePntr=true;
        }

        void definirThis(vector<vector<double>> list1)
        {
            if (type==TABLEAU_1D)
            {
                // Si la structure de donnée est 1D, on supprime les lignes en trop
                if (list1.size()>1)
                    list1.erase(list1.begin()+1, list1.end());
            }

            listeThis[0]=list1;
            nbThis=1;
        }
        void definirThis(vector<vector<double>> list1,
                         vector<vector<double>> list2)
        {
            if (type==TABLEAU_1D)
            {
                // Si la structure de donnée est 1D, on supprime les lignes en trop
                if (list1.size()>1)
                    list1.erase(list1.begin()+1, list1.end());
                if (list2.size()>1)
                    list2.erase(list2.begin()+1, list2.end());
            }
            listeThis[0]=list1;
            listeThis[1]=list2;
            nbThis=2;
        }
        void definirThis(vector<vector<double>> list1,
                         vector<vector<double>> list2,
                         vector<vector<double>> list3)
        {
            if (type==TABLEAU_1D)
            {
                // Si la structure de donnée est 1D, on supprime les lignes en trop
                if (list1.size()>1)
                    list1.erase(list1.begin()+1, list1.end());
                if (list2.size()>1)
                    list2.erase(list2.begin()+1, list2.end());
                if (list3.size()>1)
                    list3.erase(list3.begin()+1, list3.end());
            }
            listeThis[0]=list1;
            listeThis[1]=list2;
            listeThis[2]=list3;
            nbThis=3;
        }
        // Au maximum NB_MAX_JEUX_DE_DONNEES jeux de donnees
        void definirThis(vector<vector<double>> list1,
                         vector<vector<double>> list2,
                         vector<vector<double>> list3,
                         vector<vector<double>> list4)
        {
            if (type==TABLEAU_1D)
            {
                // Si la structure de donnée est 1D, on supprime les lignes en trop
                if (list1.size()>1)
                    list1.erase(list1.begin()+1, list1.end());
                if (list2.size()>1)
                    list2.erase(list2.begin()+1, list2.end());
                if (list3.size()>1)
                    list3.erase(list3.begin()+1, list3.end());
                if (list4.size()>1)
                    list4.erase(list4.begin()+1, list4.end());
            }
            listeThis[0]=list1;
            listeThis[1]=list2;
            listeThis[2]=list3;
            listeThis[3]=list4;
            nbThis=4;
        }
        void definirParam(vector<vector<double>> list1)
        {
            if (type==TABLEAU_1D)
            {
                // Si la structure de donnée est 1D, on supprime les lignes en trop
                if (list1.size()>1)
                    list1.erase(list1.begin()+1, list1.end());
            }
            listeParam[0]=list1;
            nbParam=1;
        }
        void definirParam(vector<vector<double>> list1,
                          vector<vector<double>> list2)
        {
            if (type==TABLEAU_1D)
            {
                // Si la structure de donnée est 1D, on supprime les lignes en trop
                if (list1.size()>1)
                    list1.erase(list1.begin()+1, list1.end());
                if (list2.size()>1)
                    list2.erase(list2.begin()+1, list2.end());
            }
            listeParam[0]=list1;
            listeParam[1]=list2;
            nbParam=2;
        }
        void definirParam(vector<vector<double>> list1,
                          vector<vector<double>> list2,
                          vector<vector<double>> list3)
        {
            if (type==TABLEAU_1D)
            {
                // Si la structure de donnée est 1D, on supprime les lignes en trop
                if (list1.size()>1)
                    list1.erase(list1.begin()+1, list1.end());
                if (list2.size()>1)
                    list2.erase(list2.begin()+1, list2.end());
                if (list3.size()>1)
                    list3.erase(list3.begin()+1, list3.end());
            }
            listeParam[0]=list1;
            listeParam[1]=list2;
            listeParam[2]=list3;
            nbParam=3;
        }
        // Au maximum 4 jeux de donnees
        void definirParam(vector<vector<double>> list1,
                          vector<vector<double>> list2,
                          vector<vector<double>> list3,
                          vector<vector<double>> list4)
        {
            if (type==TABLEAU_1D)
            {
                // Si la structure de donnée est 1D, on supprime les lignes en trop
                if (list1.size()>1)
                    list1.erase(list1.begin()+1, list1.end());
                if (list2.size()>1)
                    list2.erase(list2.begin()+1, list2.end());
                if (list3.size()>1)
                    list3.erase(list3.begin()+1, list3.end());
                if (list4.size()>1)
                    list4.erase(list4.begin()+1, list4.end());
            }
            listeParam[0]=list1;
            listeParam[1]=list2;
            listeParam[2]=list3;
            listeParam[3]=list4;
            nbParam=4;
        }
        void definirIdxLigneColonneDansThis(vector<vector<int>> listIdx)
        {
            listeIdxLigneColonneDansThis=listIdx;
        }
        ~JeuDeDonnees()
        {
            for (int c=0;c<NB_MAX_JEUX_DE_DONNEES;c++)
            {
                delete dataStructThis[c];
                delete dataStructParam[c];
                delete tabParam[c];
            }
        }
        template <class StructDeDonnees>
        const StructDeDonnees* getThis(int jeuID) const
        {
            int idThis, idParam, idIdx;
            bool memePntr;
            calculIndexInternes(jeuID, idThis, idParam, idIdx, memePntr);
            if(idThis<0 || idThis>=nbThis)
                assert(0);
            return ((StructDeDonnees*)(*dataStructThis[idThis]));
        }
        template <class StructDeDonnees>
        const StructDeDonnees* getParam(int jeuID) const
        {
            int idThis, idParam, idIdx;
            bool memePntr;
            calculIndexInternes(jeuID, idThis, idParam, idIdx, memePntr);
            if (memePntr)
            {
                if(idThis<0 || idThis>=nbThis)
                    assert(0);
                return ((StructDeDonnees*)(*dataStructThis[idThis]));
            }
            if(idParam<0 || idParam>=nbParam)
                assert(0);
            return ((StructDeDonnees*)(*dataStructParam[idParam]));
        }
        StructDeDonneesVerifCorr& getParamTab(int jeuID)
        {
            int idThis, idParam, idIdx;
            bool memePntr;
            calculIndexInternes(jeuID, idThis, idParam, idIdx, memePntr);
            assert(memePntr==false);
            assert(idParam>=0 && idParam<nbParam);
            return *tabParam[idParam];
        }
        int getIdxLigneDansThis(int jeuID) const
        {
            int idThis, idParam, idIdx;
            bool memePntr;
            calculIndexInternes(jeuID, idThis, idParam, idIdx, memePntr);
            if (idIdx<0||idIdx>=(int)listeIdxLigneColonneDansThis.size())
                assert(0);
            // On doit s'assurer que l'index retourne est dans l'interval [0,this.size()]
            int idxLigneDansThis=idxLigneColonneDansThis[idIdx][0];
            if (listeThis[idThis].size()==0)
            {
                if (idxLigneDansThis>0)
                    idxLigneDansThis=0;
                return idxLigneDansThis;
            }
            if (idxLigneDansThis>(int)(*listeThis[idThis].begin()).size())
                idxLigneDansThis=(int)(*listeThis[idThis].begin()).size();
            return idxLigneDansThis;
        }

        int getIdxColonneDansThis(int jeuID) const
        {
            int idThis, idParam, idIdx;
            bool memePntr;
            calculIndexInternes(jeuID, idThis, idParam, idIdx, memePntr);
            if (idIdx<0||idIdx>=(int)listeIdxLigneColonneDansThis.size())
                assert(0);
            // On doit s'assurer que l'index retourne est dans l'interval [0,this.size()]
            int idxColonneDansThis=idxLigneColonneDansThis[idIdx][1];
            if (listeThis[idThis].size()==0)
            {
                if (idxColonneDansThis>0)
                    idxColonneDansThis=0;
                return idxColonneDansThis;
            }
            if (idxColonneDansThis>(int)(*listeThis[idThis].begin()).size())
                idxColonneDansThis=(int)(*listeThis[idThis].begin()).size();
            return idxColonneDansThis;
        }
        bool thisEgalThis(int jeuID)
        {
            int idThis, idParam, idIdx;
            bool memePntr;
            calculIndexInternes(jeuID, idThis, idParam, idIdx, memePntr);
            return memePntr;
        }
        int taille() const
        {
            if (nbParam==0 && listeIdxLigneColonneDansThis.size()==0)
                return nbThis;
            if (nbParam==0 && listeIdxLigneColonneDansThis.size()!=0)
                return nbThis*listeIdxLigneColonneDansThis.size();
            int nbCombThisParam=nbThis*nbParam;
            if (inclureMemePntr)
                nbCombThisParam++;
            if (listeIdxLigneColonneDansThis.size()>0)
                return nbCombThisParam*listeIdxLigneColonneDansThis.size();
            return nbCombThisParam;
        }

        // Genere le jeu de donnees
        void generer()
        {
            // Ne doit etre appele qu'une fois
            assert(dataStructThis[0]==nullptr);
            if (inclureMemePntr && nbParam==0)
            {
                // Ca ne devrait jamais arriver
                assert(0);
                inclureMemePntr=false;
            }
            // Au minimum une donnee
            if (nbThis==0)
                nbThis=1;

            for (int i=0;i<nbThis;i++)
            {
                assert(listeThis[i].size()<=NB_MAX_JEUX_DE_DONNEES);
                dataStructThis[i]=new StructDeDonneesCorr(listeThis[i]);
            }
            for (int i=0;i<nbParam;i++)
            {
                assert(listeParam[i].size()<=NB_MAX_JEUX_DE_DONNEES);
                dataStructParam[i]=new StructDeDonneesCorr(StructDeDonneesVerifCorr(listeParam[i]));
                tabParam[i]=new StructDeDonneesVerifCorr(listeParam[i]);
            }
            assert(listeIdxLigneColonneDansThis.size()<=NB_MAX_JEUX_DE_DONNEES);
            int i=0;
            for (auto& each : listeIdxLigneColonneDansThis)
            {
                assert(each.size()==2);
                idxLigneColonneDansThis[i][0]=*(each.begin());
                idxLigneColonneDansThis[i][1]=*(each.begin()+1);
                i++;
            }
        }
    private:
        // Calcul des index pour param, this et idxLigneColonneDansThis a partir de l'index global jeuID
        void calculIndexInternes(int jeuID, int &idThis, int &idParam, int &idIdx, bool &memePntr) const
        {
            memePntr=false;
            if (nbParam==0 && listeIdxLigneColonneDansThis.size()==0)
            {
                idParam=-1;
                idIdx=-1;
                idThis=jeuID;
                return;
            }
            if (nbParam==0 && listeIdxLigneColonneDansThis.size()!=0)
            {
                idIdx=jeuID%listeIdxLigneColonneDansThis.size();
                idThis=(jeuID-idIdx)/listeIdxLigneColonneDansThis.size();
                return;
            }
            int restDiv;
            int nbCombThisParam=nbThis*nbParam;
            if (inclureMemePntr)
                nbCombThisParam++;
            // Calcul de idIdx
            if (listeIdxLigneColonneDansThis.size()==0)
                idIdx=-1;
            else
            {
                // Calcul du reste de la division
                restDiv=jeuID%nbCombThisParam;
                // Calcul de la division entiere
                idIdx=(jeuID-restDiv)/nbCombThisParam;
                jeuID=restDiv;
            }
            // Calcul de idParam
            if (inclureMemePntr)
            {
                if (jeuID==nbCombThisParam-1)
                {
                    memePntr=true;
                    idThis=nbThis-1;
                    idParam=-1;
                    return;
                }
            }
            idParam=jeuID%nbParam;
            idThis=(jeuID-idParam)/nbParam;
        }

      private:
        StructDeDonneesCorr *dataStructThis[NB_MAX_JEUX_DE_DONNEES];
        StructDeDonneesCorr *dataStructParam[NB_MAX_JEUX_DE_DONNEES];
        StructDeDonneesVerifCorr *tabParam[NB_MAX_JEUX_DE_DONNEES];
        // Tableau a 2 dimensions maximum
        Int idxLigneColonneDansThis[NB_MAX_JEUX_DE_DONNEES][2];
        int nbThis,nbParam;
        bool inclureMemePntr;

        vector<vector<double>> listeThis[NB_MAX_JEUX_DE_DONNEES];
        vector<vector<double>> listeParam[NB_MAX_JEUX_DE_DONNEES];
        // Les index pour suppression a une position specifique, etc
        vector<vector<int>> listeIdxLigneColonneDansThis;

    public:
        int type;
        const static int TABLEAU_1D=1;
        const static int TABLEAU_2D=2;
    };

    class ProcessJeuDeDonnees {
      public:
        ProcessJeuDeDonnees():pid{-1},sharedMsgErrPntr{nullptr},
                              sharedMsgErrAlgo{nullptr},
                              resultEval{nullptr},startTime{0}{}
      public:
        int idExo;
        pid_t pid;
        char *sharedMsgErrPntr;
        char *sharedMsgErrAlgo;
        ResultatEvaluation *resultEval;
        clock_t startTime;
    };

  public:
    Evaluer(int nbProcesses=-1):nbMaxProcesses{nbProcesses}{}
    ~Evaluer(){}
    double doEvaluate(double coeff, int nbMaxProcesses);

  private:
    template <class StructDeDonnees>
    void  evalUnjeuDeDonnees(int idExo,
                             JeuDeDonnees &jeuDeDonnees,
                             StructDeDonnees* &pntrRes,
                             const StructDeDonnees* &pntrThis,const StructDeDonnees* &pntrParam,
                             StructDeDonneesVerifCorr* &pntrTab,
                             Int &paramNb, Int &paramIdxColonne, Int &paramIdxLigne,
                             bool* &returnBoolPntr,bool &returnBool,
                             int* &returnIntPntr,int &returnInt,
                             double* &returnDoublePntr,double &returnDouble,
                             StructDeDonnees* &returnThis,
                             typename StructDeDonnees::ITERATEUR** it=nullptr);

    // Toutes les methodes d'evaluation doivent etre statiques parce qu'on les utilise a travers des pointeurs
    bool verifAllocationPntr(GestionPntrEtComplexite::TabAppelsInstructions &tabInstructionsCorr,
                             GestionPntrEtComplexite::TabAppelsInstructions &tabInstructionsEtud);
    bool verifComplexiteTemps(GestionPntrEtComplexite::TabAppelsInstructions &tabInstructionsCorr,
                              GestionPntrEtComplexite::TabAppelsInstructions &tabInstructionsEtud);

    ResultatEvaluation evalUnExercice(double coeff,int idExo);
    int termineProcessJeuDeDonnees(ResultatEvaluation &resultEvalExercice,
                                   ProcessJeuDeDonnees *processJeuDeDonnees,
                                   JeuDeDonnees &jeuDeDonnees);


    static void sighandler(int sig, siginfo_t* info, void *secret);

    // Les infos concernant l'exercice et le jeu de donnees actuellement evalue
    static int idExo;
    static int jeuID;

    // Toutes ces variables doivent être statiques parce qu'elles sont utilisées dans sighandler
    // Message pour les erreurs de gestion de la mémoire et boucle infinie pour un jeu de donnees
    static char* msgErrPntrJeuDeDonnees;
public:
    // Message pour les erreurs des algos pour un jeu de donnees
    static char* msgErrAlgoJeuDeDonnees;

private:
    // Message pour les erreurs de gestion de la mémoire et boucle infinie pour un exercice
    ostringstream msgErrPntrExercice;
    // Message pour les erreurs des algo pour un exercice
    ostringstream msgErrAlgoExercice;
    // Message precedent pour le jeu de donnees
    // C'est pour eviter d'afficher plusieurs fois le meme message pour un exercice
    string msgErrPrecJeuDeDonnees;

    // Nombre maximal de process defini par Caseine
    int nbMaxProcesses;

  public:
      static ResultatEvaluation *resultEvalJeuDeDonnees;

};



// On met la fonction en weak pour pouvoir compiler sans le code source evaluate.cpp
double doEvaluate(double coeff, int nbMaxProcesses) __attribute__((weak));

#endif // EVALUATE_H_INCLUDED
