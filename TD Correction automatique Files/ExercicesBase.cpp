#include "ExercicesBase.h"
#include "StructDeDonneesVerifCorr.h"
#include "Evaluate.h"


Double dummyVal;
STRUCT_DE_DONNEES_CORR dummyStructDeDonnees;

StructDeDonneesVerifCorr operator+(const StructDeDonneesVerifCorr& a, const StructDeDonneesVerifCorr& b)
{
    assert(a.nbLignes==b.nbLignes);
    assert(a.nbColonnes==b.nbColonnes);
    StructDeDonneesVerifCorr res(a);
    for (int i=0;i<res.nbLignes;i++)
        for (int j=0;j<res.nbColonnes;j++)
            res.array2D[i][j]+=b.array2D[i][j];
    return res;
}

StructDeDonneesVerifCorr operator-(const StructDeDonneesVerifCorr& a, const StructDeDonneesVerifCorr& b)
{
    assert(a.nbLignes==b.nbLignes);
    assert(a.nbColonnes==b.nbColonnes);
    StructDeDonneesVerifCorr res(a);
    for (int i=0;i<res.nbLignes;i++)
        for (int j=0;j<res.nbColonnes;j++)
            res.array2D[i][j]-=b.array2D[i][j];
    return res;
}

bool operator==(const STRUCT_DE_DONNEES_CORR& a, const StructDeDonneesVerifCorr& b)
{
    return a.getStructDeDonneesVerifCorr()==b;
}

bool operator==(const StructDeDonneesVerifCorr& a, const STRUCT_DE_DONNEES_CORR& b)
{
    return b.getStructDeDonneesVerifCorr()==a;
}

bool operator!=(const STRUCT_DE_DONNEES_CORR& a, const StructDeDonneesVerifCorr& b)
{
    return a.getStructDeDonneesVerifCorr()!=b;
}

bool operator!=(const StructDeDonneesVerifCorr& a, const STRUCT_DE_DONNEES_CORR& b)
{
    return b.getStructDeDonneesVerifCorr()!=a;
}

STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT::STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT()
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT::STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT(const STRUCT_DE_DONNEES_CORR &param)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT::STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT(Double *tab, Int nb)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT::STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT(Double **tab, Int nl, Int nc)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT::~STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT()
{
    //cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

STRUCT_DE_DONNEES_CORR::operator StructDeDonneesVerifCorr*()
{
    // Cree un pointeur qui devra etre supprime plus tard
    StructDeDonneesVerifCorr* ptr=new StructDeDonneesVerifCorr(getStructDeDonneesVerifCorr());
    return ptr;
};

STRUCT_DE_DONNEES_CORR::STRUCT_DE_DONNEES_CORR(const StructDeDonneesVerifCorr& param)
{
    memset((void*)this,0x00,sizeof(STRUCT_DE_DONNEES_CORR));
    setStructDeDonneesVerifCorr(param);
}

STRUCT_DE_DONNEES_CORR::STRUCT_DE_DONNEES_CORR()
{
    new(this) STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT();
}

STRUCT_DE_DONNEES_CORR::STRUCT_DE_DONNEES_CORR(const STRUCT_DE_DONNEES_CORR &param)
{
    new(this) STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT(param);
}

STRUCT_DE_DONNEES_CORR::STRUCT_DE_DONNEES_CORR(Double *tab, Int nb)
{
    new(this) STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT(tab,nb);
}

STRUCT_DE_DONNEES_CORR::STRUCT_DE_DONNEES_CORR(Double **tab, Int nl, Int nc)
{
    new(this) STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT(tab,nl,nc);
}

STRUCT_DE_DONNEES_CORR::~STRUCT_DE_DONNEES_CORR()
{
    (*((STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT*)this)).~STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT(); // destruct
}

void STRUCT_DE_DONNEES_CORR::concatenationDebut(const STRUCT_DE_DONNEES_CORR &param)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).concatenationDebut(param);
}

void STRUCT_DE_DONNEES_CORR_OTHER::concatenationDebut(const STRUCT_DE_DONNEES_CORR &param)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

void STRUCT_DE_DONNEES_CORR::concatenationMilieu(Int idx, const STRUCT_DE_DONNEES_CORR &param)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).concatenationMilieu(idx,param);
}

void STRUCT_DE_DONNEES_CORR_OTHER::concatenationMilieu(Int idx, const STRUCT_DE_DONNEES_CORR &param)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

void STRUCT_DE_DONNEES_CORR::concatenationFin(const STRUCT_DE_DONNEES_CORR &param)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).concatenationFin(param);
}

void STRUCT_DE_DONNEES_CORR_OTHER::concatenationFin(const STRUCT_DE_DONNEES_CORR &param)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

STRUCT_DE_DONNEES_CORR &STRUCT_DE_DONNEES_CORR::operator+=(const STRUCT_DE_DONNEES_CORR &param)
{
    STRUCT_DE_DONNEES_CORR *returnThis=this;
    STRUCT_DE_DONNEES_CORR* returnVal=&((*((STRUCT_DE_DONNEES_CORR_OTHER*)this))+=param);
    if (returnVal!=this)
        returnThis=nullptr;
    return *returnThis;
}

STRUCT_DE_DONNEES_CORR &STRUCT_DE_DONNEES_CORR_OTHER::operator+=(const STRUCT_DE_DONNEES_CORR &)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return *this;
}

bool STRUCT_DE_DONNEES_CORR::operator==(const STRUCT_DE_DONNEES_CORR &param) const
{
    return ((*((const STRUCT_DE_DONNEES_CORR_OTHER*)this))==param);
}

bool STRUCT_DE_DONNEES_CORR_OTHER::operator==(const STRUCT_DE_DONNEES_CORR &) const
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return false;
}

bool STRUCT_DE_DONNEES_CORR::operator!=(const STRUCT_DE_DONNEES_CORR &param) const
{
    return ((*((const STRUCT_DE_DONNEES_CORR_OTHER*)this))!=param);
}

bool STRUCT_DE_DONNEES_CORR_OTHER::operator!=(const STRUCT_DE_DONNEES_CORR &) const
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return false;
}

STRUCT_DE_DONNEES_CORR &STRUCT_DE_DONNEES_CORR::operator=(const STRUCT_DE_DONNEES_CORR &param)
{
    STRUCT_DE_DONNEES_CORR *returnThis=this;
    STRUCT_DE_DONNEES_CORR *returnVal=&((*((STRUCT_DE_DONNEES_CORR_OTHER*)this))=param);
    // On verfie si l'etudiant return bien "this".
    // Si ce n'est pas le cas, on transmet l'erreur plus haut en returnant nullptr
    if (returnVal != this)
        returnThis=nullptr;
    return *returnThis;
}

STRUCT_DE_DONNEES_CORR &STRUCT_DE_DONNEES_CORR_OTHER::operator=(const STRUCT_DE_DONNEES_CORR &)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return *this;
}

Double &STRUCT_DE_DONNEES_CORR::operator()(Int i)
{
    return (*((STRUCT_DE_DONNEES_CORR_OTHER*)this))(i);
}

Double &STRUCT_DE_DONNEES_CORR_OTHER::operator()(Int i)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return dummyVal;
}

Double &STRUCT_DE_DONNEES_CORR::operator()(Int i, Int j)
{
    return (*((STRUCT_DE_DONNEES_CORR_OTHER*)this))(i,j);
}

Double &STRUCT_DE_DONNEES_CORR_OTHER::operator()(Int i, Int j)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return dummyVal;
}

Double STRUCT_DE_DONNEES_CORR::operator()(Int i) const
{
    return (*((STRUCT_DE_DONNEES_CORR_OTHER*)this))(i);
}

Double STRUCT_DE_DONNEES_CORR_OTHER::operator()(Int i) const
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return dummyVal;
}

Double STRUCT_DE_DONNEES_CORR::operator()(Int i, Int j) const
{
    return (*((STRUCT_DE_DONNEES_CORR_OTHER*)this))(i,j);
}

Double STRUCT_DE_DONNEES_CORR_OTHER::operator()(Int i, Int j) const
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return dummyVal;
}

Double &STRUCT_DE_DONNEES_CORR::operator[](Int i)
{
    return (*((STRUCT_DE_DONNEES_CORR_OTHER*)this))[i];
}

Double &STRUCT_DE_DONNEES_CORR_OTHER::operator[](Int i)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return dummyVal;
}

Double STRUCT_DE_DONNEES_CORR::operator[](Int i) const
{
    return (*((STRUCT_DE_DONNEES_CORR_OTHER*)this))[i];
}

Double STRUCT_DE_DONNEES_CORR_OTHER::operator[](Int i) const
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return dummyVal;
}

void STRUCT_DE_DONNEES_CORR::set(Int i, Double val)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).set(i,val);
}

void STRUCT_DE_DONNEES_CORR_OTHER::set(Int i, Double val)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

void STRUCT_DE_DONNEES_CORR::set(Int i, Int j, Double val)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).set(i,j,val);
}

void STRUCT_DE_DONNEES_CORR_OTHER::set(Int i, Int j, Double val)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

Double STRUCT_DE_DONNEES_CORR::get(Int i) const
{
    return (*((const STRUCT_DE_DONNEES_CORR_OTHER*)this)).get(i);
}

Double STRUCT_DE_DONNEES_CORR_OTHER::get(Int i) const
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return dummyVal;
}

Double STRUCT_DE_DONNEES_CORR::get(Int i, Int j) const
{
    return (*((const STRUCT_DE_DONNEES_CORR_OTHER*)this)).get(i,j);
}

Double STRUCT_DE_DONNEES_CORR_OTHER::get(Int i, Int j) const
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return dummyVal;
}

void STRUCT_DE_DONNEES_CORR::additionnerATousLesElts(Double val)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).additionnerATousLesElts(val);
}

void STRUCT_DE_DONNEES_CORR_OTHER::additionnerATousLesElts(Double val)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

void STRUCT_DE_DONNEES_CORR::insererUnEltDebut(Double val)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererUnEltDebut(val);
}

void STRUCT_DE_DONNEES_CORR_OTHER::insererUnEltDebut(Double val)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

void STRUCT_DE_DONNEES_CORR::insererUnEltDebut(Double *val, Int nb)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererUnEltDebut(val,nb);
}

void STRUCT_DE_DONNEES_CORR_OTHER::insererUnEltDebut(Double *val, Int nb)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

void STRUCT_DE_DONNEES_CORR::insererUnEltMilieu(Int idx, Double val)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererUnEltMilieu(idx,val);
}

void STRUCT_DE_DONNEES_CORR_OTHER::insererUnEltMilieu(Int idx, Double val)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

void STRUCT_DE_DONNEES_CORR::insererUnEltMilieu(Int idx, Double *val, Int nb)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererUnEltMilieu(idx,val,nb);
}

void STRUCT_DE_DONNEES_CORR_OTHER::insererUnEltMilieu(Int idx, Double *val, Int nb)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

void STRUCT_DE_DONNEES_CORR::insererUnEltFin(Double val)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererUnEltFin(val);
}

void STRUCT_DE_DONNEES_CORR_OTHER::insererUnEltFin(Double val)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

void STRUCT_DE_DONNEES_CORR::insererUnEltFin(Double *val, Int nb)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererUnEltFin(val,nb);
}

void STRUCT_DE_DONNEES_CORR_OTHER::insererUnEltFin(Double *val, Int nb)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

void STRUCT_DE_DONNEES_CORR::enleverUnEltDebut()
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).enleverUnEltDebut();
}

void STRUCT_DE_DONNEES_CORR_OTHER::enleverUnEltDebut()
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

void STRUCT_DE_DONNEES_CORR::enleverUnEltMilieu(Int idx)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).enleverUnEltMilieu(idx);
}

void STRUCT_DE_DONNEES_CORR_OTHER::enleverUnEltMilieu(Int idx)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

void STRUCT_DE_DONNEES_CORR::enleverUnEltFin()
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).enleverUnEltFin();
}

void STRUCT_DE_DONNEES_CORR_OTHER::enleverUnEltFin()
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

void STRUCT_DE_DONNEES_CORR::insererPlusieursEltsDebut(Double *tab, Int nb)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererPlusieursEltsDebut(tab,nb);
}

void STRUCT_DE_DONNEES_CORR_OTHER::insererPlusieursEltsDebut(Double *tab, Int nb)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

void STRUCT_DE_DONNEES_CORR::insererPlusieursEltsDebut(Double **tab, Int nbl, Int nbc)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererPlusieursEltsDebut(tab,nbl,nbc);
}

void STRUCT_DE_DONNEES_CORR_OTHER::insererPlusieursEltsDebut(Double **tab, Int nbl, Int nbc)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

void STRUCT_DE_DONNEES_CORR::insererPlusieursEltsMilieu(Int idx, Double *tab, Int nb)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererPlusieursEltsMilieu(idx,tab,nb);
}

void STRUCT_DE_DONNEES_CORR_OTHER::insererPlusieursEltsMilieu(Int idx, Double *tab, Int nb)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

void STRUCT_DE_DONNEES_CORR::insererPlusieursEltsMilieu(Int idx, Double **tab, Int nbl, Int nbc)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererPlusieursEltsMilieu(idx,tab,nbl,nbc);
}

void STRUCT_DE_DONNEES_CORR_OTHER::insererPlusieursEltsMilieu(Int idx, Double **tab, Int nbl, Int nbc)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

void STRUCT_DE_DONNEES_CORR::insererPlusieursEltsFin(Double *tab, Int nb)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererPlusieursEltsFin(tab,nb);
}

void STRUCT_DE_DONNEES_CORR_OTHER::insererPlusieursEltsFin(Double *tab, Int nb)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

void STRUCT_DE_DONNEES_CORR::insererPlusieursEltsFin(Double **tab, Int nbl, Int nbc)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererPlusieursEltsFin(tab,nbl,nbc);
}

void STRUCT_DE_DONNEES_CORR_OTHER::insererPlusieursEltsFin(Double **tab, Int nbl, Int nbc)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

void STRUCT_DE_DONNEES_CORR::enleverPlusieursEltsDebut(Int nb)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).enleverPlusieursEltsDebut(nb);
}

void STRUCT_DE_DONNEES_CORR_OTHER::enleverPlusieursEltsDebut(Int nb)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

void STRUCT_DE_DONNEES_CORR::enleverPlusieursEltsMilieu(Int idx, Int nb)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).enleverPlusieursEltsMilieu(idx,nb);
}

void STRUCT_DE_DONNEES_CORR_OTHER::enleverPlusieursEltsMilieu(Int idx, Int nb)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

void STRUCT_DE_DONNEES_CORR::enleverPlusieursEltsFin(Int nb)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).enleverPlusieursEltsFin(nb);
}

void STRUCT_DE_DONNEES_CORR_OTHER::enleverPlusieursEltsFin(Int nb)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

void STRUCT_DE_DONNEES_CORR::enleverToutesLesOccurencesDUnElt(Double val)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).enleverToutesLesOccurencesDUnElt(val);
}

void STRUCT_DE_DONNEES_CORR_OTHER::enleverToutesLesOccurencesDUnElt(Double val)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

bool STRUCT_DE_DONNEES_CORR::eltsEnOrdreDecroissant() const
{
    return (*((const STRUCT_DE_DONNEES_CORR_OTHER*)this)).eltsEnOrdreDecroissant();
}

bool STRUCT_DE_DONNEES_CORR_OTHER::eltsEnOrdreDecroissant() const
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return false;
}

bool STRUCT_DE_DONNEES_CORR::contientElt(Double val) const
{
    return (*((const STRUCT_DE_DONNEES_CORR_OTHER*)this)).contientElt(val);
}

bool STRUCT_DE_DONNEES_CORR_OTHER::contientElt(Double val) const
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return false;
}

Int STRUCT_DE_DONNEES_CORR::chercherLIndexDeLaPremiereOccurenceDUnElt(Double val) const
{
    return (*((const STRUCT_DE_DONNEES_CORR_OTHER*)this)).chercherLIndexDeLaPremiereOccurenceDUnElt(val);
}

Int STRUCT_DE_DONNEES_CORR_OTHER::chercherLIndexDeLaPremiereOccurenceDUnElt(Double val) const
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return Int();
}

Int STRUCT_DE_DONNEES_CORR::chercherLIndexDeLaDerniereOccurenceDUnElt(Double val) const
{
    return (*((const STRUCT_DE_DONNEES_CORR_OTHER*)this)).chercherLIndexDeLaDerniereOccurenceDUnElt(val);
}

Int STRUCT_DE_DONNEES_CORR_OTHER::chercherLIndexDeLaDerniereOccurenceDUnElt(Double val) const
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return Int();
}

Int STRUCT_DE_DONNEES_CORR::calculerNombreDOccurencesDUnElt(Double val) const
{
    return (*((const STRUCT_DE_DONNEES_CORR_OTHER*)this)).calculerNombreDOccurencesDUnElt(val);
}

Int STRUCT_DE_DONNEES_CORR_OTHER::calculerNombreDOccurencesDUnElt(Double val) const
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return Int();
}

STRUCT_DE_DONNEES_CORR operator+(const STRUCT_DE_DONNEES_CORR& a, const STRUCT_DE_DONNEES_CORR& b)
{
    // Utilisation du "placement new" pour que STRUCT_DE_DONNEES_CORR operator+()
    // et STRUCT_DE_DONNEES_ETUD operator+() se comportent de la meme facon (c'est-a-dire,
    // meme nombre d'instructions)
    // L'operateur "placement new" permet d'appeler le constructeur STRUCT_DE_DONNEES_CORR
    // sur un objet de la classe STRUCT_DE_DONNEES_ETUD
    STRUCT_DE_DONNEES_CORR res;
    new(&res) STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT(operator_plus(a,b));
    return res;
}

STRUCT_DE_DONNEES_CORR operator_plus(const STRUCT_DE_DONNEES_CORR& a, const STRUCT_DE_DONNEES_CORR& b)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return dummyStructDeDonnees;
}

STRUCT_DE_DONNEES_CORR operator-(const STRUCT_DE_DONNEES_CORR& a, const STRUCT_DE_DONNEES_CORR& b)
{
    // Utilisation du "placement new" pour que STRUCT_DE_DONNEES_CORR operator+()
    // et STRUCT_DE_DONNEES_ETUD operator+() se comportent de la meme facon (c'est-a-dire,
    // meme nombre d'instructions)
    // L'operateur "placement new" permet d'appeler le constructeur STRUCT_DE_DONNEES_CORR
    // sur un objet de la classe STRUCT_DE_DONNEES_ETUD
    STRUCT_DE_DONNEES_CORR res;
    new(&res) STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT(operator_moins(a,b));
    return res;
}

STRUCT_DE_DONNEES_CORR operator_moins(const STRUCT_DE_DONNEES_CORR& a, const STRUCT_DE_DONNEES_CORR& b)
{
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return dummyStructDeDonnees;
}

bool STRUCT_DE_DONNEES_CORR::verifierIntegrite(const STRUCT_DE_DONNEES_ETUD &param)
{
    return STRUCT_DE_DONNEES_CORR_OTHER::verifierIntegrite(param);
}

void STRUCT_DE_DONNEES_CORR::setStructDeDonneesVerifCorr(const StructDeDonneesVerifCorr& param)
{
    (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).setStructDeDonneesVerifCorr(param);
}

StructDeDonneesVerifCorr STRUCT_DE_DONNEES_CORR::getStructDeDonneesVerifCorr() const
{
    return (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).getStructDeDonneesVerifCorr();
}

bool STRUCT_DE_DONNEES_CORR::compareCorrAvecEtud(const STRUCT_DE_DONNEES_ETUD &param) const
{
    return (*((const STRUCT_DE_DONNEES_CORR_OTHER*)this)).compareCorrAvecEtud(param);
}

#ifdef ITERATEUR
bool STRUCT_DE_DONNEES_CORR::compareCorrAvecEtud(const ITERATEUR& corrIt,
                                                 const STRUCT_DE_DONNEES_ETUD &etudStruct,
                                                 const ITERATEUR& etudIt) const
{
    return (*((const STRUCT_DE_DONNEES_CORR_OTHER*)this)).compareCorrAvecEtud(
                                        *(const STRUCT_DE_DONNEES_CORR_OTHER::ITERATEUR*)&corrIt,
                                                                etudStruct,
                                        *(const STRUCT_DE_DONNEES_CORR_OTHER::ITERATEUR*)&etudIt);
}
#endif

STRUCT_DE_DONNEES_ETUD::STRUCT_DE_DONNEES_ETUD()
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_CONSTRUCTEUR)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        new(this) STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT();
    }
    else
        new(this) STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT();
}

STRUCT_DE_DONNEES_ETUD::STRUCT_DE_DONNEES_ETUD(const STRUCT_DE_DONNEES_ETUD &param)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_CONSTRUCTEUR_PAR_RECOPIE)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        new(this) STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT(param);
    }
    else
        new(this) STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT(*((const STRUCT_DE_DONNEES_CORR*)&param));
}

STRUCT_DE_DONNEES_ETUD::STRUCT_DE_DONNEES_ETUD(Double *tab, Int nb)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_CONSTRUCTEUR_AVEC_TABLEAU)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        new(this) STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT(tab,nb);
    }
    else
        new(this) STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT(tab,nb);
}

STRUCT_DE_DONNEES_ETUD::STRUCT_DE_DONNEES_ETUD(Double **tab, Int nl, Int nc)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_CONSTRUCTEUR_AVEC_TABLEAU)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        new(this) STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT(tab,nl,nc);
    }
    else
        new(this) STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT(tab,nl,nc);
}

STRUCT_DE_DONNEES_ETUD::~STRUCT_DE_DONNEES_ETUD()
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_DESTRUCTEUR)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT*)this)).~STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT(); // destruct
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT*)this)).~STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT(); // destruct
}

void STRUCT_DE_DONNEES_ETUD::concatenationDebut(const STRUCT_DE_DONNEES_ETUD &param)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_CONCATENATION_DEBUT)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).concatenationDebut(param);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).concatenationDebut(*((const STRUCT_DE_DONNEES_CORR*)&param));
}

void STRUCT_DE_DONNEES_ETUD_OTHER::concatenationDebut(const STRUCT_DE_DONNEES_ETUD &param)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->enteteIncorrect=true;
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=false;
    concatenationDebut(*((STRUCT_DE_DONNEES_ETUD*)&param));
}

void STRUCT_DE_DONNEES_ETUD_OTHER::concatenationDebut(STRUCT_DE_DONNEES_ETUD &param)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

void STRUCT_DE_DONNEES_ETUD::concatenationMilieu(Int idx, const STRUCT_DE_DONNEES_ETUD &param)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_CONCATENATION_MILIEU)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).concatenationMilieu(idx,param);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).concatenationMilieu(idx,*((const STRUCT_DE_DONNEES_CORR*)&param));
}

void STRUCT_DE_DONNEES_ETUD_OTHER::concatenationMilieu(Int idx, const STRUCT_DE_DONNEES_ETUD &param)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->enteteIncorrect=true;
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=false;
    concatenationMilieu(idx,*((STRUCT_DE_DONNEES_ETUD*)&param));
}

void STRUCT_DE_DONNEES_ETUD_OTHER::concatenationMilieu(Int idx, STRUCT_DE_DONNEES_ETUD &param)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

void STRUCT_DE_DONNEES_ETUD::concatenationFin(const STRUCT_DE_DONNEES_ETUD &param)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_CONCATENATION_FIN)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).concatenationFin(param);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).concatenationFin(*((const STRUCT_DE_DONNEES_CORR*)&param));
}

void STRUCT_DE_DONNEES_ETUD_OTHER::concatenationFin(const STRUCT_DE_DONNEES_ETUD &param)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->enteteIncorrect=true;
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=false;
    concatenationFin(*((STRUCT_DE_DONNEES_ETUD*)&param));
}

void STRUCT_DE_DONNEES_ETUD_OTHER::concatenationFin(STRUCT_DE_DONNEES_ETUD &param)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

STRUCT_DE_DONNEES_ETUD &STRUCT_DE_DONNEES_ETUD::operator+=(const STRUCT_DE_DONNEES_ETUD &param)
{
    STRUCT_DE_DONNEES_ETUD* returnThis=this;
    STRUCT_DE_DONNEES_ETUD* returnVal=nullptr;
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_OP_CONCATENATION)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        returnVal=&((*((STRUCT_DE_DONNEES_ETUD_OTHER*)this))+=param);
    }
    else
        returnVal=(STRUCT_DE_DONNEES_ETUD*)(&((*((STRUCT_DE_DONNEES_CORR_OTHER*)this))+=*((const STRUCT_DE_DONNEES_CORR*)&param)));

    // On verfie si l'etudiant return bien "this".
    // Si ce n'est pas le cas, on transmet l'erreur plus haut en returnant nullptr
    if (returnVal!=this)
        returnThis=nullptr;
    return *returnThis;
}

STRUCT_DE_DONNEES_ETUD &STRUCT_DE_DONNEES_ETUD_OTHER::operator+=(const STRUCT_DE_DONNEES_ETUD &param)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->enteteIncorrect=true;
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=false;
    this->operator+=(*((STRUCT_DE_DONNEES_ETUD*)&param));
    return *this;
}

STRUCT_DE_DONNEES_ETUD &STRUCT_DE_DONNEES_ETUD_OTHER::operator+=(STRUCT_DE_DONNEES_ETUD &)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return *this;
}

bool STRUCT_DE_DONNEES_ETUD::operator==(const STRUCT_DE_DONNEES_ETUD &param) const
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_OP_EGAL)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        return ((*((const STRUCT_DE_DONNEES_ETUD_OTHER*)this))==param);
    }
    else
        return ((*((const STRUCT_DE_DONNEES_CORR_OTHER*)this))==*((const STRUCT_DE_DONNEES_CORR*)&param));
}

bool STRUCT_DE_DONNEES_ETUD_OTHER::operator==(const STRUCT_DE_DONNEES_ETUD &param) const
{
    bool res=false;

    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->enteteIncorrect=true;
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=false;
    res=((STRUCT_DE_DONNEES_ETUD_OTHER*)this)->operator==(*((const STRUCT_DE_DONNEES_ETUD*)&param));

    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante)
    {
        Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=false;
        res=((const STRUCT_DE_DONNEES_ETUD_OTHER*)this)->operator==(*((STRUCT_DE_DONNEES_ETUD*)&param));
    }
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante)
    {
        Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=false;
        res=((STRUCT_DE_DONNEES_ETUD_OTHER*)this)->operator==(*((STRUCT_DE_DONNEES_ETUD*)&param));
    }
    return res;
}

bool STRUCT_DE_DONNEES_ETUD_OTHER::operator==(const STRUCT_DE_DONNEES_ETUD &param)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return false;
}

bool STRUCT_DE_DONNEES_ETUD_OTHER::operator==(STRUCT_DE_DONNEES_ETUD &param) const
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return false;
}
bool STRUCT_DE_DONNEES_ETUD_OTHER::operator==(STRUCT_DE_DONNEES_ETUD &param)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return false;
}

bool STRUCT_DE_DONNEES_ETUD::operator!=(const STRUCT_DE_DONNEES_ETUD &param) const
{
    bool returnVal;
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_OP_DIFFERENT)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        returnVal=((*((const STRUCT_DE_DONNEES_ETUD_OTHER*)this))!=param);
    }
    else
        returnVal=((*((const STRUCT_DE_DONNEES_CORR_OTHER*)this))!=*((const STRUCT_DE_DONNEES_CORR*)&param));
    return returnVal;
}

bool STRUCT_DE_DONNEES_ETUD_OTHER::operator!=(const STRUCT_DE_DONNEES_ETUD &param) const
{
    bool res=false;

    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->enteteIncorrect=true;

    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=false;
    res=((STRUCT_DE_DONNEES_ETUD_OTHER*)this)->operator!=(*((const STRUCT_DE_DONNEES_ETUD*)&param));

    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante)
    {
        Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=false;
        res=((const STRUCT_DE_DONNEES_ETUD_OTHER*)this)->operator!=(*((STRUCT_DE_DONNEES_ETUD*)&param));
    }
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante)
    {
        Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=false;
        res=((STRUCT_DE_DONNEES_ETUD_OTHER*)this)->operator!=(*((STRUCT_DE_DONNEES_ETUD*)&param));
    }
    return res;
}

bool STRUCT_DE_DONNEES_ETUD_OTHER::operator!=(STRUCT_DE_DONNEES_ETUD &) const
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return false;
}

bool STRUCT_DE_DONNEES_ETUD_OTHER::operator!=(const STRUCT_DE_DONNEES_ETUD &)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return false;
}

bool STRUCT_DE_DONNEES_ETUD_OTHER::operator!=(STRUCT_DE_DONNEES_ETUD &)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return false;
}

STRUCT_DE_DONNEES_ETUD &STRUCT_DE_DONNEES_ETUD::operator=(const STRUCT_DE_DONNEES_ETUD &param)
{
    STRUCT_DE_DONNEES_ETUD* returnThis=this;
    STRUCT_DE_DONNEES_ETUD* returnVal;
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_OP_AFFECTATION)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        returnVal=&((*((STRUCT_DE_DONNEES_ETUD_OTHER*)this))=param);
    }
    else
        returnVal=(STRUCT_DE_DONNEES_ETUD*)&((*((STRUCT_DE_DONNEES_CORR_OTHER*)this))=*((const STRUCT_DE_DONNEES_CORR*)&param));
    // On verfie si l'etudiant return bien "this".
    // Si ce n'est pas le cas, on transmet l'erreur plus haut en returnant nullptr
    if (returnVal != this)
        returnThis=nullptr;
    return *returnThis;
}

STRUCT_DE_DONNEES_ETUD &STRUCT_DE_DONNEES_ETUD_OTHER::operator=(const STRUCT_DE_DONNEES_ETUD &param)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->enteteIncorrect=true;
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=false;
    this->operator=(*((STRUCT_DE_DONNEES_ETUD*)&param));
    return *this;
}

STRUCT_DE_DONNEES_ETUD &STRUCT_DE_DONNEES_ETUD_OTHER::operator=(STRUCT_DE_DONNEES_ETUD &)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return *this;
}

Double &STRUCT_DE_DONNEES_ETUD::operator()(Int i)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_OP_PARENTHESE)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        return (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this))(i);
    }
    else
        return (*((STRUCT_DE_DONNEES_CORR_OTHER*)this))(i);
}

Double &STRUCT_DE_DONNEES_ETUD_OTHER::operator()(Int i)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return dummyVal;
}

Double &STRUCT_DE_DONNEES_ETUD::operator()(Int i, Int j)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_OP_PARENTHESE)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        return ((*((STRUCT_DE_DONNEES_ETUD_OTHER*)this))(i,j));
    }
    else
        return ((*((STRUCT_DE_DONNEES_CORR_OTHER*)this))(i,j));
}

Double &STRUCT_DE_DONNEES_ETUD_OTHER::operator()(Int i, Int j)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return dummyVal;
}

Double STRUCT_DE_DONNEES_ETUD::operator()(Int i) const
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_OP_PARENTHESE_CONST)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        return (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this))(i);
    }
    else
        return (*((STRUCT_DE_DONNEES_CORR_OTHER*)this))(i);
}

Double STRUCT_DE_DONNEES_ETUD_OTHER::operator()(Int i) const
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return dummyVal;
}

Double STRUCT_DE_DONNEES_ETUD::operator()(Int i, Int j) const
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_OP_PARENTHESE_CONST)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        return ((*((STRUCT_DE_DONNEES_ETUD_OTHER*)this))(i,j));
    }
    else
        return ((*((STRUCT_DE_DONNEES_CORR_OTHER*)this))(i,j));
}

Double STRUCT_DE_DONNEES_ETUD_OTHER::operator()(Int i, Int j) const
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return dummyVal;
}

Double &STRUCT_DE_DONNEES_ETUD::operator[](Int i)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_OP_CROCHET)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        return (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this))[i];
    }
    else
        return (*((STRUCT_DE_DONNEES_CORR_OTHER*)this))[i];
}

Double &STRUCT_DE_DONNEES_ETUD_OTHER::operator[](Int i)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return dummyVal;
}

Double STRUCT_DE_DONNEES_ETUD::operator[](Int i) const
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_OP_CROCHET_CONST)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        return (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this))[i];
    }
    else
        return (*((STRUCT_DE_DONNEES_CORR_OTHER*)this))[i];
}

Double STRUCT_DE_DONNEES_ETUD_OTHER::operator[](Int i) const
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return dummyVal;
}

void STRUCT_DE_DONNEES_ETUD::set(Int i, Double val)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_SETTER)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).set(i,val);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).set(i,val);
}

void STRUCT_DE_DONNEES_ETUD_OTHER::set(Int i, Double val)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

void STRUCT_DE_DONNEES_ETUD::set(Int i, Int j, Double val)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_SETTER)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).set(i,j,val);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).set(i,j,val);
}

void STRUCT_DE_DONNEES_ETUD_OTHER::set(Int i, Int j, Double val)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

Double STRUCT_DE_DONNEES_ETUD::get(Int i) const
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_GETTER)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        return (*((const STRUCT_DE_DONNEES_ETUD_OTHER*)this)).get(i);
    }
    else
        return (*((const STRUCT_DE_DONNEES_CORR_OTHER*)this)).get(i);
}

Double STRUCT_DE_DONNEES_ETUD_OTHER::get(Int i) const
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->enteteIncorrect=true;
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=false;
    return ((STRUCT_DE_DONNEES_ETUD_OTHER*)this)->get(i);
}

Double STRUCT_DE_DONNEES_ETUD_OTHER::get(Int i)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return dummyVal;
}

Double STRUCT_DE_DONNEES_ETUD::get(Int i, Int j) const
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_GETTER)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        return (*((const STRUCT_DE_DONNEES_ETUD_OTHER*)this)).get(i,j);
    }
    else
        return (*((const STRUCT_DE_DONNEES_CORR_OTHER*)this)).get(i,j);
}

Double STRUCT_DE_DONNEES_ETUD_OTHER::get(Int i, Int j) const
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->enteteIncorrect=true;
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=false;
    return ((STRUCT_DE_DONNEES_ETUD_OTHER*)this)->get(i,j);
}

Double STRUCT_DE_DONNEES_ETUD_OTHER::get(Int i, Int j)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return dummyVal;
}

void STRUCT_DE_DONNEES_ETUD::additionnerATousLesElts(Double val)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_ADDITIONNER_A_TOUS_LES_ELTS)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).additionnerATousLesElts(val);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).additionnerATousLesElts(val);
}

void STRUCT_DE_DONNEES_ETUD_OTHER::additionnerATousLesElts(Double val)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

void STRUCT_DE_DONNEES_ETUD::insererUnEltDebut(Double val)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_INSERER_UN_ELT_DEBUT)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).insererUnEltDebut(val);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererUnEltDebut(val);
}

void STRUCT_DE_DONNEES_ETUD_OTHER::insererUnEltDebut(Double val)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

void STRUCT_DE_DONNEES_ETUD::insererUnEltDebut(Double *val, Int nb)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_INSERER_UN_ELT_DEBUT)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).insererUnEltDebut(val,nb);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererUnEltDebut(val,nb);
}

void STRUCT_DE_DONNEES_ETUD_OTHER::insererUnEltDebut(Double *val, Int nb)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

void STRUCT_DE_DONNEES_ETUD::insererUnEltMilieu(Int idx, Double val)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_INSERER_UN_ELT_MILIEU)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).insererUnEltMilieu(idx,val);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererUnEltMilieu(idx,val);
}

void STRUCT_DE_DONNEES_ETUD_OTHER::insererUnEltMilieu(Int idx, Double val)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

void STRUCT_DE_DONNEES_ETUD::insererUnEltMilieu(Int idx, Double *val, Int nb)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_INSERER_UN_ELT_MILIEU)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).insererUnEltMilieu(idx, val, nb);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererUnEltMilieu(idx, val, nb);
}

void STRUCT_DE_DONNEES_ETUD_OTHER::insererUnEltMilieu(Int idx, Double *val, Int nb)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

void STRUCT_DE_DONNEES_ETUD::insererUnEltFin(Double val)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_INSERER_UN_ELT_FIN)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).insererUnEltFin(val);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererUnEltFin(val);
}

void STRUCT_DE_DONNEES_ETUD_OTHER::insererUnEltFin(Double val)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

void STRUCT_DE_DONNEES_ETUD::insererUnEltFin(Double *val, Int nb)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_INSERER_UN_ELT_FIN)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).insererUnEltFin(val,nb);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererUnEltFin(val,nb);
}

void STRUCT_DE_DONNEES_ETUD_OTHER::insererUnEltFin(Double *val, Int nb)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

void STRUCT_DE_DONNEES_ETUD::enleverUnEltDebut()
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_ENLEVER_UN_ELT_DEBUT)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).enleverUnEltDebut();
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).enleverUnEltDebut();
}

void STRUCT_DE_DONNEES_ETUD_OTHER::enleverUnEltDebut()
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

void STRUCT_DE_DONNEES_ETUD::enleverUnEltMilieu(Int idx)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_ENLEVER_UN_ELT_MILIEU)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).enleverUnEltMilieu(idx);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).enleverUnEltMilieu(idx);
}

void STRUCT_DE_DONNEES_ETUD_OTHER::enleverUnEltMilieu(Int idx)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

void STRUCT_DE_DONNEES_ETUD::enleverUnEltFin()
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_ENLEVER_UN_ELT_FIN)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).enleverUnEltFin();
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).enleverUnEltFin();
}

void STRUCT_DE_DONNEES_ETUD_OTHER::enleverUnEltFin()
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

void STRUCT_DE_DONNEES_ETUD::insererPlusieursEltsDebut(Double *tab, Int nb)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_INSERER_PLUSIEURS_ELTS_DEBUT)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).insererPlusieursEltsDebut(tab, nb);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererPlusieursEltsDebut(tab, nb);
}

void STRUCT_DE_DONNEES_ETUD_OTHER::insererPlusieursEltsDebut(Double *tab, Int nb)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

void STRUCT_DE_DONNEES_ETUD::insererPlusieursEltsDebut(Double **tab, Int nbl, Int nbc)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_INSERER_PLUSIEURS_ELTS_DEBUT)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).insererPlusieursEltsDebut(tab, nbl, nbc);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererPlusieursEltsDebut(tab, nbl, nbc);
}

void STRUCT_DE_DONNEES_ETUD_OTHER::insererPlusieursEltsDebut(Double **tab, Int nbl, Int nbc)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

void STRUCT_DE_DONNEES_ETUD::insererPlusieursEltsMilieu(Int idx, Double *tab, Int nb)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_INSERER_PLUSIEURS_ELTS_MILIEU)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).insererPlusieursEltsMilieu(idx, tab, nb);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererPlusieursEltsMilieu(idx, tab, nb);
}

void STRUCT_DE_DONNEES_ETUD_OTHER::insererPlusieursEltsMilieu(Int idx, Double *tab, Int nb)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

void STRUCT_DE_DONNEES_ETUD::insererPlusieursEltsMilieu(Int idx, Double **tab, Int nbl, Int nbc)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_INSERER_PLUSIEURS_ELTS_MILIEU)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).insererPlusieursEltsMilieu(idx, tab, nbl, nbc);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererPlusieursEltsMilieu(idx, tab, nbl, nbc);
}

void STRUCT_DE_DONNEES_ETUD_OTHER::insererPlusieursEltsMilieu(Int idx, Double **tab, Int nbl, Int nbc)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

void STRUCT_DE_DONNEES_ETUD::insererPlusieursEltsFin(Double *tab, Int nb)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_INSERER_PLUSIEURS_ELTS_FIN)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).insererPlusieursEltsFin(tab, nb);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererPlusieursEltsFin(tab, nb);
}

void STRUCT_DE_DONNEES_ETUD_OTHER::insererPlusieursEltsFin(Double *tab, Int nb)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

void STRUCT_DE_DONNEES_ETUD::insererPlusieursEltsFin(Double **tab, Int nbl, Int nbc)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_INSERER_PLUSIEURS_ELTS_FIN)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).insererPlusieursEltsFin(tab, nbl, nbc);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).insererPlusieursEltsFin(tab, nbl, nbc);
}

void STRUCT_DE_DONNEES_ETUD_OTHER::insererPlusieursEltsFin(Double **tab, Int nbl, Int nbc)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

void STRUCT_DE_DONNEES_ETUD::enleverPlusieursEltsDebut(Int nb)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).enleverPlusieursEltsDebut(nb);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).enleverPlusieursEltsDebut(nb);
}

void STRUCT_DE_DONNEES_ETUD_OTHER::enleverPlusieursEltsDebut(Int nb)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

void STRUCT_DE_DONNEES_ETUD::enleverPlusieursEltsMilieu(Int idx, Int nb)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).enleverPlusieursEltsMilieu(idx, nb);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).enleverPlusieursEltsMilieu(idx, nb);
}

void STRUCT_DE_DONNEES_ETUD_OTHER::enleverPlusieursEltsMilieu(Int idx, Int nb)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

void STRUCT_DE_DONNEES_ETUD::enleverPlusieursEltsFin(Int nb)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_ENLEVER_PLUSIEURS_ELTS_FIN)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).enleverPlusieursEltsFin(nb);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).enleverPlusieursEltsFin(nb);
}

void STRUCT_DE_DONNEES_ETUD_OTHER::enleverPlusieursEltsFin(Int nb)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

void STRUCT_DE_DONNEES_ETUD::enleverToutesLesOccurencesDUnElt(Double val)
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        (*((STRUCT_DE_DONNEES_ETUD_OTHER*)this)).enleverToutesLesOccurencesDUnElt(val);
    }
    else
        (*((STRUCT_DE_DONNEES_CORR_OTHER*)this)).enleverToutesLesOccurencesDUnElt(val);
}

void STRUCT_DE_DONNEES_ETUD_OTHER::enleverToutesLesOccurencesDUnElt(Double val)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

bool STRUCT_DE_DONNEES_ETUD::eltsEnOrdreDecroissant() const
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_ELTS_EN_ORDRE_DECROISSANT)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        return (*((const STRUCT_DE_DONNEES_ETUD_OTHER*)this)).eltsEnOrdreDecroissant();
    }
    else
        return (*((const STRUCT_DE_DONNEES_CORR_OTHER*)this)).eltsEnOrdreDecroissant();
}

bool STRUCT_DE_DONNEES_ETUD_OTHER::eltsEnOrdreDecroissant() const
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->enteteIncorrect=true;
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=false;
    return ((STRUCT_DE_DONNEES_ETUD_OTHER*)this)->eltsEnOrdreDecroissant();
}

bool STRUCT_DE_DONNEES_ETUD_OTHER::eltsEnOrdreDecroissant()
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return false;
}

bool STRUCT_DE_DONNEES_ETUD::contientElt(Double val) const
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_CONTIENT_ELT)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        return (*((const STRUCT_DE_DONNEES_ETUD_OTHER*)this)).contientElt(val);
    }
    else
        return (*((const STRUCT_DE_DONNEES_CORR_OTHER*)this)).contientElt(val);
}

bool STRUCT_DE_DONNEES_ETUD_OTHER::contientElt(Double val) const
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->enteteIncorrect=true;
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=false;
    return ((STRUCT_DE_DONNEES_ETUD_OTHER*)this)->contientElt(val);
}

bool STRUCT_DE_DONNEES_ETUD_OTHER::contientElt(Double val)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return false;
}

Int STRUCT_DE_DONNEES_ETUD::chercherLIndexDeLaPremiereOccurenceDUnElt(Double val) const
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        return (*((const STRUCT_DE_DONNEES_ETUD_OTHER*)this)).chercherLIndexDeLaPremiereOccurenceDUnElt(val);
    }
    else
        return (*((const STRUCT_DE_DONNEES_CORR_OTHER*)this)).chercherLIndexDeLaPremiereOccurenceDUnElt(val);
}

Int STRUCT_DE_DONNEES_ETUD_OTHER::chercherLIndexDeLaPremiereOccurenceDUnElt(Double val) const
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->enteteIncorrect=true;
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=false;
    return ((STRUCT_DE_DONNEES_ETUD_OTHER*)this)->chercherLIndexDeLaPremiereOccurenceDUnElt(val);
}

Int STRUCT_DE_DONNEES_ETUD_OTHER::chercherLIndexDeLaPremiereOccurenceDUnElt(Double val)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return Int();
}

Int STRUCT_DE_DONNEES_ETUD::chercherLIndexDeLaDerniereOccurenceDUnElt(Double val) const
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        return (*((const STRUCT_DE_DONNEES_ETUD_OTHER*)this)).chercherLIndexDeLaDerniereOccurenceDUnElt(val);
    }
    else
        return (*((const STRUCT_DE_DONNEES_CORR_OTHER*)this)).chercherLIndexDeLaDerniereOccurenceDUnElt(val);
}

Int STRUCT_DE_DONNEES_ETUD_OTHER::chercherLIndexDeLaDerniereOccurenceDUnElt(Double val) const
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->enteteIncorrect=true;
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=false;
    return ((STRUCT_DE_DONNEES_ETUD_OTHER*)this)->chercherLIndexDeLaDerniereOccurenceDUnElt(val);
}

Int STRUCT_DE_DONNEES_ETUD_OTHER::chercherLIndexDeLaDerniereOccurenceDUnElt(Double val)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return Int();
}

Int STRUCT_DE_DONNEES_ETUD::calculerNombreDOccurencesDUnElt(Double val) const
{
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
               __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        return (*((const STRUCT_DE_DONNEES_ETUD_OTHER*)this)).calculerNombreDOccurencesDUnElt(val);
    }
    else
        return (*((const STRUCT_DE_DONNEES_CORR_OTHER*)this)).calculerNombreDOccurencesDUnElt(val);
}

Int STRUCT_DE_DONNEES_ETUD_OTHER::calculerNombreDOccurencesDUnElt(Double val) const
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->enteteIncorrect=true;
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=false;
    return ((STRUCT_DE_DONNEES_ETUD_OTHER*)this)->calculerNombreDOccurencesDUnElt(val);
}

Int STRUCT_DE_DONNEES_ETUD_OTHER::calculerNombreDOccurencesDUnElt(Double val)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return Int();
}

STRUCT_DE_DONNEES_ETUD operator+(const STRUCT_DE_DONNEES_ETUD& a, const STRUCT_DE_DONNEES_ETUD& b)
{
    STRUCT_DE_DONNEES_ETUD res;
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_OP_ADDITION)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
                __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        // Utilisation du "placement new".
        // Permet d'appeler le constructeur STRUCT_DE_DONNEES_CORR sur un objet
        // de la classe STRUCT_DE_DONNEES_ETUD
        new(&res) STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT(operator_plus(a,b));
    }
    else
    {
        // Utilisation du "placement new".
        // Permet d'appeler le constructeur STRUCT_DE_DONNEES_CORR sur un objet
        // de la classe STRUCT_DE_DONNEES_ETUD
        new(&res) STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT(operator_plus(*((const STRUCT_DE_DONNEES_CORR*)(&a)),*((const STRUCT_DE_DONNEES_CORR*)(&b))));
    }
    return res;
}

STRUCT_DE_DONNEES_ETUD operator_plus(const STRUCT_DE_DONNEES_ETUD& a, const STRUCT_DE_DONNEES_ETUD& b)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->enteteIncorrect=true;
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=false;
    return operator_plus((*((STRUCT_DE_DONNEES_ETUD*)&a)),(*((const STRUCT_DE_DONNEES_ETUD*)&b)));
}

STRUCT_DE_DONNEES_ETUD operator_plus(STRUCT_DE_DONNEES_ETUD& a, const STRUCT_DE_DONNEES_ETUD& b)
{
    return operator_plus((*((const STRUCT_DE_DONNEES_ETUD*)&a)),(*((STRUCT_DE_DONNEES_ETUD*)&b)));
}

STRUCT_DE_DONNEES_ETUD operator_plus(const STRUCT_DE_DONNEES_ETUD& a, STRUCT_DE_DONNEES_ETUD& b)
{
    return operator_plus((*((STRUCT_DE_DONNEES_ETUD*)&a)),(*((STRUCT_DE_DONNEES_ETUD*)&b)));
}

STRUCT_DE_DONNEES_ETUD operator_plus(STRUCT_DE_DONNEES_ETUD& a, STRUCT_DE_DONNEES_ETUD& b)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return *((STRUCT_DE_DONNEES_ETUD*)(&dummyStructDeDonnees));
}

STRUCT_DE_DONNEES_ETUD operator-(const STRUCT_DE_DONNEES_ETUD& a, const STRUCT_DE_DONNEES_ETUD& b)
{
    STRUCT_DE_DONNEES_ETUD res;
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_OP_SOUSTRACTION)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
                __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        // Utilisation du "placement new".
        // Permet d'appeler le constructeur STRUCT_DE_DONNEES_CORR sur un objet
        // de la classe STRUCT_DE_DONNEES_ETUD
        new(&res) STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT(operator_moins(a,b));
    }
    else
    {
        // Utilisation du "placement new".
        // Permet d'appeler le constructeur STRUCT_DE_DONNEES_CORR sur un objet
        // de la classe STRUCT_DE_DONNEES_ETUD
        new(&res) STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT(operator_moins(*((const STRUCT_DE_DONNEES_CORR*)(&a)),*((const STRUCT_DE_DONNEES_CORR*)(&b))));
    }
    return res;
}

STRUCT_DE_DONNEES_ETUD operator_moins(const STRUCT_DE_DONNEES_ETUD& a, const STRUCT_DE_DONNEES_ETUD& b)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->enteteIncorrect=true;
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=false;
    return operator_moins((*((STRUCT_DE_DONNEES_ETUD*)&a)),(*((const STRUCT_DE_DONNEES_ETUD*)&b)));
}

STRUCT_DE_DONNEES_ETUD operator_moins(STRUCT_DE_DONNEES_ETUD& a, const STRUCT_DE_DONNEES_ETUD& b)
{
    return operator_moins((*((const STRUCT_DE_DONNEES_ETUD*)&a)),(*((STRUCT_DE_DONNEES_ETUD*)&b)));
}

STRUCT_DE_DONNEES_ETUD operator_moins(const STRUCT_DE_DONNEES_ETUD& a, STRUCT_DE_DONNEES_ETUD& b)
{
    return operator_moins((*((STRUCT_DE_DONNEES_ETUD*)&a)),(*((STRUCT_DE_DONNEES_ETUD*)&b)));
}

STRUCT_DE_DONNEES_ETUD operator_moins(STRUCT_DE_DONNEES_ETUD& a, STRUCT_DE_DONNEES_ETUD& b)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return *((STRUCT_DE_DONNEES_ETUD*)(&dummyStructDeDonnees));
}

STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT::STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT()
{
    memset((void*)this,0x00,sizeof(STRUCT_DE_DONNEES_ETUD));
    // Dans certain cas, le constructeur est appele pour evaluer une autre methode
    // Mettre le flag manquante a true seulement ci le constructeur doit etre evalue
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_CONSTRUCTEUR)
        Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT::STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT(const STRUCT_DE_DONNEES_ETUD &param)
{
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->enteteIncorrect=true;
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=false;
    // Appel du constructeur sans le const
    new(this) STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT(*((STRUCT_DE_DONNEES_ETUD*)&param));
}

STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT::STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT(STRUCT_DE_DONNEES_ETUD &)
{
    memset((void*)this,0x00,sizeof(STRUCT_DE_DONNEES_ETUD));
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT::STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT(Double *tab, Int nb)
{
    memset((void*)this,0x00,sizeof(STRUCT_DE_DONNEES_ETUD));
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT::STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT(Double **tab, Int nbl, Int nbc)
{
    memset((void*)this,0x00,sizeof(STRUCT_DE_DONNEES_ETUD));
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT::~STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT()
{
    memset((void*)this,0x00,sizeof(STRUCT_DE_DONNEES_ETUD));
    // Dans certain cas, le destructeur est appele pour evaluer une autre methode
    // Mettre le flag manquante a true seulement ci le destructeur doit etre evalue
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_DESTRUCTEUR)
        Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

#ifdef ITERATEUR
STRUCT_DE_DONNEES_CORR::ITERATEUR STRUCT_DE_DONNEES_CORR::premier() {
    ITERATEUR it;
    STRUCT_DE_DONNEES_CORR_OTHER::ITERATEUR iter(((STRUCT_DE_DONNEES_CORR_OTHER*)this)->premier());
    memcpy((void*)&it,(void*)&iter,sizeof(ITERATEUR));
    memset((void*)&iter,0x00,sizeof(ITERATEUR));
    return it;
}

STRUCT_DE_DONNEES_CORR_OTHER::ITERATEUR STRUCT_DE_DONNEES_CORR_OTHER::premier() {
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return ITERATEUR{nullptr};
}

STRUCT_DE_DONNEES_CORR::ITERATEUR::ITERATEUR(ITERATEUR_TYPE_ELEMENT* ancre)
{
    new(this) STRUCT_DE_DONNEES_CORR_OTHER::ITERATEUR(ancre);
}

STRUCT_DE_DONNEES_CORR_OTHER::ITERATEUR::ITERATEUR(ITERATEUR_TYPE_ELEMENT* ancre)
{
    // Identique au constructeur:
    // Une class interne dans la classe mere est aussi une classe interne dans la classe fille
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
}

bool STRUCT_DE_DONNEES_CORR::ITERATEUR::fin() const {
    return ((const STRUCT_DE_DONNEES_CORR_OTHER::ITERATEUR*)this)->fin();
}

bool STRUCT_DE_DONNEES_CORR_OTHER::ITERATEUR::fin() const {
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return true;
}

Double& STRUCT_DE_DONNEES_CORR::ITERATEUR::operator*() {
    return ((STRUCT_DE_DONNEES_CORR_OTHER::ITERATEUR*)this)->operator*();
}

Double& STRUCT_DE_DONNEES_CORR_OTHER::ITERATEUR::operator*() {
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return dummyVal;
}

Double STRUCT_DE_DONNEES_CORR::ITERATEUR::operator*() const {
    return ((const STRUCT_DE_DONNEES_CORR_OTHER::ITERATEUR*)this)->operator*();
}

Double STRUCT_DE_DONNEES_CORR_OTHER::ITERATEUR::operator*() const {
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return dummyVal;
}

STRUCT_DE_DONNEES_CORR::ITERATEUR& STRUCT_DE_DONNEES_CORR::ITERATEUR::operator++() {
    ((STRUCT_DE_DONNEES_CORR_OTHER::ITERATEUR*)this)->operator++();
    return *this;
}

STRUCT_DE_DONNEES_CORR_OTHER::ITERATEUR& STRUCT_DE_DONNEES_CORR_OTHER::ITERATEUR::operator++() {
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return *this;
}

STRUCT_DE_DONNEES_CORR::ITERATEUR& STRUCT_DE_DONNEES_CORR::ITERATEUR::operator--() {
    ((STRUCT_DE_DONNEES_CORR_OTHER::ITERATEUR*)this)->operator--();
    return *this;
}

STRUCT_DE_DONNEES_CORR_OTHER::ITERATEUR& STRUCT_DE_DONNEES_CORR_OTHER::ITERATEUR::operator--() {
    cout<<__PRETTY_FUNCTION__<<" pas implementee dans la correction"<<endl;
    return *this;
}

STRUCT_DE_DONNEES_ETUD::ITERATEUR STRUCT_DE_DONNEES_ETUD::premier() {
    ITERATEUR it;
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_CREER_ITERATEUR)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
                __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        STRUCT_DE_DONNEES_ETUD_OTHER::ITERATEUR iter(((STRUCT_DE_DONNEES_ETUD_OTHER*)this)->premier());
        memcpy((void*)&it,(void*)&iter,sizeof(ITERATEUR));
        memset((void*)&iter,0x00,sizeof(ITERATEUR));
    }
    else
    {
        STRUCT_DE_DONNEES_CORR_OTHER::ITERATEUR iter(((STRUCT_DE_DONNEES_CORR_OTHER*)this)->premier());
        memcpy((void*)&it,(void*)&iter,sizeof(ITERATEUR));
        memset((void*)&iter,0x00,sizeof(ITERATEUR));
    }
    return it;
}

STRUCT_DE_DONNEES_ETUD_OTHER::ITERATEUR STRUCT_DE_DONNEES_ETUD_OTHER::premier() {
    sprintf(&(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::msgErrAlgoJeuDeDonnees
                [strlen(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::msgErrAlgoJeuDeDonnees)]),
            "%s\n", __PRETTY_FUNCTION__);
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return ITERATEUR{nullptr};
}

STRUCT_DE_DONNEES_ETUD_OTHER::ITERATEUR::ITERATEUR(ITERATEUR_TYPE_ELEMENT* ancre)
{
    sprintf(&(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::msgErrAlgoJeuDeDonnees
                [strlen(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::msgErrAlgoJeuDeDonnees)]),
            "%s\n", __PRETTY_FUNCTION__);
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
}

bool STRUCT_DE_DONNEES_ETUD::ITERATEUR::fin() const {
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_OP_FIN_ITERATEUR)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
                __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        return ((STRUCT_DE_DONNEES_ETUD_OTHER::ITERATEUR*)this)->fin();
    }
    else
        return ((STRUCT_DE_DONNEES_CORR_OTHER::ITERATEUR*)this)->fin();
}

bool STRUCT_DE_DONNEES_ETUD_OTHER::ITERATEUR::fin() const {
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return true;
}

Double& STRUCT_DE_DONNEES_ETUD::ITERATEUR::operator*() {
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_OP_ETOILE_ITERATEUR)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
                __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        return ((STRUCT_DE_DONNEES_ETUD_OTHER::ITERATEUR*)this)->operator*();
    }
    else
        return ((STRUCT_DE_DONNEES_CORR::ITERATEUR*)this)->operator*();
}

Double& STRUCT_DE_DONNEES_ETUD_OTHER::ITERATEUR::operator*() {
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return dummyVal;
}

Double STRUCT_DE_DONNEES_ETUD::ITERATEUR::operator*() const {
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_OP_ETOILE_CONST_ITERATEUR)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
                __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        return ((const STRUCT_DE_DONNEES_ETUD_OTHER::ITERATEUR*)this)->operator*();
    }
    else
        return ((const STRUCT_DE_DONNEES_CORR::ITERATEUR*)this)->operator*();
}

Double STRUCT_DE_DONNEES_ETUD_OTHER::ITERATEUR::operator*() const {
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return dummyVal;
}

STRUCT_DE_DONNEES_ETUD::ITERATEUR& STRUCT_DE_DONNEES_ETUD::ITERATEUR::operator++() {
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_OP_PLUS_PLUS_ITERATEUR)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
                __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        ((STRUCT_DE_DONNEES_ETUD_OTHER::ITERATEUR*)this)->operator++();
    }
    else
        ((STRUCT_DE_DONNEES_CORR_OTHER::ITERATEUR*)this)->operator++();
    return *this;
}

STRUCT_DE_DONNEES_ETUD_OTHER::ITERATEUR& STRUCT_DE_DONNEES_ETUD_OTHER::ITERATEUR::operator++() {
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return *this;
}

STRUCT_DE_DONNEES_ETUD::ITERATEUR& STRUCT_DE_DONNEES_ETUD::ITERATEUR::operator--() {
    if (Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->idExo==ID_EXO_OP_MOINS_MOINS_ITERATEUR)
    {
        memcpy(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->nomMethodeExo,
                __PRETTY_FUNCTION__,strlen(__PRETTY_FUNCTION__));
        ((STRUCT_DE_DONNEES_ETUD_OTHER::ITERATEUR*)this)->operator--();
    }
    else
        ((STRUCT_DE_DONNEES_CORR_OTHER::ITERATEUR*)this)->operator--();
    return *this;
}

STRUCT_DE_DONNEES_ETUD_OTHER::ITERATEUR& STRUCT_DE_DONNEES_ETUD_OTHER::ITERATEUR::operator--() {
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::resultEvalJeuDeDonnees->manquante=true;
    return *this;
}

#endif // ITERATEUR
