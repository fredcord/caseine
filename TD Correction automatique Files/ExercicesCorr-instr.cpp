////////////////////////////////////////////////////////////////////////////////////
// Fichier genere automatiquement pour l'instrumentation du code. Ne pas modifier //
////////////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include "ExercicesBase.h"


using namespace std;


/////////////////////////////////////////////////////////////////////////////
// Compare la structure de donnees de l'etudiant avec celle de la solution //
/////////////////////////////////////////////////////////////////////////////


// Verifie si la liste chainee est correcte au niveau de sa sctructure et des pointeurs
bool FileCorr_Other::verifierIntegrite(const File &param)
{
    // Verifie si les valeurs sont dans les bons intervals pour eviter les crash
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_quantite<0 || param.d_quantite>max_elt)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_premier<0 || param.d_premier>max_elt)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_dernier<0 || param.d_dernier>max_elt)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}

    Int quant=param.d_dernier-param.d_premier;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_dernier<param.d_premier)
        quant=max_elt-param.d_premier+param.d_dernier;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&quant!=param.d_quantite && quant!=0)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}

    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
}

// Compare la correction avec la struct de l'etudiant. Retourne false si different
bool FileCorr_Other::compareCorrAvecEtud(const File &student) const
{
    // Compare les champs
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this->d_quantite!=student.d_quantite)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    // Calcul la quantite a partir de d_premier et d_dernier
    Int quantCorr=this->d_dernier-this->d_premier;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this->d_dernier<this->d_premier)
        quantCorr=max_elt-this->d_premier+this->d_dernier;
    Int quantEtud=student.d_dernier-student.d_premier;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&student.d_dernier<student.d_premier)
        quantEtud=max_elt-student.d_premier+student.d_dernier;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&quantCorr!=quantEtud)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}

    // Compare le contenu du tableau
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&quantCorr>0)
    {
        Int iCorr=this->d_premier,iEtud=student.d_premier;
        while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&iCorr!=this->d_dernier && this->d_file[iCorr]==student.d_file[iEtud])
        {
            iCorr++;
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&iCorr==FileCorr_Other::max_elt)
                iCorr=0;
            iEtud++;
            if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&iEtud==FileCorr_Other::max_elt)
                iEtud=0;
        }
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&iCorr!=this->d_dernier)
            {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
}

// genere la structDeDonneesVerifCorr a partir de la liste
StructDeDonneesVerifCorr FileCorr_Other::getStructDeDonneesVerifCorr() const
{
    StructDeDonneesVerifCorr vect;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_quantite==0)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return vect;}

    Int id=d_premier;
    Int quant=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&quant<d_quantite)
    {
        vect.insererUnEltFin(d_file[id]);
        id++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&id==max_elt)
            id=0;
        quant++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return vect;}
}

// Constructeur avec la structDeDonneesVerifCorr
void FileCorr_Other::setStructDeDonneesVerifCorr(const StructDeDonneesVerifCorr &tab)
{
    d_quantite=0;
    // Les elements sont intentionnelement a cheval sur la fin et le debut du tableau
    d_premier=max_elt-2;
    d_dernier=max_elt-2;

    // On insere les elements du tableau
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<tab.getNbColonnes();i++)
    {
        d_file[d_dernier]=tab[i];
        d_dernier++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_dernier==max_elt)
            d_dernier=0;
        d_quantite++;
    }
}

// DEBUT EXO_CONSTRUCTEUR
FileCorr_ConstructDestruct::FileCorr_ConstructDestruct():d_premier{0},d_dernier{0},d_quantite{0}
{
}
// FIN EXO_CONSTRUCTEUR


// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
FileCorr_ConstructDestruct::FileCorr_ConstructDestruct(const FileCorr &param):d_premier{param.d_premier},d_dernier{param.d_dernier},d_quantite{param.d_quantite}
{
    // Copie le tableau
    Int i=d_premier,q=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&q<d_quantite)
    {
        d_file[i]=param.d_file[i];
        q++;
        i++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&i==max_elt)
            i=0;
    }
}
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE


// DEBUT EXO_CONCATENATION_FIN
void FileCorr_Other::concatenationFin(const FileCorr &param)
{
    Int id2=param.d_premier;
    Int quant=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&quant<param.d_quantite)
    {
        d_file[d_dernier]=param.d_file[id2];
        d_dernier++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_dernier==max_elt)
            d_dernier=0;
        id2++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&id2==max_elt)
            id2=0;
        quant++;
        d_quantite++;
    }
}
// FIN EXO_CONCATENATION_FIN


// DEBUT EXO_OP_CONCATENATION
FileCorr &FileCorr_Other::operator+=(const FileCorr &param)
{
    Int id2=param.d_premier;
    Int quant=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&quant<param.d_quantite)
    {
        d_file[d_dernier]=param.d_file[id2];
        d_dernier++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_dernier==max_elt)
            d_dernier=0;
        id2++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&id2==max_elt)
            id2=0;
        quant++;
        d_quantite++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_CONCATENATION


// DEBUT EXO_OP_EGAL
bool FileCorr_Other::operator==(const FileCorr &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_quantite!=param.d_quantite)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    Int i=d_premier,j=param.d_premier;
    Int quant=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&quant<d_quantite &&  d_file[i]==param.d_file[j])
    {
        i++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&i==max_elt)
            i=0;
        j++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&j==max_elt)
            j=0;
        quant++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return quant==d_quantite;}
}
// FIN EXO_OP_EGAL


// DEBUT EXO_OP_AFFECTATION
FileCorr &FileCorr_Other::operator=(const FileCorr &param)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
    d_premier=param.d_premier;
    d_dernier=param.d_dernier;
    d_quantite=param.d_quantite;

    Int id=param.d_premier;
    Int quant=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&quant<param.d_quantite)
    {
        d_file[id]=param.d_file[id];
        id++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&id==max_elt)
            id=0;
        quant++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_AFFECTATION


// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
void FileCorr_Other::additionnerATousLesElts(Double val)
{
    Int id=d_premier;
    Int quant=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&quant<d_quantite)
    {
        d_file[id]+=val;
        id++;
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&id==max_elt)
            id=0;
        quant++;
    }
}
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS


// DEBUT EXO_INSERER_UN_ELT_FIN
void FileCorr_Other::insererUnEltFin(Double val)
{
    d_file[d_dernier++] = val;
    d_dernier %= max_elt;
    d_quantite++;
}
// FIN EXO_INSERER_UN_ELT_FIN


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
void FileCorr_Other::enleverPlusieursEltsDebut(Int nb)
{
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<nb && d_quantite>0)
    {
        d_premier++;
        d_premier %= max_elt;
        d_quantite--;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT

