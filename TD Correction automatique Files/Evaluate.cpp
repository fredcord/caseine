#include <iostream>
#include <string>
#include <time.h>
#include <unistd.h>
#include <execinfo.h>
#include <dlfcn.h>
#include <regex>

#include <sys/mman.h>
#include <sys/wait.h>
#include <cxxabi.h>   // Pour demangling

#include "GestionPntrEtComplexite.h"

#if __has_include("vpl_evaluate-instr.cases")
#include "vpl_evaluate-instr.cases"
#else
#include "vpl_evaluate.cases"
#endif

#include "Evaluate.h"

// Pour afficher des messages de deboggage
//#define DEBUG
// Pour executer le code dans un seul process. Permet de mettre des points d'arret
#define DESACTIVE_EVALUATION_PARALLELE

// Pour verifier la correction
#define VERIF_CORRECTION


#ifdef DEBUG
double totalTiming=0.0;
#endif


// Le tableau contenant tous les exercices
EvaluationExercice tabEvaluationExercices[]={ Q01 Q02 Q03 Q04 Q05 Q06 Q07 Q08 Q09 Q10 \
                                              Q11 Q12 Q13 Q14 Q15 Q16 Q17 Q18 Q19 Q20 \
                                              Q21 Q22 Q23 Q24 Q25 Q26 Q27 Q28 Q29 Q30 \
                                              Q31 Q32 Q33 Q34 Q35 Q36 Q37 Q38 Q39 Q40 \
                                              Q41 Q42 Q43 Q44 Q45 Q46 Q47 Q48 Q49 Q50 };


const int getNombreTotalExercices()
{
    return sizeof(tabEvaluationExercices)/sizeof(EvaluationExercice);
}


template <class StructDeDonneesCorr, class StructDeDonneesEtud>
ResultatEvaluation* Evaluer<StructDeDonneesCorr, StructDeDonneesEtud>::resultEvalJeuDeDonnees=nullptr;
template <class StructDeDonneesCorr, class StructDeDonneesEtud>
char* Evaluer<StructDeDonneesCorr, StructDeDonneesEtud>::msgErrPntrJeuDeDonnees=nullptr;
template <class StructDeDonneesCorr, class StructDeDonneesEtud>
char* Evaluer<StructDeDonneesCorr, StructDeDonneesEtud>::msgErrAlgoJeuDeDonnees=nullptr;
template <class StructDeDonneesCorr, class StructDeDonneesEtud>
int Evaluer<StructDeDonneesCorr, StructDeDonneesEtud>::idExo=-1;
template <class StructDeDonneesCorr, class StructDeDonneesEtud>
int Evaluer<StructDeDonneesCorr, StructDeDonneesEtud>::jeuID=-1;



// http://www.miriamruiz.es/code/backtrace_test.c
template <class StructDeDonneesCorr, class StructDeDonneesEtud>
void Evaluer<StructDeDonneesCorr, StructDeDonneesEtud>::sighandler(int sig, siginfo_t *si, void *ptr)
{
    // Le programme a plante et il est probable que toutes les donnees soient corrompues
    // On recupere juste l'adresse de l'instruction du crash
    // Cette adresse sera recuperee par le processus mere pour indiquer l'endroit exact du crash
    ucontext_t *uc = (ucontext_t *)ptr;
    resultEvalJeuDeDonnees->ptrInstructCrash=(void*)uc->uc_mcontext.gregs[REG_RIP];
#ifdef DEBUG
    cout<<"sighandler, adr: "<<std::hex <<uc->uc_mcontext.gregs[REG_RIP]<<endl;
#endif // DEBUG
#ifdef DESACTIVE_EVALUATION_PARALLELE
    cout<<"sighandler pour "<<idExo<<endl;
#endif
    // On quite le processus fille
	exit(0);
}

// pour *** stack smashing detected ***
// Necessite de compiler avec l'option -fstack-protector
// Fonction appelee quand depassement d'un tableau statique
extern "C" {
unsigned long __stack_chk_guard = 0xdeadbeefa55a857;
void __stack_chk_fail(void)
{
    sprintf(Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD>::msgErrPntrJeuDeDonnees,
            "Erreur: dépassement d'un tableau statique.\n");
    exit(0);
}
}



template <class StructDeDonneesCorr, class StructDeDonneesEtud>
bool Evaluer<StructDeDonneesCorr, StructDeDonneesEtud>::verifAllocationPntr(
                                          GestionPntrEtComplexite::TabAppelsInstructions &tabInstructionsCorr,
                                          GestionPntrEtComplexite::TabAppelsInstructions &tabInstructionsEtud)
{
    // Compare le nombre total de pointeurs
    if (tabInstructionsCorr.getNbTotalPntr()!=tabInstructionsEtud.getNbTotalPntr())
    {
        // Compare le nombre total de pointeurs utilisés (libere ou encore utilise)
        if (strlen(msgErrPntrJeuDeDonnees)==0)
        {
            if (tabInstructionsEtud.getNbTotalPntr()>tabInstructionsCorr.getNbTotalPntr())
                sprintf(msgErrPntrJeuDeDonnees,"Erreur: operateur new utilisé plus que nécessaire pour la struct. de données\n");
            else
                sprintf(msgErrPntrJeuDeDonnees,"Erreur: operateur new utilisé trop peu de fois pour la struct. de données\n");
        }
        return false;
    }
    // Compare le nombre de pointeurs pas liberes
    if (tabInstructionsCorr.getNbTotalPntrPasLiberes()!=tabInstructionsEtud.getNbTotalPntrPasLiberes())
    {
        // Compare le nombre total de pointeurs utilisés (libere ou encore utilise)
        if (strlen(msgErrPntrJeuDeDonnees)==0)
        {
            if (tabInstructionsEtud.getNbTotalPntrPasLiberes()>tabInstructionsCorr.getNbTotalPntrPasLiberes())
                sprintf(msgErrPntrJeuDeDonnees,"Erreur: pointeurs pas libérés pour la struct. de données\n");
            else
                sprintf(msgErrPntrJeuDeDonnees,"Erreur: operateur delete utilisé plus que nécessaire pour la struct. de données\n");
        }
        return false;
    }
    // Compare le nombre de delete
    if (tabInstructionsCorr.getNbTotalPntrLiberes()!=tabInstructionsEtud.getNbTotalPntrLiberes())
    {
        if (strlen(msgErrPntrJeuDeDonnees)==0)
        {
            if (tabInstructionsEtud.getNbTotalPntrLiberes()<tabInstructionsCorr.getNbTotalPntrLiberes())
                sprintf(msgErrPntrJeuDeDonnees,"Erreur: pointeurs pas libérés pour la struct. de données\n");
            else
                sprintf(msgErrPntrJeuDeDonnees,"Erreur: operateur delete utilisé plus que nécessaire pour la struct. de données\n");
        }
        return false;
    }
    return true;
}

template <class StructDeDonneesCorr, class StructDeDonneesEtud>
bool Evaluer<StructDeDonneesCorr, StructDeDonneesEtud>::verifComplexiteTemps(
                                          GestionPntrEtComplexite::TabAppelsInstructions &tabInstructionsCorr,
                                          GestionPntrEtComplexite::TabAppelsInstructions &tabInstructionsEtud)
{
    // Veririfie le nombre d'instructions
    int nbInstructCorr=tabInstructionsCorr.getNbInstructionsBoucle()+tabInstructionsCorr.getNbInstructionsIf();
    int nbInstructEtud=tabInstructionsEtud.getNbInstructionsBoucle()+tabInstructionsEtud.getNbInstructionsIf();
    if (nbInstructCorr>1 && nbInstructEtud==0)
    {
        // Dans le cas ou le code de l'etudiant ne contient aucune instruction de structure de controle
        // Par exemple, le code contient seulement l'instruction return ...
        if (strlen(msgErrAlgoJeuDeDonnees)==0)
        {
            if (tabInstructionsCorr.getNbInstructionsBoucle()>0)
                sprintf(msgErrAlgoJeuDeDonnees,"Erreur: la méthode doit implémenter une boucle\n");
            else
                sprintf(msgErrAlgoJeuDeDonnees,"Erreur: la méthode doit implémenter un test \"if(...\"\n");
        }
        // Comme rien n'est implemente, on met 0
        resultEvalJeuDeDonnees->note=0;
        return false;
    }
    // Si une seule boucle dans la correction et le code de l'etudiant
    if (tabInstructionsCorr.getNbInstructionsBoucle()==1 && tabInstructionsEtud.getNbInstructionsBoucle()==1)
    {
        // Si une seule boucle, verifier son type while ou for
        if (tabInstructionsCorr.getInstructionBoucle(0).type!=tabInstructionsEtud.getInstructionBoucle(0).type)
        {
            if (strlen(msgErrAlgoJeuDeDonnees)==0)
            {
                if (tabInstructionsCorr.getInstructionBoucle(0).type==GestionPntrEtComplexite::TabAppelsInstructions::FOR)
                    sprintf(msgErrAlgoJeuDeDonnees,"Erreur: utilisation d'une boucle \"while\" au lieu d'une boucle \"for\"\n");
                else
                    sprintf(msgErrAlgoJeuDeDonnees,"Erreur: utilisation d'une boucle \"for\" au lieu d'une boucle \"while\"\n");
            }
            // Divise la note par 2
            resultEvalJeuDeDonnees->note/=2.0;
            return false;
        }
    }

    // On verifie aussi le nombre d'appels pour toutes les boucles et if
    int nbAppelsCorr=tabInstructionsCorr.getNbAppelsBoucle()+tabInstructionsCorr.getNbAppelsIf();
    int nbAppelsEtud=tabInstructionsEtud.getNbAppelsBoucle()+tabInstructionsEtud.getNbAppelsIf();
    // On met une marge de 2
    if (nbAppelsEtud-2>nbAppelsCorr)
    {
        if (strlen(msgErrAlgoJeuDeDonnees)==0)
            sprintf(msgErrAlgoJeuDeDonnees,"Erreur: nombre d'itérations de la boucle trop important\n");
        // Divise la note par 2
        resultEvalJeuDeDonnees->note/=2.0;
        return false;
    }
    return true;
}

template<class StructDeDonneesCorr, class StructDeDonneesEtud> template<class StructDeDonnees>
void Evaluer<StructDeDonneesCorr, StructDeDonneesEtud>::evalUnjeuDeDonnees(
                                 int idExo,
                                 JeuDeDonnees &jeuDeDonnees,
                                 StructDeDonnees* &pntrRes,
                                 const StructDeDonnees* &pntrThis,const StructDeDonnees* &pntrParam,
                                 StructDeDonneesVerifCorr* &pntrTab,
                                 Int &paramNb, Int &paramIdxColonne, Int &paramIdxLigne,
                                 bool* &returnBoolPntr,bool &returnBool,
                                 int* &returnIntPntr,int &returnInt,
                                 double* &returnDoublePntr,double &returnDouble,
                                 StructDeDonnees* &returnThis,
                                 typename StructDeDonnees::ITERATEUR** it)
{
    pntrRes=nullptr;
    pntrThis=jeuDeDonnees.template getThis<StructDeDonnees>(jeuID);
    pntrParam=nullptr;
    pntrTab=nullptr;
    paramNb=-1;
    paramIdxColonne=-1;
    paramIdxLigne=-1;
    returnBoolPntr=nullptr;
    returnIntPntr=nullptr;
    returnDoublePntr=nullptr;
    returnThis=nullptr;
    resultEvalJeuDeDonnees->idExo=idExo;

    GestionPntrEtComplexite::get().resetEtStartCompterInstructions();
    if (idExo==ID_EXO_CONSTRUCTEUR)
    {
        pntrThis=nullptr;
        pntrRes=new StructDeDonnees();
    }
    else if (idExo==ID_EXO_CONSTRUCTEUR_PAR_RECOPIE)
    {
        pntrThis=nullptr;
        pntrParam=jeuDeDonnees.template getParam<StructDeDonnees>(jeuID);
        pntrRes=new StructDeDonnees(*pntrParam);
    }
    else if (idExo==ID_EXO_CONSTRUCTEUR_AVEC_TABLEAU)
    {
        pntrThis=nullptr;
        pntrTab=&jeuDeDonnees.getParamTab(jeuID);
        if (jeuDeDonnees.type==JeuDeDonnees::TABLEAU_1D)
            pntrRes=new StructDeDonnees(pntrTab->getArray()[0],
                                                    pntrTab->getNbColonnes());
        else
            pntrRes=new StructDeDonnees(pntrTab->getArray(),
                                                    pntrTab->getNbLignes(),
                                                    pntrTab->getNbColonnes());
    }
    else if (idExo==ID_EXO_DESTRUCTEUR)
    {
        pntrRes=new StructDeDonnees(*pntrThis);
        delete pntrRes;
        pntrRes=nullptr;
    }
    else if (idExo==ID_EXO_CONCATENATION_DEBUT)
    {
        pntrParam=jeuDeDonnees.template getParam<StructDeDonnees>(jeuID);
        pntrRes=new StructDeDonnees(*pntrThis);
        pntrRes->concatenationDebut(*pntrParam);
    }
    else if (idExo==ID_EXO_CONCATENATION_MILIEU)
    {
        paramIdxColonne=jeuDeDonnees.getIdxColonneDansThis(jeuID);
        pntrParam=jeuDeDonnees.template getParam<StructDeDonnees>(jeuID);
        pntrRes=new StructDeDonnees(*pntrThis);
        pntrRes->concatenationMilieu(paramIdxColonne,*pntrParam);
    }
    else if (idExo==ID_EXO_CONCATENATION_FIN)
    {
        pntrParam=jeuDeDonnees.template getParam<StructDeDonnees>(jeuID);
        pntrRes=new StructDeDonnees(*pntrThis);
        pntrRes->concatenationFin(*pntrParam);
    }
    else if (idExo==ID_EXO_OP_CONCATENATION)
    {
        pntrParam=jeuDeDonnees.template getParam<StructDeDonnees>(jeuID);
        pntrRes=new StructDeDonnees(*pntrThis);
        returnThis=(StructDeDonnees*)(&((*pntrRes)+=(*pntrParam)));
    }
    else if (idExo==ID_EXO_OP_EGAL)
    {
        pntrParam=jeuDeDonnees.template getParam<StructDeDonnees>(jeuID);
        returnBoolPntr=&returnBool;
        (*returnBoolPntr)=(*pntrThis)==(*pntrParam);
    }
    else if (idExo==ID_EXO_OP_DIFFERENT)
    {
        pntrParam=jeuDeDonnees.template getParam<StructDeDonnees>(jeuID);
        returnBoolPntr=&returnBool;
        (*returnBoolPntr)=(StructDeDonnees*)((*pntrThis)!=(*pntrParam));
    }
    else if (idExo==ID_EXO_OP_AFFECTATION)
    {
        pntrParam=jeuDeDonnees.template getParam<StructDeDonnees>(jeuID);
        pntrRes=new StructDeDonnees(*pntrThis);
        if (jeuDeDonnees.thisEgalThis(jeuID))       // Autre test possible: pntrThis==pntrParam
        {
            // Pour tester this=this
            returnThis=(StructDeDonnees*)(&((*pntrRes)=(*((const StructDeDonnees*)pntrRes))));
        }
        else
            returnThis=(StructDeDonnees*)(&((*pntrRes)=(*pntrParam)));
    }
    else if (idExo==ID_EXO_OP_CROCHET)
    {
        paramIdxColonne=jeuDeDonnees.getIdxColonneDansThis(jeuID);
        pntrTab=&jeuDeDonnees.getParamTab(jeuID);
        assert(pntrTab->getNbLignes()==1 && pntrTab->getNbColonnes()==1);
        pntrRes=new StructDeDonnees(*pntrThis);

        Double* val=&(pntrRes->operator[](paramIdxColonne));
        // On doit verifier si l'adresse retournee est une adresse dans un tableau
        if (GestionPntrEtComplexite::get().pntrDansTableauValide(val))
            (*val)=pntrTab->getArray()[0][0];
        else
            assert(0);
    }
    else if (idExo==ID_EXO_OP_CROCHET_CONST)
    {
        paramIdxColonne=jeuDeDonnees.getIdxColonneDansThis(jeuID);
        returnDoublePntr=&returnDouble;
        (*returnDoublePntr)=pntrThis->operator[](paramIdxColonne);
    }
    else if (idExo==ID_EXO_OP_PARENTHESE)
    {
        paramIdxLigne=jeuDeDonnees.getIdxLigneDansThis(jeuID);
        paramIdxColonne=jeuDeDonnees.getIdxColonneDansThis(jeuID);
        pntrRes=new StructDeDonnees(*pntrThis);
        pntrTab=&jeuDeDonnees.getParamTab(jeuID);
        assert(pntrTab->getNbLignes()==1 && pntrTab->getNbColonnes()==1);
        if (jeuDeDonnees.type==JeuDeDonnees::TABLEAU_1D)
        {
            Double* val=&(pntrRes->operator()(paramIdxColonne));
            // On doit verifier si l'adresse retournee est une adresse dans un tableau
            if (GestionPntrEtComplexite::get().pntrDansTableauValide(val))
                (*val)=pntrTab->getArray()[0][0];
        }
        else
        {
            Double* val=&(pntrRes->operator()(paramIdxLigne,paramIdxColonne));
            // On doit verifier si l'adresse retournee est une adresse dans un tableau
            if (GestionPntrEtComplexite::get().pntrDansTableauValide(val))
                (*val)=pntrTab->getArray()[0][0];
        }
    }
    else if (idExo==ID_EXO_OP_PARENTHESE_CONST)
    {
        paramIdxLigne=jeuDeDonnees.getIdxLigneDansThis(jeuID);
        paramIdxColonne=jeuDeDonnees.getIdxColonneDansThis(jeuID);
        returnDoublePntr=&returnDouble;
        if (jeuDeDonnees.type==JeuDeDonnees::TABLEAU_1D)
            (*returnDoublePntr)=pntrThis->operator()(paramIdxColonne);
        else
            (*returnDoublePntr)=pntrThis->operator()(paramIdxLigne,paramIdxColonne);
    }
    else if (idExo==ID_EXO_SETTER)
    {
        paramIdxLigne=jeuDeDonnees.getIdxLigneDansThis(jeuID);
        paramIdxColonne=jeuDeDonnees.getIdxColonneDansThis(jeuID);
        pntrRes=new StructDeDonnees(*pntrThis);
        pntrTab=&jeuDeDonnees.getParamTab(jeuID);
        assert(pntrTab->getNbLignes()==1 && pntrTab->getNbColonnes()==1);
        if (jeuDeDonnees.type==JeuDeDonnees::TABLEAU_1D)
            pntrRes->set(paramIdxColonne,pntrTab->getArray()[0][0]);
        else
            pntrRes->set(paramIdxLigne,paramIdxColonne,pntrTab->getArray()[0][0]);
    }
    else if (idExo==ID_EXO_GETTER)
    {
        paramIdxLigne=jeuDeDonnees.getIdxLigneDansThis(jeuID);
        paramIdxColonne=jeuDeDonnees.getIdxColonneDansThis(jeuID);
        returnDoublePntr=&returnDouble;
        if (jeuDeDonnees.type==JeuDeDonnees::TABLEAU_1D)
            (*returnDoublePntr)=pntrThis->get(paramIdxColonne);
        else
            (*returnDoublePntr)=pntrThis->get(paramIdxLigne,paramIdxColonne);
    }
    else if (idExo==ID_EXO_ADDITIONNER_A_TOUS_LES_ELTS)
    {
        pntrTab=&jeuDeDonnees.getParamTab(jeuID);
        assert(pntrTab->getNbLignes()==1 && pntrTab->getNbColonnes()==1);
        pntrRes=new StructDeDonnees(*pntrThis);
        pntrRes->additionnerATousLesElts(pntrTab->getArray()[0][0]);
    }
    else if (idExo==ID_EXO_INSERER_UN_ELT_DEBUT)
    {
        pntrRes=new StructDeDonnees(*pntrThis);
        pntrTab=&jeuDeDonnees.getParamTab(jeuID);
        if (jeuDeDonnees.type==JeuDeDonnees::TABLEAU_1D)
        {
            assert(pntrTab->getNbLignes()==1 && pntrTab->getNbColonnes()==1);
            pntrRes->insererUnEltDebut(pntrTab->getArray()[0][0]);
        }
        else
        {
            assert(pntrTab->getNbColonnes()==1);
            pntrTab->copieColonne0VersLigne0();
            pntrRes->insererUnEltDebut(pntrTab->getArray()[0],
                                                pntrTab->getNbLignes());
        }
    }
    else if (idExo==ID_EXO_INSERER_UN_ELT_MILIEU)
    {
        paramIdxColonne=jeuDeDonnees.getIdxColonneDansThis(jeuID);
        pntrRes=new StructDeDonnees(*pntrThis);
        pntrTab=&jeuDeDonnees.getParamTab(jeuID);
        if (jeuDeDonnees.type==JeuDeDonnees::TABLEAU_1D)
        {
            assert(pntrTab->getNbLignes()==1 && pntrTab->getNbColonnes()==1);
            pntrRes->insererUnEltMilieu(paramIdxColonne,pntrTab->getArray()[0][0]);
        }
        else
        {
            assert(pntrTab->getNbColonnes()==1);
            pntrTab->copieColonne0VersLigne0();
            pntrRes->insererUnEltMilieu(paramIdxColonne,
                       pntrTab->getArray()[0],
                       pntrTab->getNbLignes());
        }
    }
    else if (idExo==ID_EXO_INSERER_UN_ELT_FIN)
    {
        pntrTab=&jeuDeDonnees.getParamTab(jeuID);
        pntrRes=new StructDeDonnees(*pntrThis);
        if (jeuDeDonnees.type==JeuDeDonnees::TABLEAU_1D)
        {
            assert(pntrTab->getNbLignes()==1 && pntrTab->getNbColonnes()==1);
            pntrRes->insererUnEltFin(pntrTab->getArray()[0][0]);
        }
        else
        {
            assert(pntrTab->getNbColonnes()==1);
            pntrTab->copieColonne0VersLigne0();
            pntrRes->insererUnEltFin(pntrTab->getArray()[0],
                    pntrTab->getNbLignes());
        }
    }
    else if (idExo==ID_EXO_INSERER_PLUSIEURS_ELTS_DEBUT)
    {
        pntrRes=new StructDeDonnees(*pntrThis);
        pntrTab=&jeuDeDonnees.getParamTab(jeuID);
        if (jeuDeDonnees.type==JeuDeDonnees::TABLEAU_1D)
        {
            assert(pntrTab->getNbLignes()==1);
            pntrRes->insererPlusieursEltsDebut(pntrTab->getArray()[0],
                      pntrTab->getNbColonnes());
        }
        else
        {
            pntrRes->insererPlusieursEltsDebut(pntrTab->getArray(),
                      pntrTab->getNbLignes(),
                      pntrTab->getNbColonnes());
        }
    }
    else if (idExo==ID_EXO_INSERER_PLUSIEURS_ELTS_MILIEU)
    {
        pntrRes=new StructDeDonnees(*pntrThis);
        paramIdxColonne=jeuDeDonnees.getIdxColonneDansThis(jeuID);
        pntrTab=&jeuDeDonnees.getParamTab(jeuID);
        if (jeuDeDonnees.type==JeuDeDonnees::TABLEAU_1D)
        {
            assert(pntrTab->getNbLignes()==1);
            pntrRes->insererPlusieursEltsMilieu(paramIdxColonne,
                       pntrTab->getArray()[0],
                       pntrTab->getNbColonnes());
        }
        else
        {
            pntrRes->insererPlusieursEltsMilieu(paramIdxColonne,
                       pntrTab->getArray(),
                       pntrTab->getNbLignes(),
                       pntrTab->getNbColonnes());
        }
    }
    else if (idExo==ID_EXO_INSERER_PLUSIEURS_ELTS_FIN)
    {
        pntrRes=new StructDeDonnees(*pntrThis);
        pntrTab=&jeuDeDonnees.getParamTab(jeuID);
        if (jeuDeDonnees.type==JeuDeDonnees::TABLEAU_1D)
        {
            assert(pntrTab->getNbLignes()==1);
            pntrRes->insererPlusieursEltsFin(pntrTab->getArray()[0],
                    pntrTab->getNbColonnes());
        }
        else
        {
            pntrRes->insererPlusieursEltsFin(pntrTab->getArray(),
                    pntrTab->getNbLignes(),
                    pntrTab->getNbColonnes());
        }
    }
    else if (idExo==ID_EXO_ENLEVER_UN_ELT_DEBUT)
    {
        pntrRes=new StructDeDonnees(*pntrThis);
        pntrRes->enleverUnEltDebut();
    }
    else if (idExo==ID_EXO_ENLEVER_UN_ELT_MILIEU)
    {
        paramIdxColonne=jeuDeDonnees.getIdxColonneDansThis(jeuID);
        pntrRes=new StructDeDonnees(*pntrThis);
        pntrRes->enleverUnEltMilieu(paramIdxColonne);
    }
    else if (idExo==ID_EXO_ENLEVER_UN_ELT_FIN)
    {
        pntrRes=new StructDeDonnees(*pntrThis);
        pntrRes->enleverUnEltFin();
    }
    else if (idExo==ID_EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT)
    {
        paramNb=jeuDeDonnees.getParamTab(jeuID).getNbColonnes();
        pntrRes=new StructDeDonnees(*pntrThis);
        pntrRes->enleverPlusieursEltsDebut(paramNb);
    }
    else if (idExo==ID_EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU)
    {
        paramIdxColonne=jeuDeDonnees.getIdxColonneDansThis(jeuID);
        paramNb=jeuDeDonnees.getParamTab(jeuID).getNbColonnes();
        pntrRes=new StructDeDonnees(*pntrThis);
        pntrRes->enleverPlusieursEltsMilieu(paramIdxColonne,paramNb);
    }
    else if (idExo==ID_EXO_ENLEVER_PLUSIEURS_ELTS_FIN)
    {
        paramNb=jeuDeDonnees.getParamTab(jeuID).getNbColonnes();
        pntrRes=new StructDeDonnees(*pntrThis);
        pntrRes->enleverPlusieursEltsFin(paramNb);
    }
    else if (idExo==ID_EXO_ELTS_EN_ORDRE_DECROISSANT)
    {
        returnBoolPntr=&returnBool;
        (*returnBoolPntr)=pntrThis->eltsEnOrdreDecroissant();
    }
    else if (idExo==ID_EXO_CONTIENT_ELT)
    {
        pntrTab=&jeuDeDonnees.getParamTab(jeuID);
        assert(pntrTab->getNbLignes()==1 && pntrTab->getNbColonnes()==1);
        returnBoolPntr=&returnBool;
        (*returnBoolPntr)=pntrThis->contientElt(pntrTab->getArray()[0][0]);
    }
    else if (idExo==ID_EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT)
    {
        pntrTab=&jeuDeDonnees.getParamTab(jeuID);
        assert(pntrTab->getNbLignes()==1 && pntrTab->getNbColonnes()==1);
        pntrRes=new StructDeDonnees(*pntrThis);
        pntrRes->enleverToutesLesOccurencesDUnElt(pntrTab->getArray()[0][0]);
    }
    else if (idExo==ID_EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT)
    {
        pntrTab=&jeuDeDonnees.getParamTab(jeuID);
        assert(pntrTab->getNbLignes()==1 && pntrTab->getNbColonnes()==1);
        returnIntPntr=&returnInt;
        (*returnIntPntr)=pntrThis->chercherLIndexDeLaPremiereOccurenceDUnElt(
                                pntrTab->getArray()[0][0]);
    }
    else if (idExo==ID_EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT)
    {
        pntrTab=&jeuDeDonnees.getParamTab(jeuID);
        assert(pntrTab->getNbLignes()==1 && pntrTab->getNbColonnes()==1);
        returnIntPntr=&returnInt;
        (*returnIntPntr)=pntrThis->chercherLIndexDeLaDerniereOccurenceDUnElt(
                                pntrTab->getArray()[0][0]);
    }
    else if (idExo==ID_EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT)
    {
        pntrTab=&jeuDeDonnees.getParamTab(jeuID);
        assert(pntrTab->getNbLignes()==1 && pntrTab->getNbColonnes()==1);
        returnIntPntr=&returnInt;
        (*returnIntPntr)=pntrThis->calculerNombreDOccurencesDUnElt(pntrTab->getArray()[0][0]);
    }
    else if (idExo==ID_EXO_OP_ADDITION)
    {
        pntrParam=jeuDeDonnees.template getParam<StructDeDonnees>(jeuID);
        // Ne pas utiliser a=b+c. Sinon, l'operateur d'affectation est aussi utilise
        // Si le return n'est pas implemente, ca peut planter dans la fonction
        // sans pouvoir afficher a l'etudiant que le return en manquant
        pntrRes=new StructDeDonnees((*pntrThis)+(*pntrParam));
    }
    else if (idExo==ID_EXO_OP_SOUSTRACTION)
    {
        pntrParam=jeuDeDonnees.template getParam<StructDeDonnees>(jeuID);
        // Ne pas utiliser a=b+c. Sinon, l'operateur d'affectation est aussi utilise
        // Si le return n'est pas implemente, ca peut planter dans la fonction
        // sans pouvoir afficher a l'etudiant que le return en manquant
        pntrRes=new StructDeDonnees((*pntrThis)-(*pntrParam));
    }
#ifdef ITERATEUR
    else if (idExo==ID_EXO_CREER_ITERATEUR)
    {
        pntrRes=new StructDeDonnees(*pntrThis);
        (*it)=new typename StructDeDonnees::ITERATEUR(pntrRes->premier());
    }
    else if (idExo==ID_EXO_OP_PLUS_PLUS_ITERATEUR)
    {
        pntrRes=new StructDeDonnees(*pntrThis);
        (*it)=new typename StructDeDonnees::ITERATEUR(pntrRes->premier());
        int nbIter=jeuDeDonnees.getIdxColonneDansThis(jeuID);
        for (int i=0;i<nbIter;i++)
            (**it)=(*it)->operator++();
    }
    else if (idExo==ID_EXO_OP_MOINS_MOINS_ITERATEUR)
    {
        pntrRes=new StructDeDonnees(*pntrThis);
        (*it)=new typename StructDeDonnees::ITERATEUR(pntrRes->premier());
        int nbIter=jeuDeDonnees.getIdxColonneDansThis(jeuID);
        for (int i=0;i<nbIter;i++)
            (**it)=(*it)->operator--();
    }
    else if (idExo==ID_EXO_OP_ETOILE_ITERATEUR)
    {
        pntrRes=new StructDeDonnees(*pntrThis);
        (*it)=new typename StructDeDonnees::ITERATEUR(pntrRes->premier());
        (*it)->operator++();
        Double* val=&((*it)->operator*());
        // On doit verifier si l'adresse retournee est une adresse dans un tableau
        pntrTab=&jeuDeDonnees.getParamTab(jeuID);
        if (GestionPntrEtComplexite::get().pntrDansTableauValide(val))
            (*val)=pntrTab->getArray()[0][0];
    }
    else if (idExo==ID_EXO_OP_ETOILE_CONST_ITERATEUR)
    {
        (*it)=new typename StructDeDonnees::ITERATEUR(((StructDeDonnees*)pntrThis)->premier());
        (*it)->operator++();
        returnDoublePntr=&returnDouble;
        (*returnDoublePntr)=((const typename StructDeDonnees::ITERATEUR*)*it)->operator*();
    }
    else if (idExo==ID_EXO_OP_FIN_ITERATEUR)
    {
        (*it)=new typename StructDeDonnees::ITERATEUR(((StructDeDonnees*)pntrThis)->premier());
        int nbIter=jeuDeDonnees.getIdxColonneDansThis(jeuID);
        for (int i=0;i<nbIter;i++)
            (**it)=(*it)->operator++();
        returnBoolPntr=&returnBool;
        (*returnBoolPntr)=((typename StructDeDonnees::ITERATEUR*)*it)->fin();
    }
#endif
    else
        assert(0);

    GestionPntrEtComplexite::get().stopCompterInstructions();
}

template <class StructDeDonneesCorr, class StructDeDonneesEtud>
ResultatEvaluation Evaluer<StructDeDonneesCorr, StructDeDonneesEtud>::evalUnExercice(double coeffExo, int idExo)
{
    //////////////////////////////////
    // Recupere le nom de la classe //
    //////////////////////////////////
    char nomClasse[255]={};
    void *ptrInstruction=nullptr;
	backtrace(&ptrInstruction, 1);          // Recupere le pointeur de l'instruction courante
    // dladdr retourne 0 le pointeur ne correspond a aucun objet
    Dl_info info{};
    assert(dladdr(ptrInstruction, &info));
    // Pour que le nom de la methode s'affiche, il faut utiliser l'option -rdynamic pour le linker
    if(info.dli_sname!=nullptr)
    {
        // Retrouve le nom original dans le fichier source de la methode
        int status = -1;
        char *demangledName = abi::__cxa_demangle(info.dli_sname, NULL, 0, &status);
        if (demangledName!=nullptr)
            GestionPntrEtComplexite::get().extraitNomClasseEtMethode(demangledName, nomClasse);
        free(demangledName);
    }
    else
    {
        cout<<"Il faut utiliser l'option -rdynamic pour le linker"<<endl;
        //assert(0);
    }
    //////////////////////////////////////////////////////////
    // Definit si c'est une structure de données 1D ou 2D   //
    // Pour une structure 1D, si on lui donne une structure //
    // 2D, elle genere une structure 1D                     //
    //////////////////////////////////////////////////////////
    int dimStructDeDonnees=JeuDeDonnees::TABLEAU_1D;
    StructDeDonneesVerifCorr tab2x2({{0,0},{0,0}});
    StructDeDonneesCorr structDeDonnees(tab2x2);
    if (structDeDonnees.getStructDeDonneesVerifCorr().getNbLignes()==tab2x2.getNbLignes() &&
        structDeDonnees.getStructDeDonneesVerifCorr().getNbColonnes()==tab2x2.getNbColonnes())
        dimStructDeDonnees=JeuDeDonnees::TABLEAU_2D;

    ////////////////////////////////
    // Creation du jeu de donnees //
    ////////////////////////////////
    JeuDeDonnees jeuDeDonnees(dimStructDeDonnees);
    if (idExo==ID_EXO_CONSTRUCTEUR)
    {
    }
    else if (idExo==ID_EXO_CONSTRUCTEUR_PAR_RECOPIE)
    {
        jeuDeDonnees.definirParam({}, {{1}}, {{1, 2 ,3},{4, 5 ,6}}, {{1, 2 ,3, 4}});
    }
    else if (idExo==ID_EXO_CONSTRUCTEUR_AVEC_TABLEAU)
    {
        jeuDeDonnees.definirParam({}, {{1}}, {{1, 2 ,3},{1, 2 ,3}}, {{1, 2 ,3, 4}});
    }
    else if (idExo==ID_EXO_DESTRUCTEUR)
    {
        jeuDeDonnees.definirThis({},{{1},{2}}, {{1, 2 ,3}}, {{1, 2 ,3, 4},{1, 2 ,3, 4},{1, 2 ,3, 4}});
    }
    else if (idExo==ID_EXO_CONCATENATION_DEBUT)
    {
        jeuDeDonnees.definirThis({}, {{1},{2}}, {{2, 3 ,4},{5, 6 ,7}});
        jeuDeDonnees.definirParam({}, {{8},{9}}, {{10, 11 ,12},{13, 14 ,15}});
    }
    else if (idExo==ID_EXO_CONCATENATION_MILIEU)
    {
        jeuDeDonnees.definirThis({}, {{1},{2}}, {{2, 3 ,4},{5, 6 ,7}});
        jeuDeDonnees.definirParam({}, {{8},{9}}, {{10, 11 ,12},{13, 14 ,15}});
        jeuDeDonnees.definirIdxLigneColonneDansThis({{{0},{0}}, {{0},{1}} ,{{0},{2}}, {{0},{3}}});
    }
    else if (idExo==ID_EXO_CONCATENATION_FIN)
    {
        jeuDeDonnees.definirThis({}, {{1},{2}}, {{2, 3 ,4},{5, 6 ,7}});
        jeuDeDonnees.definirParam({}, {{8},{9}}, {{10, 11 ,12},{13, 14 ,15}});
    }
    else if (idExo==ID_EXO_OP_CONCATENATION)
    {
        jeuDeDonnees.definirThis({}, {{1},{2}}, {{2, 3 ,4},{5, 6 ,7}});
        jeuDeDonnees.definirParam({}, {{8},{9}}, {{10, 11 ,12},{13, 14 ,15}});
    }
    else if (idExo==ID_EXO_OP_EGAL)
    {
        jeuDeDonnees.inclureThisEgalThis();
        jeuDeDonnees.definirThis({}, {{9}}, {{2, 3 ,4}}, {{5, 6 ,7, 9},{8, 9 ,10, 12}});
        jeuDeDonnees.definirParam({}, {{9}}, {{5, 6 ,7, 8}}, {{5, 6 ,7, 8},{9, 10 ,11, 12}});
    }
    else if (idExo==ID_EXO_OP_DIFFERENT)
    {
        jeuDeDonnees.inclureThisEgalThis();
        jeuDeDonnees.definirThis({}, {{9}}, {{2, 3 ,4}}, {{5, 6 ,7},{8, 9 ,10}});
        jeuDeDonnees.definirParam({}, {{9}}, {{5, 6 ,7}}, {{5, 6 ,7, 8},{9, 10 ,11, 12}});
    }
    else if (idExo==ID_EXO_OP_AFFECTATION)
    {
        jeuDeDonnees.inclureThisEgalThis();
        jeuDeDonnees.definirThis( {}, {{0},{1}}, {{2, 3 , 4},{5, 6, 7}},
                                            {{8, 9, 10},{11, 12, 13},{14, 15, 16}});
        jeuDeDonnees.definirParam({}, {{17},{18}}, {{19, 20, 21},{22, 23, 24}},
                                            {{25, 26, 27},{28, 29, 30},{31, 32, 33}});
    }
    else if (idExo==ID_EXO_OP_CROCHET)
    {
        jeuDeDonnees.definirThis({{2, 3 ,4, 5}});
        jeuDeDonnees.definirIdxLigneColonneDansThis({{{0},{0}}, {{0},{1}} ,{{0},{2}}, {{0},{3}}});
        jeuDeDonnees.definirParam({{100}});
    }
    else if (idExo==ID_EXO_OP_CROCHET_CONST)
    {
        jeuDeDonnees.definirThis({{2, 3 ,4, 5}});
        jeuDeDonnees.definirIdxLigneColonneDansThis({{{0},{0}}, {{0},{1}} ,{{0},{2}}, {{0},{3}}});
    }
    else if (idExo==ID_EXO_OP_PARENTHESE)
    {
        jeuDeDonnees.definirThis({{2, 3 , 4, 5},{6, 7 , 8, 9}});
        jeuDeDonnees.definirIdxLigneColonneDansThis({{{0},{0}}, {{1},{1}} ,{{0},{2}}, {{1},{3}}});
        jeuDeDonnees.definirParam({{100}});
    }
    else if (idExo==ID_EXO_OP_PARENTHESE_CONST)
    {
        jeuDeDonnees.definirThis({{12, 13 , 14, 15},{16, 17 , 18, 19}});
        jeuDeDonnees.definirIdxLigneColonneDansThis({{{0},{0}}, {{1},{1}} ,{{0},{2}}, {{1},{3}}});
    }
    else if (idExo==ID_EXO_SETTER)
    {
        jeuDeDonnees.definirThis({{2, 3 , 4, 5},{6, 7 , 8, 9}});
        jeuDeDonnees.definirIdxLigneColonneDansThis({{{0},{0}}, {{1},{1}} ,{{0},{2}}, {{1},{3}}});
        jeuDeDonnees.definirParam({{100}});
    }
    else if (idExo==ID_EXO_GETTER)
    {
        jeuDeDonnees.definirThis({{2, 3 , 4, 5},{6, 7 , 8, 9}});
        jeuDeDonnees.definirIdxLigneColonneDansThis({{{0},{0}}, {{1},{1}} ,{{0},{2}}, {{1},{3}}});
    }
    else if (idExo==ID_EXO_ADDITIONNER_A_TOUS_LES_ELTS)
    {
        jeuDeDonnees.definirThis({}, {{1},{2}}, {{3, 4 , 5},{6, 7 , 8}});
        jeuDeDonnees.definirParam({{100}});
    }
    else if (idExo==ID_EXO_INSERER_UN_ELT_DEBUT)
    {
        jeuDeDonnees.definirThis({}, {{1},{2}}, {{4, 5 , 6},{7, 8 , 9}});
        jeuDeDonnees.definirParam({{100},{101}});
    }
    else if (idExo==ID_EXO_INSERER_UN_ELT_MILIEU)
    {
        jeuDeDonnees.definirThis({}, {{1},{2}}, {{4, 5 , 6},{7, 8 , 9}});
        jeuDeDonnees.definirParam({{100},{101}});
        jeuDeDonnees.definirIdxLigneColonneDansThis({{{0},{0}}, {{0},{1}} ,{{0},{2}}, {{0},{3}}});
    }
    else if (idExo==ID_EXO_INSERER_UN_ELT_FIN)
    {
        jeuDeDonnees.definirThis({}, {{1},{2}}, {{4, 5 , 6},{7, 8 , 9}});
        jeuDeDonnees.definirParam({{100},{101}});
    }
    else if (idExo==ID_EXO_INSERER_PLUSIEURS_ELTS_DEBUT)
    {
        jeuDeDonnees.definirThis({}, {{1},{2}}, {{3, 4 ,5},{6, 7 ,8}});
        jeuDeDonnees.definirParam({{9},{10}}, {{11, 12 ,13},{14, 15 ,16}});
    }
    else if (idExo==ID_EXO_INSERER_PLUSIEURS_ELTS_MILIEU)
    {
        jeuDeDonnees.definirThis({}, {{1},{2}}, {{3, 4 ,5},{6, 7 ,8}});
        jeuDeDonnees.definirParam({{9},{10}}, {{11, 12 ,13},{14, 15 ,16}});
        jeuDeDonnees.definirIdxLigneColonneDansThis({{{0},{0}}, {{0},{1}} ,{{0},{2}}, {{0},{3}}});
    }
    else if (idExo==ID_EXO_INSERER_PLUSIEURS_ELTS_FIN)
    {
        jeuDeDonnees.definirThis({}, {{1},{2}}, {{3, 4 ,5},{6, 7 ,8}});
        jeuDeDonnees.definirParam({{9},{10}}, {{11, 12 ,13},{14, 15 ,16}});
    }
    else if (idExo==ID_EXO_ENLEVER_UN_ELT_DEBUT)
    {
        jeuDeDonnees.definirThis({}, {{1},{2}}, {{3, 4 ,5},{6, 7 ,8}});
    }
    else if (idExo==ID_EXO_ENLEVER_UN_ELT_MILIEU)
    {
        jeuDeDonnees.definirThis({}, {{1},{2}}, {{3, 4 ,5},{6, 7 ,8}});
        jeuDeDonnees.definirIdxLigneColonneDansThis({{{0},{0}}, {{0},{1}} ,{{0},{2}}});
    }
    else if (idExo==ID_EXO_ENLEVER_UN_ELT_FIN)
    {
        jeuDeDonnees.definirThis({}, {{1},{2}}, {{3, 4 ,5},{6, 7 ,8}});
    }
    else if (idExo==ID_EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT)
    {
        jeuDeDonnees.definirThis({}, {{1},{2}}, {{2, 3 ,4},{5, 6 ,7}});
        jeuDeDonnees.definirParam({}, {{9}}, {{10, 11 ,12}});
    }
    else if (idExo==ID_EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU)
    {
        jeuDeDonnees.definirThis({}, {{1},{2}}, {{2, 3 ,4},{5, 6 ,7}});
        jeuDeDonnees.definirParam({}, {{9}}, {{10, 11 ,12}});
        jeuDeDonnees.definirIdxLigneColonneDansThis({{{0},{0}}, {{0},{1}} ,{{0},{2}}, {{0},{3}}});
    }
    else if (idExo==ID_EXO_ENLEVER_PLUSIEURS_ELTS_FIN)
    {
        jeuDeDonnees.definirThis({}, {{1},{2}}, {{2, 3 ,4},{5, 6 ,7}});
        jeuDeDonnees.definirParam({}, {{9}}, {{10, 11 ,12}});
    }
    else if (idExo==ID_EXO_ELTS_EN_ORDRE_DECROISSANT)
    {
        jeuDeDonnees.definirThis({}, {{1}}, {{4, 3 ,2}}, {{15, 3 ,4}});
    }
    else if (idExo==ID_EXO_CONTIENT_ELT)
    {
        jeuDeDonnees.definirThis({}, {{1}}, {{4, 3 ,2},{6, 5 ,4}}, {{15, 3 ,4}});
        jeuDeDonnees.definirParam({{1}}, {{3}}, {{15}});
    }
    else if (idExo==ID_EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT)
    {
        jeuDeDonnees.definirThis({}, {{1}}, {{3, 4 ,3}}, {{15, 15 ,15, 15}});
        jeuDeDonnees.definirParam({{1}}, {{3}}, {{15}});
    }
    else if (idExo==ID_EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT)
    {
        jeuDeDonnees.definirThis({}, {{1}}, {{16}}, {{3, 2 ,15}});
        jeuDeDonnees.definirParam({{1}}, {{3}}, {{15}});
    }
    else if (idExo==ID_EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT)
    {
        jeuDeDonnees.definirThis({}, {{1}}, {{16}}, {{3, 2 ,15}});
        jeuDeDonnees.definirParam({{1}}, {{3}}, {{15}});
    }
    else if (idExo==ID_EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT)
    {
        jeuDeDonnees.definirThis({}, {{1}}, {{3, 2 ,15},{3, 2 ,15}}, {{15, 15, 15}});
        jeuDeDonnees.definirParam({{1}}, {{3}}, {{15}});
    }
    else if (idExo==ID_EXO_OP_ADDITION)
    {
        jeuDeDonnees.definirThis({{0, 1 ,2},{3, 4 ,5}});
        jeuDeDonnees.definirParam({{6, 7 ,8},{9, 10 ,11}});
    }
    else if (idExo==ID_EXO_OP_SOUSTRACTION)
    {
        jeuDeDonnees.definirThis({{0, 1 ,2},{3, 4 ,5}});
        jeuDeDonnees.definirParam({{6, 7 ,8},{9, 10 ,11}});
    }
    else if (idExo==ID_EXO_CREER_ITERATEUR)
    {
        jeuDeDonnees.definirThis({{0}});
    }
    else if (idExo==ID_EXO_OP_PLUS_PLUS_ITERATEUR)
    {
        jeuDeDonnees.definirThis({{1,2,3}});
        jeuDeDonnees.definirIdxLigneColonneDansThis({{{0},{1}}, {{0},{2}}, {{0},{3}}});
    }
    else if (idExo==ID_EXO_OP_MOINS_MOINS_ITERATEUR)
    {
        jeuDeDonnees.definirThis({{1,2,3}});
        jeuDeDonnees.definirIdxLigneColonneDansThis({{{0},{1}}, {{0},{2}}, {{0},{3}}});
    }
    else if (idExo==ID_EXO_OP_ETOILE_ITERATEUR)
    {
        jeuDeDonnees.definirThis({{1,2,3}});
        jeuDeDonnees.definirParam({{100}});
    }
    else if (idExo==ID_EXO_OP_ETOILE_CONST_ITERATEUR)
    {
        jeuDeDonnees.definirThis({{1,2,3}});
    }
    else if (idExo==ID_EXO_OP_FIN_ITERATEUR)
    {
        jeuDeDonnees.definirThis({{1,2,3}});
        jeuDeDonnees.definirIdxLigneColonneDansThis({{{0},{0}}, {{0},{1}}, {{0},{3}}});
    }

    // Genere le jeu de donnees
    jeuDeDonnees.generer();

    // Variable pour le calcul de la note pour tout l'exercice
    ResultatEvaluation resultEvalExercice(coeffExo);

    // Variable pour la gestion des process
    ProcessJeuDeDonnees processJeuDeDonnees[jeuDeDonnees.taille()];

    // Chaque test sur le jeu de donnees est execute dans un process separe
    // Verifier que Caseine permet suffisemment de process pour l'evaluation
    // Par defaut, c'est 100 process


    // Evalue la methode sur tout le jeu de donnees
    for (int jeuID=0;jeuID<jeuDeDonnees.taille();jeuID++)
    {
        // Alloue la memoire partagee entre les process
        processJeuDeDonnees[jeuID].resultEval = (ResultatEvaluation*)mmap(NULL, sizeof(ResultatEvaluation), PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS, -1, 0);
        processJeuDeDonnees[jeuID].sharedMsgErrPntr = (char*)mmap(NULL, 4096*sizeof(char), PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS, -1, 0);
        processJeuDeDonnees[jeuID].sharedMsgErrAlgo = (char*)mmap(NULL, 4096*sizeof(char), PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS, -1, 0);
        processJeuDeDonnees[jeuID].idExo=idExo;

        // Libere les process deja demarres
        // Attend jusqu'a ce que suffisamment de process se terminent
        // Mettre nbMaxProcesses a 1 pour avoir les process executes sequentiellement
        nbMaxProcesses=10;
        while(termineProcessJeuDeDonnees(resultEvalExercice,processJeuDeDonnees,jeuDeDonnees)>=nbMaxProcesses)
            ;

#ifndef DESACTIVE_EVALUATION_PARALLELE
        // Demarre le process suivant
        int nbEssais=0;
        while((processJeuDeDonnees[jeuID].pid=fork())==-1 && nbEssais<20)
        {
            // Trop de forks sont appeles en meme temps. On fait une pause de 5000 us
            usleep(5000);
            // Libere les process deja demarres
            termineProcessJeuDeDonnees(resultEvalExercice,processJeuDeDonnees,jeuDeDonnees);
            nbEssais++;
#ifdef DEBUG
            cout<<"Process: "<<nbEssais<<" essai(s) pour "<<idExo<<", "<<jeuID<<endl;
#endif
        }
        if (processJeuDeDonnees[jeuID].pid==-1)
            cout<<"Process pas demarre"<<idExo<<", "<<jeuID<<endl;
        else
            // Enregistre le temps de demarrage du process
            processJeuDeDonnees[jeuID].startTime=clock();

#ifdef DEBUG
        if(processJeuDeDonnees[jeuID].pid>0)
            cout<<"Process demarre: (\""<<idExo<<"\", "<<jeuID<<")"<<endl;
#endif

        if(processJeuDeDonnees[jeuID].pid==0)
#else
        processJeuDeDonnees[jeuID].pid=1;
#endif // DESACTIVE_EVALUATION_PARALLELE
        {
            // Demarrage du process enfant
            // Definir les variables statiques au cas ou sighandler est appele
            Evaluer::idExo=idExo;
            Evaluer::jeuID=jeuID;

            msgErrPntrJeuDeDonnees=processJeuDeDonnees[jeuID].sharedMsgErrPntr;
            msgErrAlgoJeuDeDonnees=processJeuDeDonnees[jeuID].sharedMsgErrAlgo;
            resultEvalJeuDeDonnees=processJeuDeDonnees[jeuID].resultEval;

            resultEvalJeuDeDonnees->manquante=false;
            resultEvalJeuDeDonnees->enteteIncorrect=false;
            memset(resultEvalJeuDeDonnees->nomMethodeExo,0x00,sizeof(resultEvalJeuDeDonnees->nomMethodeExo));
            // Par defaut, correct est false pour gerer le cas ou le code se crashe avant le calcul de la note
            resultEvalJeuDeDonnees->correct=false;


            // Variables pour evaluer la correction
            // pour la gestion des boucles et des pointeurs
            GestionPntrEtComplexite::TabAppelsInstructions tabInstructionsCorr, tabInstructionsEtud;

            StructDeDonneesVerifCorr* pntrTab=nullptr;
            Int paramNb, paramIdxColonne, paramIdxLigne;

            StructDeDonneesCorr *corrPntrRes=nullptr;
            const StructDeDonneesCorr* corrPntrThis=nullptr;
            const StructDeDonneesCorr* corrPntrParam=nullptr;
            StructDeDonneesCorr* corrReturnThis=nullptr;
            double* corrReturnDoublePntr=nullptr;
            double corrReturnDouble;
            bool* corrReturnBoolPntr=nullptr;
            bool corrReturnBool;
            int* corrReturnIntPntr=nullptr;
            int corrReturnInt;
            typename StructDeDonneesCorr::ITERATEUR *corrIt=nullptr;

            StructDeDonneesEtud *etudPntrRes=nullptr;
            const StructDeDonneesEtud *etudPntrThis=nullptr;
            const StructDeDonneesEtud *etudPntrParam=nullptr;
            StructDeDonneesEtud* etudReturnThis=nullptr;
            double* etudReturnDoublePntr=nullptr;
            double etudReturnDouble;
            bool* etudReturnBoolPntr=nullptr;
            bool etudReturnBool;
            int* etudReturnIntPntr=nullptr;
            int etudReturnInt;
            typename StructDeDonneesEtud::ITERATEUR *etudIt=nullptr;

            // Evalue la correction
            evalUnjeuDeDonnees(idExo,jeuDeDonnees,corrPntrRes,corrPntrThis,corrPntrParam,
                               pntrTab, paramNb, paramIdxColonne, paramIdxLigne,
                               corrReturnBoolPntr, corrReturnBool,
                               corrReturnIntPntr, corrReturnInt,
                               corrReturnDoublePntr, corrReturnDouble,
                               corrReturnThis, &corrIt);
            tabInstructionsCorr=GestionPntrEtComplexite::get().recupereTabAppelInstructions();


#ifdef VERIF_CORRECTION
            // Verifie la correction
            // Verifie l'integite de la struct de donnees de la correction
            if (corrPntrRes!=nullptr)
                if (StructDeDonneesCorr::verifierIntegrite(*(StructDeDonneesEtud*)corrPntrRes)==false)
                {
                    cout<<"Correction pas bonne pour "<<idExo<<endl;
                    cout<<"Struct. de données mal construite"<<endl;
                }

            StructDeDonneesVerifCorr *verifCorrPntrRes=nullptr;
            const StructDeDonneesVerifCorr *verifCorrPntrThis=nullptr;
            const StructDeDonneesVerifCorr* verifCorrPntrParam=nullptr;
            StructDeDonneesVerifCorr* verifCorrReturnThis=nullptr;
            double* verifCorrReturnDoublePntr=nullptr;
            double verifCorrReturnDouble;
            bool* verifCorrReturnBoolPntr=nullptr;
            bool verifCorrReturnBool;
            int* verifCorrReturnIntPntr=nullptr;
            int verifCorrReturnInt;
            typename StructDeDonneesVerifCorr::ITERATEUR *verifCorrIt=nullptr;

            evalUnjeuDeDonnees(idExo,jeuDeDonnees,verifCorrPntrRes,verifCorrPntrThis,verifCorrPntrParam,
                               pntrTab, paramNb, paramIdxColonne, paramIdxLigne,
                               verifCorrReturnBoolPntr, verifCorrReturnBool,
                               verifCorrReturnIntPntr, verifCorrReturnInt,
                               verifCorrReturnDoublePntr, verifCorrReturnDouble,
                               verifCorrReturnThis,&verifCorrIt);


            // Compare notre correction avec celle de la classe generique
            // Ceci est pour detecter les erreurs dans notre correction
            if (verifCorrPntrRes!=nullptr && (*verifCorrPntrRes)!=(*corrPntrRes))
            {
                cout<<"Correction pas bonne pour "<<idExo<<endl;
                if (corrPntrThis)
                    cout<<"this:               "<<corrPntrThis->getStructDeDonneesVerifCorr().toString()<<endl;
                if (corrPntrParam)
                    cout<<"parametre:          "<<corrPntrParam->getStructDeDonneesVerifCorr().toString()<<endl;
                if (pntrTab)
                    cout<<"parametre:          "<<pntrTab->toString()<<endl;
                if (paramIdxLigne!=-1 && paramIdxColonne!=-1)
                {
                    cout<<"index ligne:        "<<paramIdxLigne<<endl;
                    cout<<"index colonne:      "<<paramIdxColonne<<endl;
                }
                else if (paramIdxColonne!=-1)
                    cout<<"index:              "<<paramIdxColonne<<endl;
                if (paramNb!=-1)
                    cout<<"nb:                 "<<paramNb<<endl;
                cout<<"resultat a obtenir: "<<verifCorrPntrRes->toString()<<endl;
                cout<<"correction:         "<<corrPntrRes->getStructDeDonneesVerifCorr().toString()<<endl;
                resultEvalJeuDeDonnees->correct=false;
            }
            if (verifCorrReturnBoolPntr!=nullptr && (*verifCorrReturnBoolPntr)!=(*verifCorrReturnBoolPntr))
            {
                cout<<"Correction pas bonne pour "<<idExo<<endl;
                resultEvalJeuDeDonnees->correct=false;
            }
            if (verifCorrReturnIntPntr!=nullptr && (*verifCorrReturnIntPntr)!=(*corrReturnIntPntr))
            {
                cout<<"Correction pas bonne pour "<<idExo<<endl;
                resultEvalJeuDeDonnees->correct=false;
            }
            if (verifCorrReturnDoublePntr!=nullptr && !tabInstructionsCorr.contientReturn())
            {
                cout<<"Pas de return pour "<<idExo<<endl;
                resultEvalJeuDeDonnees->correct=false;
            }
            if (verifCorrReturnDoublePntr!=nullptr && (*verifCorrReturnDoublePntr)!=(*corrReturnDoublePntr))
            {
                cout<<"Correction pas bonne pour "<<idExo<<endl;
                resultEvalJeuDeDonnees->correct=false;
            }
            if(verifCorrReturnThis!=nullptr && corrReturnThis!=corrPntrRes)
            {
                cout<<"Return *this manquant pour "<<idExo<<endl;
                resultEvalJeuDeDonnees->correct=false;
            }
            // Libere les pointeurs pour la verification de la correction
            delete verifCorrPntrRes;
            verifCorrPntrRes=nullptr;
            delete verifCorrPntrThis;
            verifCorrPntrThis=nullptr;
            delete verifCorrPntrParam;
            verifCorrPntrParam=nullptr;
            delete verifCorrPntrRes;
            verifCorrPntrRes=nullptr;
            delete verifCorrIt;
            verifCorrIt=nullptr;

#endif // VERIF_CORRECTION

            // Evalue la solution de l'etudiant
            GestionPntrEtComplexite::get().msgErreur=msgErrPntrJeuDeDonnees;
            GestionPntrEtComplexite::get().exitApresErreur=true;
            evalUnjeuDeDonnees(idExo,jeuDeDonnees,etudPntrRes,etudPntrThis,etudPntrParam,
                               pntrTab, paramNb, paramIdxColonne, paramIdxLigne,
                               etudReturnBoolPntr, etudReturnBool,
                               etudReturnIntPntr, etudReturnInt,
                               etudReturnDoublePntr, etudReturnDouble,
                               etudReturnThis, &etudIt);
            tabInstructionsEtud=GestionPntrEtComplexite::get().recupereTabAppelInstructions();

            // Arret du controle de la gestion des pointeurs
            GestionPntrEtComplexite::get().msgErreur=nullptr;
            GestionPntrEtComplexite::get().exitApresErreur=false;


            if (resultEvalJeuDeDonnees->manquante)
            {
                // Methode par encore ecrite par l'etudiant. Pas besoin d'aller plus loin.
                resultEvalJeuDeDonnees->correct=false;
                // Libere les pointeurs
                delete corrPntrRes;
                corrPntrRes=nullptr;
                //delete etudPntrRes;           // Evite le plantage mais provoque fuite de mémoire
                etudPntrRes=nullptr;
                delete corrIt;
                corrIt=nullptr;
                delete etudIt;
                etudIt=nullptr;
#ifndef DESACTIVE_EVALUATION_PARALLELE
                exit(0);
#else
                continue;
#endif
            }
            // Recupere la liste des instructions du code de l'etudiants
            tabInstructionsEtud=GestionPntrEtComplexite::get().recupereTabAppelInstructions();

            // Supprime les instruction qui sont les memes dans la correction et le code de l'etudiant
            // Plus necessaire parce que le code execute pour la correction et l'etudiant doit etre identique
            //tabInstructionsCorr.supprInstructionsIdentiques(tabInstructionsEtud);

            // Supprime les instructions appartenant à la classe Evaluer
            tabInstructionsCorr.supprInstructionsDUneClasse(nomClasse);
            tabInstructionsEtud.supprInstructionsDUneClasse(nomClasse);

            // Le code de l'etudiant a pu etre execute. On peut maintenant evaluer le resultat sans
            // risquer de crash
            resultEvalJeuDeDonnees->correct=true;

            // S'il y a deja un message d'erreur de pointeurs ou de boucle infinie, on met correct a false
            if (strlen(msgErrPntrJeuDeDonnees)>0)
                resultEvalJeuDeDonnees->correct=false;

            // Verifie bien si l'instruction "return ...;" existe bien pour les methodes qui retournent
            // un bouleen ou un entier
            if (resultEvalJeuDeDonnees->correct && (etudReturnBoolPntr!=nullptr ||
                                                    etudReturnIntPntr!=nullptr ||
                                                    etudReturnDoublePntr!=nullptr))
            {
                if (!tabInstructionsEtud.contientReturn())
                {
                    // Comme on ne peut pas verifier la val retournee, on met la note a zero
                    resultEvalJeuDeDonnees->note=0;
                    resultEvalJeuDeDonnees->correct=false;
                    sprintf(msgErrAlgoJeuDeDonnees,"Erreur: l'instruction \"return ...;\" n'a pas été implémentée.\n");
                }
            }
            // Verifie bien si l'instruction "return *this;" existe bien
            if (resultEvalJeuDeDonnees->correct && corrReturnThis!=nullptr)
            {
                if (!tabInstructionsEtud.contientReturn() || etudReturnThis!=etudPntrRes)
                {
                    // Divise la note par 2 si cette instruction est manquante
                    resultEvalJeuDeDonnees->note/=2.0;
                    resultEvalJeuDeDonnees->correct=false;
                    sprintf(msgErrAlgoJeuDeDonnees,"Erreur: l'instruction \"return *this;\" n'a pas été implémentée.\n");
                }
            }
            // Compare la correction et la solution par l'etudiant
            if (resultEvalJeuDeDonnees->correct)
            {
                // Compare les structures de données de la solution et celle de l'etudiant
                if (corrPntrRes)
                {
                    // On verifie l'integrite de la structure de donnees de l'etudiant
                    resultEvalJeuDeDonnees->correct=StructDeDonneesCorr::verifierIntegrite(*etudPntrRes);
                    if (!resultEvalJeuDeDonnees->correct)
                    {
                        assert(strlen(msgErrPntrJeuDeDonnees)==0);
                        sprintf(msgErrPntrJeuDeDonnees,"Erreur: struct. de données mal construite\n");
                    }
                    else
                    {
                        resultEvalJeuDeDonnees->correct=corrPntrRes->compareCorrAvecEtud(*etudPntrRes);
#ifdef ITERATEUR
                        if (resultEvalJeuDeDonnees->correct && corrIt!=nullptr)
                            resultEvalJeuDeDonnees->correct=corrPntrRes->compareCorrAvecEtud(*corrIt,
                                            *etudPntrRes,*((typename StructDeDonneesCorr::ITERATEUR*)etudIt));

#endif
                    }
                }
                else if (etudReturnBoolPntr)
                    resultEvalJeuDeDonnees->correct=(*corrReturnBoolPntr == *etudReturnBoolPntr);
                else if (etudReturnIntPntr)
                    resultEvalJeuDeDonnees->correct=(*corrReturnIntPntr == *etudReturnIntPntr);
                else if (etudReturnDoublePntr)
                    resultEvalJeuDeDonnees->correct=(*corrReturnDoublePntr == *etudReturnDoublePntr);
                else
                    resultEvalJeuDeDonnees->note=(coeffExo/jeuDeDonnees.taille());

                // Si correct, on ajoute les points
                if (resultEvalJeuDeDonnees->correct)
                    resultEvalJeuDeDonnees->note=(coeffExo/jeuDeDonnees.taille());
                else
                {
                    // N'affiche les erreurs d'algo que si il n'y a pas d'erreur de pointeurs ou boucles infinies
                    if (strlen(msgErrPntrJeuDeDonnees)==0)
                    {
                        ostringstream tmpStr;
#ifndef DEBUG
                        tmpStr<<"Erreur: la méthode ne fonctionne pas pour ces données:"<<endl;
#else
                        tmpStr<<"Erreur: la méthode ne fonctionne pas pour ces données "<<idExo\
                                                <<", "<<jeuID<<endl;
#endif
                        if (corrPntrThis)
                            tmpStr<<">  this:              "<<corrPntrThis->getStructDeDonneesVerifCorr().toString()<<endl;
                        if (paramNb!=-1)
                            tmpStr<<">  paramètre nb:      "<<paramNb<<endl;
                        if (paramIdxLigne!=-1 && paramIdxColonne!=-1)
                        {
                            tmpStr<<">  paramètre ligne:    "<<paramIdxLigne<<endl;
                            tmpStr<<">  paramètre colonne:  "<<paramIdxColonne<<endl;
                        }
                        else if (paramIdxColonne!=-1)
                            tmpStr<<">  paramètre idx:     "<<paramIdxColonne<<endl;
                        if (corrPntrParam)
                            tmpStr<<">  paramètre:         "<<corrPntrParam->getStructDeDonneesVerifCorr().toString()<<endl;
                        if (pntrTab)
                            tmpStr<<">  paramètre:         "<<pntrTab->toString()<<endl;
                        if (corrPntrRes)
                        {
                            tmpStr<<">  résultat attendu:  "<<corrPntrRes->getStructDeDonneesVerifCorr().toString()<<endl;
                            tmpStr<<">  résultat obtenu:   "<<((StructDeDonneesCorr*)etudPntrRes)->getStructDeDonneesVerifCorr().toString()<<endl;
                        }
                        else if (corrReturnBoolPntr)
                        {
                            if (*corrReturnBoolPntr)
                                tmpStr<<">  résultat attendu: true"<<endl;
                            else
                                tmpStr<<">  résultat attendu: false"<<endl;
                            if (*etudReturnBoolPntr)
                                tmpStr<<">  résultat obtenu:  true"<<endl;
                            else
                                tmpStr<<">  résultat obtenu:  false"<<endl;
                        }
                        else if (corrReturnIntPntr)
                        {
                            tmpStr<<">  résultat attendu:  "<<*corrReturnIntPntr<<endl;
                            tmpStr<<">  résultat obtenu:   "<<*etudReturnIntPntr<<endl;
                        }
                        else if (corrReturnDoublePntr)
                        {
                            tmpStr<<">  résultat attendu:  "<<*corrReturnDoublePntr<<endl;
                            tmpStr<<">  résultat obtenu:   "<<*etudReturnDoublePntr<<endl;
                        }
                        memcpy(msgErrAlgoJeuDeDonnees,tmpStr.str().c_str(),tmpStr.str().length());
                    }
                }
            }
            // Verifier si this==&l est bien implemente
            if (resultEvalJeuDeDonnees->correct && jeuDeDonnees.thisEgalThis(jeuID))
            {
                // Verifie si la methode contient un if et un return dans cet ordre
                /*
                if(tabInstructionsCorr.getNbAppelsStructDeCtrl()!=2)
                    assert(0);
                assert(tabInstructionsCorr.getInstructionStructDeCtrl(0).type==GestionPntrEtComplexite::TabAppelsInstructions::IF &&
                        tabInstructionsCorr.getInstructionStructDeCtrl(1).type==GestionPntrEtComplexite::TabAppelsInstructions::RETURN);
                if (!(tabInstructionsEtud.getNbAppelsStructDeCtrl()==2 &&
                                tabInstructionsCorr.getInstructionStructDeCtrl(0).type==GestionPntrEtComplexite::TabAppelsInstructions::IF &&
                                tabInstructionsCorr.getInstructionStructDeCtrl(1).type==GestionPntrEtComplexite::TabAppelsInstructions::RETURN))*/
                // Facon plus simple de faire. On verifie juste le nombre d'instruction de controle
                if (tabInstructionsCorr.getNbAppelsStructDeCtrl()<tabInstructionsEtud.getNbAppelsStructDeCtrl())
                {
                    if (strlen(msgErrAlgoJeuDeDonnees)==0)
                        sprintf(msgErrAlgoJeuDeDonnees,"Erreur: l'instruction \"if (this==&...\" n'a pas été implémentée.\n");
                    resultEvalJeuDeDonnees->note=0.0;
                    resultEvalJeuDeDonnees->correct=false;
                }
            }
            // Verifie si l'en-tete de la methode est ecrit correctement (pas de const manquant)
            // Cette verification doit se faire avant le teste pour verifier si l'etudiant
            // n'appelle une autre methode dans son code et les tests pour la complexire en temps
            // en en memoire
            if (resultEvalJeuDeDonnees->correct)
            {
                // Divise la note par 2 si l'en-ete n'est pas correct
                if (resultEvalJeuDeDonnees->enteteIncorrect)
                {
                    sprintf(msgErrAlgoJeuDeDonnees,"Erreur: en-tête de la méthode incorrect (const manquant).\n");
                    resultEvalJeuDeDonnees->note/=2.0;
                    resultEvalJeuDeDonnees->correct=false;
                }
            }
            // Compare la complexite en memoire
            if (resultEvalJeuDeDonnees->correct)
            {
                resultEvalJeuDeDonnees->correct=verifAllocationPntr(tabInstructionsCorr,tabInstructionsEtud);
                if (!resultEvalJeuDeDonnees->correct)
                    resultEvalJeuDeDonnees->note/=2.0;
            }
            // Verifie la complexite en temps et l'utilisation des boucles while et for
            if (resultEvalJeuDeDonnees->correct)
                resultEvalJeuDeDonnees->correct=verifComplexiteTemps(tabInstructionsCorr,tabInstructionsEtud);
            // Verifie si l'etudiant n'appelle pas une autre methode dans son code
            if (resultEvalJeuDeDonnees->correct)
            {
                int nbMethodesCorr=tabInstructionsCorr.getNbMethodes();
                // Une seule methode doit etre appelee
                if (tabInstructionsEtud.getNbMethodes()>nbMethodesCorr)
                    resultEvalJeuDeDonnees->correct=false;
                else if (tabInstructionsEtud.getNbMethodes()==1)
                {
                    // Dans le cas d'un operateur qui n'est pas membre d'une classe, la methode retourne la
                    // chaine de caracteres vide
                    char nomMethodeCorr[255],nomClasseCorr[255],nomMethodeEtud[255],nomClasseEtud[255];
                    tabInstructionsCorr.getNomsClasseEtMethode(nomClasseCorr,nomMethodeCorr);
                    tabInstructionsEtud.getNomsClasseEtMethode(nomClasseEtud,nomMethodeEtud);
                    // Si l'exercice concerne l'implementation d'une methode
                    if (strlen(nomClasseCorr)>0)
                    {
                        // Le nom de la classe doit etre different
                        if (strcmp(nomClasseCorr,nomClasseEtud)==0)
                            resultEvalJeuDeDonnees->correct=false;
                        // Le nom de la methode doit etre la meme sauf si c'est un constructeur ou destructeur
                        else if (strcmp(nomMethodeEtud,"~")==0 &&
                                    strcmp(nomMethodeEtud,nomClasseEtud)==0 &&
                                    strcmp(nomMethodeEtud,nomMethodeCorr)!=0)
                            resultEvalJeuDeDonnees->correct=false;
                    }
                }
                if (resultEvalJeuDeDonnees->correct==false)
                {
                    if (strlen(msgErrAlgoJeuDeDonnees)==0)
                        sprintf(msgErrAlgoJeuDeDonnees,"Erreur: la méthode implémentée ne doit pas appeler une autre méthode.\n");
                    // On met 0. L'etudiant ne doit pas ecrire une methode qui ne fait qu'appeler une autre methode
                    resultEvalJeuDeDonnees->note=0;
                    resultEvalJeuDeDonnees->correct=false;
                }
            }

            // Libere les pointeurs
            delete corrPntrRes;
            corrPntrRes=nullptr;
            // Celui de l'etudiant n'est pas libere parce que la struct peut contenir des erreurs
            //delete etudPntrRes;
            etudPntrRes=nullptr;
            delete corrIt;
            corrIt=nullptr;
            delete etudIt;
            etudIt=nullptr;


            // Fin du process enfant pour l'evaluation pour un jeu de donnees
#ifndef DESACTIVE_EVALUATION_PARALLELE
            exit(0);
#endif
        }
    }

    // Attend la fin de tous les process pour les jeux de donnees
    while(termineProcessJeuDeDonnees(resultEvalExercice,processJeuDeDonnees,jeuDeDonnees)>0)
        ;
    return resultEvalExercice;
}


//////////////////////////////////////////////////////////////////////////////////
// Recupere la note calculee par les process demarre pour chaque jeu de donnees //
// Retourne le nombre de process encore en fonctionnement                       //
//////////////////////////////////////////////////////////////////////////////////
template <class StructDeDonneesCorr, class StructDeDonneesEtud>
int Evaluer<StructDeDonneesCorr, StructDeDonneesEtud>::termineProcessJeuDeDonnees(ResultatEvaluation &resultEvalExercice,
                                                                ProcessJeuDeDonnees *processJeuDeDonnees,
                                                                JeuDeDonnees &jeuDeDonnees)
{
    int nbActifProcess=0;
    for(int jeuID=0;jeuID<jeuDeDonnees.taille();jeuID++)
    {
        // Si le process est deja termine, on passe au suivante
        if (processJeuDeDonnees[jeuID].pid==-1)
            continue;
        // Attend la fin de l'evaluation
        // Dans certain cas, le process enfant ne se termine jamais
        int childstate;
        int processFini=waitpid(processJeuDeDonnees[jeuID].pid, &childstate, WNOHANG | WUNTRACED);
        if(processFini==0)
        {
            double duree=(double)(clock()-processJeuDeDonnees[jeuID].startTime)/CLOCKS_PER_SEC;
            // Si le process a dure plus longtemps que la limite de 0.2 s
            if (duree>0.2)
            {
                // Le process enfant est tue apres 0.2 seconde  (O.O2 trop petit)
                kill(processJeuDeDonnees[jeuID].pid, SIGKILL);

                // Compter comme faux
                resultEvalExercice.correct=false;
                // Si pas de message, on en met un generique
                if (strlen(processJeuDeDonnees[jeuID].sharedMsgErrPntr)==0)
                {
                    sprintf(processJeuDeDonnees[jeuID].sharedMsgErrPntr,"Erreur: la méthode ne fonctionne pas correctement.\n");
                }
#ifdef DEBUG
                cout<<"Process killed: (\""<<processJeuDeDonnees[jeuID].idExo<<"\", "<<jeuID<<")"<<endl;
#endif
            }
            else
            {
                // pour compter le nombre de process encore actifs
                nbActifProcess++;
                continue;
            }
        }
        // Pour indiquer que le process est termine
        processJeuDeDonnees[jeuID].pid=-1;
#ifdef DEBUG
        cout<<"Process termine: (\""<<processJeuDeDonnees[jeuID].idExo<<"\", "<<jeuID<<")"<<endl;
#endif
        // Mis a jour de l'adresse d'instruction crashee
        resultEvalExercice.ptrInstructCrash=processJeuDeDonnees[jeuID].resultEval->ptrInstructCrash;
        // Mis a jour de la note
        resultEvalExercice.note+=processJeuDeDonnees[jeuID].resultEval->note;
        // La methode manquante
        resultEvalExercice.manquante=processJeuDeDonnees[jeuID].resultEval->manquante;
        // Recupere le nom de la methode
        memcpy(resultEvalExercice.nomMethodeExo,
               processJeuDeDonnees[jeuID].resultEval->nomMethodeExo,
               sizeof(resultEvalExercice.nomMethodeExo));
        // Si message d'erreur sur les pointeurs ou boucle infini, il y a une erreur
        if (strlen(processJeuDeDonnees[jeuID].sharedMsgErrPntr)>0)
            processJeuDeDonnees[jeuID].resultEval->correct=false;
        // L'exercice est faux s'il y a une erreur sur au moins un jeu de donnee
        if (processJeuDeDonnees[jeuID].resultEval->correct==false)
            resultEvalExercice.correct=false;
        // Mise a jour des messages pour l'exercice
        if (strlen(processJeuDeDonnees[jeuID].sharedMsgErrPntr)>0)
        {
#ifdef DEBUG
            msgErrPntrExercice<<processJeuDeDonnees[jeuID].idExo<<", "<<jeuID<<": ";
#endif
            // N'affiche le commentaire que si il est different du precedent
            if (string(processJeuDeDonnees[jeuID].sharedMsgErrPntr)!=msgErrPrecJeuDeDonnees)
            {
                msgErrPntrExercice<<processJeuDeDonnees[jeuID].sharedMsgErrPntr;
                msgErrPrecJeuDeDonnees=string(processJeuDeDonnees[jeuID].sharedMsgErrPntr);
            }
            // Les message sur les pointeurs et boucles infinies sont prioritaires
            // Les messages sur l'algo ne devraient pas etre presents
            assert(strlen(processJeuDeDonnees[jeuID].sharedMsgErrAlgo)==0);
        }
        if (strlen(processJeuDeDonnees[jeuID].sharedMsgErrAlgo)>0)
        {
            // Si msg algo, il ne devrait pas y avoir de msg sur les pointeurs et boucles infinies
            assert(strlen(processJeuDeDonnees[jeuID].sharedMsgErrPntr)==0);
#ifdef DEBUG
            msgErrAlgoExercice<<processJeuDeDonnees[jeuID].idExo<<", "<<jeuID<<": ";
#endif
            // N'affiche le commentaire que si il est different du precedent
            if (string(processJeuDeDonnees[jeuID].sharedMsgErrAlgo)!=msgErrPrecJeuDeDonnees)
            {
                msgErrAlgoExercice<<processJeuDeDonnees[jeuID].sharedMsgErrAlgo;
                msgErrPrecJeuDeDonnees=string(processJeuDeDonnees[jeuID].sharedMsgErrAlgo);
            }
        }

        // libere les pointeurs partages
        munmap(processJeuDeDonnees[jeuID].sharedMsgErrPntr, 4096*sizeof(char));
        munmap(processJeuDeDonnees[jeuID].sharedMsgErrAlgo, 4096*sizeof(char));
        munmap(processJeuDeDonnees[jeuID].resultEval, sizeof(ResultatEvaluation));
    }
    return nbActifProcess;
}


/////////////////////////////////////////////////////
// Methode pour evaluer les methodes de l'etudiant //
/////////////////////////////////////////////////////
template <class StructDeDonneesCorr, class StructDeDonneesEtud>
double Evaluer<StructDeDonneesCorr, StructDeDonneesEtud>::doEvaluate(double coeff, int nbMaxProcesses)
{
	// Installe le signal handler pour crash causes par les erreurs de pointeurs
    struct sigaction sa;
    sa.sa_sigaction = sighandler;
    sigemptyset (&sa.sa_mask);
    sigaddset(&(sa.sa_mask), SIGINT);
    sa.sa_flags = SA_RESTART | SA_SIGINFO;
    sigaction(SIGSEGV, &sa, NULL);
    sigaction(SIGABRT, &sa, NULL);      // pour *** stack smashing detected ***: terminated


    int status = -1;
    char *nomClasseCorrTmp = abi::__cxa_demangle(typeid(StructDeDonneesCorr).name(), NULL, 0, &status);
    char *nomClasseEtudTmp = abi::__cxa_demangle(typeid(StructDeDonneesEtud).name(), NULL, 0, &status);
    if (nomClasseCorrTmp==nullptr || nomClasseEtudTmp==nullptr)
    {
        free(nomClasseCorrTmp);
        free(nomClasseEtudTmp);
        cout<<"Il faut utiliser l'option -rdynamic pour le linker"<<endl;
    }
    char nomClasseCorr[1024]={};
    char nomClasseEtud[1024]={};
    memcpy(nomClasseCorr,nomClasseCorrTmp,strlen(nomClasseCorrTmp));
    memcpy(nomClasseEtud,nomClasseEtudTmp,strlen(nomClasseEtudTmp));
    free(nomClasseCorrTmp);
    free(nomClasseEtudTmp);
    /*
    // Le code ci-desoous pour inclure que les methodes de la classe
    nomClasseCorr[strlen(nomClasseCorr)]=':';
    nomClasseCorr[strlen(nomClasseCorr)]=':';
    nomClasseEtud[strlen(nomClasseEtud)]=':';
    nomClasseEtud[strlen(nomClasseEtud)]=':';*/
    GestionPntrEtComplexite::get().definirFiltresPile({nomClasseCorr,nomClasseEtud});

    // Pour la note globale
    ResultatEvaluation noteTotale;
    // Pour la note de chaque exercice
    ResultatEvaluation noteExercices[getNombreTotalExercices()];
    // Prepare l'affichage
    ostringstream msgAAfficher;
    msgAAfficher << fixed;
    msgAAfficher << setprecision(2);


    // On calcule tout de suite le coeff total
    for (int exerciceID=0;exerciceID<getNombreTotalExercices();exerciceID++)
        noteTotale.coeff+=tabEvaluationExercices[exerciceID].coeff;
    double ratioNote = coeff/noteTotale.coeff;

    for (int exerciceID=0;exerciceID<getNombreTotalExercices();exerciceID++)
    {
#ifdef DEBUG
        cout<<"exerciceID: "<<exerciceID<<" "<<noteExercices[exerciceID].nomMethodeExo<<endl;
#endif
        // Si coeff==0, pas devaluation
        if (tabEvaluationExercices[exerciceID].coeff==0.0)
            continue;

        // Remet tous les messages a zero
        msgErrPntrExercice.str(string());
        msgErrAlgoExercice.str(string());
        msgErrPrecJeuDeDonnees=string();

        // Lance l'evaluation
#ifdef DEBUG
        clock_t tStart = clock();
#endif
        noteExercices[exerciceID]=evalUnExercice(tabEvaluationExercices[exerciceID].coeff,
                                                 tabEvaluationExercices[exerciceID].idExo);
#ifdef DEBUG
        double timing=(double)(clock()-tStart)/CLOCKS_PER_SEC;
        totalTiming+=timing;
        cout<<noteExercices[exerciceID].nomMethodeExo<<": "<<timing<<"sec."<<endl;
#endif

        if (!noteExercices[exerciceID].correct && !noteExercices[exerciceID].manquante &&
             msgErrAlgoExercice.str().length()==0 && msgErrPntrExercice.str().length()==0)
        {
            // Si l'instruction crash est egal a nullptr
            if (noteExercices[exerciceID].ptrInstructCrash==nullptr)
                // C'est une erreur inconnue. On met un message generique
                msgErrAlgoExercice<<"Erreur: la méthode ne fonctionne pas correctement."<<endl;
            else
            {
                // Cherche la ligne du crash
                char *str=GestionPntrEtComplexite::get().recupereFichierEtNumeroTraceSymbol(noteExercices[exerciceID].ptrInstructCrash);
                if (str)
                {
                    msgErrAlgoExercice<<"Erreur à la ligne suivante:"<<endl<<"  "<<str<<endl;
                    free(str);
                }
                else
                    msgErrAlgoExercice<<"Erreur: la méthode ne fonctionne pas correctement."<<endl;
            }
        }

        if ((msgErrPntrExercice.str().length()>0 || msgErrAlgoExercice.str().length()>0)
                                                && !noteExercices[exerciceID].manquante)
        {
            msgAAfficher<<"\n<|--"<<endl;
            msgAAfficher<<"-"<<noteExercices[exerciceID].nomMethodeExo<<":"<<endl;

            if (tabEvaluationExercices[exerciceID].montrerCommentaires)
            {
                msgAAfficher<<msgErrPntrExercice.str();
                msgAAfficher<<msgErrAlgoExercice.str();
            }

            msgAAfficher<<"Note: "<<
                    noteExercices[exerciceID].note*ratioNote<<"/"<<
                    noteExercices[exerciceID].coeff*ratioNote<<endl;
            msgAAfficher<<endl;
            msgAAfficher<<"--|>"<<endl;
        }

        // Mise à jour de la note totale
        noteTotale.note+=noteExercices[exerciceID].note;
    }

    // Finalement on affiche le détail de l'évaluation
    bool contientMethodesManquantes=false;
    for (int exerciceID=0;exerciceID<getNombreTotalExercices();exerciceID++)
    {
        if (noteExercices[exerciceID].coeff!=0.0 && noteExercices[exerciceID].manquante)
        {
            if (!contientMethodesManquantes)
            {
                msgAAfficher<<"\n<|--"<<endl;
                msgAAfficher<<"-Méthodes manquantes:"<<endl;
                contientMethodesManquantes=true;
            }
            // Le nom de la methode manquante peut etre dans msgErrAlgoExercice
            // Dans ce cas, on l'affiche. Sinon, on affiche le nom standard
            if (msgErrAlgoExercice.str().length()>0)
                msgAAfficher<<msgErrAlgoExercice.str();
            else
                msgAAfficher<<noteExercices[exerciceID].nomMethodeExo<<endl;
        }
    }
    if (contientMethodesManquantes)
    {
        msgAAfficher<<endl;
        msgAAfficher<<"--|>"<<endl;
    }

    bool contientMethodesJustes=false;
    for (int exerciceID=0;exerciceID<getNombreTotalExercices();exerciceID++)
    {
        if (noteExercices[exerciceID].coeff!=0.0 && !noteExercices[exerciceID].manquante &&
                                                            noteExercices[exerciceID].correct)
        {
            if (!contientMethodesJustes)
            {
                msgAAfficher<<"\n<|--"<<endl;
                msgAAfficher<<"-Méthodes justes:"<<endl;
                contientMethodesJustes=true;
            }
            msgAAfficher<<noteExercices[exerciceID].nomMethodeExo<<": "<<
                    noteExercices[exerciceID].note*ratioNote<<"/"<<
                    noteExercices[exerciceID].coeff*ratioNote<<endl;
        }
    }
    if (contientMethodesJustes)
        msgAAfficher<<"--|>"<<endl;

    // Remplace "Int" par "int"
    string str = std::regex_replace(msgAAfficher.str(), std::regex("([^a-zA-Z0-9])Int([^a-zA-Z0-9])"), "$1int$2");
    // Remplace "Double" par "double"
    str = std::regex_replace(str, std::regex("([^a-zA-Z0-9])Double([^a-zA-Z0-9])"), "$1double$2");
    // Remplace pour le nom du fichier source de l'etudiant
    str = std::regex_replace(str, std::regex("([^a-zA-Z0-9])Exercices-instr.cpp([^a-zA-Z0-9])"), "$1Exercices.cpp$2");
    // Remplace pour afficher correctement le symbome & dans la page HTML
    str = std::regex_replace(str, std::regex("\\&"), "&amp;");
    // Afiche sur la console
    cout<<str;
#ifdef DEBUG
    cout<<"Temps total: "<<totalTiming<<"sec."<<endl;
#endif

    // Return une note par rapport au coeff
    return (noteTotale.note/noteTotale.coeff)*coeff;
}


double doEvaluate(double coeff, int nbMaxProcesses)
{
    // Definit le nom du fichier de l'etudiant
    GestionPntrEtComplexite::get().nomFichierSourceEtudiant="Exercices-instr.cpp";
    // Les structures de donnees Etudiant et Correction doivent avoir les memes champs et la meme taille
    assert(sizeof(STRUCT_DE_DONNEES_CORR)==sizeof(STRUCT_DE_DONNEES_ETUD));

    // Ne comprends pas pourquoi
    // L'utilisation de la classe template dans le main provoque "symbole not defined"
    Evaluer<STRUCT_DE_DONNEES_CORR, STRUCT_DE_DONNEES_ETUD> eval(nbMaxProcesses);
    return eval.doEvaluate(coeff,nbMaxProcesses);
}

