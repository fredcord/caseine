#include "Evaluate.h"

// Inclut le fichier .h genere sur Caseine s'il existe
// Ce fichier contient VPL_GRADEMIN, VPL_GRADEMAX et VPL_MAXPROCESSES
#if __has_include("ParametresCaseine.h")
#include "ParametresCaseine.h"
#endif

// Si VPL_GRADEMIN, VPL_GRADEMAX et VPL_MAXPROCESSES ne sont pas definis dans "ParametresCaseine.h"
// on definit leur valeur par defaut ici.
#ifndef VPL_GRADEMIN
#define VPL_GRADEMIN 0
#endif
#ifndef VPL_GRADEMAX
#define VPL_GRADEMAX 20
#endif
#ifndef VPL_MAXPROCESSES
#define VPL_MAXPROCESSES -1
#endif



int main(int argc, char** argv)
{
    int maxProcesses=VPL_MAXPROCESSES;
    if (maxProcesses!=-1)
        // Enleve 1 pour le process courant
        maxProcesses--;
    // Lance l'évaluation pour calculer une note sur (VPL_GRADEMAX-VPL_GRADEMIN)
    double note=doEvaluate(VPL_GRADEMAX-VPL_GRADEMIN, VPL_MAXPROCESSES);
    // Calcule la note avec VPL_GRADEMIN donne par Caseine
    note=note+(double)VPL_GRADEMIN;
    // Affiche la note
    float grade=note;
    char buf[100];
    sprintf(buf, "%5.2f", grade);
    int len = strlen(buf);
    if (len > 3 && strcmp(buf + (len - 3), ".00") == 0)
        buf[len - 3] = 0;
    printf("\nGrade :=>>%s\n", buf);

    return EXIT_SUCCESS;
}

