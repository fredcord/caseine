#include "Exercices.h"



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le constructeur pour la structure de données ci-dessous
//
// class File
// {
//   public:
//     File();
//     ~File();
//     ...
//   private:
//     static const int max_elt = 50;         // Pas plus de max_elt
//     double d_file[max_elt] ;               // les valeurs sont là-dedans
//     int d_premier, d_dernier, d_quantite;  // Indices dans d_file donnant le lieu d’emfilage
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
File::File():d_premier{0},d_dernier{0},d_quantite{0}
{
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le constructeur par recopie pour la structure de données ci-dessous
//
// class File
// {
//   public:
//     File();
//     ~File();
//     ...
//   private:
//     static const int max_elt = 50;         // Pas plus de max_elt
//     double d_file[max_elt] ;               // les valeurs sont là-dedans
//     int d_premier, d_dernier, d_quantite;  // Indices dans d_file donnant le lieu d’emfilage
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
File::File(const File &param):d_premier{param.d_premier},d_dernier{param.d_dernier},d_quantite{param.d_quantite}
{
    // Copie le tableau
    int i=d_premier,q=0;
    while(q<d_quantite)
    {
        d_file[i]=param.d_file[i];
        q++;
        i++;
        if (i==max_elt)
            i=0;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode concatenationFin(param) qui ajoute les elements de la
// structure de données param à la fin de la structure de données this. Cette
// structure de données param est passée en paramètre. L'ordre des éléments
// doit être gardé.
//
// class File
// {
//   public:
//     File();
//     ~File();
//     ...
//   private:
//     static const int max_elt = 50;         // Pas plus de max_elt
//     double d_file[max_elt] ;               // les valeurs sont là-dedans
//     int d_premier, d_dernier, d_quantite;  // Indices dans d_file donnant le lieu d’emfilage
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void File::concatenationFin(const File &param)
{
    int id2=param.d_premier;
    int quant=0;
    while(quant<param.d_quantite)
    {
        d_file[d_dernier]=param.d_file[id2];
        d_dernier++;
        if (d_dernier==max_elt)
            d_dernier=0;
        id2++;
        if (id2==max_elt)
            id2=0;
        quant++;
        d_quantite++;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode operator+=(param) qui ajoute les elements de la structure de
// données param à la fin de la structure de données this. Cette structure de
// données param est passée en paramètre
//
// class File
// {
//   public:
//     File();
//     ~File();
//     ...
//   private:
//     static const int max_elt = 50;         // Pas plus de max_elt
//     double d_file[max_elt] ;               // les valeurs sont là-dedans
//     int d_premier, d_dernier, d_quantite;  // Indices dans d_file donnant le lieu d’emfilage
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
File &File::operator+=(const File &param)
{
    int id2=param.d_premier;
    int quant=0;
    while(quant<param.d_quantite)
    {
        d_file[d_dernier]=param.d_file[id2];
        d_dernier++;
        if (d_dernier==max_elt)
            d_dernier=0;
        id2++;
        if (id2==max_elt)
            id2=0;
        quant++;
        d_quantite++;
    }
    return *this;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode operator==(param) qui retourne true si la structure de
// données param passée en paramètre est égale à la structure de données this.
//
// class File
// {
//   public:
//     File();
//     ~File();
//     ...
//   private:
//     static const int max_elt = 50;         // Pas plus de max_elt
//     double d_file[max_elt] ;               // les valeurs sont là-dedans
//     int d_premier, d_dernier, d_quantite;  // Indices dans d_file donnant le lieu d’emfilage
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
bool File::operator==(const File &param) const
{
    if (this==&param)
        return true;
    if (d_quantite!=param.d_quantite)
        return false;
    int i=d_premier,j=param.d_premier;
    int quant=0;
    while(quant<d_quantite &&  d_file[i]==param.d_file[j])
    {
        i++;
        if (i==max_elt)
            i=0;
        j++;
        if (j==max_elt)
            j=0;
        quant++;
    }
    return quant==d_quantite;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire l'opérateur d'affectation operator=(param). Après cette méthode, la structure
// de données this doit contenir les mêmes éléments que la structure de données param
// passée en paramètre.
//
// class File
// {
//   public:
//     File();
//     ~File();
//     ...
//   private:
//     static const int max_elt = 50;         // Pas plus de max_elt
//     double d_file[max_elt] ;               // les valeurs sont là-dedans
//     int d_premier, d_dernier, d_quantite;  // Indices dans d_file donnant le lieu d’emfilage
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
File &File::operator=(const File &param)
{
    if (this==&param)
        return *this;
    d_premier=param.d_premier;
    d_dernier=param.d_dernier;
    d_quantite=param.d_quantite;

    int id=param.d_premier;
    int quant=0;
    while(quant<param.d_quantite)
    {
        d_file[id]=param.d_file[id];
        id++;
        if (id==max_elt)
            id=0;
        quant++;
    }
    return *this;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void additionnerATousLesElts(double val) qui ajoute la valeur
// val passée en paramètre à tous les éléments de la stucture de données.
//
// class File
// {
//   public:
//     File();
//     ~File();
//     ...
//   private:
//     static const int max_elt = 50;         // Pas plus de max_elt
//     double d_file[max_elt] ;               // les valeurs sont là-dedans
//     int d_premier, d_dernier, d_quantite;  // Indices dans d_file donnant le lieu d’emfilage
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void File::additionnerATousLesElts(double val)
{
    int id=d_premier;
    int quant=0;
    while(quant<d_quantite)
    {
        d_file[id]+=val;
        id++;
        if (id==max_elt)
            id=0;
        quant++;
    }
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void push(double val) qui insère un élément
// contenant la valeur val passée en paramètre. Cet élément est inséré à la
// fin de la stucture de données.
// Remarques:
// - La structure de données peut être vide
//
// class File
// {
//   public:
//     File();
//     ~File();
//     ...
//   private:
//     static const int max_elt = 50;         // Pas plus de max_elt
//     double d_file[max_elt] ;               // les valeurs sont là-dedans
//     int d_premier, d_dernier, d_quantite;  // Indices dans d_file donnant le lieu d’emfilage
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void File::push(double val)
{
    d_file[d_dernier++] = val;
    d_dernier %= max_elt;
    d_quantite++;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void pull(int nb) qui enlève nb
// éléments au début de la structures de données. Cette valeur nb est passée
// en paramètre.
// Remarque:
// - La valeur nb peut être plus grande que la taille de la structures de
// données. Dans ce cas, tous les éléments doivent être supprimés.
// - La valeur nb peut être égale à 0. Dans ce cas, rien n'est supprimé.
//
// class File
// {
//   public:
//     File();
//     ~File();
//     ...
//   private:
//     static const int max_elt = 50;         // Pas plus de max_elt
//     double d_file[max_elt] ;               // les valeurs sont là-dedans
//     int d_premier, d_dernier, d_quantite;  // Indices dans d_file donnant le lieu d’emfilage
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void File::pull(int nb)
{
    int i=0;
    while (i<nb && d_quantite>0)
    {
        d_premier++;
        d_premier %= max_elt;
        d_quantite--;
        i++;
    }
}



