#include "Exercices.h"



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le constructeur pour la structure de données ci-dessous
//
// class File
// {
//   public:
//     File();
//     ~File();
//     ...
//   private:
//     static const int max_elt = 50;         // Pas plus de max_elt
//     double d_file[max_elt] ;               // les valeurs sont là-dedans
//     int d_premier, d_dernier, d_quantite;  // Indices dans d_file donnant le lieu d’emfilage
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le constructeur par recopie pour la structure de données ci-dessous
//
// class File
// {
//   public:
//     File();
//     ~File();
//     ...
//   private:
//     static const int max_elt = 50;         // Pas plus de max_elt
//     double d_file[max_elt] ;               // les valeurs sont là-dedans
//     int d_premier, d_dernier, d_quantite;  // Indices dans d_file donnant le lieu d’emfilage
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode concatenationFin(param) qui ajoute les elements de la
// structure de données param à la fin de la structure de données this. Cette
// structure de données param est passée en paramètre. L'ordre des éléments
// doit être gardé.
//
// class File
// {
//   public:
//     File();
//     ~File();
//     ...
//   private:
//     static const int max_elt = 50;         // Pas plus de max_elt
//     double d_file[max_elt] ;               // les valeurs sont là-dedans
//     int d_premier, d_dernier, d_quantite;  // Indices dans d_file donnant le lieu d’emfilage
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode operator+=(param) qui ajoute les elements de la structure de
// données param à la fin de la structure de données this. Cette structure de
// données param est passée en paramètre
//
// class File
// {
//   public:
//     File();
//     ~File();
//     ...
//   private:
//     static const int max_elt = 50;         // Pas plus de max_elt
//     double d_file[max_elt] ;               // les valeurs sont là-dedans
//     int d_premier, d_dernier, d_quantite;  // Indices dans d_file donnant le lieu d’emfilage
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode operator==(param) qui retourne true si la structure de
// données param passée en paramètre est égale à la structure de données this.
//
// class File
// {
//   public:
//     File();
//     ~File();
//     ...
//   private:
//     static const int max_elt = 50;         // Pas plus de max_elt
//     double d_file[max_elt] ;               // les valeurs sont là-dedans
//     int d_premier, d_dernier, d_quantite;  // Indices dans d_file donnant le lieu d’emfilage
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire l'opérateur d'affectation operator=(param). Après cette méthode, la structure
// de données this doit contenir les mêmes éléments que la structure de données param
// passée en paramètre.
//
// class File
// {
//   public:
//     File();
//     ~File();
//     ...
//   private:
//     static const int max_elt = 50;         // Pas plus de max_elt
//     double d_file[max_elt] ;               // les valeurs sont là-dedans
//     int d_premier, d_dernier, d_quantite;  // Indices dans d_file donnant le lieu d’emfilage
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void additionnerATousLesElts(double val) qui ajoute la valeur
// val passée en paramètre à tous les éléments de la stucture de données.
//
// class File
// {
//   public:
//     File();
//     ~File();
//     ...
//   private:
//     static const int max_elt = 50;         // Pas plus de max_elt
//     double d_file[max_elt] ;               // les valeurs sont là-dedans
//     int d_premier, d_dernier, d_quantite;  // Indices dans d_file donnant le lieu d’emfilage
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void push(double val) qui insère un élément
// contenant la valeur val passée en paramètre. Cet élément est inséré à la
// fin de la stucture de données.
// Remarques:
// - La structure de données peut être vide
//
// class File
// {
//   public:
//     File();
//     ~File();
//     ...
//   private:
//     static const int max_elt = 50;         // Pas plus de max_elt
//     double d_file[max_elt] ;               // les valeurs sont là-dedans
//     int d_premier, d_dernier, d_quantite;  // Indices dans d_file donnant le lieu d’emfilage
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void pull(int nb) qui enlève nb
// éléments au début de la structures de données. Cette valeur nb est passée
// en paramètre.
// Remarque:
// - La valeur nb peut être plus grande que la taille de la structures de
// données. Dans ce cas, tous les éléments doivent être supprimés.
// - La valeur nb peut être égale à 0. Dans ce cas, rien n'est supprimé.
//
// class File
// {
//   public:
//     File();
//     ~File();
//     ...
//   private:
//     static const int max_elt = 50;         // Pas plus de max_elt
//     double d_file[max_elt] ;               // les valeurs sont là-dedans
//     int d_premier, d_dernier, d_quantite;  // Indices dans d_file donnant le lieu d’emfilage
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////



