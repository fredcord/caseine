#ifndef EXERCICESBASE_INCLUDED
#define EXERCICESBASE_INCLUDED

#include "GestionPntrEtComplexite.h"

#if __has_include("vpl_evaluate-instr.cases")
#include "vpl_evaluate-instr.cases"
#define INSTRUMENTATION
#else
#include "vpl_evaluate.cases"
#endif

#include "StructDeDonneesVerifCorr.h"

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#define CONCAT_(a, b) a##b
#define CONCAT(a, b) CONCAT_(a, b)

#define STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT CONCAT(STRUCT_DE_DONNEES_CORR, _ConstructDestruct)
#define STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT CONCAT(STRUCT_DE_DONNEES_ETUD, _ConstructDestruct)
#define STRUCT_DE_DONNEES_CORR_OTHER CONCAT(STRUCT_DE_DONNEES_CORR, _Other)
#define STRUCT_DE_DONNEES_ETUD_OTHER CONCAT(STRUCT_DE_DONNEES_ETUD, _Other)

class STRUCT_DE_DONNEES_CORR;
class STRUCT_DE_DONNEES_ETUD;
class STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT;
class STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT;
class STRUCT_DE_DONNEES_CORR_OTHER;
class STRUCT_DE_DONNEES_ETUD_OTHER;

STRUCT_DE_DONNEES_CORR operator+(const STRUCT_DE_DONNEES_CORR& a, const STRUCT_DE_DONNEES_CORR& b) __attribute__((weak));
STRUCT_DE_DONNEES_CORR operator_plus(const STRUCT_DE_DONNEES_CORR& a, const STRUCT_DE_DONNEES_CORR& b) __attribute__((weak,no_instrument_function));


STRUCT_DE_DONNEES_ETUD operator+(const STRUCT_DE_DONNEES_ETUD& a, const STRUCT_DE_DONNEES_ETUD& b) __attribute__((weak));
STRUCT_DE_DONNEES_ETUD operator+(STRUCT_DE_DONNEES_ETUD& a, const STRUCT_DE_DONNEES_ETUD& b) __attribute__((weak,no_instrument_function));
STRUCT_DE_DONNEES_ETUD operator+(const STRUCT_DE_DONNEES_ETUD& a, STRUCT_DE_DONNEES_ETUD& b) __attribute__((weak,no_instrument_function));
STRUCT_DE_DONNEES_ETUD operator+(STRUCT_DE_DONNEES_ETUD& a, STRUCT_DE_DONNEES_ETUD& b) __attribute__((weak,no_instrument_function));
STRUCT_DE_DONNEES_ETUD operator_plus(const STRUCT_DE_DONNEES_ETUD& a, const STRUCT_DE_DONNEES_ETUD& b) __attribute__((weak,no_instrument_function));
STRUCT_DE_DONNEES_ETUD operator_plus(STRUCT_DE_DONNEES_ETUD& a, const STRUCT_DE_DONNEES_ETUD& b) __attribute__((weak,no_instrument_function));
STRUCT_DE_DONNEES_ETUD operator_plus(const STRUCT_DE_DONNEES_ETUD& a, STRUCT_DE_DONNEES_ETUD& b) __attribute__((weak,no_instrument_function));
STRUCT_DE_DONNEES_ETUD operator_plus(STRUCT_DE_DONNEES_ETUD& a, STRUCT_DE_DONNEES_ETUD& b) __attribute__((weak,no_instrument_function));


STRUCT_DE_DONNEES_CORR operator-(const STRUCT_DE_DONNEES_CORR& a, const STRUCT_DE_DONNEES_CORR& b) __attribute__((weak));
STRUCT_DE_DONNEES_CORR operator_moins(const STRUCT_DE_DONNEES_CORR& a, const STRUCT_DE_DONNEES_CORR& b) __attribute__((weak,no_instrument_function));

STRUCT_DE_DONNEES_ETUD operator-(const STRUCT_DE_DONNEES_ETUD& a, const STRUCT_DE_DONNEES_ETUD& b) __attribute__((weak));
STRUCT_DE_DONNEES_ETUD operator-(STRUCT_DE_DONNEES_ETUD& a, const STRUCT_DE_DONNEES_ETUD& b) __attribute__((weak,no_instrument_function));
STRUCT_DE_DONNEES_ETUD operator-(const STRUCT_DE_DONNEES_ETUD& a, STRUCT_DE_DONNEES_ETUD& b) __attribute__((weak,no_instrument_function));
STRUCT_DE_DONNEES_ETUD operator-(STRUCT_DE_DONNEES_ETUD& a, STRUCT_DE_DONNEES_ETUD& b) __attribute__((weak,no_instrument_function));
STRUCT_DE_DONNEES_ETUD operator_moins(const STRUCT_DE_DONNEES_ETUD& a, const STRUCT_DE_DONNEES_ETUD& b) __attribute__((weak,no_instrument_function));
STRUCT_DE_DONNEES_ETUD operator_moins(STRUCT_DE_DONNEES_ETUD& a, const STRUCT_DE_DONNEES_ETUD& b) __attribute__((weak,no_instrument_function));
STRUCT_DE_DONNEES_ETUD operator_moins(const STRUCT_DE_DONNEES_ETUD& a, STRUCT_DE_DONNEES_ETUD& b) __attribute__((weak,no_instrument_function));
STRUCT_DE_DONNEES_ETUD operator_moins(STRUCT_DE_DONNEES_ETUD& a, STRUCT_DE_DONNEES_ETUD& b) __attribute__((weak,no_instrument_function));


// Methode pour comparer des structures de donner pour la verification de la correction
bool operator==(const STRUCT_DE_DONNEES_CORR& a, const StructDeDonneesVerifCorr& b) __attribute__((no_instrument_function));
bool operator==(const StructDeDonneesVerifCorr& a, const STRUCT_DE_DONNEES_CORR& b) __attribute__((no_instrument_function));
bool operator!=(const STRUCT_DE_DONNEES_CORR& a, const StructDeDonneesVerifCorr& b) __attribute__((no_instrument_function));
bool operator!=(const StructDeDonneesVerifCorr& a, const STRUCT_DE_DONNEES_CORR& b) __attribute__((no_instrument_function));

class STRUCT_DE_DONNEES_CORR{

    friend STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT;
    friend STRUCT_DE_DONNEES_CORR_OTHER;
    friend STRUCT_DE_DONNEES_CORR operator+(const STRUCT_DE_DONNEES_CORR& a, const STRUCT_DE_DONNEES_CORR& b);
    friend STRUCT_DE_DONNEES_CORR operator_plus(const STRUCT_DE_DONNEES_CORR& a, const STRUCT_DE_DONNEES_CORR& b);
    friend STRUCT_DE_DONNEES_CORR operator-(const STRUCT_DE_DONNEES_CORR& a, const STRUCT_DE_DONNEES_CORR& b);
    friend STRUCT_DE_DONNEES_CORR operator_moins(const STRUCT_DE_DONNEES_CORR& a, const STRUCT_DE_DONNEES_CORR& b);

public:
    STRUCT_DE_DONNEES_CORR() __attribute__((weak));
    // Pas d'attribut weak pour le constructeur par recopie
    // Il doit toujours etre implemente pour la correction
    STRUCT_DE_DONNEES_CORR(const STRUCT_DE_DONNEES_CORR&);
    STRUCT_DE_DONNEES_CORR(Double*, Int) __attribute__((weak));
    STRUCT_DE_DONNEES_CORR(Double**, Int, Int) __attribute__((weak));
    ~STRUCT_DE_DONNEES_CORR() __attribute__((weak));

    void concatenationDebut(const STRUCT_DE_DONNEES_CORR&) __attribute__((weak));
    void concatenationMilieu(Int, const STRUCT_DE_DONNEES_CORR&) __attribute__((weak));
    void concatenationFin(const STRUCT_DE_DONNEES_CORR&) __attribute__((weak));

    STRUCT_DE_DONNEES_CORR &operator+=(const STRUCT_DE_DONNEES_CORR &) __attribute__((weak));
    bool operator==(const STRUCT_DE_DONNEES_CORR&) const __attribute__((weak));
    bool operator!=(const STRUCT_DE_DONNEES_CORR&) const __attribute__((weak));
    STRUCT_DE_DONNEES_CORR &operator=(const STRUCT_DE_DONNEES_CORR&) __attribute__((weak));
    Double &operator[](Int) __attribute__((weak));
    Double operator[](Int) const __attribute__((weak));

    Double &operator()(Int) __attribute__((weak));
    Double operator()(Int) const __attribute__((weak));
    Double &operator()(Int,Int) __attribute__((weak));
    Double operator()(Int,Int) const __attribute__((weak));

    Double get(Int)const __attribute__((weak));
    Double get(Int, Int)const __attribute__((weak));
    void set(Int, Double) __attribute__((weak));
    void set(Int, Int, Double) __attribute__((weak));

    void additionnerATousLesElts(Double) __attribute__((weak));

    void insererUnEltDebut(Double val) __attribute__((weak));
    void insererUnEltDebut(Double *val, Int nb) __attribute__((weak));
    void insererUnEltMilieu(Int, Double) __attribute__((weak));
    void insererUnEltMilieu(Int, Double*, Int) __attribute__((weak));
    void insererUnEltFin(Double) __attribute__((weak));
    void insererUnEltFin(Double *val, Int nb) __attribute__((weak));

    void enleverUnEltDebut() __attribute__((weak));
    void enleverUnEltMilieu(Int) __attribute__((weak));
    void enleverUnEltFin() __attribute__((weak));

    void insererPlusieursEltsDebut(Double*, Int) __attribute__((weak));
    void insererPlusieursEltsDebut(Double**, Int, Int) __attribute__((weak));
    void insererPlusieursEltsMilieu(Int, Double*, Int) __attribute__((weak));
    void insererPlusieursEltsMilieu(Int, Double**, Int, Int) __attribute__((weak));
    void insererPlusieursEltsFin(Double*, Int) __attribute__((weak));
    void insererPlusieursEltsFin(Double**, Int, Int) __attribute__((weak));

    void enleverPlusieursEltsDebut(Int) __attribute__((weak));
    void enleverPlusieursEltsMilieu(Int, Int) __attribute__((weak));
    void enleverPlusieursEltsFin(Int) __attribute__((weak));

    void enleverToutesLesOccurencesDUnElt(Double) __attribute__((weak));
    bool eltsEnOrdreDecroissant()const __attribute__((weak));
    bool contientElt(Double)const __attribute__((weak));
    Int chercherLIndexDeLaPremiereOccurenceDUnElt(Double)const __attribute__((weak));
    Int chercherLIndexDeLaDerniereOccurenceDUnElt(Double)const __attribute__((weak));
    Int calculerNombreDOccurencesDUnElt(Double)const __attribute__((weak));

public:
    operator STRUCT_DE_DONNEES_CORR*() __attribute__((no_instrument_function))
    {
        return (STRUCT_DE_DONNEES_CORR*)this;
    }
    operator STRUCT_DE_DONNEES_ETUD*() __attribute__((no_instrument_function))
    {
        return (STRUCT_DE_DONNEES_ETUD*)this;
    }
    operator StructDeDonneesVerifCorr*() __attribute__((no_instrument_function));

    STRUCT_DE_DONNEES_CORR(const StructDeDonneesVerifCorr &) __attribute__((weak,no_instrument_function));
    StructDeDonneesVerifCorr getStructDeDonneesVerifCorr() const __attribute__((weak,no_instrument_function));
    static bool verifierIntegrite(const STRUCT_DE_DONNEES_ETUD &) __attribute__((weak,no_instrument_function));
    bool compareCorrAvecEtud(const STRUCT_DE_DONNEES_ETUD &) const __attribute__((weak,no_instrument_function));
private:
    void setStructDeDonneesVerifCorr(const StructDeDonneesVerifCorr&) __attribute__((weak,no_instrument_function));

private:
    MEMBRES_STRUCT_DE_DONNEES

#ifdef ITERATEUR
    public:
    class ITERATEUR {
        friend STRUCT_DE_DONNEES_CORR;
        friend STRUCT_DE_DONNEES_CORR_OTHER;
        friend STRUCT_DE_DONNEES_ETUD;
        friend STRUCT_DE_DONNEES_ETUD_OTHER;
        public:
            Double operator*() const __attribute__((weak));
            Double& operator*() __attribute__((weak));
            ITERATEUR& operator++() __attribute__((weak));
            ITERATEUR& operator--() __attribute__((weak));
            bool fin() const __attribute__((weak));
        private:
            ITERATEUR(ITERATEUR_TYPE_ELEMENT* ancre) __attribute__((weak));
            ITERATEUR_TYPE_ELEMENT* d_ancre;
            ITERATEUR_TYPE_ELEMENT* d_crt;
        // Pour la correction
        public:
            ITERATEUR() __attribute__((weak,no_instrument_function))
            {
                memset((void*)this,0x00,sizeof(ITERATEUR));
            }
    };
    ITERATEUR premier() __attribute__((weak));
    bool compareCorrAvecEtud(const typename STRUCT_DE_DONNEES_CORR::ITERATEUR&,
                             const STRUCT_DE_DONNEES_ETUD &,
                             const typename STRUCT_DE_DONNEES_CORR::ITERATEUR&) const __attribute__((weak,no_instrument_function));
#else
public:
    class ITERATEUR {};
#endif
};


class STRUCT_DE_DONNEES_ETUD{

    friend STRUCT_DE_DONNEES_CORR;
    friend STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT;
    friend STRUCT_DE_DONNEES_CORR_OTHER;

    friend STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT;
    friend STRUCT_DE_DONNEES_ETUD_OTHER;

    friend STRUCT_DE_DONNEES_ETUD operator+(const STRUCT_DE_DONNEES_ETUD& a, const STRUCT_DE_DONNEES_ETUD& b);
    friend STRUCT_DE_DONNEES_ETUD operator_plus(const STRUCT_DE_DONNEES_ETUD& a, const STRUCT_DE_DONNEES_ETUD& b);

    friend STRUCT_DE_DONNEES_ETUD operator+(STRUCT_DE_DONNEES_ETUD& a, const STRUCT_DE_DONNEES_ETUD& b);
    friend STRUCT_DE_DONNEES_ETUD operator+(const STRUCT_DE_DONNEES_ETUD& a, STRUCT_DE_DONNEES_ETUD& b);
    friend STRUCT_DE_DONNEES_ETUD operator+(STRUCT_DE_DONNEES_ETUD& a, STRUCT_DE_DONNEES_ETUD& b);
    friend STRUCT_DE_DONNEES_ETUD operator_plus(STRUCT_DE_DONNEES_ETUD& a, const STRUCT_DE_DONNEES_ETUD& b);
    friend STRUCT_DE_DONNEES_ETUD operator_plus(const STRUCT_DE_DONNEES_ETUD& a, STRUCT_DE_DONNEES_ETUD& b);
    friend STRUCT_DE_DONNEES_ETUD operator_plus(STRUCT_DE_DONNEES_ETUD& a, STRUCT_DE_DONNEES_ETUD& b);

    friend STRUCT_DE_DONNEES_ETUD operator-(const STRUCT_DE_DONNEES_ETUD& a, const STRUCT_DE_DONNEES_ETUD& b);
    friend STRUCT_DE_DONNEES_ETUD operator_moins(const STRUCT_DE_DONNEES_ETUD& a, const STRUCT_DE_DONNEES_ETUD& b);

    friend STRUCT_DE_DONNEES_ETUD operator-(STRUCT_DE_DONNEES_ETUD& a, const STRUCT_DE_DONNEES_ETUD& b);
    friend STRUCT_DE_DONNEES_ETUD operator-(const STRUCT_DE_DONNEES_ETUD& a, STRUCT_DE_DONNEES_ETUD& b);
    friend STRUCT_DE_DONNEES_ETUD operator-(STRUCT_DE_DONNEES_ETUD& a, STRUCT_DE_DONNEES_ETUD& b);
    friend STRUCT_DE_DONNEES_ETUD operator_moins(STRUCT_DE_DONNEES_ETUD& a, const STRUCT_DE_DONNEES_ETUD& b);
    friend STRUCT_DE_DONNEES_ETUD operator_moins(const STRUCT_DE_DONNEES_ETUD& a, STRUCT_DE_DONNEES_ETUD& b);
    friend STRUCT_DE_DONNEES_ETUD operator_moins(STRUCT_DE_DONNEES_ETUD& a, STRUCT_DE_DONNEES_ETUD& b);

public:
    // Les constructeurs
    // Ces constructeurs appellent les constructeurs de la correction ou ceux de l'etudiant
    // suivant la valeur du drapeau "utiliserConstructCorr"
    STRUCT_DE_DONNEES_ETUD() __attribute__((weak));
    STRUCT_DE_DONNEES_ETUD(const STRUCT_DE_DONNEES_ETUD &) __attribute__((weak));
    STRUCT_DE_DONNEES_ETUD(Double*, Int) __attribute__((weak));
    STRUCT_DE_DONNEES_ETUD(Double**, Int, Int) __attribute__((weak));

    // Le desctructeur
    // Ce destructeurs appellent le destructeurs de la correction ou celui de l'etudiant
    // suivant la valeur du drapeau "utiliserDestructCorr"
    ~STRUCT_DE_DONNEES_ETUD() __attribute__((weak));

    void concatenationDebut(const STRUCT_DE_DONNEES_ETUD&) __attribute__((weak));
    void concatenationDebut(STRUCT_DE_DONNEES_ETUD&) __attribute__((weak,no_instrument_function));

    void concatenationMilieu(Int, const STRUCT_DE_DONNEES_ETUD&) __attribute__((weak));
    void concatenationMilieu(Int, STRUCT_DE_DONNEES_ETUD&) __attribute__((weak,no_instrument_function));

    void concatenationFin(const STRUCT_DE_DONNEES_ETUD&) __attribute__((weak));
    void concatenationFin(STRUCT_DE_DONNEES_ETUD&) __attribute__((weak,no_instrument_function));

    STRUCT_DE_DONNEES_ETUD &operator+=(const STRUCT_DE_DONNEES_ETUD &) __attribute__((weak));
    STRUCT_DE_DONNEES_ETUD &operator+=(STRUCT_DE_DONNEES_ETUD &) __attribute__((weak,no_instrument_function));

    bool operator==(const STRUCT_DE_DONNEES_ETUD &) const __attribute__((weak));
    bool operator==(STRUCT_DE_DONNEES_ETUD &) const __attribute__((weak,no_instrument_function));
    bool operator==(const STRUCT_DE_DONNEES_ETUD &) __attribute__((weak,no_instrument_function));
    bool operator==(STRUCT_DE_DONNEES_ETUD &) __attribute__((weak,no_instrument_function));

    bool operator!=(const STRUCT_DE_DONNEES_ETUD &) const __attribute__((weak));
    bool operator!=(STRUCT_DE_DONNEES_ETUD &) const __attribute__((weak,no_instrument_function));
    bool operator!=(const STRUCT_DE_DONNEES_ETUD &) __attribute__((weak,no_instrument_function));
    bool operator!=(STRUCT_DE_DONNEES_ETUD &) __attribute__((weak,no_instrument_function));

    STRUCT_DE_DONNEES_ETUD &operator=(const STRUCT_DE_DONNEES_ETUD &) __attribute__((weak));
    STRUCT_DE_DONNEES_ETUD &operator=(STRUCT_DE_DONNEES_ETUD &) __attribute__((weak,no_instrument_function));

    Double &operator[](Int) __attribute__((weak));
    Double operator[](Int)const __attribute__((weak));

    Double &operator()(Int) __attribute__((weak));
    Double operator()(Int) const __attribute__((weak));
    Double &operator()(Int,Int) __attribute__((weak));
    Double operator()(Int,Int) const __attribute__((weak));

    Double get(Int)const __attribute__((weak));
    Double get(Int) __attribute__((weak,no_instrument_function));

    Double get(Int, Int)const __attribute__((weak));
    Double get(Int, Int) __attribute__((weak,no_instrument_function));

    void set(Int, Double) __attribute__((weak));
    void set(Int, Int, Double) __attribute__((weak));

    void additionnerATousLesElts(Double) __attribute__((weak));

    void insererUnEltDebut(Double) __attribute__((weak));
    void insererUnEltDebut(Double *val, Int nb) __attribute__((weak));
    void insererUnEltMilieu(Int, Double) __attribute__((weak));
    void insererUnEltMilieu(Int, Double*, Int) __attribute__((weak));
    void insererUnEltFin(Double) __attribute__((weak));
    void insererUnEltFin(Double *val, Int nb) __attribute__((weak));

    void enleverUnEltDebut() __attribute__((weak));
    void enleverUnEltMilieu(Int) __attribute__((weak));
    void enleverUnEltFin() __attribute__((weak));

    void insererPlusieursEltsDebut(Double*, Int) __attribute__((weak));
    void insererPlusieursEltsDebut(Double**, Int, Int) __attribute__((weak));
    void insererPlusieursEltsMilieu(Int, Double*, Int) __attribute__((weak));
    void insererPlusieursEltsMilieu(Int, Double**, Int, Int) __attribute__((weak));
    void insererPlusieursEltsFin(Double*, Int) __attribute__((weak));
    void insererPlusieursEltsFin(Double**, Int, Int) __attribute__((weak));

    void enleverPlusieursEltsDebut(Int) __attribute__((weak));
    void enleverPlusieursEltsMilieu(Int, Int) __attribute__((weak));
    void enleverPlusieursEltsFin(Int) __attribute__((weak));

    void enleverToutesLesOccurencesDUnElt(Double) __attribute__((weak));

    bool eltsEnOrdreDecroissant()const __attribute__((weak));
    bool eltsEnOrdreDecroissant() __attribute__((weak,no_instrument_function));

    bool contientElt(Double)const __attribute__((weak));
    bool contientElt(Double) __attribute__((weak,no_instrument_function));

    Int chercherLIndexDeLaPremiereOccurenceDUnElt(Double)const __attribute__((weak));
    Int chercherLIndexDeLaPremiereOccurenceDUnElt(Double) __attribute__((weak,no_instrument_function));

    Int chercherLIndexDeLaDerniereOccurenceDUnElt(Double)const __attribute__((weak));
    Int chercherLIndexDeLaDerniereOccurenceDUnElt(Double) __attribute__((weak,no_instrument_function));

    Int calculerNombreDOccurencesDUnElt(Double)const __attribute__((weak));
    Int calculerNombreDOccurencesDUnElt(Double) __attribute__((weak,no_instrument_function));


    // Version avec int et double pour la compilation dans l'instrumentation
    STRUCT_DE_DONNEES_ETUD(double*, int) __attribute__((weak));
    STRUCT_DE_DONNEES_ETUD(double**, int, int) __attribute__((weak));
    void concatenationMilieu(int, const STRUCT_DE_DONNEES_ETUD&) __attribute__((weak));
    void concatenationMilieu(int, STRUCT_DE_DONNEES_ETUD&) __attribute__((weak));
    double &operator[](int) __attribute__((weak));
    double operator[](int)const __attribute__((weak));
    double get(int)const __attribute__((weak));
    double get(int) __attribute__((weak));
    double get(int, int)const __attribute__((weak));
    double get(int, int) __attribute__((weak));
    double &operator()(int) __attribute__((weak));
    double operator()(int) const __attribute__((weak));
    double &operator()(int,int) __attribute__((weak));
    double operator()(int,int) const __attribute__((weak));
    void set(int, double) __attribute__((weak));
    void set(int, int, double) __attribute__((weak));
    void additionnerATousLesElts(double) __attribute__((weak));
    void insererUnEltDebut(double) __attribute__((weak));
    void insererUnEltDebut(double *val, int nb) __attribute__((weak));
    void insererUnEltMilieu(int, double) __attribute__((weak));
    void insererUnEltMilieu(int, double*, int) __attribute__((weak));
    void insererUnEltFin(double) __attribute__((weak));
    void insererUnEltFin(double *val, int nb) __attribute__((weak));
    void enleverUnEltMilieu(int) __attribute__((weak));
    void insererPlusieursEltsDebut(double*, int) __attribute__((weak));
    void insererPlusieursEltsDebut(double**, int, int) __attribute__((weak));
    void insererPlusieursEltsMilieu(int, double*, int) __attribute__((weak));
    void insererPlusieursEltsMilieu(int, double**, int, int) __attribute__((weak));
    void insererPlusieursEltsFin(double*, int) __attribute__((weak));
    void insererPlusieursEltsFin(double**, int, int) __attribute__((weak));
    void enleverPlusieursEltsDebut(int) __attribute__((weak));
    void enleverPlusieursEltsMilieu(int, int) __attribute__((weak));
    void enleverPlusieursEltsFin(int) __attribute__((weak));
    void enleverToutesLesOccurencesDUnElt(double) __attribute__((weak));
    bool contientElt(double)const __attribute__((weak));
    bool contientElt(double) __attribute__((weak));
    int chercherLIndexDeLaPremiereOccurenceDUnElt(double)const __attribute__((weak));
    int chercherLIndexDeLaPremiereOccurenceDUnElt(double) __attribute__((weak));
    int chercherLIndexDeLaDerniereOccurenceDUnElt(double)const __attribute__((weak));
    int chercherLIndexDeLaDerniereOccurenceDUnElt(double) __attribute__((weak));
    int calculerNombreDOccurencesDUnElt(double)const __attribute__((weak));
    int calculerNombreDOccurencesDUnElt(double) __attribute__((weak));

public:
    // Methodes pour la correction
    operator STRUCT_DE_DONNEES_CORR*() __attribute__((no_instrument_function))
    {
        return (STRUCT_DE_DONNEES_CORR*)this;
    }
    operator STRUCT_DE_DONNEES_ETUD*() __attribute__((no_instrument_function))
    {
        return (STRUCT_DE_DONNEES_ETUD*)this;
    }

private:
    MEMBRES_STRUCT_DE_DONNEES

#ifdef ITERATEUR
    public:
    class ITERATEUR {
        friend STRUCT_DE_DONNEES_CORR;
        friend STRUCT_DE_DONNEES_CORR_OTHER;
        friend STRUCT_DE_DONNEES_ETUD;
        friend STRUCT_DE_DONNEES_ETUD_OTHER;
        public:
#ifdef INSTRUMENTATION
            Double operator*() const __attribute__((weak));
            Double& operator*() __attribute__((weak));
#else
            double operator*() const __attribute__((weak));
            double& operator*() __attribute__((weak));
#endif
            ITERATEUR& operator++() __attribute__((weak));
            ITERATEUR& operator--() __attribute__((weak));
            bool fin() const __attribute__((weak));
        private:
            ITERATEUR(ITERATEUR_TYPE_ELEMENT* ancre) __attribute__((weak));
            ITERATEUR_TYPE_ELEMENT* d_ancre;
            ITERATEUR_TYPE_ELEMENT* d_crt;
        // Pour la correction
        public:
            ITERATEUR() __attribute__((weak,no_instrument_function))
            {
                memset((void*)this,0x00,sizeof(ITERATEUR));
            }
    };
    ITERATEUR premier() __attribute__((weak));
#else
    public:
    class ITERATEUR {};
#endif
};


// La classe pour isoler les constructeur ecrits pas l'etudiant
// Cela nous permet de choisir le constructeur corrige ou le constructeur de l'etudiant
// si le constructeur est appele
class STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT {
    friend STRUCT_DE_DONNEES_ETUD;
    friend STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT;
public:
    STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT() __attribute__((weak,no_instrument_function));
    STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT(const STRUCT_DE_DONNEES_ETUD &) __attribute__((weak,no_instrument_function));
    STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT(STRUCT_DE_DONNEES_ETUD &) __attribute__((weak,no_instrument_function));
    STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT(Double *tab, Int nb) __attribute__((weak,no_instrument_function));
    STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT(Double **tab, Int nbl, Int nbc) __attribute__((weak,no_instrument_function));
    ~STRUCT_DE_DONNEES_ETUD_CONSTRUCT_DESTRUCT() __attribute__((weak,no_instrument_function));

private:
    MEMBRES_STRUCT_DE_DONNEES
};

class STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT {
    friend STRUCT_DE_DONNEES_CORR;
    friend STRUCT_DE_DONNEES_ETUD;
public:
    STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT() __attribute__((weak,no_instrument_function));
    STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT(const STRUCT_DE_DONNEES_CORR &) __attribute__((weak,no_instrument_function));
    STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT(STRUCT_DE_DONNEES_CORR &) __attribute__((weak,no_instrument_function));
    STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT(Double *tab, Int nb) __attribute__((weak,no_instrument_function));
    STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT(Double **tab, Int nbl, Int nbc) __attribute__((weak,no_instrument_function));
    ~STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT() __attribute__((weak,no_instrument_function));

    MEMBRES_STRUCT_DE_DONNEES
};

class STRUCT_DE_DONNEES_ETUD_OTHER: public STRUCT_DE_DONNEES_ETUD {
    friend STRUCT_DE_DONNEES_ETUD;
    friend STRUCT_DE_DONNEES_CORR_CONSTRUCT_DESTRUCT;
public:

    void concatenationDebut(const STRUCT_DE_DONNEES_ETUD&) __attribute__((weak,no_instrument_function));
    void concatenationDebut(STRUCT_DE_DONNEES_ETUD&) __attribute__((weak,no_instrument_function));

    void concatenationMilieu(Int, const STRUCT_DE_DONNEES_ETUD&) __attribute__((weak,no_instrument_function));
    void concatenationMilieu(Int, STRUCT_DE_DONNEES_ETUD&) __attribute__((weak,no_instrument_function));

    void concatenationFin(const STRUCT_DE_DONNEES_ETUD&) __attribute__((weak,no_instrument_function));
    void concatenationFin(STRUCT_DE_DONNEES_ETUD&) __attribute__((weak,no_instrument_function));

    STRUCT_DE_DONNEES_ETUD &operator+=(const STRUCT_DE_DONNEES_ETUD &) __attribute__((weak,no_instrument_function));
    STRUCT_DE_DONNEES_ETUD &operator+=(STRUCT_DE_DONNEES_ETUD &) __attribute__((weak,no_instrument_function));

    bool operator==(const STRUCT_DE_DONNEES_ETUD &) const __attribute__((weak,no_instrument_function));
    bool operator==(STRUCT_DE_DONNEES_ETUD &) const __attribute__((weak,no_instrument_function));
    bool operator==(const STRUCT_DE_DONNEES_ETUD &) __attribute__((weak,no_instrument_function));
    bool operator==(STRUCT_DE_DONNEES_ETUD &) __attribute__((weak,no_instrument_function));

    bool operator!=(const STRUCT_DE_DONNEES_ETUD &) const __attribute__((weak,no_instrument_function));
    bool operator!=(STRUCT_DE_DONNEES_ETUD &) const __attribute__((weak,no_instrument_function));
    bool operator!=(const STRUCT_DE_DONNEES_ETUD &) __attribute__((weak,no_instrument_function));
    bool operator!=(STRUCT_DE_DONNEES_ETUD &) __attribute__((weak,no_instrument_function));

    STRUCT_DE_DONNEES_ETUD &operator=(const STRUCT_DE_DONNEES_ETUD &) __attribute__((weak,no_instrument_function));
    STRUCT_DE_DONNEES_ETUD &operator=(STRUCT_DE_DONNEES_ETUD &) __attribute__((weak,no_instrument_function));

    Double &operator[](Int) __attribute__((weak,no_instrument_function));
    Double operator[](Int)const __attribute__((weak,no_instrument_function));

    Double &operator()(Int) __attribute__((weak));
    Double operator()(Int) const __attribute__((weak));
    Double &operator()(Int,Int) __attribute__((weak));
    Double operator()(Int,Int) const __attribute__((weak));

    Double get(Int)const __attribute__((weak,no_instrument_function));
    Double get(Int) __attribute__((weak,no_instrument_function));

    Double get(Int, Int)const __attribute__((weak,no_instrument_function));
    Double get(Int, Int) __attribute__((weak,no_instrument_function));

    void set(Int, Double) __attribute__((weak,no_instrument_function));
    void set(Int, Int, Double) __attribute__((weak,no_instrument_function));

    void additionnerATousLesElts(Double) __attribute__((weak,no_instrument_function));

    void insererUnEltDebut(Double) __attribute__((weak,no_instrument_function));
    void insererUnEltDebut(Double *val, Int nb) __attribute__((weak,no_instrument_function));
    void insererUnEltMilieu(Int, Double) __attribute__((weak,no_instrument_function));
    void insererUnEltMilieu(Int, Double*, Int) __attribute__((weak,no_instrument_function));
    void insererUnEltFin(Double) __attribute__((weak,no_instrument_function));
    void insererUnEltFin(Double *val, Int nb) __attribute__((weak,no_instrument_function));

    void enleverUnEltDebut() __attribute__((weak,no_instrument_function));
    void enleverUnEltMilieu(Int) __attribute__((weak,no_instrument_function));
    void enleverUnEltFin() __attribute__((weak,no_instrument_function));

    void insererPlusieursEltsDebut(Double*, Int) __attribute__((weak,no_instrument_function));
    void insererPlusieursEltsDebut(Double**, Int, Int) __attribute__((weak,no_instrument_function));
    void insererPlusieursEltsMilieu(Int, Double*, Int) __attribute__((weak,no_instrument_function));
    void insererPlusieursEltsMilieu(Int, Double**, Int, Int) __attribute__((weak,no_instrument_function));
    void insererPlusieursEltsFin(Double*, Int) __attribute__((weak,no_instrument_function));
    void insererPlusieursEltsFin(Double**, Int, Int) __attribute__((weak,no_instrument_function));

    void enleverPlusieursEltsDebut(Int) __attribute__((weak,no_instrument_function));
    void enleverPlusieursEltsMilieu(Int, Int) __attribute__((weak,no_instrument_function));
    void enleverPlusieursEltsFin(Int) __attribute__((weak,no_instrument_function));

    void enleverToutesLesOccurencesDUnElt(Double) __attribute__((weak,no_instrument_function));

    bool eltsEnOrdreDecroissant()const __attribute__((weak,no_instrument_function));
    bool eltsEnOrdreDecroissant() __attribute__((weak,no_instrument_function));

    bool contientElt(Double)const __attribute__((weak,no_instrument_function));
    bool contientElt(Double) __attribute__((weak,no_instrument_function));

    Int chercherLIndexDeLaPremiereOccurenceDUnElt(Double)const __attribute__((weak,no_instrument_function));
    Int chercherLIndexDeLaPremiereOccurenceDUnElt(Double) __attribute__((weak,no_instrument_function));

    Int chercherLIndexDeLaDerniereOccurenceDUnElt(Double)const __attribute__((weak,no_instrument_function));
    Int chercherLIndexDeLaDerniereOccurenceDUnElt(Double) __attribute__((weak,no_instrument_function));

    Int calculerNombreDOccurencesDUnElt(Double)const __attribute__((weak,no_instrument_function));
    Int calculerNombreDOccurencesDUnElt(Double) __attribute__((weak,no_instrument_function));

#ifdef ITERATEUR
    public:
    class ITERATEUR {
        friend STRUCT_DE_DONNEES_CORR;
        friend STRUCT_DE_DONNEES_CORR_OTHER;
        friend STRUCT_DE_DONNEES_ETUD;
        friend STRUCT_DE_DONNEES_ETUD_OTHER;
        public:
            Double operator*() const __attribute__((weak,no_instrument_function));
            Double& operator*() __attribute__((weak,no_instrument_function));
            ITERATEUR& operator++() __attribute__((weak,no_instrument_function));
            ITERATEUR& operator--() __attribute__((weak,no_instrument_function));
            bool fin() const __attribute__((weak,no_instrument_function));
        private:
            ITERATEUR(ITERATEUR_TYPE_ELEMENT* ancre) __attribute__((weak));
            ITERATEUR_TYPE_ELEMENT* d_ancre;
            ITERATEUR_TYPE_ELEMENT* d_crt;
    };
    ITERATEUR premier() __attribute__((weak,no_instrument_function));
#endif
};

class STRUCT_DE_DONNEES_CORR_OTHER: public STRUCT_DE_DONNEES_CORR {
    friend STRUCT_DE_DONNEES_CORR;
    friend STRUCT_DE_DONNEES_ETUD;
public:

    void concatenationDebut(const STRUCT_DE_DONNEES_CORR&) __attribute__((weak,no_instrument_function));
    void concatenationMilieu(Int, const STRUCT_DE_DONNEES_CORR&) __attribute__((weak,no_instrument_function));
    void concatenationFin(const STRUCT_DE_DONNEES_CORR&) __attribute__((weak,no_instrument_function));

    STRUCT_DE_DONNEES_CORR &operator+=(const STRUCT_DE_DONNEES_CORR &) __attribute__((weak,no_instrument_function));
    bool operator==(const STRUCT_DE_DONNEES_CORR&) const __attribute__((weak,no_instrument_function));
    bool operator!=(const STRUCT_DE_DONNEES_CORR&) const __attribute__((weak,no_instrument_function));
    STRUCT_DE_DONNEES_CORR &operator=(const STRUCT_DE_DONNEES_CORR&) __attribute__((weak,no_instrument_function));
    Double &operator[](Int) __attribute__((weak,no_instrument_function));
    Double operator[](Int) const __attribute__((weak,no_instrument_function));

    Double &operator()(Int) __attribute__((weak));
    Double operator()(Int) const __attribute__((weak));
    Double &operator()(Int,Int) __attribute__((weak));
    Double operator()(Int,Int) const __attribute__((weak));

    Double get(Int)const __attribute__((weak,no_instrument_function));
    Double get(Int, Int)const __attribute__((weak,no_instrument_function));
    void set(Int, Double) __attribute__((weak,no_instrument_function));
    void set(Int, Int, Double) __attribute__((weak,no_instrument_function));

    void additionnerATousLesElts(Double) __attribute__((weak,no_instrument_function));

    void insererUnEltDebut(Double val) __attribute__((weak,no_instrument_function));
    void insererUnEltDebut(Double *val, Int nb) __attribute__((weak,no_instrument_function));
    void insererUnEltMilieu(Int, Double) __attribute__((weak,no_instrument_function));
    void insererUnEltMilieu(Int, Double*, Int) __attribute__((weak,no_instrument_function));
    void insererUnEltFin(Double) __attribute__((weak,no_instrument_function));
    void insererUnEltFin(Double *val, Int nb) __attribute__((weak,no_instrument_function));

    void enleverUnEltDebut() __attribute__((weak,no_instrument_function));
    void enleverUnEltMilieu(Int) __attribute__((weak,no_instrument_function));
    void enleverUnEltFin() __attribute__((weak,no_instrument_function));

    void insererPlusieursEltsDebut(Double*, Int) __attribute__((weak,no_instrument_function));
    void insererPlusieursEltsDebut(Double**, Int, Int) __attribute__((weak,no_instrument_function));
    void insererPlusieursEltsMilieu(Int, Double*, Int) __attribute__((weak,no_instrument_function));
    void insererPlusieursEltsMilieu(Int, Double**, Int, Int) __attribute__((weak,no_instrument_function));
    void insererPlusieursEltsFin(Double*, Int) __attribute__((weak,no_instrument_function));
    void insererPlusieursEltsFin(Double**, Int, Int) __attribute__((weak,no_instrument_function));

    void enleverPlusieursEltsDebut(Int) __attribute__((weak,no_instrument_function));
    void enleverPlusieursEltsMilieu(Int, Int) __attribute__((weak,no_instrument_function));
    void enleverPlusieursEltsFin(Int) __attribute__((weak,no_instrument_function));

    void enleverToutesLesOccurencesDUnElt(Double) __attribute__((weak,no_instrument_function));
    bool eltsEnOrdreDecroissant()const __attribute__((weak,no_instrument_function));
    bool contientElt(Double)const __attribute__((weak,no_instrument_function));
    Int chercherLIndexDeLaPremiereOccurenceDUnElt(Double)const __attribute__((weak,no_instrument_function));
    Int chercherLIndexDeLaDerniereOccurenceDUnElt(Double)const __attribute__((weak,no_instrument_function));
    Int calculerNombreDOccurencesDUnElt(Double)const __attribute__((weak,no_instrument_function));

    StructDeDonneesVerifCorr getStructDeDonneesVerifCorr() const __attribute__((weak,no_instrument_function));
    static bool verifierIntegrite(const STRUCT_DE_DONNEES_ETUD &) __attribute__((weak,no_instrument_function));
    bool compareCorrAvecEtud(const STRUCT_DE_DONNEES_ETUD &) const __attribute__((weak,no_instrument_function));
    void setStructDeDonneesVerifCorr(const StructDeDonneesVerifCorr&) __attribute__((weak,no_instrument_function));

#ifdef ITERATEUR
    public:
    class ITERATEUR {
        friend STRUCT_DE_DONNEES_CORR;
        friend STRUCT_DE_DONNEES_CORR_OTHER;
        friend STRUCT_DE_DONNEES_ETUD;
        friend STRUCT_DE_DONNEES_ETUD_OTHER;
        public:
            Double operator*() const __attribute__((weak,no_instrument_function));
            Double& operator*() __attribute__((weak,no_instrument_function));
            ITERATEUR& operator++() __attribute__((weak,no_instrument_function));
            ITERATEUR& operator--() __attribute__((weak,no_instrument_function));
            bool fin() const __attribute__((weak,no_instrument_function));
        private:
            ITERATEUR(ITERATEUR_TYPE_ELEMENT* ancre) __attribute__((weak));
            ITERATEUR_TYPE_ELEMENT* d_ancre;
            ITERATEUR_TYPE_ELEMENT* d_crt;
    };
    ITERATEUR premier() __attribute__((weak,no_instrument_function));

    bool compareCorrAvecEtud(const ITERATEUR&,
                             const STRUCT_DE_DONNEES_ETUD &,
                             const ITERATEUR&) const __attribute__((weak,no_instrument_function));
#endif

};

#endif // EXERCICESBASE_INCLUDED
