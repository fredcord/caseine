#!/bin/bash


#####################################################################################
# Remarque importante: si Caseine n'affiche aucune information lors de l'evaluation #
# cela indique que le fichier vpl_execution ne s'execute pas                        #
#####################################################################################


# Charge les fonctions check_program, get_source_files, etc
# Initialise aussi les variables pour la notation: VPL_GRADEMIN, VPL_GRADEMAX, etc.
. common_script.sh

# Verifie si g++ est disponible
check_program g++


# Fait une premiere compilation sans l'instrumentation pour afficher
# les eventuelles erreurs de compilation pour l'etudiant(e).
# Cette compilation n'inclut que 2 fichiers. Cela ne genere pas d'erreurs
# de compilation; toutes les methodes manquantes ont l'attribut __attribute__((weak))
# Par contre, le fichier ne peut pas etre execute
# -w: turn on warning
g++ -w -fno-diagnostics-color -rdynamic -ldl -o vpl_execution Exercices.cpp main.cpp

# Si la compilation n'a pas fonctionne, on s'arrete
if [ ! -f vpl_execution ]; then
    exit
fi

# Supprime le fichier pour eviter qu'il soit utilise pour l'evaluation
# Cela permet d'afficher une erreur si la deuxième compilation ne fonctionne pas
rm vpl_execution


# Recupere les parametres pour la notation sur caseine
if [ "$VPL_GRADEMIN" = "" ] ; then
	export VPL_GRADEMIN=0
	export VPL_GRADEMAX=10
fi

# Ajoute les parametres pour l'evaluation dans le fichier .h qui sera utilise pour la compilation
echo "#define VPL_GRADEMIN $VPL_GRADEMIN" >> ParametresCaseine.h
echo "#define VPL_GRADEMAX $VPL_GRADEMAX" >> ParametresCaseine.h
if [ "$VPL_MAXPROCESSES" != "" ] ; then
	echo "#define VPL_MAXPROCESSES $VPL_MAXPROCESSES" >> ParametresCaseine.h
fi

# Modifie les fichiers de l'etudiant pour la correction (instrumentation du code)
python3 instrumenteCodeSourceEtudiant.py

# La deuxieme compilation se fait avec tous les fichiers. Le code sera execute pour
# l'evaluation de l'exercice
# -g: generer debugging information
# -Wno-parentheses: supprimer les warning sur les parentheses dans les if et while
# -fstack-protector-strong: detecte les erreurs de depassement du tas (depassement d'un tableau statique)
# -no-pie: les fonctions ont les memes adresses dans le fichier executable et dans la memoire vive.
#          Permet d'identifier les fonctions par leur adresse bien plus facilement
# -fno-exceptions: supprime la gestion des exceptions. C'est gerer par notre programme directement
# -finstrument-functions: ajoute une function qui est executee systematiquement avant et apres l'execution des
# methodes de l'etudiant(e)
g++ -g -fno-diagnostics-color -rdynamic -fstack-protector-strong -Wno-parentheses -fno-exceptions -finstrument-functions -finstrument-functions-exclude-file-list=Evaluate.cpp,GestionPntrEtComplexite.cpp,main.cpp,Evaluate.h,Exercices.h,ExercicesCorr.h,GestionPntrEtComplexite.h,StructDeDonneesVerifCorr.h,initializer,list,vector,iterator,alloc,trait,stream,string,base,memory,uninitialized,new -o vpl_execution GestionPntrEtComplexite.cpp Evaluate.cpp Exercices-instr.cpp ExercicesCorr-instr.cpp ExercicesBase.cpp main.cpp -no-pie -ldl > /dev/null 2>&1


#g++ -g -fno-diagnostics-color -rdynamic -fsanitize=address -fno-omit-frame-pointer -fsanitize-recover=all -fno-optimize-sibling-calls -fstack-protector-strong -Wno-parentheses -finstrument-functions -finstrument-functions-exclude-file-list=Evaluate.cpp,GestionPntrEtComplexite.cpp,main.cpp,Evaluate.h,Exercices.h,ExercicesCorr.h,GestionPntrEtComplexite.h,StructDeDonnees.h,StructDeDonneesVerifCorr.h -o vpl_execution GestionPntrEtComplexite.cpp Evaluate.cpp Exercices.cpp ExercicesCorr.cpp main.cpp -no-pie -ldl > test.txt 2>&1
#clang++ -g -w -rdynamic -no-pie -ldl -fsanitize=address -fno-omit-frame-pointer -fsanitize-recover=all -fsanitize-memory-track-origins -fno-optimize-sibling-calls -fstack-protector-strong -Wno-parentheses -finstrument-functions  -o vpl_execution GestionPntrEtComplexite.cpp Evaluate.cpp Exercices-instr.cpp ExercicesCorr-instr.cpp main.cpp



