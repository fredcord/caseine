#include <iostream>
#include "ExercicesBase.h"


using namespace std;


/////////////////////////////////////////////////////////////////////////////
// Compare la structure de donnees de l'etudiant avec celle de la solution //
/////////////////////////////////////////////////////////////////////////////


// Verifie si la liste chainee est correcte au niveau de sa sctructure et des pointeurs
bool FileCorr::verifierIntegrite(const File &param)
{
    // Verifie si les valeurs sont dans les bons intervals pour eviter les crash
    if (param.d_quantite<0 || param.d_quantite>max_elt)
        return false;
    if (param.d_premier<0 || param.d_premier>max_elt)
        return false;
    if (param.d_dernier<0 || param.d_dernier>max_elt)
        return false;

    int quant=param.d_dernier-param.d_premier;
    if (param.d_dernier<param.d_premier)
        quant=max_elt-param.d_premier+param.d_dernier;
    if (quant!=param.d_quantite && quant!=0)
        return false;

    return true;
}

// Compare la correction avec la struct de l'etudiant. Retourne false si different
bool FileCorr::compareCorrAvecEtud(const File &student) const
{
    // Compare les champs
    if (this->d_quantite!=student.d_quantite)
        return false;
    // Calcul la quantite a partir de d_premier et d_dernier
    int quantCorr=this->d_dernier-this->d_premier;
    if (this->d_dernier<this->d_premier)
        quantCorr=max_elt-this->d_premier+this->d_dernier;
    int quantEtud=student.d_dernier-student.d_premier;
    if (student.d_dernier<student.d_premier)
        quantEtud=max_elt-student.d_premier+student.d_dernier;
    if (quantCorr!=quantEtud)
        return false;

    // Compare le contenu du tableau
    if (quantCorr>0)
    {
        int iCorr=this->d_premier,iEtud=student.d_premier;
        while(iCorr!=this->d_dernier && this->d_file[iCorr]==student.d_file[iEtud])
        {
            iCorr++;
            if (iCorr==FileCorr::max_elt)
                iCorr=0;
            iEtud++;
            if (iEtud==FileCorr::max_elt)
                iEtud=0;
        }
        if (iCorr!=this->d_dernier)
            return false;
    }
    return true;
}

// genere la structDeDonneesVerifCorr a partir de la liste
StructDeDonneesVerifCorr FileCorr::getStructDeDonneesVerifCorr() const
{
    StructDeDonneesVerifCorr vect;
    if (d_quantite==0)
        return vect;

    int id=d_premier;
    int quant=0;
    while(quant<d_quantite)
    {
        vect.insererUnEltFin(d_file[id]);
        id++;
        if (id==max_elt)
            id=0;
        quant++;
    }
    return vect;
}

// Constructeur avec la structDeDonneesVerifCorr
void FileCorr::setStructDeDonneesVerifCorr(const StructDeDonneesVerifCorr &tab)
{
    d_quantite=0;
    // Les elements sont intentionnelement a cheval sur la fin et le debut du tableau
    d_premier=max_elt-2;
    d_dernier=max_elt-2;

    // On insere les elements du tableau
    for (int i=0;i<tab.getNbColonnes();i++)
    {
        d_file[d_dernier]=tab[i];
        d_dernier++;
        if (d_dernier==max_elt)
            d_dernier=0;
        d_quantite++;
    }
}

// DEBUT EXO_CONSTRUCTEUR
FileCorr::FileCorr():d_premier{0},d_dernier{0},d_quantite{0}
{
}
// FIN EXO_CONSTRUCTEUR


// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
FileCorr::FileCorr(const FileCorr &param):d_premier{param.d_premier},d_dernier{param.d_dernier},d_quantite{param.d_quantite}
{
    // Copie le tableau
    int i=d_premier,q=0;
    while(q<d_quantite)
    {
        d_file[i]=param.d_file[i];
        q++;
        i++;
        if (i==max_elt)
            i=0;
    }
}
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE


// DEBUT EXO_CONCATENATION_FIN
void FileCorr::concatenationFin(const FileCorr &param)
{
    int id2=param.d_premier;
    int quant=0;
    while(quant<param.d_quantite)
    {
        d_file[d_dernier]=param.d_file[id2];
        d_dernier++;
        if (d_dernier==max_elt)
            d_dernier=0;
        id2++;
        if (id2==max_elt)
            id2=0;
        quant++;
        d_quantite++;
    }
}
// FIN EXO_CONCATENATION_FIN


// DEBUT EXO_OP_CONCATENATION
FileCorr &FileCorr::operator+=(const FileCorr &param)
{
    int id2=param.d_premier;
    int quant=0;
    while(quant<param.d_quantite)
    {
        d_file[d_dernier]=param.d_file[id2];
        d_dernier++;
        if (d_dernier==max_elt)
            d_dernier=0;
        id2++;
        if (id2==max_elt)
            id2=0;
        quant++;
        d_quantite++;
    }
    return *this;
}
// FIN EXO_OP_CONCATENATION


// DEBUT EXO_OP_EGAL
bool FileCorr::operator==(const FileCorr &param) const
{
    if (this==&param)
        return true;
    if (d_quantite!=param.d_quantite)
        return false;
    int i=d_premier,j=param.d_premier;
    int quant=0;
    while(quant<d_quantite &&  d_file[i]==param.d_file[j])
    {
        i++;
        if (i==max_elt)
            i=0;
        j++;
        if (j==max_elt)
            j=0;
        quant++;
    }
    return quant==d_quantite;
}
// FIN EXO_OP_EGAL


// DEBUT EXO_OP_AFFECTATION
FileCorr &FileCorr::operator=(const FileCorr &param)
{
    if (this==&param)
        return *this;
    d_premier=param.d_premier;
    d_dernier=param.d_dernier;
    d_quantite=param.d_quantite;

    int id=param.d_premier;
    int quant=0;
    while(quant<param.d_quantite)
    {
        d_file[id]=param.d_file[id];
        id++;
        if (id==max_elt)
            id=0;
        quant++;
    }
    return *this;
}
// FIN EXO_OP_AFFECTATION


// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
void FileCorr::additionnerATousLesElts(double val)
{
    int id=d_premier;
    int quant=0;
    while(quant<d_quantite)
    {
        d_file[id]+=val;
        id++;
        if (id==max_elt)
            id=0;
        quant++;
    }
}
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS


// DEBUT EXO_INSERER_UN_ELT_FIN
void FileCorr::insererUnEltFin(double val)
{
    d_file[d_dernier++] = val;
    d_dernier %= max_elt;
    d_quantite++;
}
// FIN EXO_INSERER_UN_ELT_FIN


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
void FileCorr::enleverPlusieursEltsDebut(int nb)
{
    int i=0;
    while (i<nb && d_quantite>0)
    {
        d_premier++;
        d_premier %= max_elt;
        d_quantite--;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT

