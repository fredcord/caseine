#include "ExercicesBase.h"

using namespace std;

// DEBUT EXO_CONSTRUCTEUR
File::File():d_premier{0},d_dernier{0},d_quantite{0}
{
}
// FIN EXO_CONSTRUCTEUR


// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
File::File(const File &param):d_premier{param.d_premier},d_dernier{param.d_dernier},d_quantite{param.d_quantite}
{
    // Copie le tableau
    int i=d_premier,q=0;
    while(q<d_quantite)
    {
        d_file[i]=param.d_file[i];
        q++;
        i++;
        if (i==max_elt)
            i=0;
    }
}
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE


// DEBUT EXO_CONCATENATION_FIN
void File::concatenationFin(const File &param)
{
    int id2=param.d_premier;
    int quant=0;
    while(quant<param.d_quantite)
    {
        d_file[d_dernier]=param.d_file[id2];
        d_dernier++;
        if (d_dernier==max_elt)
            d_dernier=0;
        id2++;
        if (id2==max_elt)
            id2=0;
        quant++;
        d_quantite++;
    }
}
// FIN EXO_CONCATENATION_FIN

// DEBUT EXO_OP_CONCATENATION
File &File::operator+=(const File &param)
{
    int id2=param.d_premier;
    int quant=0;
    while(quant<param.d_quantite)
    {
        d_file[d_dernier]=param.d_file[id2];
        d_dernier++;
        if (d_dernier==max_elt)
            d_dernier=0;
        id2++;
        if (id2==max_elt)
            id2=0;
        quant++;
        d_quantite++;
    }
    return *this;
}
// FIN EXO_OP_CONCATENATION


// DEBUT EXO_OP_EGAL
bool File::operator==(const File &param) const
{
    if (this==&param)
        return true;
    if (d_quantite!=param.d_quantite)
        return false;
    int i=d_premier,j=param.d_premier;
    int quant=0;
    while(quant<d_quantite &&  d_file[i]==param.d_file[j])
    {
        i++;
        if (i==max_elt)
            i=0;
        j++;
        if (j==max_elt)
            j=0;
        quant++;
    }
    return quant==d_quantite;
}
// FIN EXO_OP_EGAL


// DEBUT EXO_OP_AFFECTATION
File &File::operator=(const File &param)
{
    if (this==&param)
        return *this;
    d_premier=param.d_premier;
    d_dernier=param.d_dernier;
    d_quantite=param.d_quantite;

    int id=param.d_premier;
    int quant=0;
    while(quant<param.d_quantite)
    {
        d_file[id]=param.d_file[id];
        id++;
        if (id==max_elt)
            id=0;
        quant++;
    }
    return *this;
}
// FIN EXO_OP_AFFECTATION


// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
void File::additionnerATousLesElts(double val)
{
    int id=d_premier;
    int quant=0;
    while(quant<d_quantite)
    {
        d_file[id]+=val;
        id++;
        if (id==max_elt)
            id=0;
        quant++;
    }
}
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS


// DEBUT EXO_INSERER_UN_ELT_FIN
void File::insererUnEltFin(double val)
{
    d_file[d_dernier++] = val;
    d_dernier %= max_elt;
    d_quantite++;
}
// FIN EXO_INSERER_UN_ELT_FIN


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
void File::enleverPlusieursEltsDebut(int nb)
{
    int i=0;
    while (i<nb && d_quantite>0)
    {
        d_premier++;
        d_premier %= max_elt;
        d_quantite--;
        i++;
    }
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT


