#ifndef DOUBLE_ET_INT_H
#define DOUBLE_ET_INT_H

#include <string.h>
#include <sstream>
#include <assert.h>
#include <iostream>

using namespace std;


#define DEFAULT_VAL 0xAAAAAAAA


class Int;
class Double;

extern "C"
{
void __cyg_profile_func_enter(void *this_fn, void *call_site) __attribute__ ((no_instrument_function));
void __cyg_profile_func_exit(void *this_fn, void *call_site) __attribute__((no_instrument_function));
void libere_memoire();
}

class GestionPntrEtComplexite
{
    friend class Int;
    friend class Double;

    friend void* operator new (size_t size);
    friend void *operator new[](size_t size);
    friend void operator delete(void* ptr, size_t size);
    friend void operator delete(void* ptr) _GLIBCXX_USE_NOEXCEPT;
    friend void operator delete[](void* ptr, size_t taille);
    friend void operator delete[](void* ptr) _GLIBCXX_USE_NOEXCEPT;
    friend void __cyg_profile_func_enter(void *this_fn, void *call_site);
    friend void __cyg_profile_func_exit(void *this_fn, void *call_site);
    friend void libere_memoire();
private:
    // La marge pour agrandir les tableaux dynamiques
    const static int MARGE=250;
    void* safe_malloc(size_t taille);
private:
    // Tableau pour enregistrer les pointeurs allocations dynamiques (new et delete)
    class TabPntr
    {
        friend class GestionPntrEtComplexite;
    private:
        struct Pntr
        {
            void *adr;              // Adresse du pointeur aloue
            unsigned int tag;       // Un tag unique pour differencier 2 pointeurs avec la meme adresse
                                    // Ca peut arriver avec des new et delete successifs
            int taille;             // Taille du pointeur aloue
            bool allocPourTab;      // Indique si le pointeur a ete alloue pour un tableau
            bool libere;            // Indique si le pointeur a ete libere
        };
        // Le constructeur n'est jamais appele parce qu'on utilise calloc
        TabPntr() __attribute__((no_instrument_function))
        {
            nb=0;
            tab=nullptr;
        }
        // Le desctructeur n'est jamais appele parce qu'on doit garder cette structure de donnees jusqu'a la fin
        ~TabPntr() __attribute__((no_instrument_function))
        {
            free(tab);
            tab=nullptr;
            nb=0;
        }
        Pntr& operator[](int i) __attribute__((no_instrument_function))
        {
            return tab[i];
        }
        int nb;
        Pntr *tab;
    };


    // Tableau pour enregistrer la creation/destruction des instances
    class TabInstance
    {
        friend class GestionPntrEtComplexite;
    private:
        struct Inst
        {
            void* adr;              // Adresse de l'instance
            bool dyn;              // Indique si l'instance a ete creee dynamiquement (avec new/delete)
            bool libere;            // Indique si l'instance (dans le cas ou elle est dans un pointeur) a ete libere
        };
        // Le constructeur n'est jamais appele parce qu'on utilise calloc
        TabInstance():nb{0},tab{nullptr}{}
        // Le desctructeur n'est jamais appele parce qu'on doit garder cette structure de donnees jusqu'a la fin
        ~TabInstance()
        {
            free(tab);
            tab=nullptr;
            nb=0;
        }
        Inst& operator[](int i){return tab[i];}
        int nb;
        Inst *tab;
    };


  public:
    class TabAppelsInstructions
    {
        friend class Double;
        friend class Int;
        friend void* operator new (size_t size);
        friend void *operator new[](size_t size);
        friend void operator delete(void* ptr, size_t size);
        friend void operator delete(void* ptr) _GLIBCXX_USE_NOEXCEPT;
        friend void operator delete[](void* ptr, size_t taille);
        friend void operator delete[](void* ptr) _GLIBCXX_USE_NOEXCEPT;
        friend class GestionPntrEtComplexite;
        friend void __cyg_profile_func_enter(void *this_fn, void *call_site);
        friend void __cyg_profile_func_exit(void *this_fn, void *call_site);

    public:
        enum Type { UNDEF=0, FOR=1, WHILE=2, IF=3, RETURN=4, NEW=5, DELETE=6,
                    ENTREE_FONCT=7, SORTIE_FONCT=8,
                    DOUBLE_NEW=9, DOUBLE_DELETE=10, DOUBLE_CONSTRUCTEUR=11, DOUBLE_AFFECTATION=12,
                    DOUBLE_CAST=13, DOUBLE_PLUS_EGAL=14, DOUBLE_MOINS_EGAL=15, DOUBLE_MULT_EGAL=16,
                    DOUBLE_DIV_EGAL=17, DOUBLE_PRE_PLUS_PLUS=18, DOUBLE_POST_PLUS_PLUS=19,
                    DOUBLE_PRE_MOINS_MOINS=20, DOUBLE_POST_MOINS_MOINS=21, DOUBLE_ADRESSE=22,
                    DOUBLE_EGAL_EGAL=23, DOUBLE_INFERIEUR_EGAL=24, DOUBLE_INFERIEUR=25,
                    DOUBLE_DIFFERENT=26, DOUBLE_SUPERIEUR_EGAL=27, DOUBLE_SUPERIEUR=28,
                    INT_NEW=29, INT_DELETE=30, INT_CONSTRUCTEUR=31, INT_AFFECTATION=32, INT_CAST=33,
                    INT_PLUS_EGAL=34, INT_MOINS_EGAL=35, INT_MULT_EGAL=36, INT_DIV_EGAL=37,
                    INT_PRE_PLUS_PLUS=38, INT_POST_PLUS_PLUS=39, INT_PRE_MOINS_MOINS=40,
                    INT_POST_MOINS_MOINS=41, INT_ADRESSE=42, INT_POURCENT_EGAL=43,
                    INT_EGAL_EGAL=44, INT_INFERIEUR_EGAL=45, INT_INFERIEUR=46,
                    INT_DIFFERENT=47, INT_SUPERIEUR_EGAL=48, INT_SUPERIEUR=49
                    };

        // Structure pour enregistrer une instruction
        struct Instruction
        {
            Type type;          // valeurs possibles: FOR, WHILE, IF, etc
            int nbAppels;       // Nombre de fois que cette instruction a ete appelee
            char* nomMethode;   // Nom de la methode dans laquelle se trouve l'instruction
            const void* ptr;          // Pointeur vers l'instruction
        };
        // Structure pour enregistrer un appel a une instruction
        struct Appel
        {
            int idInstruct;     // L'id de l'instruction
            const void* pthis;  // Pointeur vers this
            const void* param;  // Pointeur vers le parameter
        };
        // Structure pour enregistrer une methode dans la pile d'execution
        struct PileMethode
        {
            const void* ptr;    // L'adresse de la methode de l'instruction
            char nom[1024];     // Nom de la methode
        };

    public:
        TabAppelsInstructions():nbInstructions{0},nbAppels{0},nbPileMethodes{0},tabPileMethodes{},
                                tabInstructions{nullptr},tabAppels{nullptr} {}
        TabAppelsInstructions(const TabAppelsInstructions &t):nbInstructions{t.nbInstructions},nbAppels{t.nbAppels},
                                                              nbPileMethodes{t.nbPileMethodes},tabPileMethodes{t.tabPileMethodes},
                                                              tabInstructions{nullptr},tabAppels{nullptr}
        {
            tabInstructions=(Instruction*)calloc(nbInstructions-nbInstructions%MARGE+MARGE,sizeof(Instruction));
            for (int i=0;i<nbInstructions;i++)
            {
                tabInstructions[i].nbAppels=t.tabInstructions[i].nbAppels;
                tabInstructions[i].ptr=t.tabInstructions[i].ptr;
                tabInstructions[i].type=t.tabInstructions[i].type;
                if (t.tabInstructions[i].nomMethode==nullptr)
                    tabInstructions[i].nomMethode=nullptr;
                else
                {
                    tabInstructions[i].nomMethode=(char*)calloc(strlen(t.tabInstructions[i].nomMethode)+1,sizeof(char));
                    memcpy(tabInstructions[i].nomMethode,t.tabInstructions[i].nomMethode,strlen(t.tabInstructions[i].nomMethode));
                }
            }
            tabAppels=(Appel*)calloc(nbAppels-nbAppels%MARGE+MARGE,sizeof(Appel));
            memcpy(tabAppels,t.tabAppels,sizeof(Appel)*nbAppels);
        }
        TabAppelsInstructions &operator=(const TabAppelsInstructions &t)
        {
            for (int i=0;i<nbInstructions;i++)
                free(tabInstructions[i].nomMethode);
            nbInstructions=t.nbInstructions;
            tabInstructions=(Instruction*)realloc(tabInstructions,(nbInstructions-nbInstructions%MARGE+MARGE)*sizeof(Instruction));
            for (int i=0;i<nbInstructions;i++)
            {
                tabInstructions[i].nbAppels=t.tabInstructions[i].nbAppels;
                tabInstructions[i].ptr=t.tabInstructions[i].ptr;
                tabInstructions[i].type=t.tabInstructions[i].type;
                if (t.tabInstructions[i].nomMethode==nullptr)
                    tabInstructions[i].nomMethode=nullptr;
                else
                {
                    tabInstructions[i].nomMethode=(char*)calloc(strlen(t.tabInstructions[i].nomMethode)+1,sizeof(char));
                    memcpy(tabInstructions[i].nomMethode,t.tabInstructions[i].nomMethode,strlen(t.tabInstructions[i].nomMethode));
                }
            }
            nbAppels=t.nbAppels;
            tabAppels=(Appel*)realloc(tabAppels,(nbAppels-nbAppels%MARGE+MARGE)*sizeof(Appel));
            memcpy(tabAppels,t.tabAppels,sizeof(Appel)*nbAppels);
            return *this;
        }
        ~TabAppelsInstructions()
        {
            reset();
        }
        void reset()
        {
            for (int i=0;i<nbInstructions;i++)
                free(tabInstructions[i].nomMethode);
            free(tabInstructions);
            tabInstructions=nullptr;
            nbInstructions=0;
            free(tabAppels);
            tabAppels=nullptr;
            nbAppels=0;
        }
        Instruction& operator[](int i){return tabInstructions[i];}
        void supprInstructionsIdentiques(TabAppelsInstructions &t);
        void supprInstructionsDUneClasse(const char* nomClasse);
        int getNbAppels() const
        {
            return nbAppels;
        }
        void print() const
        {
            std::cout<<"liste des appels:"<<endl;
            for (int i=0;i<nbAppels;i++)
                std::cout<<tabInstructions[tabAppels[i].idInstruct].type<<": "<<tabInstructions[tabAppels[i].idInstruct].nomMethode\
                         <<endl;
            std::cout<<endl;
        }
        int getNbAppelsStructDeCtrl() const
        {
            int c=0;
            for (int i=0;i<nbInstructions;i++)
                if (tabInstructions[i].type==WHILE || tabInstructions[i].type==FOR ||
                    tabInstructions[i].type==IF || tabInstructions[i].type==RETURN)
                    c+=tabInstructions[i].nbAppels;
            return c;
        }
        int getNbAppelsBoucle() const
        {
            int c=0;
            for (int i=0;i<nbInstructions;i++)
                if (tabInstructions[i].type==WHILE || tabInstructions[i].type==FOR)
                    c+=tabInstructions[i].nbAppels;
            return c;
        }
        int getNbAppelsIf() const
        {
            int c=0;
            for (int i=0;i<nbInstructions;i++)
                if (tabInstructions[i].type==IF)
                    c+=tabInstructions[i].nbAppels;
            return c;
        }
        int getNbInstructions() const
        {
            return nbInstructions;
        }
        int getNbInstructionsBoucle() const
        {
            int c=0;
            for (int i=0;i<nbInstructions;i++)
                if (tabInstructions[i].type==WHILE || tabInstructions[i].type==FOR)
                    c++;
            return c;
        }
        int getNbInstructionsIf() const
        {
            int c=0;
            for (int i=0;i<nbInstructions;i++)
                if (tabInstructions[i].type==IF)
                    c++;
            return c;
        }
        Instruction& getInstructionBoucle(int id)
        {
            int c=-1,i=0;
            while(i<nbInstructions)
            {
                if (c==-1 && (tabInstructions[i].type==WHILE || tabInstructions[i].type==FOR))
                    c=0;
                if (id==c)
                    return tabInstructions[i];
                i++;
                if (tabInstructions[i].type==WHILE || tabInstructions[i].type==FOR)
                    c++;
            }
            assert(0);
            return tabInstructions[0];
        }
        Instruction& getInstructionStructDeCtrl(int id)
        {
            int c=-1,i=0;
            while(i<nbInstructions)
            {
                if (c==-1 && (tabInstructions[i].type==WHILE || tabInstructions[i].type==FOR ||
                              tabInstructions[i].type==IF || tabInstructions[i].type==RETURN))
                    c=0;
                if (id==c)
                    return tabInstructions[i];
                i++;
                if (tabInstructions[i].type==WHILE || tabInstructions[i].type==FOR ||
                    tabInstructions[i].type==IF || tabInstructions[i].type==RETURN)
                    c++;
            }
            assert(0);
            return tabInstructions[0];
        }
        // Retourne true si l'instruction return a ete appelee. Pas forcement toujours la derniere instruction
        // Des instructions cast peuvent etre executees apres
        bool contientReturn()
        {
            int i=nbInstructions-1;
            while(i>=0 && tabInstructions[i].type!=RETURN)
                i--;
            return i>=0;
        }
        // Retourne le nombre de classes
        int getNbClasses();
        // Retourne le nombre de methodes
        int getNbMethodes();
        // Retourne les noms de la classe et de la methode
        // Suppose que toutes les instruction sont de la meme methode
        void getNomsClasseEtMethode(char* nomClasse, char* nomMethode);
        void getNomMethodeOuFonction(char* nomMethode);

        // Retourne le nombre total de pointeur alloues
        int getNbTotalPntr();
        // Retourne le nombre total de pointeurs alloues et pas encore liberes
        int getNbTotalPntrPasLiberes();
        // Retourne le nombre total de pointeurs liberes
        // On n'a pas forcement getNbTotalPntr = getNbTotalPntrPasLiberes + getNbTotalPntrLiberes
        // Certains pointeurs peuvent etre alloue avant le comptage des instructions
        int getNbTotalPntrLiberes();

        // Nombre d'appels au dela duquel on considere qu'il y a une boucle infinie
        const static int NB_MAX_APPELS_BOUCLE_INFINIE=1000;

    private:
        int nbInstructions;
        int nbAppels;
        int nbPileMethodes;
        // La pile d'execution des methodes
        PileMethode tabPileMethodes[500];
        int nbFiltresPile;
        void* tabFiltresPile[1024];
        // Tableau contenant les instructions
        Instruction *tabInstructions;
        // Tableau des appels contenant les instructions appelees dans l'ordre
        Appel *tabAppels;
        // Extrait le nom de la classe et de la methode a partir du nom complet
        void supprInstruction(const void *ptr);
        // Ajoute l'instruction et retourne son index
        int ajoutInstruction(TabAppelsInstructions::Type type, const void* pThis=nullptr,
                                 const void *param=nullptr) __attribute__((no_instrument_function));
        int ajoutPntrInstruction(TabAppelsInstructions::Type type, const void* pntrInstruct, const char* nomMethode,
                                 const void* pThis=nullptr,
                                 const void* param=nullptr) __attribute__((no_instrument_function));
        void ajoutPileMethode(const void *ptr) __attribute__((no_instrument_function));
        void supprPileMethode(const void *ptr) __attribute__((no_instrument_function));
    };

  public:
    // Tableau pour les fonctions disponibles dans le fichier executable
    class TabFonctionsExecutable
    {
        friend class GestionPntrEtComplexite;
    public:
        // Les etats possibles pour une fonction
        enum Etat { PAS_DECLARE=0, DECLARE=1, IMPLEMENTE=2};
    private:

        struct FonctionExecutable
        {
            char nom[1024];        // Nom de la fonction
            void* addr;            // Son adresse
        };
        // Le constructeur n'est jamais appele parce qu'on utilise calloc
        TabFonctionsExecutable():nb{0},tab{nullptr}{}
        // Le desctructeur n'est jamais appele parce qu'on doit garder cette structure de donnees jusqu'a la fin
        ~TabFonctionsExecutable()
        {
            free(tab);
            tab=nullptr;
            nb=0;
        }
        void ajout(char* nom, void *addr)
        {
            if (nb%MARGE==0)
            {
                tab=(FonctionExecutable*)realloc(tab,sizeof(FonctionExecutable)*(nb+MARGE));
                memset(&tab[nb],0x00,sizeof(FonctionExecutable)*MARGE);
            }
            assert(strlen(nom)+1<=sizeof(FonctionExecutable::nom));
            memcpy(tab[nb].nom,nom,strlen(nom)+1);
            tab[nb].addr=addr;
            nb++;
        }
        FonctionExecutable& operator[](int i){return tab[i];}
    public:
        FonctionExecutable& operator[](const void *addr)
        {
            for (int i=0;i<nb;i++)
                if (tab[i].addr==addr)
                    return tab[i];
            assert(0);
            return tab[0];
        }
        void* getAddr(const char* nom)
        {
            // Si la table est vide, on la remplit
            if (nb==0)
                init();
            for (int i=0;i<nb;i++)
            {
                if (strcmp(tab[i].nom,nom)==0)
                    return tab[i].addr;
            }
            return 0;
        }
    private:
        void init() __attribute__ ((no_instrument_function));
        void print() const
        {
            std::cout<<"liste des fonctions:"<<endl;
            for (int i=0;i<nb;i++)
                cout<<i<<": "<<tab[i].nom<<endl;
        }

        int nb;
        FonctionExecutable *tab;
    };

public:
    // Active/desactive le suivi des appels
    bool compterInstructions=false;

private:
    // Classe entièrement statique
    // Pour compter les instances
    TabInstance tabInstance;
    // Pour compter les allocations dynamiques
    TabPntr tabPntr;

    // Pour lister les instructions et leurs appels
    TabAppelsInstructions tabAppelsInstructions;

    // Constructeur en private. Ce constructeur n'est pas utilise.
    // C'est juste pour forcer l'utilisation de static GestionPntrEtComplexite &get()
    // pour recuperer l'instance
    GestionPntrEtComplexite(){}
    // Destructeur en private
    ~GestionPntrEtComplexite(){}

    // Methodes pour gerer les allocation memoire et les instances
    void ajoutPntr(void* ptr, int taille, bool allocPourTab);
    void supprPntr(void* ptr, bool allocPourTab=false);
    void ajoutInst(void* ptr);
    void supprInst(void* ptr, int taille);
    bool instValide(const void* instAdr);           // Verifie si l'instance correspond a un pntr valide
    void doExit(int numLigne);
    int chercherPntrIdx(const void* ptr);
public:
    TabFonctionsExecutable tabFonctionsExecutable;
public:

    // Methode pour etre certain que la structure de donnees pour gerer les pointeurs est initialisee en premier
    // https://isocpp.org/wiki/faq/ctors#static-init-order-on-first-use
    static GestionPntrEtComplexite &get() __attribute__ ((no_instrument_function))
    {
        // Allocation avec calloc. L'allocation avec new cree un appel recursif infini
        // Probleme: les constructeur ne sont pas appeles.
        // Mais avec calloc, on initialise tous les champs à 0x00
        static GestionPntrEtComplexite* gestPntrEtComplex = nullptr;
        if (gestPntrEtComplex == nullptr)
        {
            // Creation de l'instance
            gestPntrEtComplex = (GestionPntrEtComplexite*)calloc(sizeof(GestionPntrEtComplexite),1);

        }
        return *gestPntrEtComplex;
    }

    // Variable pour indiquer que le programme doit s'arreter apres une erreur
    bool exitApresErreur=false;
    bool pntrValide(const void* ptr) {return chercherPntrIdx(ptr)>=0;}
    bool pntrDansTableauValide(const void* ptr);
    int pntrTailleAllouee(void* ptr);
    bool peutSupprPntr(void* ptr, bool allocPourTab=false);

    // Methodes pour la complexite en temps
    bool ajoutInstruction(const char* typeInstruction, const char* fichier, int numLigne);
    void resetEtStartCompterInstructions();
    void stopCompterInstructions();
    TabAppelsInstructions recupereTabAppelInstructions()
    {
        return tabAppelsInstructions;
    }

    // Pour les messages d'erreur pour l'etudiant
    char* msgErreur=nullptr;
    // Nom du fichier de l'etudiant pour afficher la localisation des erreurs
    string nomFichierSourceEtudiant;


    // Extrait le nom de la methode et de la classe a partir du nom complet
    static bool extraitNomClasseEtMethode(const char* str, char *nomClasse, char *nomMethode=nullptr);
    // Retourne le fichier et le numero de la ligne pour l'instruction courante
    char* recupereFichierEtNumeroLigneCourants(const char* fichier_specifique=nullptr);
    char* recupereFichierEtNumeroTraceSymbol(void* ptrFonction, const char* fichier_specifique=nullptr);

    // Definit le nom des classes a enregistrer dans la pile
    void definirFiltresPile( std::initializer_list<string> list )
    {
        if (tabFonctionsExecutable.nb==0)
            tabFonctionsExecutable.init();
        tabAppelsInstructions.nbFiltresPile=0;
        memset(tabAppelsInstructions.tabFiltresPile,0x00,sizeof(tabAppelsInstructions.tabFiltresPile));
        for( auto elem : list )
        {
            // Le filtre se fait avec les adresses des fonctions. C'est beaucoup plus rapide
            // Par contre, ca necessite de linker avec l'option -no-pie
            // -no-pie: les fonctions ont les memes adresses dans le fichier executable et dans la memoire vive.

            for (int i=0;i<tabFonctionsExecutable.nb;i++)
            {
                if (strstr(tabFonctionsExecutable[i].nom,elem.c_str())!=nullptr)
                {
                    // Une methode trouvee. On enregistre son adresse.
                    tabAppelsInstructions.tabFiltresPile[tabAppelsInstructions.nbFiltresPile]=tabFonctionsExecutable[i].addr;
                    tabAppelsInstructions.nbFiltresPile++;
                }
            }
        }
        if (tabAppelsInstructions.nbFiltresPile==0)
        {
            cout<<"Compiler avec ces options:"<<endl;
            // Permet d'appeler une fonction avant et apres chaque methode
            cout<<"-finstrument-functions"<<endl;
            cout<<"-finstrument-functions-exclude-file-list=Evaluate.cpp,GestionPntrEtComplexite.cpp,main.cpp,Evaluate.h,Exercices.h,GestionPntrEtComplexite.h,StructDeDonnees.h,StructDeDonneesVerifCorr.h"<<endl;
            // -no-pie: telling gcc not to make a position independent executable
            // Les addresses (pointeurs) des fonctions dans le fichier executable sont les
            // memes que les adresses dans la memoire vive
            cout<<"-no-pie"<<endl;
            assert(0);
        }

    }
};


class Double
{
    friend class Int;
    public:
        // Constructeur
        Double();
        constexpr Double(double val):d_v(val)
        {
#ifdef DEBUG
            cout<<"Double::Double(double val)"<<endl;
#endif
            GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::DOUBLE_CONSTRUCTEUR);
            // Ajout de l'instance
            GestionPntrEtComplexite::get().ajoutInst(this);
        }
        Double(const Double &val);
        Double(Double &val);
        Double(const Int &val);

        // TRES IMPORTANT: ne pas mettre le destructeur
        // Autrement la taille allouee est augmentee de 8 octets
        // et l'adresse du pntr est differente pour new et malloc
        // Ceci rend le tracage des allocations plus complique
        //~Double();

        // Affectation
        //Double &operator=(double val);
        Double &operator=(const Double &d);

        // Necessaire
        Double &operator+=(const Double &);
        Double &operator-=(const Double &);
        Double &operator*=(const Double &);
        Double &operator/=(const Double &);

        // Incrementation
        Double& operator++();       // Prefix increment operator (++i)
        Double operator++(int);     // Postfix increment operator  (i++)
        Double& operator--();       // Prefix decrement operator (--i)
        Double operator--(int);     // Postfix decrement operator (i--)

        // Test egalite, inferiorite, superiorite
        // Ne pas les mettre sinon l'erreur suivante apparait
        // error: ambiguous overload for ‘operator==’ (operand types are ‘Int’ and ‘int’)
        // candidate: ‘operator==(int, int)’ <built-in>

        // Operateur de conversion en double, seul le cast en double doit etre implemente
        // Sinon, l'erreur "ambiguous overload" apparait et tous les
        // operateurs (==, !=, <, etc) doivent etre implementes
        operator double() const;
        // Ce deuxieme operateur de conversion ne semble pas necessaire. S'il est absent, c'est le
        // premier qui est appele. En tout, clang ne compile pas avec.
        //&operator double();
        /*
        operator int() const;
        operator float() const;*/

        // operateur de surcharge de &
        Double* operator&();

        // Surcharge des operateurs d'allocation et de liberation
        static void *operator new(size_t size);
        static void *operator new[](size_t size);
        static void operator delete(void* ptr);
        static void operator delete[](void* ptr);
        static void operator delete[](void* ptr, size_t taille);

    public:
        void ajoutInst();
        void supprInst();
        bool instValide() const;

    private:
        // Variables de la classe
        // On donne une valeur par defaut pour verifier
        // si la variable a bien ete initialisee par l'etudiant
        double d_v=DEFAULT_VAL;
};

class Int
{
    friend class Double;
    public:
        // Constructeur
        Int();
        // constexpr est necessaire pour const int MAX=4; double tab[MAX];
        constexpr Int(int val):d_v(val)
        {
#ifdef DEBUG
            cout<<"Int::Int(int val)"<<endl;
#endif
            GestionPntrEtComplexite::get().tabAppelsInstructions.ajoutInstruction(GestionPntrEtComplexite::TabAppelsInstructions::INT_CONSTRUCTEUR);
            // Ajout de l'instance
            GestionPntrEtComplexite::get().ajoutInst(this);
        }

        //Int(int val);
        Int(const Int &val);
        Int(Int &val);
        //Int(Double &val);
        Int(const Double &val);
        // TRES IMPORTANT: ne pas mettre le destructeur
        // Autrement la taille allouee est augmentee de 8 octets
        // et l'adresse du pntr est differente pour new et malloc
        // Ceci rend le tracage des allocations plus compliquees
        //~Int();

        // Affectation
        //Int &operator=(int val);
        Int &operator=(const Int &d);

        // Necessaire
        Int &operator+=(const Int &);
        Int &operator-=(const Int &);
        Int &operator*=(const Int &);
        Int &operator/=(const Int &);
        Int &operator%=(const Int &);

        // Incrementation
        Int& operator++();       // Prefix increment operator (++i)
        Int operator++(int);     // Postfix increment operator  (i++)
        Int& operator--();       // Prefix decrement operator (--i)
        Int operator--(int);     // Postfix decrement operator (i--)

        // Test egalite, inferiorite, superiorite
        // Ne pas les mettre sinon l'erreur suivante apparait
        // error: ambiguous overload for ‘operator==’ (operand types are ‘Int’ and ‘int’)
        // candidate: ‘operator==(int, int)’ <built-in>

        // Operateur de conversion en int, seul le cast en int doit etre implemente
        // Sinon, l'erreur "ambiguous overload" apparait et tous les
        // operateurs (==, !=, <, etc) doivent etre implementes
        operator int() const;
        // Ce deuxieme operateur de conversion ne semble pas necessaire. S'il est absent, c'est le
        // premier qui est appele. En tout, clang ne compile pas avec.
        //&operator int();

        // operateur de surcharge de &
        Int* operator&();

        // Surcharge des operateurs d'allocation et de liberation
        static void *operator new(size_t size);
        static void *operator new[](size_t size);
        static void operator delete(void* ptr);
        static void operator delete[](void* ptr);
        static void operator delete[](void* ptr, size_t taille);

    public:
        void ajoutInst();
        void supprInst();
        bool instValide() const;
    private:
        // Variables de la classe
        // On donne une valeur par defaut pour verifier
        // si la variable a bien ete initialise
        int d_v=DEFAULT_VAL;
};

#endif // DOUBLE_ET_INT_H

