////////////////////////////////////////////////////////////////////////////////////
// Fichier genere automatiquement pour l'instrumentation du code. Ne pas modifier //
////////////////////////////////////////////////////////////////////////////////////


#include "ExercicesBase.h"


// DEBUT EXO_CONSTRUCTEUR
TableauPseudoDynamique_ConstructDestruct::TableauPseudoDynamique_ConstructDestruct():d_n{0}
{
}
// FIN EXO_CONSTRUCTEUR


// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
TableauPseudoDynamique_ConstructDestruct::TableauPseudoDynamique_ConstructDestruct(const TableauPseudoDynamique &param):d_n{param.d_n}
{
    // Copie le tableau
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        d_table[i]=param.d_table[i];
}
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE



// DEBUT EXO_CONSTRUCTEUR_AVEC_TABLEAU
TableauPseudoDynamique_ConstructDestruct::TableauPseudoDynamique_ConstructDestruct(Double *tab, Int nb):d_n{nb}
{
    // Copie le tableau
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        d_table[i]=tab[i];
}
// FIN EXO_CONSTRUCTEUR_AVEC_TABLEAU


// DEBUT EXO_DESTRUCTEUR
TableauPseudoDynamique_ConstructDestruct::~TableauPseudoDynamique_ConstructDestruct()
{
}
// FIN EXO_DESTRUCTEUR


// DEBUT EXO_CONCATENATION_DEBUT
void TableauPseudoDynamique_Other::concatenationDebut(const TableauPseudoDynamique &param)
{
    // Deplace les donnees dans le tableau
    for(Int i=d_n-1;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i>=0;i--)
        d_table[i+param.d_n]=d_table[i];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<param.d_n;i++)
        d_table[i]=param.d_table[i];
    d_n+=param.d_n;
}
// FIN EXO_CONCATENATION_DEBUT


// DEBUT EXO_CONCATENATION_MILIEU
void TableauPseudoDynamique_Other::concatenationMilieu(Int idx, const TableauPseudoDynamique &param)
{
    // Deplace les donnees dans le tableau
    for(Int i=d_n-1;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i>=idx;i--)
        d_table[i+param.d_n]=d_table[i];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<param.d_n;i++)
        d_table[i+idx]=param.d_table[i];
    d_n+=param.d_n;
}
// FIN EXO_CONCATENATION_MILIEU


// DEBUT EXO_CONCATENATION_FIN
void TableauPseudoDynamique_Other::concatenationFin(const TableauPseudoDynamique &param)
{
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<param.d_n;i++)
        d_table[i+d_n]=param.d_table[i];
    d_n+=param.d_n;
}
// FIN EXO_CONCATENATION_FIN


// DEBUT EXO_OP_CONCATENATION
TableauPseudoDynamique &TableauPseudoDynamique_Other::operator+=(const TableauPseudoDynamique &param)
{
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<param.d_n;i++)
        d_table[i+d_n]=param.d_table[i];
    d_n+=param.d_n;

    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_CONCATENATION


// DEBUT EXO_OP_EGAL
bool TableauPseudoDynamique_Other::operator==(const TableauPseudoDynamique &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n!=param.d_n)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_n &&  d_table[i]==param.d_table[i])
        i++;
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i==d_n;}
}
// FIN EXO_OP_EGAL


// DEBUT EXO_OP_DIFFERENT
bool TableauPseudoDynamique_Other::operator!=(const TableauPseudoDynamique &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n!=param.d_n)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_n &&  d_table[i]==param.d_table[i])
        i++;
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i!=d_n;}
}
// FIN EXO_OP_DIFFERENT


// DEBUT EXO_OP_AFFECTATION
TableauPseudoDynamique &TableauPseudoDynamique_Other::operator=(const TableauPseudoDynamique &param)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
    d_n=param.d_n;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        d_table[i]=param.d_table[i];

    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_AFFECTATION


// DEBUT EXO_OP_PARENTHESE
Double& TableauPseudoDynamique_Other::operator()(Int i)
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_table[i];}
}
// FIN EXO_OP_PARENTHESE


// DEBUT EXO_OP_PARENTHESE_CONST
Double TableauPseudoDynamique_Other::operator()(Int i) const
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_table[i];}
}
// FIN EXO_OP_PARENTHESE_CONST


// DEBUT EXO_OP_CROCHET
Double &TableauPseudoDynamique_Other::operator[](Int i)
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_table[i];}
}
// FIN EXO_OP_CROCHET


// DEBUT EXO_OP_CROCHET_CONST
Double TableauPseudoDynamique_Other::operator[](Int i) const
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_table[i];}
}
// FIN EXO_OP_CROCHET_CONST


// DEBUT EXO_SETTER
void TableauPseudoDynamique_Other::set(Int i, Double val)
{
    d_table[i]=val;
}
// FIN EXO_SETTER


// DEBUT EXO_GETTER
Double TableauPseudoDynamique_Other::get(Int i) const
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_table[i];}
}
// FIN EXO_GETTER


// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
void TableauPseudoDynamique_Other::additionnerATousLesElts(Double val)
{
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        d_table[i]+=val;
}
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS


// DEBUT EXO_INSERER_UN_ELT_DEBUT
void TableauPseudoDynamique_Other::insererUnEltDebut(Double valeur)
{
    for(Int i=d_n-1;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i>=0;i--)
        d_table[i+1]=d_table[i];
    d_table[0]=valeur;
    d_n++;
}
// FIN EXO_INSERER_UN_ELT_DEBUT


// DEBUT EXO_INSERER_UN_ELT_MILIEU
void TableauPseudoDynamique_Other::insererUnEltMilieu(Int idx, Double valeur)
{
    for(Int i=d_n-1;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i>=idx;i--)
        d_table[i+1]=d_table[i];
    d_table[idx]=valeur;
    d_n++;
}
// FIN EXO_INSERER_UN_ELT_MILIEU


// DEBUT EXO_INSERER_UN_ELT_FIN
void TableauPseudoDynamique_Other::insererUnEltFin(Double valeur)
{
    d_table[d_n]=valeur;
    d_n++;
}
// FIN EXO_INSERER_UN_ELT_FIN


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_DEBUT
void TableauPseudoDynamique_Other::insererPlusieursEltsDebut(Double *tab, Int nb)
{
    for(Int i=d_n-1;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i>=0;i--)
        d_table[i+nb]=d_table[i];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
        d_table[i]=tab[i];
    d_n+=nb;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_MILIEU
void TableauPseudoDynamique_Other::insererPlusieursEltsMilieu(Int idx, Double *tab, Int nb)
{
    for(Int i=d_n-1;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i>=idx;i--)
        d_table[i+nb]=d_table[i];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
        d_table[i+idx]=tab[i];
    d_n+=nb;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_FIN
void TableauPseudoDynamique_Other::insererPlusieursEltsFin(Double *tab, Int nb)
{
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
        d_table[i+d_n]=tab[i];
    d_n+=nb;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ENLEVER_UN_ELT_DEBUT
void TableauPseudoDynamique_Other::enleverUnEltDebut()
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n==0)
        return;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n-1;i++)
        d_table[i]=d_table[i+1];
    d_n--;
}
// FIN EXO_ENLEVER_UN_ELT_DEBUT


// DEBUT EXO_ENLEVER_UN_ELT_MILIEU
void TableauPseudoDynamique_Other::enleverUnEltMilieu(Int idx)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx>=d_n)
        return;
    for(Int i=idx;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n-1;i++)
        d_table[i]=d_table[i+1];
    d_n--;
}
// FIN EXO_ENLEVER_UN_ELT_MILIEU


// DEBUT EXO_ENLEVER_UN_ELT_FIN
void TableauPseudoDynamique_Other::enleverUnEltFin()
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n==0)
        return;
    d_n--;
}
// FIN EXO_ENLEVER_UN_ELT_FIN


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
void TableauPseudoDynamique_Other::enleverPlusieursEltsDebut(Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb>=d_n)
    {
        d_n=0;
        return;
    }
    for(Int i=nb;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        d_table[i-nb]=d_table[i];
    d_n-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU
void TableauPseudoDynamique_Other::enleverPlusieursEltsMilieu(Int idx, Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx>=d_n)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb+idx>d_n)
        nb=d_n-idx;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0)
        return;
    for(Int i=idx+nb;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        d_table[i-nb]=d_table[i];
    d_n-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU



// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_FIN
void TableauPseudoDynamique_Other::enleverPlusieursEltsFin(Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb>=d_n)
    {
        d_n=0;
        return;
    }
    d_n-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ELTS_EN_ORDRE_DECROISSANT
bool TableauPseudoDynamique_Other::eltsEnOrdreDecroissant() const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n<=1)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    Int i=1;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_n && d_table[i-1]>d_table[i])
        i++;
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i==d_n;}
}
// FIN EXO_ELTS_EN_ORDRE_DECROISSANT


// DEBUT EXO_CONTIENT_ELT
bool TableauPseudoDynamique_Other::contientElt(Double val) const
{
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_n && d_table[i]!=val)
        i++;
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i<d_n;}
}
// FIN EXO_CONTIENT_ELT


// DEBUT EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT
void TableauPseudoDynamique_Other::enleverToutesLesOccurencesDUnElt(Double val)
{
    Int nbOccurences=0;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_table[i]==val)
            nbOccurences++;
        else
            d_table[i-nbOccurences]=d_table[i];
    }
    d_n-=nbOccurences;
}
// FIN EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT
Int TableauPseudoDynamique_Other::chercherLIndexDeLaPremiereOccurenceDUnElt(Double val) const
{
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_n && d_table[i]!=val)
        i++;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&i==d_n)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return -1;}
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i;}
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT
Int TableauPseudoDynamique_Other::chercherLIndexDeLaDerniereOccurenceDUnElt(Double val) const
{
    Int i=d_n-1;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i>=0 && d_table[i]!=val)
        i--;
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i;}
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
Int TableauPseudoDynamique_Other::calculerNombreDOccurencesDUnElt(Double val) const
{
    Int i=0;
    Int nbOccurences=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_n)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_table[i]==val)
            nbOccurences++;
        i++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return nbOccurences;}
}
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT


// DEBUT EXO_OP_ADDITION
TableauPseudoDynamique operator_plus(const TableauPseudoDynamique& a, const TableauPseudoDynamique& b)
{
    TableauPseudoDynamique res(a);
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<res.d_n;i++)
        res.d_table[i]+=b.d_table[i];
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return res;}
}
// FIN EXO_OP_ADDITION


// DEBUT EXO_OP_SOUSTRACTION
TableauPseudoDynamique operator_moins(const TableauPseudoDynamique& a, const TableauPseudoDynamique& b)
{
    TableauPseudoDynamique res(a);
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<res.d_n;i++)
        res.d_table[i]-=b.d_table[i];
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return res;}
}
// FIN EXO_OP_SOUSTRACTION

