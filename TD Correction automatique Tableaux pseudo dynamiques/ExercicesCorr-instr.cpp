////////////////////////////////////////////////////////////////////////////////////
// Fichier genere automatiquement pour l'instrumentation du code. Ne pas modifier //
////////////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include "ExercicesBase.h"

using namespace std;


/////////////////////////////////////////////////////////////////////////////
// Compare la structure de donnees de param'etudiant avec celle de la solution //
/////////////////////////////////////////////////////////////////////////////


// Verifie si la liste chainee est correcte au niveau de sa sctructure et des poInteurs
bool TableauPseudoDynamiqueCorr_Other::verifierIntegrite(const TableauPseudoDynamique &param)
{
    // Verifie si les valeurs sont dans les bons intervals pour eviter les crash
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&param.d_n<0)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
}

// Compare la correction avec la struct de param'etudiant. Retourne false si different
bool TableauPseudoDynamiqueCorr_Other::compareCorrAvecEtud(const TableauPseudoDynamique &student) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n!=student.d_n)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_n &&  d_table[i]==student.d_table[i])
        i++;
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i==d_n;}
}

// genere la structDeDonneesVerifCorr a partir de la liste
StructDeDonneesVerifCorr TableauPseudoDynamiqueCorr_Other::getStructDeDonneesVerifCorr() const
{
    StructDeDonneesVerifCorr vect;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n==0)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return vect;}

    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        vect.insererUnEltFin(d_table[i]);
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return vect;}
}

// Constructeur avec la structDeDonneesVerifCorr
void TableauPseudoDynamiqueCorr_Other::setStructDeDonneesVerifCorr(const StructDeDonneesVerifCorr &tab)
{
    d_n=tab.getNbColonnes();

    // On insere les elements du tableau
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<tab.getNbColonnes();i++)
        d_table[i]=tab[i];
}

// DEBUT EXO_CONSTRUCTEUR
TableauPseudoDynamiqueCorr_ConstructDestruct::TableauPseudoDynamiqueCorr_ConstructDestruct():d_n{0}
{
}
// FIN EXO_CONSTRUCTEUR


// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
TableauPseudoDynamiqueCorr_ConstructDestruct::TableauPseudoDynamiqueCorr_ConstructDestruct(const TableauPseudoDynamiqueCorr &param):d_n{param.d_n}
{
    // Copie le tableau
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        d_table[i]=param.d_table[i];
}
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE


// DEBUT EXO_CONSTRUCTEUR_AVEC_TABLEAU
TableauPseudoDynamiqueCorr_ConstructDestruct::TableauPseudoDynamiqueCorr_ConstructDestruct(Double *tab, Int nb):d_n{nb}
{
    // Copie le tableau
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        d_table[i]=tab[i];
}
// FIN EXO_CONSTRUCTEUR_AVEC_TABLEAU


// DEBUT DESTRUCTEUR
TableauPseudoDynamiqueCorr_ConstructDestruct::~TableauPseudoDynamiqueCorr_ConstructDestruct()
{
}
// FIN DESTRUCTEUR


// DEBUT EXO_CONCATENATION_DEBUT
void TableauPseudoDynamiqueCorr_Other::concatenationDebut(const TableauPseudoDynamiqueCorr &param)
{
    // Deplace les donnees dans le tableau
    for(Int i=d_n-1;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i>=0;i--)
        d_table[i+param.d_n]=d_table[i];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<param.d_n;i++)
        d_table[i]=param.d_table[i];
    d_n+=param.d_n;
}
// FIN EXO_CONCATENATION_DEBUT


// DEBUT EXO_CONCATENATION_MILIEU
void TableauPseudoDynamiqueCorr_Other::concatenationMilieu(Int idx, const TableauPseudoDynamiqueCorr &param)
{
    // Deplace les donnees dans le tableau
    for(Int i=d_n-1;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i>=idx;i--)
        d_table[i+param.d_n]=d_table[i];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<param.d_n;i++)
        d_table[i+idx]=param.d_table[i];
    d_n+=param.d_n;
}
// FIN EXO_CONCATENATION_MILIEU


// DEBUT EXO_CONCATENATION_FIN
void TableauPseudoDynamiqueCorr_Other::concatenationFin(const TableauPseudoDynamiqueCorr &param)
{
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<param.d_n;i++)
        d_table[i+d_n]=param.d_table[i];
    d_n+=param.d_n;
}
// FIN EXO_CONCATENATION_FIN


// DEBUT EXO_OP_CONCATENATION
TableauPseudoDynamiqueCorr &TableauPseudoDynamiqueCorr_Other::operator+=(const TableauPseudoDynamiqueCorr &param)
{
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<param.d_n;i++)
        d_table[i+d_n]=param.d_table[i];
    d_n+=param.d_n;

    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_CONCATENATION



// DEBUT EXO_OP_EGAL
bool TableauPseudoDynamiqueCorr_Other::operator==(const TableauPseudoDynamiqueCorr &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n!=param.d_n)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_n &&  d_table[i]==param.d_table[i])
        i++;
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i==d_n;}
}
// FIN EXO_OP_EGAL


// DEBUT EXO_OP_DIFFERENT
bool TableauPseudoDynamiqueCorr_Other::operator!=(const TableauPseudoDynamiqueCorr &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n!=param.d_n)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_n &&  d_table[i]==param.d_table[i])
        i++;
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i!=d_n;}
}
// FIN EXO_OP_DIFFERENT


// DEBUT EXO_OP_AFFECTATION
TableauPseudoDynamiqueCorr &TableauPseudoDynamiqueCorr_Other::operator=(const TableauPseudoDynamiqueCorr &param)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
    d_n=param.d_n;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        d_table[i]=param.d_table[i];

    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_AFFECTATION


// DEBUT EXO_OP_CROCHET
Double &TableauPseudoDynamiqueCorr_Other::operator[](Int i)
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_table[i];}
}
// FIN EXO_OP_CROCHET


// DEBUT EXO_OP_CROCHET_CONST
Double TableauPseudoDynamiqueCorr_Other::operator[](Int i) const
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_table[i];}
}
// FIN EXO_OP_CROCHET_CONST


// DEBUT EXO_OP_PARENTHESE
Double& TableauPseudoDynamiqueCorr_Other::operator()(Int i)
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_table[i];}
}
// FIN EXO_OP_PARENTHESE


// DEBUT EXO_OP_PARENTHESE_CONST
Double TableauPseudoDynamiqueCorr_Other::operator()(Int i) const
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_table[i];}
}
// FIN EXO_OP_PARENTHESE_CONST


// DEBUT EXO_SETTER
void TableauPseudoDynamiqueCorr_Other::set(Int i, Double val)
{
    d_table[i]=val;
}
// FIN EXO_SETTER


// DEBUT EXO_GETTER
Double TableauPseudoDynamiqueCorr_Other::get(Int i) const
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_table[i];}
}
// FIN EXO_GETTER


void set(Int i, Double val) __attribute__((weak));

// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
void TableauPseudoDynamiqueCorr_Other::additionnerATousLesElts(Double val)
{
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        d_table[i]+=val;
}
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS


// DEBUT EXO_INSERER_UN_ELT_DEBUT
void TableauPseudoDynamiqueCorr_Other::insererUnEltDebut(Double valeur)
{
    for(Int i=d_n-1;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i>=0;i--)
        d_table[i+1]=d_table[i];
    d_table[0]=valeur;
    d_n++;
}
// FIN EXO_INSERER_UN_ELT_DEBUT


// DEBUT EXO_INSERER_UN_ELT_MILIEU
void TableauPseudoDynamiqueCorr_Other::insererUnEltMilieu(Int idx, Double valeur)
{
    for(Int i=d_n-1;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i>=idx;i--)
        d_table[i+1]=d_table[i];
    d_table[idx]=valeur;
    d_n++;
}
// FIN EXO_INSERER_UN_ELT_MILIEU


// DEBUT EXO_INSERER_UN_ELT_FIN
void TableauPseudoDynamiqueCorr_Other::insererUnEltFin(Double valeur)
{
    d_table[d_n]=valeur;
    d_n++;
}
// FIN EXO_INSERER_UN_ELT_FIN


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_DEBUT
void TableauPseudoDynamiqueCorr_Other::insererPlusieursEltsDebut(Double *tab, Int nb)
{
    for(Int i=d_n-1;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i>=0;i--)
        d_table[i+nb]=d_table[i];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
        d_table[i]=tab[i];
    d_n+=nb;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_MILIEU
void TableauPseudoDynamiqueCorr_Other::insererPlusieursEltsMilieu(Int idx, Double *tab, Int nb)
{
    for(Int i=d_n-1;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i>=idx;i--)
        d_table[i+nb]=d_table[i];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
        d_table[i+idx]=tab[i];
    d_n+=nb;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_FIN
void TableauPseudoDynamiqueCorr_Other::insererPlusieursEltsFin(Double *tab, Int nb)
{
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
        d_table[i+d_n]=tab[i];
    d_n+=nb;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ENLEVER_UN_ELT_DEBUT
void TableauPseudoDynamiqueCorr_Other::enleverUnEltDebut()
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n==0)
        return;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n-1;i++)
        d_table[i]=d_table[i+1];
    d_n--;
}
// FIN EXO_ENLEVER_UN_ELT_DEBUT


// DEBUT EXO_ENLEVER_UN_ELT_MILIEU
void TableauPseudoDynamiqueCorr_Other::enleverUnEltMilieu(Int idx)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx>=d_n)
        return;
    for(Int i=idx;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n-1;i++)
        d_table[i]=d_table[i+1];
    d_n--;
}
// FIN EXO_ENLEVER_UN_ELT_MILIEU


// DEBUT EXO_ENLEVER_UN_ELT_FIN
void TableauPseudoDynamiqueCorr_Other::enleverUnEltFin()
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n==0)
        return;
    d_n--;
}
// FIN EXO_ENLEVER_UN_ELT_FIN


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
void TableauPseudoDynamiqueCorr_Other::enleverPlusieursEltsDebut(Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb>=d_n)
    {
        d_n=0;
        return;
    }
    for(Int i=nb;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        d_table[i-nb]=d_table[i];
    d_n-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU
void TableauPseudoDynamiqueCorr_Other::enleverPlusieursEltsMilieu(Int idx, Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx>=d_n)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb+idx>d_n)
        nb=d_n-idx;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0)
        return;
    for(Int i=idx+nb;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        d_table[i-nb]=d_table[i];
    d_n-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_FIN
void TableauPseudoDynamiqueCorr_Other::enleverPlusieursEltsFin(Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb>=d_n)
    {
        d_n=0;
        return;
    }
    d_n-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_FIN



// DEBUT EXO_ELTS_EN_ORDRE_DECROISSANT
bool TableauPseudoDynamiqueCorr_Other::eltsEnOrdreDecroissant() const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n<=1)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    Int i=1;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_n && d_table[i-1]>d_table[i])
        i++;
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i==d_n;}
}
// FIN EXO_ELTS_EN_ORDRE_DECROISSANT


// DEBUT EXO_CONTIENT_ELT
bool TableauPseudoDynamiqueCorr_Other::contientElt(Double val) const
{
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_n && d_table[i]!=val)
        i++;
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i<d_n;}
}
// FIN EXO_CONTIENT_ELT


// DEBUT EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT
void TableauPseudoDynamiqueCorr_Other::enleverToutesLesOccurencesDUnElt(Double val)
{
    Int nbOccurences=0;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_table[i]==val)
            nbOccurences++;
        else
            d_table[i-nbOccurences]=d_table[i];
    }
    d_n-=nbOccurences;
}
// FIN EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT
Int TableauPseudoDynamiqueCorr_Other::chercherLIndexDeLaPremiereOccurenceDUnElt(Double val) const
{
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_n && d_table[i]!=val)
        i++;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&i==d_n)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return -1;}
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i;}
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT
Int TableauPseudoDynamiqueCorr_Other::chercherLIndexDeLaDerniereOccurenceDUnElt(Double val) const
{
    Int i=d_n-1;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i>=0 && d_table[i]!=val)
        i--;
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i;}
}
// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
Int TableauPseudoDynamiqueCorr_Other::calculerNombreDOccurencesDUnElt(Double val) const
{
    Int i=0;
    Int nbOccurences=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_n)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_table[i]==val)
            nbOccurences++;
        i++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return nbOccurences;}
}
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT


// DEBUT EXO_OP_ADDITION
TableauPseudoDynamiqueCorr operator_plus(const TableauPseudoDynamiqueCorr& a, const TableauPseudoDynamiqueCorr& b)
{
    TableauPseudoDynamiqueCorr res(a);
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<res.d_n;i++)
        res.d_table[i]+=b.d_table[i];
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return res;}
}
// FIN EXO_OP_ADDITION


// DEBUT EXO_OP_SOUSTRACTION
TableauPseudoDynamiqueCorr operator_moins(const TableauPseudoDynamiqueCorr& a, const TableauPseudoDynamiqueCorr& b)
{
    TableauPseudoDynamiqueCorr res(a);
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<res.d_n;i++)
        res.d_table[i]-=b.d_table[i];
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return res;}
}
// FIN EXO_OP_SOUSTRACTION
