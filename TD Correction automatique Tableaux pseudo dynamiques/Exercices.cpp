#include "ExercicesBase.h"


// DEBUT EXO_CONSTRUCTEUR
TableauPseudoDynamique::TableauPseudoDynamique():d_n{0}
{
}
// FIN EXO_CONSTRUCTEUR


// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
TableauPseudoDynamique::TableauPseudoDynamique(const TableauPseudoDynamique &param):d_n{param.d_n}
{
    // Copie le tableau
    for (int i=0;i<d_n;i++)
        d_table[i]=param.d_table[i];
}
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE



// DEBUT EXO_CONSTRUCTEUR_AVEC_TABLEAU
TableauPseudoDynamique::TableauPseudoDynamique(double *tab, int nb):d_n{nb}
{
    // Copie le tableau
    for (int i=0;i<d_n;i++)
        d_table[i]=tab[i];
}
// FIN EXO_CONSTRUCTEUR_AVEC_TABLEAU


// DEBUT EXO_DESTRUCTEUR
TableauPseudoDynamique::~TableauPseudoDynamique()
{
}
// FIN EXO_DESTRUCTEUR


// DEBUT EXO_CONCATENATION_DEBUT
void TableauPseudoDynamique::concatenationDebut(const TableauPseudoDynamique &param)
{
    // Deplace les donnees dans le tableau
    for (int i=d_n-1;i>=0;i--)
        d_table[i+param.d_n]=d_table[i];
    for (int i=0;i<param.d_n;i++)
        d_table[i]=param.d_table[i];
    d_n+=param.d_n;
}
// FIN EXO_CONCATENATION_DEBUT


// DEBUT EXO_CONCATENATION_MILIEU
void TableauPseudoDynamique::concatenationMilieu(int idx, const TableauPseudoDynamique &param)
{
    // Deplace les donnees dans le tableau
    for (int i=d_n-1;i>=idx;i--)
        d_table[i+param.d_n]=d_table[i];
    for (int i=0;i<param.d_n;i++)
        d_table[i+idx]=param.d_table[i];
    d_n+=param.d_n;
}
// FIN EXO_CONCATENATION_MILIEU


// DEBUT EXO_CONCATENATION_FIN
void TableauPseudoDynamique::concatenationFin(const TableauPseudoDynamique &param)
{
    for (int i=0;i<param.d_n;i++)
        d_table[i+d_n]=param.d_table[i];
    d_n+=param.d_n;
}
// FIN EXO_CONCATENATION_FIN


// DEBUT EXO_OP_CONCATENATION
TableauPseudoDynamique &TableauPseudoDynamique::operator+=(const TableauPseudoDynamique &param)
{
    for (int i=0;i<param.d_n;i++)
        d_table[i+d_n]=param.d_table[i];
    d_n+=param.d_n;

    return *this;
}
// FIN EXO_OP_CONCATENATION


// DEBUT EXO_OP_EGAL
bool TableauPseudoDynamique::operator==(const TableauPseudoDynamique &param) const
{
    if (this==&param)
        return true;
    if (d_n!=param.d_n)
        return false;
    int i=0;
    while(i<d_n &&  d_table[i]==param.d_table[i])
        i++;
    return i==d_n;
}
// FIN EXO_OP_EGAL


// DEBUT EXO_OP_DIFFERENT
bool TableauPseudoDynamique::operator!=(const TableauPseudoDynamique &param) const
{
    if (this==&param)
        return false;
    if (d_n!=param.d_n)
        return true;
    int i=0;
    while(i<d_n &&  d_table[i]==param.d_table[i])
        i++;
    return i!=d_n;
}
// FIN EXO_OP_DIFFERENT


// DEBUT EXO_OP_AFFECTATION
TableauPseudoDynamique &TableauPseudoDynamique::operator=(const TableauPseudoDynamique &param)
{
    if (this==&param)
        return *this;
    d_n=param.d_n;
    for (int i=0;i<d_n;i++)
        d_table[i]=param.d_table[i];

    return *this;
}
// FIN EXO_OP_AFFECTATION


// DEBUT EXO_OP_PARENTHESE
double& TableauPseudoDynamique::operator()(int i)
{
    return d_table[i];
}
// FIN EXO_OP_PARENTHESE


// DEBUT EXO_OP_PARENTHESE_CONST
double TableauPseudoDynamique::operator()(int i) const
{
    return d_table[i];
}
// FIN EXO_OP_PARENTHESE_CONST


// DEBUT EXO_OP_CROCHET
double &TableauPseudoDynamique::operator[](int i)
{
    return d_table[i];
}
// FIN EXO_OP_CROCHET


// DEBUT EXO_OP_CROCHET_CONST
double TableauPseudoDynamique::operator[](int i) const
{
    return d_table[i];
}
// FIN EXO_OP_CROCHET_CONST


// DEBUT EXO_SETTER
void TableauPseudoDynamique::set(int i, double val)
{
    d_table[i]=val;
}
// FIN EXO_SETTER


// DEBUT EXO_GETTER
double TableauPseudoDynamique::get(int i) const
{
    return d_table[i];
}
// FIN EXO_GETTER


// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
void TableauPseudoDynamique::additionnerATousLesElts(double val)
{
    for (int i=0;i<d_n;i++)
        d_table[i]+=val;
}
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS


// DEBUT EXO_INSERER_UN_ELT_DEBUT
void TableauPseudoDynamique::insererUnEltDebut(double valeur)
{
    for (int i=d_n-1;i>=0;i--)
        d_table[i+1]=d_table[i];
    d_table[0]=valeur;
    d_n++;
}
// FIN EXO_INSERER_UN_ELT_DEBUT


// DEBUT EXO_INSERER_UN_ELT_MILIEU
void TableauPseudoDynamique::insererUnEltMilieu(int idx, double valeur)
{
    for (int i=d_n-1;i>=idx;i--)
        d_table[i+1]=d_table[i];
    d_table[idx]=valeur;
    d_n++;
}
// FIN EXO_INSERER_UN_ELT_MILIEU


// DEBUT EXO_INSERER_UN_ELT_FIN
void TableauPseudoDynamique::insererUnEltFin(double valeur)
{
    d_table[d_n]=valeur;
    d_n++;
}
// FIN EXO_INSERER_UN_ELT_FIN


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_DEBUT
void TableauPseudoDynamique::insererPlusieursEltsDebut(double *tab, int nb)
{
    for (int i=d_n-1;i>=0;i--)
        d_table[i+nb]=d_table[i];
    for (int i=0;i<nb;i++)
        d_table[i]=tab[i];
    d_n+=nb;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_MILIEU
void TableauPseudoDynamique::insererPlusieursEltsMilieu(int idx, double *tab, int nb)
{
    for (int i=d_n-1;i>=idx;i--)
        d_table[i+nb]=d_table[i];
    for (int i=0;i<nb;i++)
        d_table[i+idx]=tab[i];
    d_n+=nb;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_FIN
void TableauPseudoDynamique::insererPlusieursEltsFin(double *tab, int nb)
{
    for (int i=0;i<nb;i++)
        d_table[i+d_n]=tab[i];
    d_n+=nb;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ENLEVER_UN_ELT_DEBUT
void TableauPseudoDynamique::enleverUnEltDebut()
{
    if (d_n==0)
        return;
    for (int i=0;i<d_n-1;i++)
        d_table[i]=d_table[i+1];
    d_n--;
}
// FIN EXO_ENLEVER_UN_ELT_DEBUT


// DEBUT EXO_ENLEVER_UN_ELT_MILIEU
void TableauPseudoDynamique::enleverUnEltMilieu(int idx)
{
    if (d_n==0)
        return;
    if (idx>=d_n)
        return;
    for (int i=idx;i<d_n-1;i++)
        d_table[i]=d_table[i+1];
    d_n--;
}
// FIN EXO_ENLEVER_UN_ELT_MILIEU


// DEBUT EXO_ENLEVER_UN_ELT_FIN
void TableauPseudoDynamique::enleverUnEltFin()
{
    if (d_n==0)
        return;
    d_n--;
}
// FIN EXO_ENLEVER_UN_ELT_FIN


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
void TableauPseudoDynamique::enleverPlusieursEltsDebut(int nb)
{
    if (nb>=d_n)
    {
        d_n=0;
        return;
    }
    for (int i=nb;i<d_n;i++)
        d_table[i-nb]=d_table[i];
    d_n-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU
void TableauPseudoDynamique::enleverPlusieursEltsMilieu(int idx, int nb)
{
    if (idx>=d_n)
        return;
    if (nb+idx>d_n)
        nb=d_n-idx;
    if (nb==0)
        return;
    for (int i=idx+nb;i<d_n;i++)
        d_table[i-nb]=d_table[i];
    d_n-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU



// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_FIN
void TableauPseudoDynamique::enleverPlusieursEltsFin(int nb)
{
    if (nb==0)
        return;
    if (nb>=d_n)
    {
        d_n=0;
        return;
    }
    d_n-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ELTS_EN_ORDRE_DECROISSANT
bool TableauPseudoDynamique::eltsEnOrdreDecroissant() const
{
    if (d_n<=1)
        return true;
    int i=1;
    while(i<d_n && d_table[i-1]>d_table[i])
        i++;
    return i==d_n;
}
// FIN EXO_ELTS_EN_ORDRE_DECROISSANT


// DEBUT EXO_CONTIENT_ELT
bool TableauPseudoDynamique::contientElt(double val) const
{
    int i=0;
    while(i<d_n && d_table[i]!=val)
        i++;
    return i<d_n;
}
// FIN EXO_CONTIENT_ELT


// DEBUT EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT
void TableauPseudoDynamique::enleverToutesLesOccurencesDUnElt(double val)
{
    int nbOccurences=0;
    for(int i=0;i<d_n;i++)
    {
        if (d_table[i]==val)
            nbOccurences++;
        else
            d_table[i-nbOccurences]=d_table[i];
    }
    d_n-=nbOccurences;
}
// FIN EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT
int TableauPseudoDynamique::chercherLIndexDeLaPremiereOccurenceDUnElt(double val) const
{
    int i=0;
    while(i<d_n && d_table[i]!=val)
        i++;
    if (i==d_n)
        return -1;
    return i;
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT
int TableauPseudoDynamique::chercherLIndexDeLaDerniereOccurenceDUnElt(double val) const
{
    int i=d_n-1;
    while(i>=0 && d_table[i]!=val)
        i--;
    return i;
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
int TableauPseudoDynamique::calculerNombreDOccurencesDUnElt(double val) const
{
    int i=0;
    int nbOccurences=0;
    while(i<d_n)
    {
        if (d_table[i]==val)
            nbOccurences++;
        i++;
    }
    return nbOccurences;
}
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT


// DEBUT EXO_OP_ADDITION
TableauPseudoDynamique operator+(const TableauPseudoDynamique& a, const TableauPseudoDynamique& b)
{
    TableauPseudoDynamique res(a);
    for (int i=0;i<res.d_n;i++)
        res.d_table[i]+=b.d_table[i];
    return res;
}
// FIN EXO_OP_ADDITION


// DEBUT EXO_OP_SOUSTRACTION
TableauPseudoDynamique operator-(const TableauPseudoDynamique& a, const TableauPseudoDynamique& b)
{
    TableauPseudoDynamique res(a);
    for (int i=0;i<res.d_n;i++)
        res.d_table[i]-=b.d_table[i];
    return res;
}
// FIN EXO_OP_SOUSTRACTION

