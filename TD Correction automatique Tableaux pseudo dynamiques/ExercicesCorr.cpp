#include <iostream>
#include "ExercicesBase.h"

using namespace std;


/////////////////////////////////////////////////////////////////////////////
// Compare la structure de donnees de param'etudiant avec celle de la solution //
/////////////////////////////////////////////////////////////////////////////


// Verifie si la liste chainee est correcte au niveau de sa sctructure et des poInteurs
bool TableauPseudoDynamiqueCorr::verifierIntegrite(const TableauPseudoDynamique &param)
{
    // Verifie si les valeurs sont dans les bons intervals pour eviter les crash
    if (param.d_n<0)
        return false;
    return true;
}

// Compare la correction avec la struct de param'etudiant. Retourne false si different
bool TableauPseudoDynamiqueCorr::compareCorrAvecEtud(const TableauPseudoDynamique &student) const
{
    if (d_n!=student.d_n)
        return false;
    int i=0;
    while(i<d_n &&  d_table[i]==student.d_table[i])
        i++;
    return i==d_n;
}

// genere la structDeDonneesVerifCorr a partir de la liste
StructDeDonneesVerifCorr TableauPseudoDynamiqueCorr::getStructDeDonneesVerifCorr() const
{
    StructDeDonneesVerifCorr vect;
    if (d_n==0)
        return vect;

    for (int i=0;i<d_n;i++)
        vect.insererUnEltFin(d_table[i]);
    return vect;
}

// Constructeur avec la structDeDonneesVerifCorr
void TableauPseudoDynamiqueCorr::setStructDeDonneesVerifCorr(const StructDeDonneesVerifCorr &tab)
{
    d_n=tab.getNbColonnes();

    // On insere les elements du tableau
    for (int i=0;i<tab.getNbColonnes();i++)
        d_table[i]=tab[i];
}

// DEBUT EXO_CONSTRUCTEUR
TableauPseudoDynamiqueCorr::TableauPseudoDynamiqueCorr():d_n{0}
{
}
// FIN EXO_CONSTRUCTEUR


// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
TableauPseudoDynamiqueCorr::TableauPseudoDynamiqueCorr(const TableauPseudoDynamiqueCorr &param):d_n{param.d_n}
{
    // Copie le tableau
    for (int i=0;i<d_n;i++)
        d_table[i]=param.d_table[i];
}
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE


// DEBUT EXO_CONSTRUCTEUR_AVEC_TABLEAU
TableauPseudoDynamiqueCorr::TableauPseudoDynamiqueCorr(double *tab, int nb):d_n{nb}
{
    // Copie le tableau
    for (int i=0;i<d_n;i++)
        d_table[i]=tab[i];
}
// FIN EXO_CONSTRUCTEUR_AVEC_TABLEAU


// DEBUT DESTRUCTEUR
TableauPseudoDynamiqueCorr::~TableauPseudoDynamiqueCorr()
{
}
// FIN DESTRUCTEUR


// DEBUT EXO_CONCATENATION_DEBUT
void TableauPseudoDynamiqueCorr::concatenationDebut(const TableauPseudoDynamiqueCorr &param)
{
    // Deplace les donnees dans le tableau
    for (int i=d_n-1;i>=0;i--)
        d_table[i+param.d_n]=d_table[i];
    for (int i=0;i<param.d_n;i++)
        d_table[i]=param.d_table[i];
    d_n+=param.d_n;
}
// FIN EXO_CONCATENATION_DEBUT


// DEBUT EXO_CONCATENATION_MILIEU
void TableauPseudoDynamiqueCorr::concatenationMilieu(int idx, const TableauPseudoDynamiqueCorr &param)
{
    // Deplace les donnees dans le tableau
    for (int i=d_n-1;i>=idx;i--)
        d_table[i+param.d_n]=d_table[i];
    for (int i=0;i<param.d_n;i++)
        d_table[i+idx]=param.d_table[i];
    d_n+=param.d_n;
}
// FIN EXO_CONCATENATION_MILIEU


// DEBUT EXO_CONCATENATION_FIN
void TableauPseudoDynamiqueCorr::concatenationFin(const TableauPseudoDynamiqueCorr &param)
{
    for (int i=0;i<param.d_n;i++)
        d_table[i+d_n]=param.d_table[i];
    d_n+=param.d_n;
}
// FIN EXO_CONCATENATION_FIN


// DEBUT EXO_OP_CONCATENATION
TableauPseudoDynamiqueCorr &TableauPseudoDynamiqueCorr::operator+=(const TableauPseudoDynamiqueCorr &param)
{
    for (int i=0;i<param.d_n;i++)
        d_table[i+d_n]=param.d_table[i];
    d_n+=param.d_n;

    return *this;
}
// FIN EXO_OP_CONCATENATION



// DEBUT EXO_OP_EGAL
bool TableauPseudoDynamiqueCorr::operator==(const TableauPseudoDynamiqueCorr &param) const
{
    if (this==&param)
        return true;
    if (d_n!=param.d_n)
        return false;
    int i=0;
    while(i<d_n &&  d_table[i]==param.d_table[i])
        i++;
    return i==d_n;
}
// FIN EXO_OP_EGAL


// DEBUT EXO_OP_DIFFERENT
bool TableauPseudoDynamiqueCorr::operator!=(const TableauPseudoDynamiqueCorr &param) const
{
    if (this==&param)
        return false;
    if (d_n!=param.d_n)
        return true;
    int i=0;
    while(i<d_n &&  d_table[i]==param.d_table[i])
        i++;
    return i!=d_n;
}
// FIN EXO_OP_DIFFERENT


// DEBUT EXO_OP_AFFECTATION
TableauPseudoDynamiqueCorr &TableauPseudoDynamiqueCorr::operator=(const TableauPseudoDynamiqueCorr &param)
{
    if (this==&param)
        return *this;
    d_n=param.d_n;
    for (int i=0;i<d_n;i++)
        d_table[i]=param.d_table[i];

    return *this;
}
// FIN EXO_OP_AFFECTATION


// DEBUT EXO_OP_CROCHET
double &TableauPseudoDynamiqueCorr::operator[](int i)
{
    return d_table[i];
}
// FIN EXO_OP_CROCHET


// DEBUT EXO_OP_CROCHET_CONST
double TableauPseudoDynamiqueCorr::operator[](int i) const
{
    return d_table[i];
}
// FIN EXO_OP_CROCHET_CONST


// DEBUT EXO_OP_PARENTHESE
double& TableauPseudoDynamiqueCorr::operator()(int i)
{
    return d_table[i];
}
// FIN EXO_OP_PARENTHESE


// DEBUT EXO_OP_PARENTHESE_CONST
double TableauPseudoDynamiqueCorr::operator()(int i) const
{
    return d_table[i];
}
// FIN EXO_OP_PARENTHESE_CONST


// DEBUT EXO_SETTER
void TableauPseudoDynamiqueCorr::set(int i, double val)
{
    d_table[i]=val;
}
// FIN EXO_SETTER


// DEBUT EXO_GETTER
double TableauPseudoDynamiqueCorr::get(int i) const
{
    return d_table[i];
}
// FIN EXO_GETTER


void set(int i, double val) __attribute__((weak));

// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
void TableauPseudoDynamiqueCorr::additionnerATousLesElts(double val)
{
    for (int i=0;i<d_n;i++)
        d_table[i]+=val;
}
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS


// DEBUT EXO_INSERER_UN_ELT_DEBUT
void TableauPseudoDynamiqueCorr::insererUnEltDebut(double valeur)
{
    for (int i=d_n-1;i>=0;i--)
        d_table[i+1]=d_table[i];
    d_table[0]=valeur;
    d_n++;
}
// FIN EXO_INSERER_UN_ELT_DEBUT


// DEBUT EXO_INSERER_UN_ELT_MILIEU
void TableauPseudoDynamiqueCorr::insererUnEltMilieu(int idx, double valeur)
{
    for (int i=d_n-1;i>=idx;i--)
        d_table[i+1]=d_table[i];
    d_table[idx]=valeur;
    d_n++;
}
// FIN EXO_INSERER_UN_ELT_MILIEU


// DEBUT EXO_INSERER_UN_ELT_FIN
void TableauPseudoDynamiqueCorr::insererUnEltFin(double valeur)
{
    d_table[d_n]=valeur;
    d_n++;
}
// FIN EXO_INSERER_UN_ELT_FIN


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_DEBUT
void TableauPseudoDynamiqueCorr::insererPlusieursEltsDebut(double *tab, int nb)
{
    for (int i=d_n-1;i>=0;i--)
        d_table[i+nb]=d_table[i];
    for (int i=0;i<nb;i++)
        d_table[i]=tab[i];
    d_n+=nb;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_MILIEU
void TableauPseudoDynamiqueCorr::insererPlusieursEltsMilieu(int idx, double *tab, int nb)
{
    for (int i=d_n-1;i>=idx;i--)
        d_table[i+nb]=d_table[i];
    for (int i=0;i<nb;i++)
        d_table[i+idx]=tab[i];
    d_n+=nb;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_FIN
void TableauPseudoDynamiqueCorr::insererPlusieursEltsFin(double *tab, int nb)
{
    for (int i=0;i<nb;i++)
        d_table[i+d_n]=tab[i];
    d_n+=nb;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ENLEVER_UN_ELT_DEBUT
void TableauPseudoDynamiqueCorr::enleverUnEltDebut()
{
    if (d_n==0)
        return;
    for (int i=0;i<d_n-1;i++)
        d_table[i]=d_table[i+1];
    d_n--;
}
// FIN EXO_ENLEVER_UN_ELT_DEBUT


// DEBUT EXO_ENLEVER_UN_ELT_MILIEU
void TableauPseudoDynamiqueCorr::enleverUnEltMilieu(int idx)
{
    if (d_n==0)
        return;
    if (idx>=d_n)
        return;
    for (int i=idx;i<d_n-1;i++)
        d_table[i]=d_table[i+1];
    d_n--;
}
// FIN EXO_ENLEVER_UN_ELT_MILIEU


// DEBUT EXO_ENLEVER_UN_ELT_FIN
void TableauPseudoDynamiqueCorr::enleverUnEltFin()
{
    if (d_n==0)
        return;
    d_n--;
}
// FIN EXO_ENLEVER_UN_ELT_FIN


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
void TableauPseudoDynamiqueCorr::enleverPlusieursEltsDebut(int nb)
{
    if (nb>=d_n)
    {
        d_n=0;
        return;
    }
    for (int i=nb;i<d_n;i++)
        d_table[i-nb]=d_table[i];
    d_n-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU
void TableauPseudoDynamiqueCorr::enleverPlusieursEltsMilieu(int idx, int nb)
{
    if (idx>=d_n)
        return;
    if (nb+idx>d_n)
        nb=d_n-idx;
    if (nb==0)
        return;
    for (int i=idx+nb;i<d_n;i++)
        d_table[i-nb]=d_table[i];
    d_n-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_FIN
void TableauPseudoDynamiqueCorr::enleverPlusieursEltsFin(int nb)
{
    if (nb==0)
        return;
    if (nb>=d_n)
    {
        d_n=0;
        return;
    }
    d_n-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_FIN



// DEBUT EXO_ELTS_EN_ORDRE_DECROISSANT
bool TableauPseudoDynamiqueCorr::eltsEnOrdreDecroissant() const
{
    if (d_n<=1)
        return true;
    int i=1;
    while(i<d_n && d_table[i-1]>d_table[i])
        i++;
    return i==d_n;
}
// FIN EXO_ELTS_EN_ORDRE_DECROISSANT


// DEBUT EXO_CONTIENT_ELT
bool TableauPseudoDynamiqueCorr::contientElt(double val) const
{
    int i=0;
    while(i<d_n && d_table[i]!=val)
        i++;
    return i<d_n;
}
// FIN EXO_CONTIENT_ELT


// DEBUT EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT
void TableauPseudoDynamiqueCorr::enleverToutesLesOccurencesDUnElt(double val)
{
    int nbOccurences=0;
    for(int i=0;i<d_n;i++)
    {
        if (d_table[i]==val)
            nbOccurences++;
        else
            d_table[i-nbOccurences]=d_table[i];
    }
    d_n-=nbOccurences;
}
// FIN EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT
int TableauPseudoDynamiqueCorr::chercherLIndexDeLaPremiereOccurenceDUnElt(double val) const
{
    int i=0;
    while(i<d_n && d_table[i]!=val)
        i++;
    if (i==d_n)
        return -1;
    return i;
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT
int TableauPseudoDynamiqueCorr::chercherLIndexDeLaDerniereOccurenceDUnElt(double val) const
{
    int i=d_n-1;
    while(i>=0 && d_table[i]!=val)
        i--;
    return i;
}
// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
int TableauPseudoDynamiqueCorr::calculerNombreDOccurencesDUnElt(double val) const
{
    int i=0;
    int nbOccurences=0;
    while(i<d_n)
    {
        if (d_table[i]==val)
            nbOccurences++;
        i++;
    }
    return nbOccurences;
}
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT


// DEBUT EXO_OP_ADDITION
TableauPseudoDynamiqueCorr operator+(const TableauPseudoDynamiqueCorr& a, const TableauPseudoDynamiqueCorr& b)
{
    TableauPseudoDynamiqueCorr res(a);
    for (int i=0;i<res.d_n;i++)
        res.d_table[i]+=b.d_table[i];
    return res;
}
// FIN EXO_OP_ADDITION


// DEBUT EXO_OP_SOUSTRACTION
TableauPseudoDynamiqueCorr operator-(const TableauPseudoDynamiqueCorr& a, const TableauPseudoDynamiqueCorr& b)
{
    TableauPseudoDynamiqueCorr res(a);
    for (int i=0;i<res.d_n;i++)
        res.d_table[i]-=b.d_table[i];
    return res;
}
// FIN EXO_OP_SOUSTRACTION
