////////////////////////////////////////////////////////////////////////////////////
// Fichier genere automatiquement pour l'instrumentation du code. Ne pas modifier //
////////////////////////////////////////////////////////////////////////////////////


#include "ExercicesBase.h"


// DEBUT EXO_CONSTRUCTEUR
TableauDynamique_ConstructDestruct::TableauDynamique_ConstructDestruct():d_n{0},d_table{nullptr}
{
}
// FIN EXO_CONSTRUCTEUR


// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
TableauDynamique_ConstructDestruct::TableauDynamique_ConstructDestruct(const TableauDynamique &param):d_n{param.d_n},d_table{new Double[d_n]}
{
    // Copie le tableau
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        d_table[i]=param.d_table[i];
}
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE



// DEBUT EXO_CONSTRUCTEUR_AVEC_TABLEAU
TableauDynamique_ConstructDestruct::TableauDynamique_ConstructDestruct(Double *tab, Int nb):d_n{nb},d_table{new Double[nb]}
{
    // Copie le tableau
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        d_table[i]=tab[i];
}
// FIN EXO_CONSTRUCTEUR_AVEC_TABLEAU


// DEBUT EXO_DESTRUCTEUR
TableauDynamique_ConstructDestruct::~TableauDynamique_ConstructDestruct()
{
    {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
}
// FIN EXO_DESTRUCTEUR


// DEBUT EXO_CONCATENATION_DEBUT
void TableauDynamique_Other::concatenationDebut(const TableauDynamique &param)
{
    Double *tmp=new Double[d_n+param.d_n];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<param.d_n;i++)
        tmp[i]=param.d_table[i];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        tmp[param.d_n+i]=d_table[i];
    {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
    d_table=tmp;
    d_n+=param.d_n;
}
// FIN EXO_CONCATENATION_DEBUT


// DEBUT EXO_CONCATENATION_MILIEU
void TableauDynamique_Other::concatenationMilieu(Int idx, const TableauDynamique &param)
{
    Double *tmp=new Double[d_n+param.d_n];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<idx;i++)
        tmp[i]=d_table[i];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<param.d_n;i++)
        tmp[idx+i]=param.d_table[i];
    for(Int i=idx;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        tmp[param.d_n+i]=d_table[i];
    {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
    d_table=tmp;
    d_n+=param.d_n;
}
// FIN EXO_CONCATENATION_MILIEU


// DEBUT EXO_CONCATENATION_FIN
void TableauDynamique_Other::concatenationFin(const TableauDynamique &param)
{
    Double *tmp=new Double[d_n+param.d_n];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        tmp[i]=d_table[i];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<param.d_n;i++)
        tmp[d_n+i]=param.d_table[i];
    {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
    d_table=tmp;
    d_n+=param.d_n;
}
// FIN EXO_CONCATENATION_FIN


// DEBUT EXO_OP_CONCATENATION
TableauDynamique &TableauDynamique_Other::operator+=(const TableauDynamique &param)
{
    Double *tmp=new Double[d_n+param.d_n];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        tmp[i]=d_table[i];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<param.d_n;i++)
        tmp[d_n+i]=param.d_table[i];
    {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
    d_table=tmp;
    d_n+=param.d_n;

    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_CONCATENATION


// DEBUT EXO_OP_EGAL
bool TableauDynamique_Other::operator==(const TableauDynamique &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n!=param.d_n)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_n &&  d_table[i]==param.d_table[i])
        i++;
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i==d_n;}
}
// FIN EXO_OP_EGAL


// DEBUT EXO_OP_DIFFERENT
bool TableauDynamique_Other::operator!=(const TableauDynamique &param) const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return false;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n!=param.d_n)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_n &&  d_table[i]==param.d_table[i])
        i++;
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i!=d_n;}
}
// FIN EXO_OP_DIFFERENT


// DEBUT EXO_OP_AFFECTATION
TableauDynamique &TableauDynamique_Other::operator=(const TableauDynamique &param)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&this==&param)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n!=param.d_n)
    {
        {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
        d_table=new Double[param.d_n];
    }
    d_n=param.d_n;
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        d_table[i]=param.d_table[i];

    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return *this;}
}
// FIN EXO_OP_AFFECTATION


// DEBUT EXO_OP_CROCHET
Double &TableauDynamique_Other::operator[](Int i)
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_table[i];}
}
// FIN EXO_OP_CROCHET


// DEBUT EXO_OP_CROCHET_CONST
Double TableauDynamique_Other::operator[](Int i) const
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_table[i];}
}
// FIN EXO_OP_CROCHET_CONST


// DEBUT EXO_OP_PARENTHESE
Double& TableauDynamique_Other::operator()(Int i)
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_table[i];}
}
// FIN EXO_OP_PARENTHESE


// DEBUT EXO_OP_PARENTHESE_CONST
Double TableauDynamique_Other::operator()(Int i) const
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_table[i];}
}
// FIN EXO_OP_PARENTHESE_CONST


// DEBUT EXO_SETTER
void TableauDynamique_Other::set(Int i, Double val)
{
    d_table[i]=val;
}
// FIN EXO_SETTER


// DEBUT EXO_GETTER
Double TableauDynamique_Other::get(Int i) const
{
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return d_table[i];}
}
// FIN EXO_GETTER


// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
void TableauDynamique_Other::additionnerATousLesElts(Double val)
{
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        d_table[i]+=val;
}
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS


// DEBUT EXO_INSERER_UN_ELT_DEBUT
void TableauDynamique_Other::insererUnEltDebut(Double valeur)
{
    Double *tmp=new Double[d_n+1];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        tmp[i+1]=d_table[i];
    tmp[0]=valeur;
    d_n++;
    {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
    d_table=tmp;
}
// FIN EXO_INSERER_UN_ELT_DEBUT


// DEBUT EXO_INSERER_UN_ELT_MILIEU
void TableauDynamique_Other::insererUnEltMilieu(Int idx, Double valeur)
{
    Double *tmp=new Double[d_n+1];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<idx;i++)
        tmp[i]=d_table[i];
    tmp[idx]=valeur;
    for(Int i=idx;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        tmp[i+1]=d_table[i];
    d_n++;
    {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
    d_table=tmp;
}
// FIN EXO_INSERER_UN_ELT_MILIEU


// DEBUT EXO_INSERER_UN_ELT_FIN
void TableauDynamique_Other::insererUnEltFin(Double valeur)
{
    Double *tmp=new Double[d_n+1];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        tmp[i]=d_table[i];
    tmp[d_n]=valeur;
    d_n++;
    {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
    d_table=tmp;
}
// FIN EXO_INSERER_UN_ELT_FIN


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_DEBUT
void TableauDynamique_Other::insererPlusieursEltsDebut(Double *tab, Int nb)
{
    Double *tmp=new Double[d_n+nb];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
        tmp[i]=tab[i];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        tmp[i+nb]=d_table[i];
    d_n+=nb;
    {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
    d_table=tmp;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_MILIEU
void TableauDynamique_Other::insererPlusieursEltsMilieu(Int idx, Double *tab, Int nb)
{
    Double *tmp=new Double[d_n+nb];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<idx;i++)
        tmp[i]=d_table[i];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
        tmp[idx+i]=tab[i];
    for(Int i=idx;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        tmp[i+nb]=d_table[i];
    d_n+=nb;
    {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
    d_table=tmp;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_FIN
void TableauDynamique_Other::insererPlusieursEltsFin(Double *tab, Int nb)
{
    Double *tmp=new Double[d_n+nb];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        tmp[i]=d_table[i];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<nb;i++)
        tmp[i+d_n]=tab[i];
    d_n+=nb;
    {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
    d_table=tmp;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ENLEVER_UN_ELT_DEBUT
void TableauDynamique_Other::enleverUnEltDebut()
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n==1)
    {
        {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
        d_table=nullptr;
        d_n=0;
        return;
    }
    Double *tmp=new Double[d_n-1];
    for(Int i=1;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        tmp[i-1]=d_table[i];
    d_n--;
    {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
    d_table=tmp;
}
// FIN EXO_ENLEVER_UN_ELT_DEBUT


// DEBUT EXO_ENLEVER_UN_ELT_MILIEU
void TableauDynamique_Other::enleverUnEltMilieu(Int idx)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx>=d_n)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n==1)
    {
        {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
        d_table=nullptr;
        d_n=0;
        return;
    }
    Double *tmp=new Double[d_n-1];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<idx;i++)
        tmp[i]=d_table[i];
    for(Int i=idx+1;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        tmp[i-1]=d_table[i];
    d_n--;
    {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
    d_table=tmp;
}
// FIN EXO_ENLEVER_UN_ELT_MILIEU


// DEBUT EXO_ENLEVER_UN_ELT_FIN
void TableauDynamique_Other::enleverUnEltFin()
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n==1)
    {
        {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
        d_table=nullptr;
        d_n=0;
        return;
    }
    Double *tmp=new Double[d_n-1];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n-1;i++)
        tmp[i]=d_table[i];
    d_n--;
    {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
    d_table=tmp;
}
// FIN EXO_ENLEVER_UN_ELT_FIN


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
void TableauDynamique_Other::enleverPlusieursEltsDebut(Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb>=d_n)
    {
        {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
        d_table=nullptr;
        d_n=0;
        return;
    }
    Double* tmp=new Double[d_n-nb];
    for(Int i=nb;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        tmp[i-nb]=d_table[i];
    {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
    d_table=tmp;
    d_n-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU
void TableauDynamique_Other::enleverPlusieursEltsMilieu(Int idx, Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&idx>=d_n)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb+idx>d_n)
        nb=d_n-idx;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0)
        return;
    Double* tmp=new Double[d_n-nb];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<idx;i++)
        tmp[i]=d_table[i];
    for(Int i=idx+nb;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n;i++)
        tmp[i-nb]=d_table[i];
    {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
    d_table=tmp;
    d_n-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_FIN
void TableauDynamique_Other::enleverPlusieursEltsFin(Int nb)
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nb>=d_n)
    {
        d_n=0;
        {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
        d_table=nullptr;
        return;
    }
    Double* tmp=new Double[d_n-nb];
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<d_n-nb;i++)
        tmp[i]=d_table[i];
    {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
    d_table=tmp;
    d_n-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ELTS_EN_ORDRE_DECROISSANT
bool TableauDynamique_Other::eltsEnOrdreDecroissant() const
{
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_n<=1)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return true;}
    Int i=1;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_n && d_table[i-1]>d_table[i])
        i++;
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i==d_n;}
}
// FIN EXO_ELTS_EN_ORDRE_DECROISSANT


// DEBUT EXO_CONTIENT_ELT
bool TableauDynamique_Other::contientElt(Double val) const
{
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_n && d_table[i]!=val)
        i++;
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i<d_n;}
}
// FIN EXO_CONTIENT_ELT


// DEBUT EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT
void TableauDynamique_Other::enleverToutesLesOccurencesDUnElt(Double val)
{
    Int nbOccurences=0;
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_n)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_table[i]==val)
            nbOccurences++;
        i++;
    }
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nbOccurences==0)
        return;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&nbOccurences==d_n)
    {
        {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
        d_table=nullptr;
        d_n=0;
        return;
    }
    Double* tmp=new Double[d_n-nbOccurences];
    i=0;
    Int j=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_n)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_table[i]!=val)
        {
            tmp[j]=d_table[i];
            j++;
        }
        i++;
    }
    {GestionPntrEtComplexite::get().peutSupprPntr(d_table, true); delete[] d_table;}
    d_table=tmp;
    d_n=j;
}
// FIN EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT
Int TableauDynamique_Other::chercherLIndexDeLaPremiereOccurenceDUnElt(Double val) const
{
    Int i=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_n && d_table[i]!=val)
        i++;
    if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&i==d_n)
        {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return -1;}
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i;}
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT
Int TableauDynamique_Other::chercherLIndexDeLaDerniereOccurenceDUnElt(Double val) const
{
    Int i=d_n-1;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i>=0 && d_table[i]!=val)
        i--;
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return i;}
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
Int TableauDynamique_Other::calculerNombreDOccurencesDUnElt(Double val) const
{
    Int i=0;
    Int nbOccurences=0;
    while(GestionPntrEtComplexite::get().ajoutInstruction("while",__FILENAME__,__LINE__)&&i<d_n)
    {
        if(GestionPntrEtComplexite::get().ajoutInstruction("if",__FILENAME__,__LINE__)&&d_table[i]==val)
            nbOccurences++;
        i++;
    }
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return nbOccurences;}
}
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT


// DEBUT EXO_OP_ADDITION
TableauDynamique operator_plus(const TableauDynamique& a, const TableauDynamique& b)
{
    TableauDynamique res(a);
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<res.d_n;i++)
        res.d_table[i]+=b.d_table[i];
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return res;}
}
// FIN EXO_OP_ADDITION


// DEBUT EXO_OP_SOUSTRACTION
TableauDynamique operator_moins(const TableauDynamique& a, const TableauDynamique& b)
{
    TableauDynamique res(a);
    for(Int i=0;GestionPntrEtComplexite::get().ajoutInstruction("for",__FILENAME__,__LINE__),i<res.d_n;i++)
        res.d_table[i]-=b.d_table[i];
    {GestionPntrEtComplexite::get().ajoutInstruction("return",__FILENAME__,__LINE__);return res;}
}
// FIN EXO_OP_SOUSTRACTION
