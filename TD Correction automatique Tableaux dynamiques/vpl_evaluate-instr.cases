////////////////////////////////////////////////////////////////////////////////////
// Fichier genere automatiquement pour l'instrumentation du code. Ne pas modifier //
////////////////////////////////////////////////////////////////////////////////////


#ifndef VPL_EVALUATE_CASES
#define VPL_EVALUATE_CASES

////////////////////////////////////////////////////////////////////////////////////////
// Fichier de configuration pour les exercices. Il contient:                          //
// - Pour chaque exercice, le coefficient (nombre de points) pour le calcul de la     //
//   note. Si le coefficent n'est pas spécifié ou s'il est égal à 0, l'exercices est  //
//   ignoré.                                                                          //
// - Un macro pour indiquer si les commentaires doivent être affichés pour l'etudiant //
//                                                                                    //
// Ce fichier doit respecter la syntaxe C++.                                          //
////////////////////////////////////////////////////////////////////////////////////////


// Le nom des classes pour l'etudiant et la correction
#define STRUCT_DE_DONNEES_ETUD TableauDynamique
#define STRUCT_DE_DONNEES_CORR TableauDynamiqueCorr


// Les membres de la classe
#define MEMBRES_STRUCT_DE_DONNEES Int d_n;\
                                  Double* d_table;\


#define MONTRER_COMMENTAIRES_ETUDIANT true

// Pour le renomage des methodes
#define concatenationDebut                             concatenationDebut
#define concatenationMilieu                            concatenationMilieu
#define concatenationFin                               concatenationFin
#define set                                            set
#define get                                            get
#define additionnerATousLesElts                        additionnerATousLesElts
#define insererUnEltDebut                              insererUnEltDebut
#define insererUnEltMilieu                             insererUnEltMilieu
#define insererUnEltFin                                insererUnEltFin
#define insererPlusieursEltsDebut                      insererPlusieursEltsDebut
#define insererPlusieursEltsMilieu                     insererPlusieursEltsMilieu
#define insererPlusieursEltsFin                        insererPlusieursEltsFin
#define enleverUnEltDebut                              enleverUnEltDebut
#define enleverUnEltMilieu                             enleverUnEltMilieu
#define enleverUnEltFin                                enleverUnEltFin
#define enleverPlusieursEltsDebut                      enleverPlusieursEltsDebut
#define enleverPlusieursEltsMilieu                     enleverPlusieursEltsMilieu
#define enleverPlusieursEltsFin                        enleverPlusieursEltsFin
#define eltsEnOrdreDecroissant                         eltsEnOrdreDecroissant
#define contientElt                                    contientElt
#define enleverToutesLesOccurencesDUnElt               enleverToutesLesOccurencesDUnElt
#define chercherLIndexDeLaPremiereOccurenceDUnElt      chercherLIndexDeLaPremiereOccurenceDUnElt
#define chercherLIndexDeLaDerniereOccurenceDUnElt      chercherLIndexDeLaDerniereOccurenceDUnElt
#define calculerNombreDOccurencesDUnElt                calculerNombreDOccurencesDUnElt


#define Q01 {1.0, ID_EXO_CONSTRUCTEUR,                                         MONTRER_COMMENTAIRES_ETUDIANT },
#define Q02 {1.0, ID_EXO_DESTRUCTEUR,                                          MONTRER_COMMENTAIRES_ETUDIANT },
#define Q03 {1.0, ID_EXO_CONSTRUCTEUR_PAR_RECOPIE,                             MONTRER_COMMENTAIRES_ETUDIANT },
#define Q04 {1.0, ID_EXO_CONSTRUCTEUR_AVEC_TABLEAU,                            MONTRER_COMMENTAIRES_ETUDIANT },
#define Q05 {1.0, ID_EXO_CONCATENATION_DEBUT,                                  MONTRER_COMMENTAIRES_ETUDIANT },
#define Q06 {1.0, ID_EXO_CONCATENATION_MILIEU,                                 MONTRER_COMMENTAIRES_ETUDIANT },
#define Q07 {1.0, ID_EXO_CONCATENATION_FIN,                                    MONTRER_COMMENTAIRES_ETUDIANT },
#define Q08 {1.0, ID_EXO_OP_CONCATENATION,                                     MONTRER_COMMENTAIRES_ETUDIANT },
#define Q09 {1.0, ID_EXO_OP_EGAL,                                              MONTRER_COMMENTAIRES_ETUDIANT },
#define Q10 {1.0, ID_EXO_OP_DIFFERENT,                                         MONTRER_COMMENTAIRES_ETUDIANT },
#define Q11 {1.0, ID_EXO_OP_AFFECTATION,                                       MONTRER_COMMENTAIRES_ETUDIANT },
#define Q12 {1.0, ID_EXO_OP_CROCHET,                                           MONTRER_COMMENTAIRES_ETUDIANT },
#define Q13 {1.0, ID_EXO_OP_CROCHET_CONST,                                     MONTRER_COMMENTAIRES_ETUDIANT },
#define Q14 {1.0, ID_EXO_OP_PARENTHESE,                                        MONTRER_COMMENTAIRES_ETUDIANT },
#define Q15 {1.0, ID_EXO_OP_PARENTHESE_CONST,                                  MONTRER_COMMENTAIRES_ETUDIANT },
#define Q16 {1.0, ID_EXO_SETTER,                                               MONTRER_COMMENTAIRES_ETUDIANT },
#define Q17 {1.0, ID_EXO_GETTER,                                               MONTRER_COMMENTAIRES_ETUDIANT },
#define Q18 {1.0, ID_EXO_ADDITIONNER_A_TOUS_LES_ELTS,                          MONTRER_COMMENTAIRES_ETUDIANT },
#define Q19 {1.0, ID_EXO_INSERER_UN_ELT_DEBUT,                                 MONTRER_COMMENTAIRES_ETUDIANT },
#define Q20 {1.0, ID_EXO_INSERER_UN_ELT_MILIEU,                                MONTRER_COMMENTAIRES_ETUDIANT },
#define Q21 {1.0, ID_EXO_INSERER_UN_ELT_FIN,                                   MONTRER_COMMENTAIRES_ETUDIANT },
#define Q22 {1.0, ID_EXO_INSERER_PLUSIEURS_ELTS_DEBUT,                         MONTRER_COMMENTAIRES_ETUDIANT },
#define Q23 {1.0, ID_EXO_INSERER_PLUSIEURS_ELTS_MILIEU,                        MONTRER_COMMENTAIRES_ETUDIANT },
#define Q24 {1.0, ID_EXO_INSERER_PLUSIEURS_ELTS_FIN,                           MONTRER_COMMENTAIRES_ETUDIANT },
#define Q25 {1.0, ID_EXO_ENLEVER_UN_ELT_DEBUT,                                 MONTRER_COMMENTAIRES_ETUDIANT },
#define Q26 {1.0, ID_EXO_ENLEVER_UN_ELT_MILIEU,                                MONTRER_COMMENTAIRES_ETUDIANT },
#define Q27 {1.0, ID_EXO_ENLEVER_UN_ELT_FIN,                                   MONTRER_COMMENTAIRES_ETUDIANT },
#define Q28 {1.0, ID_EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT,                         MONTRER_COMMENTAIRES_ETUDIANT },
#define Q29 {1.0, ID_EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU,                        MONTRER_COMMENTAIRES_ETUDIANT },
#define Q30 {1.0, ID_EXO_ENLEVER_PLUSIEURS_ELTS_FIN,                           MONTRER_COMMENTAIRES_ETUDIANT },
#define Q31 {1.0, ID_EXO_ELTS_EN_ORDRE_DECROISSANT,                            MONTRER_COMMENTAIRES_ETUDIANT },
#define Q32 {1.0, ID_EXO_CONTIENT_ELT,                                         MONTRER_COMMENTAIRES_ETUDIANT },
#define Q33 {1.0, ID_EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT,               MONTRER_COMMENTAIRES_ETUDIANT },
#define Q34 {1.0, ID_EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT,   MONTRER_COMMENTAIRES_ETUDIANT },
#define Q35 {1.0, ID_EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT,   MONTRER_COMMENTAIRES_ETUDIANT },
#define Q36 {1.0, ID_EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT,                MONTRER_COMMENTAIRES_ETUDIANT },
#define Q37 {1.0, ID_EXO_OP_ADDITION,                                          MONTRER_COMMENTAIRES_ETUDIANT },
#define Q38 {1.0, ID_EXO_OP_SOUSTRACTION,                                      MONTRER_COMMENTAIRES_ETUDIANT },


// DEBUT_ENONCE_STRUCT_DE_DONNEES
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     Double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     Int d_n;         // Sa taille.
// };
// FIN_ENONCE_STRUCT_DE_DONNEES

#endif
