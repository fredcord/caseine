#include <iostream>
#include "ExercicesBase.h"
#include "StructDeDonneesVerifCorr.h"

using namespace std;


/////////////////////////////////////////////////////////////////////////////
// Compare la structure de donnees de param'etudiant avec celle de la solution //
/////////////////////////////////////////////////////////////////////////////


// Verifie si la liste chainee est correcte au niveau de sa sctructure et des poInteurs
bool TableauDynamiqueCorr::verifierIntegrite(const TableauDynamique &param)
{
    // Verifie si les valeurs sont dans les bons intervals pour eviter les crash
    if (param.d_n<0)
        return false;
    if (param.d_table==nullptr && param.d_n>0)
        return false;
    if (param.d_table==nullptr && param.d_n==0)
        return true;
    // Le pointeur doit etre valide
    if (!GestionPntrEtComplexite::get().pntrValide(param.d_table))
        return false;
    // Sa taille allouee superieure ou egale a d_n
    if (GestionPntrEtComplexite::get().pntrTailleAllouee(param.d_table)<param.d_n)
        return false;

    return true;
}

// Compare la correction avec la struct de param'etudiant. Retourne false si different
bool TableauDynamiqueCorr::compareCorrAvecEtud(const TableauDynamique &student) const
{
    if (d_n!=student.d_n)
        return false;
    int i=0;
    while(i<d_n &&  d_table[i]==student.d_table[i])
        i++;
    return i==d_n;
}

// genere la structDeDonneesVerifCorr a partir de la liste
StructDeDonneesVerifCorr TableauDynamiqueCorr::getStructDeDonneesVerifCorr() const
{
    StructDeDonneesVerifCorr vect;
    vect.estVecteur=true;
    if (d_n==0)
        return vect;

    for (int i=0;i<d_n;i++)
        vect.insererUnEltFin(d_table[i]);
    return vect;
}

// Constructeur avec la structDeDonneesVerifCorr
void TableauDynamiqueCorr::setStructDeDonneesVerifCorr(const StructDeDonneesVerifCorr &tab)
{
    d_n=tab.getNbColonnes();
    d_table=new double[d_n];

    // On insere les elements du tableau
    for (int i=0;i<tab.getNbColonnes();i++)
        d_table[i]=tab[i];
}


// DEBUT EXO_CONSTRUCTEUR
TableauDynamiqueCorr::TableauDynamiqueCorr():d_n{0},d_table{nullptr}
{
}
// FIN EXO_CONSTRUCTEUR


// DEBUT EXO_CONSTRUCTEUR_PAR_RECOPIE
TableauDynamiqueCorr::TableauDynamiqueCorr(const TableauDynamiqueCorr &param):d_n{param.d_n},d_table{new double[d_n]}
{
    // Copie le tableau
    for (int i=0;i<d_n;i++)
        d_table[i]=param.d_table[i];
}
// FIN EXO_CONSTRUCTEUR_PAR_RECOPIE


// DEBUT EXO_CONSTRUCTEUR_AVEC_TABLEAU
TableauDynamiqueCorr::TableauDynamiqueCorr(double *tab, int nb):d_n{nb},d_table{new double[nb]}
{
    // Copie le tableau
    for (int i=0;i<d_n;i++)
        d_table[i]=tab[i];
}
// FIN EXO_CONSTRUCTEUR_AVEC_TABLEAU


// DEBUT DESTRUCTEUR
TableauDynamiqueCorr::~TableauDynamiqueCorr()
{
    delete[] d_table;
}
// FIN DESTRUCTEUR



// DEBUT EXO_CONCATENATION_DEBUT
void TableauDynamiqueCorr::concatenationDebut(const TableauDynamiqueCorr &param)
{
    double *tmp=new double[d_n+param.d_n];
    for (int i=0;i<param.d_n;i++)
        tmp[i]=param.d_table[i];
    for (int i=0;i<d_n;i++)
        tmp[param.d_n+i]=d_table[i];
    delete[] d_table;
    d_table=tmp;
    d_n+=param.d_n;
}
// FIN EXO_CONCATENATION_DEBUT



// DEBUT EXO_CONCATENATION_MILIEU
void TableauDynamiqueCorr::concatenationMilieu(int idx, const TableauDynamiqueCorr &param)
{
    double *tmp=new double[d_n+param.d_n];
    for (int i=0;i<idx;i++)
        tmp[i]=d_table[i];
    for (int i=0;i<param.d_n;i++)
        tmp[idx+i]=param.d_table[i];
    for (int i=idx;i<d_n;i++)
        tmp[param.d_n+i]=d_table[i];
    delete[] d_table;
    d_table=tmp;
    d_n+=param.d_n;
}
// FIN EXO_CONCATENATION_MILIEU


// DEBUT EXO_CONCATENATION_FIN
void TableauDynamiqueCorr::concatenationFin(const TableauDynamiqueCorr &param)
{
    double *tmp=new double[d_n+param.d_n];
    for (int i=0;i<d_n;i++)
        tmp[i]=d_table[i];
    for (int i=0;i<param.d_n;i++)
        tmp[d_n+i]=param.d_table[i];
    delete[] d_table;
    d_table=tmp;
    d_n+=param.d_n;
}
// FIN EXO_CONCATENATION_FIN


// DEBUT EXO_OP_CONCATENATION
TableauDynamiqueCorr &TableauDynamiqueCorr::operator+=(const TableauDynamiqueCorr &param)
{
    double *tmp=new double[d_n+param.d_n];
    for (int i=0;i<d_n;i++)
        tmp[i]=d_table[i];
    for (int i=0;i<param.d_n;i++)
        tmp[d_n+i]=param.d_table[i];
    delete[] d_table;
    d_table=tmp;
    d_n+=param.d_n;

    return *this;
}
// FIN EXO_OP_CONCATENATION



// DEBUT EXO_OP_EGAL
bool TableauDynamiqueCorr::operator==(const TableauDynamiqueCorr &param) const
{
    if (this==&param)
        return true;
    if (d_n!=param.d_n)
        return false;
    int i=0;
    while(i<d_n &&  d_table[i]==param.d_table[i])
        i++;
    return i==d_n;
}
// FIN EXO_OP_EGAL


// DEBUT EXO_OP_DIFFERENT
bool TableauDynamiqueCorr::operator!=(const TableauDynamiqueCorr &param) const
{
    if (this==&param)
        return false;
    if (d_n!=param.d_n)
        return true;
    int i=0;
    while(i<d_n &&  d_table[i]==param.d_table[i])
        i++;
    return i!=d_n;
}
// FIN EXO_OP_DIFFERENT


// DEBUT EXO_OP_AFFECTATION
TableauDynamiqueCorr &TableauDynamiqueCorr::operator=(const TableauDynamiqueCorr &param)
{
    if (this==&param)
        return *this;
    if (d_n!=param.d_n)
    {
        delete[] d_table;
        d_table=new double[param.d_n];
    }
    d_n=param.d_n;
    for (int i=0;i<d_n;i++)
        d_table[i]=param.d_table[i];

    return *this;
}
// FIN EXO_OP_AFFECTATION


// DEBUT EXO_OP_CROCHET
double &TableauDynamiqueCorr::operator[](int i)
{
    return d_table[i];
}
// FIN EXO_OP_CROCHET


// DEBUT EXO_OP_CROCHET_CONST
double TableauDynamiqueCorr::operator[](int i) const
{
    return d_table[i];
}
// FIN EXO_OP_CROCHET_CONST


// DEBUT EXO_OP_PARENTHESE
double& TableauDynamiqueCorr::operator()(int i)
{
    return d_table[i];
}
// FIN EXO_OP_PARENTHESE


// DEBUT EXO_OP_PARENTHESE_CONST
double TableauDynamiqueCorr::operator()(int i) const
{
    return d_table[i];
}
// FIN EXO_OP_PARENTHESE_CONST


// DEBUT EXO_SETTER
void TableauDynamiqueCorr::set(int i, double val)
{
    d_table[i]=val;
}
// FIN EXO_SETTER


// DEBUT EXO_GETTER
double TableauDynamiqueCorr::get(int i) const
{
    return d_table[i];
}
// FIN EXO_GETTER


// DEBUT EXO_ADDITIONNER_A_TOUS_LES_ELTS
void TableauDynamiqueCorr::additionnerATousLesElts(double val)
{
    for (int i=0;i<d_n;i++)
        d_table[i]+=val;
}
// FIN EXO_ADDITIONNER_A_TOUS_LES_ELTS


// DEBUT EXO_INSERER_UN_ELT_DEBUT
void TableauDynamiqueCorr::insererUnEltDebut(double valeur)
{
    double *tmp=new double[d_n+1];
    for (int i=0;i<d_n;i++)
        tmp[i+1]=d_table[i];
    tmp[0]=valeur;
    d_n++;
    delete[] d_table;
    d_table=tmp;
}
// FIN EXO_INSERER_UN_ELT_DEBUT


// DEBUT EXO_INSERER_UN_ELT_MILIEU
void TableauDynamiqueCorr::insererUnEltMilieu(int idx, double valeur)
{
    double *tmp=new double[d_n+1];
    for (int i=0;i<idx;i++)
        tmp[i]=d_table[i];
    tmp[idx]=valeur;
    for (int i=idx;i<d_n;i++)
        tmp[i+1]=d_table[i];
    d_n++;
    delete[] d_table;
    d_table=tmp;
}
// FIN EXO_INSERER_UN_ELT_MILIEU


// DEBUT EXO_INSERER_UN_ELT_FIN
void TableauDynamiqueCorr::insererUnEltFin(double valeur)
{
    double *tmp=new double[d_n+1];
    for (int i=0;i<d_n;i++)
        tmp[i]=d_table[i];
    tmp[d_n]=valeur;
    d_n++;
    delete[] d_table;
    d_table=tmp;
}
// FIN EXO_INSERER_UN_ELT_FIN


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_DEBUT
void TableauDynamiqueCorr::insererPlusieursEltsDebut(double *tab, int nb)
{
    double *tmp=new double[d_n+nb];
    for (int i=0;i<nb;i++)
        tmp[i]=tab[i];
    for (int i=0;i<d_n;i++)
        tmp[i+nb]=d_table[i];
    d_n+=nb;
    delete[] d_table;
    d_table=tmp;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_MILIEU
void TableauDynamiqueCorr::insererPlusieursEltsMilieu(int idx, double *tab, int nb)
{
    double *tmp=new double[d_n+nb];
    for (int i=0;i<idx;i++)
        tmp[i]=d_table[i];
    for (int i=0;i<nb;i++)
        tmp[idx+i]=tab[i];
    for (int i=idx;i<d_n;i++)
        tmp[i+nb]=d_table[i];
    d_n+=nb;
    delete[] d_table;
    d_table=tmp;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_INSERER_PLUSIEURS_ELTS_FIN
void TableauDynamiqueCorr::insererPlusieursEltsFin(double *tab, int nb)
{
    double *tmp=new double[d_n+nb];
    for (int i=0;i<d_n;i++)
        tmp[i]=d_table[i];
    for (int i=0;i<nb;i++)
        tmp[i+d_n]=tab[i];
    d_n+=nb;
    delete[] d_table;
    d_table=tmp;
}
// FIN EXO_INSERER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ENLEVER_UN_ELT_DEBUT
void TableauDynamiqueCorr::enleverUnEltDebut()
{
    if (d_n==0)
        return;
    if (d_n==1)
    {
        delete[] d_table;
        d_table=nullptr;
        d_n=0;
        return;
    }
    double *tmp=new double[d_n-1];
    for (int i=1;i<d_n;i++)
        tmp[i-1]=d_table[i];
    d_n--;
    delete[] d_table;
    d_table=tmp;
}
// FIN EXO_ENLEVER_UN_ELT_DEBUT


// DEBUT EXO_ENLEVER_UN_ELT_MILIEU
void TableauDynamiqueCorr::enleverUnEltMilieu(int idx)
{
    if (d_n==0)
        return;
    if (idx>=d_n)
        return;
    if (d_n==1)
    {
        delete[] d_table;
        d_table=nullptr;
        d_n=0;
        return;
    }
    double *tmp=new double[d_n-1];
    for (int i=0;i<idx;i++)
        tmp[i]=d_table[i];
    for (int i=idx+1;i<d_n;i++)
        tmp[i-1]=d_table[i];
    d_n--;
    delete[] d_table;
    d_table=tmp;
}
// FIN EXO_ENLEVER_UN_ELT_MILIEU


// DEBUT EXO_ENLEVER_UN_ELT_FIN
void TableauDynamiqueCorr::enleverUnEltFin()
{
    if (d_n==0)
        return;
    if (d_n==1)
    {
        delete[] d_table;
        d_table=nullptr;
        d_n=0;
        return;
    }
    double *tmp=new double[d_n-1];
    for (int i=0;i<d_n-1;i++)
        tmp[i]=d_table[i];
    d_n--;
    delete[] d_table;
    d_table=tmp;
}
// FIN EXO_ENLEVER_UN_ELT_FIN


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT
void TableauDynamiqueCorr::enleverPlusieursEltsDebut(int nb)
{
    if (nb>=d_n)
    {
        delete[] d_table;
        d_table=nullptr;
        d_n=0;
        return;
    }
    double* tmp=new double[d_n-nb];
    for (int i=nb;i<d_n;i++)
        tmp[i-nb]=d_table[i];
    delete[] d_table;
    d_table=tmp;
    d_n-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_DEBUT


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU
void TableauDynamiqueCorr::enleverPlusieursEltsMilieu(int idx, int nb)
{
    if (idx>=d_n)
        return;
    if (nb+idx>d_n)
        nb=d_n-idx;
    if (nb==0)
        return;
    double* tmp=new double[d_n-nb];
    for (int i=0;i<idx;i++)
        tmp[i]=d_table[i];
    for (int i=idx+nb;i<d_n;i++)
        tmp[i-nb]=d_table[i];
    delete[] d_table;
    d_table=tmp;
    d_n-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_MILIEU


// DEBUT EXO_ENLEVER_PLUSIEURS_ELTS_FIN
void TableauDynamiqueCorr::enleverPlusieursEltsFin(int nb)
{
    if (nb==0)
        return;
    if (nb>=d_n)
    {
        d_n=0;
        delete[] d_table;
        d_table=nullptr;
        return;
    }
    double* tmp=new double[d_n-nb];
    for (int i=0;i<d_n-nb;i++)
        tmp[i]=d_table[i];
    delete[] d_table;
    d_table=tmp;
    d_n-=nb;
}
// FIN EXO_ENLEVER_PLUSIEURS_ELTS_FIN


// DEBUT EXO_ELTS_EN_ORDRE_DECROISSANT
bool TableauDynamiqueCorr::eltsEnOrdreDecroissant() const
{
    if (d_n<=1)
        return true;
    int i=1;
    while(i<d_n && d_table[i-1]>d_table[i])
        i++;
    return i==d_n;
}
// FIN EXO_ELTS_EN_ORDRE_DECROISSANT


// DEBUT EXO_CONTIENT_ELT
bool TableauDynamiqueCorr::contientElt(double val) const
{
    int i=0;
    while(i<d_n && d_table[i]!=val)
        i++;
    return i<d_n;
}
// FIN EXO_CONTIENT_ELT


// DEBUT EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT
void TableauDynamiqueCorr::enleverToutesLesOccurencesDUnElt(double val)
{
    int nbOccurences=0;
    int i=0;
    while(i<d_n)
    {
        if (d_table[i]==val)
            nbOccurences++;
        i++;
    }
    if (nbOccurences==0)
        return;
    if (nbOccurences==d_n)
    {
        delete[] d_table;
        d_table=nullptr;
        d_n=0;
        return;
    }
    double* tmp=new double[d_n-nbOccurences];
    i=0;
    int j=0;
    while(i<d_n)
    {
        if (d_table[i]!=val)
        {
            tmp[j]=d_table[i];
            j++;
        }
        i++;
    }
    delete[] d_table;
    d_table=tmp;
    d_n=j;
}
// FIN EXO_ENLEVER_TOUTES_LES_OCCURENCES_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT
int TableauDynamiqueCorr::chercherLIndexDeLaPremiereOccurenceDUnElt(double val) const
{
    int i=0;
    while(i<d_n && d_table[i]!=val)
        i++;
    if (i==d_n)
        return -1;
    return i;
}
// FIN EXO_CHERCHER_L_INDEX_DE_LA_PREMIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT
int TableauDynamiqueCorr::chercherLIndexDeLaDerniereOccurenceDUnElt(double val) const
{
    int i=d_n-1;
    while(i>=0 && d_table[i]!=val)
        i--;
    return i;
}
// DEBUT EXO_CHERCHER_L_INDEX_DE_LA_DERNIERE_OCCURENCE_D_UN_ELT


// DEBUT EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT
int TableauDynamiqueCorr::calculerNombreDOccurencesDUnElt(double val) const
{
    int i=0;
    int nbOccurences=0;
    while(i<d_n)
    {
        if (d_table[i]==val)
            nbOccurences++;
        i++;
    }
    return nbOccurences;
}
// FIN EXO_CALCULER_NOMBRE_D_OCCURENCES_D_UN_ELT


// DEBUT EXO_OP_ADDITION
TableauDynamiqueCorr operator+(const TableauDynamiqueCorr& a, const TableauDynamiqueCorr& b)
{
    TableauDynamiqueCorr res(a);
    for (int i=0;i<res.d_n;i++)
        res.d_table[i]+=b.d_table[i];
    return res;
}
// FIN EXO_OP_ADDITION


// DEBUT EXO_OP_SOUSTRACTION
TableauDynamiqueCorr operator-(const TableauDynamiqueCorr& a, const TableauDynamiqueCorr& b)
{
    TableauDynamiqueCorr res(a);
    for (int i=0;i<res.d_n;i++)
        res.d_table[i]-=b.d_table[i];
    return res;
}
// FIN EXO_OP_SOUSTRACTION

