#include "ExercicesBase.h"



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le constructeur pour la structure de données ci-dessous
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
TableauDynamique::TableauDynamique():d_n{0},d_table{nullptr}
{
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le destructeur pour la structure de données ci-dessous
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
TableauDynamique::~TableauDynamique()
{
    delete[] d_table;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le constructeur par recopie pour la structure de données ci-dessous
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
TableauDynamique::TableauDynamique(const TableauDynamique &param):d_n{param.d_n},d_table{new double[d_n]}
{
    // Copie le tableau
    for (int i=0;i<d_n;i++)
        d_table[i]=param.d_table[i];
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire le constructeur qui prend comme paramètres un tableau avec sa
// taille: double *tab, int val. Ce constructeur doit créer une
// structure de données contenant tous les éléments dans le tableau
// passé en paramètre
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
TableauDynamique::TableauDynamique(double *tab, int nb):d_n{nb},d_table{new double[nb]}
{
    // Copie le tableau
    for (int i=0;i<d_n;i++)
        d_table[i]=tab[i];
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode concatenationDebut(param) qui ajoute les elements de la
// structure de données param au début de la structure de données this. Cette
// structure de données param est passée en paramètre. L'ordre des éléments
// doit être gardé.
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauDynamique::concatenationDebut(const TableauDynamique &param)
{
    double *tmp=new double[d_n+param.d_n];
    for (int i=0;i<param.d_n;i++)
        tmp[i]=param.d_table[i];
    for (int i=0;i<d_n;i++)
        tmp[param.d_n+i]=d_table[i];
    delete[] d_table;
    d_table=tmp;
    d_n+=param.d_n;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode concatenationMilieu(int idx, param) qui insère les
// éléments de la structure de données param en paramètre. Ces éléments
// sont insérés à la position idx dans la stucture de données.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - L'index d'insertion peut être une valeur égale ou plus grande que la taille
//   de la structure de données. Dans ce cas, l'insertion se fait à la fin de la
//   structure de données.
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauDynamique::concatenationMilieu(int idx, const TableauDynamique &param)
{
    if (idx>d_n)
        idx=d_n;

    double *tmp=new double[d_n+param.d_n];
    for (int i=0;i<idx;i++)
        tmp[i]=d_table[i];
    for (int i=0;i<param.d_n;i++)
        tmp[idx+i]=param.d_table[i];
    for (int i=idx;i<d_n;i++)
        tmp[param.d_n+i]=d_table[i];
    delete[] d_table;
    d_table=tmp;
    d_n+=param.d_n;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode concatenationFin(param) qui ajoute les elements de la
// structure de données param à la fin de la structure de données this. Cette
// structure de données param est passée en paramètre. L'ordre des éléments
// doit être gardé.
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauDynamique::concatenationFin(const TableauDynamique &param)
{
    double *tmp=new double[d_n+param.d_n];
    for (int i=0;i<d_n;i++)
        tmp[i]=d_table[i];
    for (int i=0;i<param.d_n;i++)
        tmp[d_n+i]=param.d_table[i];
    delete[] d_table;
    d_table=tmp;
    d_n+=param.d_n;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode operator+=(param) qui ajoute les elements de la structure de
// données param à la fin de la structure de données this. Cette structure de
// données param est passée en paramètre
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
TableauDynamique &TableauDynamique::operator+=(const TableauDynamique &param)
{
    double *tmp=new double[d_n+param.d_n];
    for (int i=0;i<d_n;i++)
        tmp[i]=d_table[i];
    for (int i=0;i<param.d_n;i++)
        tmp[d_n+i]=param.d_table[i];
    delete[] d_table;
    d_table=tmp;
    d_n+=param.d_n;

    return *this;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode operator==(param) qui retourne true si la structure de
// données param passée en paramètre est égale à la structure de données this.
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
bool TableauDynamique::operator==(const TableauDynamique &param) const
{
    if (this==&param)
        return true;
    if (d_n!=param.d_n)
        return false;
    int i=0;
    while(i<d_n &&  d_table[i]==param.d_table[i])
        i++;
    return i==d_n;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode operator!=(param) qui retourne true si la structure de
// données param passée en paramètre est différente à la structure de données this.
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
bool TableauDynamique::operator!=(const TableauDynamique &param) const
{
    if (this==&param)
        return false;
    if (d_n!=param.d_n)
        return true;
    int i=0;
    while(i<d_n &&  d_table[i]==param.d_table[i])
        i++;
    return i!=d_n;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire l'opérateur d'affectation operator=(param). Après cette méthode, la structure
// de données this doit contenir les mêmes éléments que la structure de données param
// passée en paramètre.
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
TableauDynamique &TableauDynamique::operator=(const TableauDynamique &param)
{
    if (this==&param)
        return *this;
    if (d_n!=param.d_n)
    {
        delete[] d_table;
        d_table=new double[param.d_n];
    }
    d_n=param.d_n;
    for (int i=0;i<d_n;i++)
        d_table[i]=param.d_table[i];

    return *this;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire l'opérateur crochet [](idx). Cette méthode permet l'accès aux éléments en 
// écriture. Elle doit retourne la référence de l'élément d'index idx dans la structure de
// données. Le premier élément a pour index 0.
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
double &TableauDynamique::operator[](int i)
{
    return d_table[i];
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire l'opérateur crochet [](idx) const. Cette méthode permet l'accès aux éléments en 
// lecture. Elle doit retourne la valeur de l'élément d'index idx dans la structure de
// données. Le premier élément a pour index 0. Cette méthode ne modifie pas l'objet; elle 
// doit être déclarée constante.
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
double TableauDynamique::operator[](int i) const
{
    return d_table[i];
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode double set(int idx, double val). Cette méthode permet de mettre la
// valeur val à l'index idx dans la structure de données. Le premier élément a pour 
// index 0.
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauDynamique::set(int i, double val)
{
    d_table[i]=val;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode double get(int idx). Cette méthode doit retourne la valeur de 
// l'élément d'index idx dans la structure de données. Le premier élément a pour index 0. 
// Cette méthode ne modifie pas l'objet; elle doit être déclarée constante.
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
double TableauDynamique::get(int i) const
{
    return d_table[i];
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void additionnerATousLesElts(double val) qui ajoute la valeur
// val passée en paramètre à tous les éléments de la stucture de données.
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauDynamique::additionnerATousLesElts(double val)
{
    for (int i=0;i<d_n;i++)
        d_table[i]+=val;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererUnEltDebut(double val) qui insère un élément
// contenant la valeur val passée en paramètre. Cet élément est inséré au
// début de la stucture de données.
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauDynamique::insererUnEltDebut(double valeur)
{
    double *tmp=new double[d_n+1];
    for (int i=0;i<d_n;i++)
        tmp[i+1]=d_table[i];
    tmp[0]=valeur;
    d_n++;
    delete[] d_table;
    d_table=tmp;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererUnEltMilieu(int idx, double val) qui
// insère un élément contenant la valeur val passée en paramètre. Cet élément
// est inséré à la position idx dans la stucture de données.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - L'index d'insertion peut être une valeur égale ou plus grande que la taille
//   de la structure de données. Dans ce cas, l'insertion se fait à la fin de la
//   structure de données.
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauDynamique::insererUnEltMilieu(int idx, double valeur)
{
    double *tmp=new double[d_n+1];
    for (int i=0;i<idx;i++)
        tmp[i]=d_table[i];
    tmp[idx]=valeur;
    for (int i=idx;i<d_n;i++)
        tmp[i+1]=d_table[i];
    d_n++;
    delete[] d_table;
    d_table=tmp;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void push(double val) qui insère un élément
// contenant la valeur val passée en paramètre. Cet élément est inséré à la
// fin de la stucture de données.
// Remarques:
// - La structure de données peut être vide
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauDynamique::push(double valeur)
{
    double *tmp=new double[d_n+1];
    for (int i=0;i<d_n;i++)
        tmp[i]=d_table[i];
    tmp[d_n]=valeur;
    d_n++;
    delete[] d_table;
    d_table=tmp;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererPlusieursEltsDebut(double *tab, int nb)
// qui insère nb éléments au début de la structures de données. Les éléments
// à inserer sont le tableau tab. L'ordre des éléments dans le tableau doit
// être gardé dans la structure de données.
// Remarques:
// - Le paramètre nb peut être égal à zéro
// - La structure de données peut être vide
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauDynamique::insererPlusieursEltsDebut(double *tab, int nb)
{
    double *tmp=new double[d_n+nb];
    for (int i=0;i<nb;i++)
        tmp[i]=tab[i];
    for (int i=0;i<d_n;i++)
        tmp[i+nb]=d_table[i];
    d_n+=nb;
    delete[] d_table;
    d_table=tmp;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererPlusieursEltsMilieu(int idx, double *tab, int nb)
// qui insère nb éléments à la position idx dans la structures de données. Les éléments
// à inserer sont le tableau tab. L'ordre des éléments dans le tableau doit
// être gardé dans la structure de données.
// Remarques:
// - Le paramètre nb peut être égal à 0.
// - L'index du premier élément de la structure de données est 0.
// - L'index d'insertion idx peut être une valeur égale ou plus grande que la taille
//   de la structure de données. Dans ce cas, l'insertion se fait à la fin de la
//   structure de données.
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauDynamique::insererPlusieursEltsMilieu(int idx, double *tab, int nb)
{
    double *tmp=new double[d_n+nb];
    for (int i=0;i<idx;i++)
        tmp[i]=d_table[i];
    for (int i=0;i<nb;i++)
        tmp[idx+i]=tab[i];
    for (int i=idx;i<d_n;i++)
        tmp[i+nb]=d_table[i];
    d_n+=nb;
    delete[] d_table;
    d_table=tmp;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void insererPlusieursEltsFin(double *tab, int nb)
// qui insère nb éléments à la fin de la structures de données. Les éléments
// à inserer sont le tableau tab. L'ordre des éléments dans le tableau doit
// être gardé dans la structure de données.
// Remarques:
// - Le paramètre nb peut être égal à zéro
// - La structure de données peut être vide
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauDynamique::insererPlusieursEltsFin(double *tab, int nb)
{
    double *tmp=new double[d_n+nb];
    for (int i=0;i<d_n;i++)
        tmp[i]=d_table[i];
    for (int i=0;i<nb;i++)
        tmp[i+d_n]=tab[i];
    d_n+=nb;
    delete[] d_table;
    d_table=tmp;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverUnEltDebut() qui enlève le premier élément
// de la structure de données.
// Remarque:
// - La structure de données peut être vide
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauDynamique::enleverUnEltDebut()
{
    if (d_n==0)
        return;
    if (d_n==1)
    {
        delete[] d_table;
        d_table=nullptr;
        d_n=0;
        return;
    }
    double *tmp=new double[d_n-1];
    for (int i=1;i<d_n;i++)
        tmp[i-1]=d_table[i];
    d_n--;
    delete[] d_table;
    d_table=tmp;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverUnEltMilieu(int idx) qui enlève
// l'élément d'index idx dans la structure de données.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - La structure de données peut être vide.
// - La valeur idx peut être plus grande ou égale à la taille de la structure
//   de données. Dans ce cas, l'élément n'est pas supprimé.
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauDynamique::enleverUnEltMilieu(int idx)
{
    if (d_n==0)
        return;
    if (idx>=d_n)
        return;
    if (d_n==1)
    {
        delete[] d_table;
        d_table=nullptr;
        d_n=0;
        return;
    }
    double *tmp=new double[d_n-1];
    for (int i=0;i<idx;i++)
        tmp[i]=d_table[i];
    for (int i=idx+1;i<d_n;i++)
        tmp[i-1]=d_table[i];
    d_n--;
    delete[] d_table;
    d_table=tmp;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverUnEltFin() qui enlève le dernier élément
// de la structure de données.
// Remarque:
// - La structure de données peut être vide
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauDynamique::enleverUnEltFin()
{
    if (d_n==0)
        return;
    if (d_n==1)
    {
        delete[] d_table;
        d_table=nullptr;
        d_n=0;
        return;
    }
    double *tmp=new double[d_n-1];
    for (int i=0;i<d_n-1;i++)
        tmp[i]=d_table[i];
    d_n--;
    delete[] d_table;
    d_table=tmp;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverPlusieursEltsDebut(int nb) qui enlève nb
// éléments au début de la structures de données. Cette valeur nb est passée
// en paramètre.
// Remarque:
// - La valeur nb peut être plus grande que la taille de la structures de
// données. Dans ce cas, tous les éléments doivent être supprimés.
// - La valeur nb peut être égale à 0. Dans ce cas, rien n'est supprimé.
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauDynamique::enleverPlusieursEltsDebut(int nb)
{
    if (nb>=d_n)
    {
        delete[] d_table;
        d_table=nullptr;
        d_n=0;
        return;
    }
    double* tmp=new double[d_n-nb];
    for (int i=nb;i<d_n;i++)
        tmp[i-nb]=d_table[i];
    delete[] d_table;
    d_table=tmp;
    d_n-=nb;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverPlusieursEltsMilieu(int idx, int nb) qui
// enlève nb éléments à partir de l'élément d'index idx. Ces valeurs idx et
// nb sont passées en paramètre.
// Remarques:
// - L'index du premier élément de la structure de données est 0.
// - La valeur nb peut être plus grande que la taille de la structures de
// données.
// - La valeur idx peut être plus grande ou égale à la taille de la structure
// de données. Dans ce cas, les éléments ne sont pas supprimés.
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauDynamique::enleverPlusieursEltsMilieu(int idx, int nb)
{
    if (idx>=d_n)
        return;
    if (nb+idx>d_n)
        nb=d_n-idx;
    if (nb==0)
        return;
    double* tmp=new double[d_n-nb];
    for (int i=0;i<idx;i++)
        tmp[i]=d_table[i];
    for (int i=idx+nb;i<d_n;i++)
        tmp[i-nb]=d_table[i];
    delete[] d_table;
    d_table=tmp;
    d_n-=nb;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverPlusieursEltsFin(int nb) qui enlève nb
// éléments à la fin de la structures de données. Cette valeur nb est passée
// en paramètre.
// Remarque:
// - La valeur nb peut être plus grande que la taille de la structures de
// données.
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauDynamique::enleverPlusieursEltsFin(int nb)
{
    if (nb==0)
        return;
    if (nb>=d_n)
    {
        d_n=0;
        delete[] d_table;
        d_table=nullptr;
        return;
    }
    double* tmp=new double[d_n-nb];
    for (int i=0;i<d_n-nb;i++)
        tmp[i]=d_table[i];
    delete[] d_table;
    d_table=tmp;
    d_n-=nb;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode eltsEnOrdreDecroissant() qui retourne true si les éléments
// dans la structures de données sont classés par ordre strictement décroissant.
// Remarque:
// - eltsEnOrdreDecroissant() doit retourner true pour une structure de données vide
// ou ne contenant qu'un seul élément.
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
bool TableauDynamique::eltsEnOrdreDecroissant() const
{
    if (d_n<=1)
        return true;
    int i=1;
    while(i<d_n && d_table[i-1]>d_table[i])
        i++;
    return i==d_n;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode contientElt(double val) qui retourne true si la structure
// de données contient au moins un élément avec la valeur val, val étant un
// paramètre de la méthode.
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
bool TableauDynamique::contientElt(double val) const
{
    int i=0;
    while(i<d_n && d_table[i]!=val)
        i++;
    return i<d_n;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode void enleverToutesLesOccurencesDUnElt(double val) qui
// enlève tous les éléments contenant la valeur val dans la structure de
// données, val étant un paramètre de la méthode.
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
void TableauDynamique::enleverToutesLesOccurencesDUnElt(double val)
{
    int nbOccurences=0;
    int i=0;
    while(i<d_n)
    {
        if (d_table[i]==val)
            nbOccurences++;
        i++;
    }
    if (nbOccurences==0)
        return;
    if (nbOccurences==d_n)
    {
        delete[] d_table;
        d_table=nullptr;
        d_n=0;
        return;
    }
    double* tmp=new double[d_n-nbOccurences];
    i=0;
    int j=0;
    while(i<d_n)
    {
        if (d_table[i]!=val)
        {
            tmp[j]=d_table[i];
            j++;
        }
        i++;
    }
    delete[] d_table;
    d_table=tmp;
    d_n=j;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode int chercherLIndexDeLaPremiereOccurenceDUnElt(double val)
// qui retourne l'index du premier l'élément contenant la valeur val dans la
// structure de données, val étant un paramètre de la méthode.
// Remarques:
// - La structure de données ne contient pas nécessairement la valeur val. Dans ce
//   cas, la méthode doit retourner -1
// - La structure de données peut être vide
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
int TableauDynamique::chercherLIndexDeLaPremiereOccurenceDUnElt(double val) const
{
    int i=0;
    while(i<d_n && d_table[i]!=val)
        i++;
    if (i==d_n)
        return -1;
    return i;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode int chercherLIndexDeLaDerniereOccurenceDUnElt(double val)
// qui retourne l'index du dernier l'élément contenant la valeur val dans la
// structure de données, val étant un paramètre de la méthode.
// Remarques:
// - La structure de données ne contient pas nécessairement la valeur val. Dans ce
//   cas, la méthode doit retourner -1
// - La structure de données peut être vide
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
int TableauDynamique::chercherLIndexDeLaDerniereOccurenceDUnElt(double val) const
{
    int i=d_n-1;
    while(i>=0 && d_table[i]!=val)
        i--;
    return i;
}



////////////////////////////////////////////////////////////////////////////////////////////////
//
// Ecrire la méthode int calculerNombreDOccurencesDUnElt(double val) qui
// retourne le nombre d'éléments contenant la valeur val dans la
// structure de données, val étant un paramètre de la méthode.
// Remarques:
// - La structure de données ne contient pas nécessairement la valeur val. Dans ce
//   cas, la méthode doit retourner 0
// - La structure de données peut être vide
//
// class TableauDynamique
// {
//   public:
//     TableauDynamique();
//     ~TableauDynamique();
//     ...
//   private:
//     double* d_table; // Un pointeur pour en faire un tableau de taille arbitraire
//     int d_n;         // Sa taille.
// };
//
////////////////////////////////////////////////////////////////////////////////////////////////
int TableauDynamique::calculerNombreDOccurencesDUnElt(double val) const
{
    int i=0;
    int nbOccurences=0;
    while(i<d_n)
    {
        if (d_table[i]==val)
            nbOccurences++;
        i++;
    }
    return nbOccurences;
}



